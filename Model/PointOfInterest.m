//
// Created by FelixLeber on 07.05.15.
// Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import "PointOfInterest.h"
#import "Constants.h"
#import "WKTObject.h"


@implementation PointOfInterest {

}


#pragma mark - ReportAnnotationProtocol

- (BOOL)isMovable {
    return NO;
}

- (UIImage *)image {
    UIImage *ret = [UIImage imageNamed:[NSString stringWithFormat:@"marker-white-%@.png", self.markerID]];

    if (!ret)
        ret = [UIImage imageNamed:kMarkerDefault];

    return ret;
}

@end