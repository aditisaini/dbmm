//
// Created by Martin on 07.05.15.
// Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MapBoundingBox.h"

@interface MapBoundingBox ()
@property(nonatomic, assign) MKMapRect mapRect;
@end

@implementation MapBoundingBox
- (instancetype)initWithMapRect:(MKMapRect)rect {
    self = [super init];

    if (self) {
        _mapRect = rect;
    }

    return self;
}

- (NSNumber *)left {
    CLLocationCoordinate2D coordinate2D = [self getCoordinateFromMapRectanglePoint:MKMapRectGetMinX(self.mapRect)
                                                                                 y:self.mapRect.origin.y];

    return @(coordinate2D.longitude);
}

- (NSNumber *)right {
    CLLocationCoordinate2D coordinate2D = [self getCoordinateFromMapRectanglePoint:MKMapRectGetMaxX(self.mapRect)
                                                                                 y:MKMapRectGetMaxY(self.mapRect)];

    return @(coordinate2D.longitude);
}

- (NSNumber *)top {
    CLLocationCoordinate2D coordinate2D = [self getCoordinateFromMapRectanglePoint:MKMapRectGetMinX(self.mapRect)
                                                                                 y:self.mapRect.origin.y];
    return @(coordinate2D.latitude);
}

- (NSNumber *)bottom {
    CLLocationCoordinate2D coordinate2D = [self getCoordinateFromMapRectanglePoint:MKMapRectGetMaxX(self.mapRect)
                                                                                 y:MKMapRectGetMaxY(self.mapRect)];

    return @(coordinate2D.latitude);
}

- (CLLocationCoordinate2D)getCoordinateFromMapRectanglePoint:(double)x y:(double)y {
    MKMapPoint swMapPoint = MKMapPointMake(x, y);
    return MKCoordinateForMapPoint(swMapPoint);
}

@end