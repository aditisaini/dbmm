//
// Created by Martin on 07.05.15.
// Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapBoundingBox : NSObject

- (instancetype)initWithMapRect:(MKMapRect)rect;

- (NSNumber *)left;

- (NSNumber *)right;

- (NSNumber *)top;

- (NSNumber *)bottom;

@end