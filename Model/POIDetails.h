//
// Created by Martin on 11.05.15.
// Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface POIDetails : NSObject
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *content;

@end