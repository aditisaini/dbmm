//
// Created by FelixLeber on 07.05.15.
// Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReportAnnotationProtocol.h"

@protocol WKTObject;


@interface PointOfInterest : NSObject <ReportAnnotationProtocol>

@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSNumber *id;
@property(nonatomic, strong) NSString *color;
@property(nonatomic, strong) NSNumber *markerID;
@property(nonatomic, assign) CLLocationCoordinate2D coordinate;

//Details
@property (nonatomic, strong) id<WKTObject> geometry;
@property (nonatomic, strong) NSArray *messageDetails;
@property (nonatomic, strong) NSString *pictureURL;

@end