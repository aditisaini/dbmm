//
//  DuplicateViewController.swift
//  WerDenktWas
//
//  Created by Albert Tra on 05/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import UIKit

class DuplicateViewController: UIViewController {
    
    

    @IBOutlet weak var tableView: UITableView!
    var network = AFramework.sharedInstance.network

    
    var report: Report?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        print(report?.text)
        
        network.getRawData(self.report?.pictureURL) { (data) -> Void in
            self.report!.image = UIImage(data: data)
            
            guard self.report!.image != nil else { return }
            self.tableView.reloadData()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "detailSegue" {
            let detailViewController = segue.destinationViewController as! DetailViewController
            detailViewController.report = self.report
//            print(detailViewController.report?.messageID)
        }
        
    }
    

}

extension DuplicateViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard report != nil else {
            return 0
        }
        
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        
        cell.textLabel?.text = self.report?.text
        
        if let image = self.report!.image {
            cell.imageView?.image = image
        }
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("detailSegue", sender: self)
    }
    
}
