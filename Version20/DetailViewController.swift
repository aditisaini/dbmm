//
//  ReportViewController.swift
//  WerDenktWas
//
//  Created by Albert Tra on 05/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import UIKit
import MapKit
import CoreData
import SwiftyJSON

class DetailViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    
    var detail: JSON?
    
    
    var fetchedResultsController: NSFetchedResultsController!
    var network = AFramework.sharedInstance.network
    
    var numberOfRows = 0
    var numberOfSection = 0
    
    var managedObjectContext: NSManagedObjectContext!
    
    /**
     We get the report for DuplicateViewController, it will be set when the use tap on to a row to select a report
     */
    var report: Report?
    
    
    var viewIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).coreDataStack.managedObjectContext /// 40BEF936-29E3-4CD4-981C-AD6172B0D8AE

        self.setupDelegation()
        
        
        
        guard self.report != nil else {
            print("D80FA130-4B68-4947-9F15-740541672C08 \t report is NIL")
            return
        }
        
        self.getMessageWithID((self.report?.messageID)!)
        
        self.imageView.image = report?.image
        
        let coordinate = CLLocationCoordinate2DMake(self.report!.latitude!, self.report!.longitude!)
        
        
        let marker = Marker(json: (self.report?.rawJSON)!)
        self.mapView.addAnnotation(marker)
        
        self.zoomToCoordinate(coordinate)
        
        

        for _ in (self.navigationController?.childViewControllers)! {
            viewIndex++
        }
        
        if self.navigationController?.childViewControllers[viewIndex-2].isKindOfClass(DuplicateViewController) == true {
            self.getMessageWithID((self.report?.messageID)!)
        }
        
        if self.navigationController?.childViewControllers[viewIndex-2].isKindOfClass(ReportViewController) == true {
            
        }

        self.setupLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        ///////////////////////////////////////////////////////////////////
        ///
        /// SET TABLEVIEWHEIGHT EQUAL TO TABLE VIEW CONTENT SIZE
        /// 33752E3F-6867-4154-9065-527CB75E633F
        ///
        ///////////////////////////////////////////////////////////////////
        self.view.layoutIfNeeded()
        self.tableViewHeight.constant = self.tableView.contentSize.height
        
    }
    
    private func setupCoreData() {
        let fetchRequest = NSFetchRequest(entityName: "Detail")
        let sortDescriptor = NSSortDescriptor(key: "anfrage", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        self.fetchedResultsController.delegate = self
    }
    
    private func setupLayout() {
        self.tableView.estimatedRowHeight = 20
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        let subscribeBarButton = UIBarButtonItem(title: "Subscribe", style: .Plain, target: self, action: "subscribeBarButtonPressed")
        self.navigationItem.rightBarButtonItem = subscribeBarButton
    }
    
    private func setupDelegation() {
        self.mapView.delegate = self
        
        ///////////////////////////////////////////////////////////////////
        ///
        /// DO NOT ALLOW SCROLL THE MAP
        ///
        ///////////////////////////////////////////////////////////////////
        self.mapView.scrollEnabled = false
    }
    
    private func zoomToCoordinate(coordinate: CLLocationCoordinate2D) {
        let distance = CLLocationDistance(1000)
        let coordinationRegion = MKCoordinateRegionMakeWithDistance(coordinate, distance, distance)
        self.mapView.setRegion(coordinationRegion, animated: true)
    }
    
    
    
    // MARK: - Methods
    private func getMessageWithID(id: String) {
        let url = "https://api.werdenktwas.de/bmsapi/get_message_detail"
        let param = [
            "id" : id
        ]
        
        network.getRawJSON(url, parameters: param) { (json) -> Void in
            print(json)
            let detailEntity = NSEntityDescription.entityForName("Detail", inManagedObjectContext: self.managedObjectContext)
            _ = Detail(entity: detailEntity!, insertIntoManagedObjectContext: self.managedObjectContext)
            
//            let detail = JSONParser(json: json)
            self.numberOfRows = json["details"].count/2
            self.detail = json["details"]
            self.tableView.reloadData()
//            detailD.addresse = detail.a
        }
    }
    
    // MARK: - IB Outlets
    func subscribeBarButtonPressed() {
        self.performSegueWithIdentifier("subscribeSegue", sender: self)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "subscribeSegue" {
            let subscribeViewController = segue.destinationViewController as! SubscribeViewController
            
            guard self.report != nil else {
                print("114C0C70-8C6A-4A44-A008-BEEB419C4059 \t report is NIL")
                return
            }
            
            subscribeViewController.id = self.report?.messageID
        }
    }
    

}

extension DetailViewController: MKMapViewDelegate {
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "pin"
        
        if annotation.isKindOfClass(Marker) {
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
            
            if annotationView == nil {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            }
            
            annotationView?.image = (annotation as! Marker).image
            
            return annotationView
        }
        
        return nil
    }
}

extension DetailViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
//        if self.navigationController?.childViewControllers[self.viewIndex-2].isKindOfClass(DuplicateViewController) == true {
//            return 1
//        }
//        
//        if self.navigationController?.childViewControllers[self.viewIndex-2].isKindOfClass(ReportViewController) == true {
//            return 2
//        }
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard self.report != nil else {
            return 0
        }
//        return 1
        
        return 6
    }
    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
//        
//        guard self.detail != nil else {
//            return cell
//        }
//        
//        print(detail![indexPath.row].stringValue)
//        
//        cell.textLabel?.text = detail![indexPath.row].stringValue
////        cell.detailTextLabel?.text = detail![indexPath.row + 1].stringValue
//        
//        return cell
//    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! TableViewCell

        guard self.detail != nil else {
            return cell
        }
        
        cell.title.text = detail![indexPath.row * 2].stringValue
        cell.subtitle.text = detail![(indexPath.row * 2) + 1].stringValue
        
        return cell
    }
}

extension DetailViewController: NSFetchedResultsControllerDelegate {
//    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
    
//    }
}
