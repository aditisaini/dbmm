//
//  CategoryViewController.swift
//  WerDenktWas
//
//  Created by Albert Tra on 04/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import UIKit
import MapKit

class CategoryViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    var coordinate: CLLocationCoordinate2D?
    var report: Report?
    var selectedCategoryID: String = ""
    var types = [Type]()
    
    let network = AFramework.sharedInstance.network

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.tableView.delegate = self
        
        guard coordinate != nil else {
            print("0F6AFE93-2D87-4AAE-AF8B-DBCA679E9611 \t Coordinate ist EMPTY")
            return
        }
        
        let param = [
            "lat": String(self.coordinate?.latitude),
            "long": String(self.coordinate?.longitude)
        ]
        let url = "https://api.werdenktwas.de/bmsapi/get_domainbundle"
        network.getRawJSON(url, parameters: param) { (json) -> Void in
            for (_, item) in json["types"] {
                let type = Type(json: item)
                self.types.append(type)
            }
//            print(self.types[0].category)
            
            self.tableView.reloadData()
        }
        
        ///////////////////////////////////////////////////////////////////
        ///
        /// ADD NEXT BUTTON TO NAVIGATION BAR
        ///
        ///////////////////////////////////////////////////////////////////
        let nextBarButton = UIBarButtonItem(title: "Next", style: .Plain, target: self, action: "nextButtonPressed")
        self.navigationItem.rightBarButtonItem = nextBarButton
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    func nextButtonPressed() {
        getDuplicate()
    }

    func getDuplicate() {
        let url = "https://api.werdenktwas.de/bmsapi/find_duplicates"
        let param = [
//            "lat":          String(self.coordinate?.latitude),
            "lat":          String(self.coordinate!.latitude),
            "long":         String(self.coordinate!.longitude),
            "categoryid":   selectedCategoryID,
            "lang":         "de"
        ]
        
        print(param)
        network.getRawJSON(url, parameters: param) { (json) -> Void in
            if json.count != 0 { /// IN CASE OF DUPLICATE, GO TO duplicateSegue"
                self.report = Report(json: json[0])
                self.performSegueWithIdentifier("duplicateSegue", sender: self)
            } else { /// OTHERWISE GO TO detailSegue
                self.report?.categoryID = self.selectedCategoryID
                self.performSegueWithIdentifier("categoryViewControllerToReportViewController", sender: self)
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "duplicateSegue" {
            let destinationViewController = segue.destinationViewController as! DuplicateViewController
            destinationViewController.report = self.report
        }
        
        if segue.identifier == "categoryViewControllerToReportViewController" {
            let reportViewController = segue.destinationViewController as! ReportViewController
            reportViewController.report = self.report
        }
    }
}

extension CategoryViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.types.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        
        cell.textLabel?.text = self.types[indexPath.row].category
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedCategoryID = self.types[indexPath.row].categoryID
        Logger(sender: self, message: selectedCategoryID)
    }
    
}
