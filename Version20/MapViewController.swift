//
//  MapViewController.swift
//  WerDenktWas
//
//  Created by Albert Tra on 04/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import UIKit
import MapKit
//import CoreLocation

/**
 MapViewController responsible for selecting a point on the map, which is used as coordinate for report
 
 :Author: Albert Tra
 */
class MapViewController: UIViewController {
    
    
    @IBOutlet weak var mapView: MKMapView!
    
    let coreLocation = AFramework.sharedInstance.coreLocation

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        ///////////////////////////////////////////////////////////////////
        ///
        /// ADD NEXT BUTTON TO NAVIGATION BAR
        ///
        ///////////////////////////////////////////////////////////////////
        let nextBarButton = UIBarButtonItem(title: "Next", style: .Plain, target: self, action: "goToCategoryViewController")
        self.navigationItem.rightBarButtonItem = nextBarButton
        
        
        self.zoomToCurrentLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func zoomToCurrentLocation() { /// 87F65B82-F8D2-4B49-8CC2-DF3AB3AC0BD8
        guard self.coreLocation.currentLocation?.coordinate != nil else {
            print("A25B5604-AF17-4DBB-A90A-21852816B28B \t coordinate is EMPTY")
            return
        }
        
        let regionRadius = CLLocationDistance(1000)
        let coordinationRegion = MKCoordinateRegionMakeWithDistance((coreLocation.currentLocation?.coordinate)!, regionRadius, regionRadius)
        mapView.setRegion(coordinationRegion, animated: true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "categoryViewController" {
            ///////////////////////////////////////////////////////////////////
            ///
            /// CONVERT CENTER OF MAIN VIEW TO COORDINATE ON THE MAP, WHICH WILL BE PASSED TO NEXT SCREEN. THIS COORDINATE THEN WILL BE USED TO SEND TO THE SERVER TO GET CATEGORY LIST
            ///
            ///////////////////////////////////////////////////////////////////
            let pointOnView = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds))
            self.view.convertPoint(pointOnView, toView: self.mapView)
            let coordinate = self.mapView.convertPoint(pointOnView, toCoordinateFromView: self.mapView)
            _ = Logger(sender: self, message: coordinate)
            
            let categoryViewController = segue.destinationViewController as! CategoryViewController
            categoryViewController.coordinate = coordinate
        }
    }
    
    func goToCategoryViewController() {
        performSegueWithIdentifier("categoryViewController", sender: self)
    }

}
