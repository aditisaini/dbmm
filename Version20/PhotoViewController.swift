//
//  PhotoViewController.swift
//  WerDenktWas
//
//  Created by Albert Tra on 04/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import UIKit

class PhotoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    
    let picker = UIImagePickerController()
    
    var selectedImage: UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let nextButton = UIBarButtonItem(title: "Next", style: .Plain, target: self, action: "goToMapViewController")
        
        ///////////////////////////////////////////////////////////////////
        ///
        /// ADD NEXT BUTTON TO TTHE RIGHT SIDE OF NAVIGATION BAR ON THE TOP
        ///
        ///////////////////////////////////////////////////////////////////
        self.navigationItem.rightBarButtonItem = nextButton
        
        
        picker.delegate = self
        
        
        
        
        
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Methods

    // MARK: - IB Actions
    
    @IBAction func selectImageButtonPressed(sender: AnyObject) { /// AEE0596D-2F48-45C6-8BAA-EEE9F811766D
        self.picker.allowsEditing = false
        self.picker.sourceType = .PhotoLibrary
        presentViewController(picker, animated: true, completion: nil)
    }
    
    @IBAction func takePhotoButtonPressed(sender: AnyObject) { /// AEE0596D-2F48-45C6-8BAA-EEE9F811766Ds
        _ = Logger(sender: self)
        self.picker.allowsEditing = false
        self.picker.sourceType = .Camera
        presentViewController(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.imageView.image = self.selectedImage
        dismissViewControllerAnimated(true, completion: nil)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    func goToMapViewController() {
        performSegueWithIdentifier("mapViewController", sender: self)
    }

}
