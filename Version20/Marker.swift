//
//  Marker.swift
//  WerDenktWas
//
//  Created by Albert Tra on 02/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit


class Marker: NSObject, MKAnnotation {
    var poi: POI!
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var image: UIImage?
    
    init(json: JSON) {
        self.poi = POI(json: json)
        
        
        self.coordinate = CLLocationCoordinate2DMake(self.poi.latitude!, self.poi.longitude!)
        self.title = poi.descriptionString
        self.subtitle = poi.state
        
        super.init()
        self.setPinImage()
    }
    
    func setPinImage() {
        let imageName = "marker-" + poi.color + "-" + poi.markerID
//        Logger(sender: self, message: imageName)
        if let image = UIImage(named: imageName) {
            self.image = image
        }
    }
    
}