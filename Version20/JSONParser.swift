//
//  JSONParser.swift
//  WerDenktWas
//
//  Created by Albert Tra on 02/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import Foundation
import SwiftyJSON


class JSONParser {
    var rawJSON: JSON?

    // MARK: A
    var anfrage = ""
    
    // MARK: C
    var category: String = ""
    var categoryID: String = ""
    var color: String = ""
    
    var descriptionString: String?
    var distance: Double = 0
    
    
    var latitude: Double?
    var longitude: Double?

    var markerID: String = ""
    var messageID: String = ""
    
    var order: Int?
    
    var pictureURL: String = ""
    
    var state: String?
//    var status:

    // MARK: T
    var text: String = ""
    var title: String = ""
    
    /**
     from detail
     https://api.werdenktwas.de/bmsapi/get_message_detail?id=10180
     */
    var typ: String = ""
    
    var type: String = ""
    
    
    var typeid: Int?

    
    init(json: JSON) {
        self.rawJSON = json

        self.anfrage = json["details"][1].stringValue
        
        self.category = json[1].stringValue
        self.categoryID = json[4].stringValue
        self.color = json["color"].stringValue
        
        self.descriptionString = json["description"].stringValue
        self.distance = json["distance"].doubleValue
        
        self.latitude = json["lat"].doubleValue
        self.longitude = json["long"].doubleValue
        
        
        
        self.markerID = json["markerid"].stringValue
        self.messageID = json["messageid"].stringValue
        
        self.order = json[0].intValue
        
        self.pictureURL = json["pictureUrl"].stringValue
        
        
        self.state = json["state"].stringValue
        
        self.text = json["text"].stringValue
        self.title = json["title"].stringValue
        self.typ = json["details"][3].stringValue
        self.type = json["type"].stringValue
        self.typeid = json["typeid"].intValue
        
    }
}

//protocol jsonToObject(json: JSON) _ = {
//    
//}