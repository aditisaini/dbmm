//
//  Detail+CoreDataProperties.swift
//  WerDenktWas
//
//  Created by Albert Tra on 08/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Detail {

    @NSManaged var anfrage: String?
    @NSManaged var typ: String?
    @NSManaged var status: String?
    @NSManaged var datum: NSDate?
    @NSManaged var addresse: String?
    @NSManaged var historie: String?

}
