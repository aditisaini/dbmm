//
//  ReportViewController.swift
//  WerDenktWas
//
//  Created by Albert Tra on 08/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import UIKit

class ReportViewController: UIViewController {
    
    var report: Report?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        guard report != nil else {
            print("F3B5BDDF-20E8-480E-A2FC-0BBE12E756D7 \t report is NIL")
            return
        }
        
        Logger(sender: self, message: self.report?.messageID)
        
        let nextBarButton = UIBarButtonItem(title: "next", style: .Plain, target: self, action: "nextBarButtonPressed")
        self.navigationItem.rightBarButtonItem = nextBarButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - IB Actions
    func nextBarButtonPressed() {
        self.performSegueWithIdentifier("detailSegue", sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
