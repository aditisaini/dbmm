//
//  SubscribeViewController.swift
//  WerDenktWas
//
//  Created by Albert Tra on 09/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import UIKit

class SubscribeViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK: - Properties
    var id: String?
    let network = AFramework.sharedInstance.network

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.registerForKeyboardNotifications()
        
        guard id != nil else {
            print("00916759-B8C2-496D-AC26-E1C587ACDAD1 \t ID is NIL")
            return
        }
        
        let sendBarButton = UIBarButtonItem(title: "Absenden", style: .Plain, target: self, action: "sendBarButtonPressed")
        
        self.navigationItem.rightBarButtonItem = sendBarButton
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        self.deregisterFromKeyboardNotifications()
    }
    
    // MARK: - Methods
    private func registerForKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWasShown:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillBeHidden:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    private func deregisterFromKeyboardNotifications() {
        //Removing notifies on keyboard appearing
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWasShown(notification: NSNotification) {
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.scrollEnabled = true
        let info : NSDictionary = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height + 20, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        
        if let _ = self.emailTextField {
            if (!CGRectContainsPoint(aRect, emailTextField!.frame.origin)) {
                self.scrollView.scrollRectToVisible(emailTextField!.frame, animated: true)
            }
        }
    }
    
    
    func keyboardWillBeHidden(notification: NSNotification) {
        //Once keyboard disappears, restore original positions
        let info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.scrollEnabled = false
    }
    
    
    
    // MARK: - IB Outlets
    func sendBarButtonPressed() {
        let url = "https://api.werdenktwas.de/bmsapi/subscribe_message"
        let param = [
            "id":       id!,
            "email":    self.emailTextField.text!
        ]
        
        network.getRawJSON(url, parameters: param) { (json) -> Void in
            print(json)
            if json["result"].intValue == 1 {
                ///////////////////////////////////////////////////////////////////
                ///
                /// JUMP TO ROOT VIEW CONTROLLER, ALSO ViewController, IF SUBCRIBE PROCESS SUCCESS
                ///
                ///////////////////////////////////////////////////////////////////
                self.navigationController?.popToRootViewControllerAnimated(true)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension SubscribeViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(textField: UITextField) {
        self.emailTextField = textField
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.emailTextField = nil
    }
}
