//
//  ViewController.swift
//  Version20
//
//  Created by Albert Tra on 02/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import UIKit
import MapKit
import Alamofire

class ViewController: UIViewController, MKMapViewDelegate {

    // MARK: - IB Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: - Properties
    var markers = [Marker]()
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    
    override func viewWillAppear(animated: Bool) {
        
    }
    
    /**
     this func populate all nescessary data to UI
     */
    func dataBinding() {
//        for marker in self.markers {
//            print(marker.poi.descriptionString)
//        }
        self.updatePOIs()
    }
    
    /**
     This func take the poi from markes and then add it to the map
     */
    private func updatePOIs() {
        
        ///////////////////////////////////////////////////////////////////
        ///
        /// MAKE SURE THAT WE DONT HAVE CRASH AT RUN TIME
        ///
        ///////////////////////////////////////////////////////////////////
        guard self.markers.count != 0 else {
            print("8838ADD3-D6E4-490E-8994-93E40A7AA52B \t the number of markers is ZERO")
            return
        }
        
        for marker in self.markers {
            self.mapView.addAnnotation(marker)
        }
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "pin"
        if annotation.isKindOfClass(Marker) {
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
            
            if annotationView == nil { /// THIS BLOCK GET CALL ONLY ONCE
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            }
            
            annotationView!.canShowCallout = true
            annotationView?.image = (annotation as! Marker).image
            
            
            return annotationView!
        }
        
        return nil
    }
    
    @IBAction func neuMeldungButtonPressed(sender: AnyObject) {
        
    }
    
}

