//
//  AppDelegate.swift
//  Version20
//
//  Created by Albert Tra on 02/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CoreLocationServicesDelegate {

    var window: UIWindow?
    
    let network = AFramework.sharedInstance.network
    var markers = [Marker]()
    var coreLocation = AFramework.sharedInstance.coreLocation
    lazy var coreDataStack = CoreDataStack( /// 64F3FB33-4D78-4F43-B7D1-1E7CC5D3FF62
        modelName: "CoreData",
        storeName: "maengelmelder",
        options: [
            NSMigratePersistentStoresAutomaticallyOption: true,
            NSInferMappingModelAutomaticallyOption: true
        ])

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        coreLocation.delegate = self
        
        ///////////////////////////////////////////////////////////////////
        ///
        /// TEST
        ///
        ///////////////////////////////////////////////////////////////////
//        self.testGetDomainBundle()
//        self.testGetDuplicate()
//        self.testParsingType()
//        self.testGetMessageDetail()

        return true
    }
    
    
    func testGetDomainBundle() {
        var types = [Type]()
        let domainBundleURL = "https://api.werdenktwas.de/bmsapi/get_domainbundle"
        let param = [ /// SOMEWHRE AROUND LUISENPLATZ
            "lat": "51.4463131846354",
            "long": "10.3352192657213"
        ]
        network.getRawJSON(domainBundleURL, parameters: param) { (json) -> Void in
            //            print(json["types"][0])
            //            print(json["types"][1])
            
            for (_, item) in json["types"] {
//                print(type)
                let type = Type(json: item)
                types.append(type)
            }
            
            for type in types {
                print(type.category)
            }
        }
    }
    
    func testParsingType() {
        let url = "https://api.werdenktwas.de/bmsapi/find_duplicates"
        
        let param = [
            "lat": "49.8630585638098",
            "long": "8.63250891926461",
            "lang": "de",
            "categoryid": "67"
        ]
        
        network.getRawJSON(url, parameters: param) { (json) -> Void in
            print(json[0])
            let report = Report(json: json[0])
            
            print(report.messageID)
            print(report.title)
            print(report.pictureURL)
            print(report.distance)
            print(report.state)
            print(report.descriptionString)
            print(report.type)
            print(report.text)
            print(report.markerID)
            print(report.color)
            print(report.latitude)
            print(report.longitude)
        }
    }
    
    func testGetDuplicate() {
        let url = "https://api.werdenktwas.de/bmsapi/find_duplicates"
//        let param = [
//            //            "lat":          String(self.coordinate?.latitude),
//            "lat":          String(self.coordinate?.latitude),
//            "long":         String(self.coordinate?.longitude),
//            "categoryid":   selectedCategoryID,
//            "lang":         "de"
//        ]
        let param = [
            "lat": "49.8630585638098",
            "long": "8.63250891926461",
            "lang": "de",
            "categoryid": "67"
        ]
        
//        print(param)
        network.getRawJSON(url, parameters: param) { (json) -> Void in
            print(json)
        }
        
//        let temp = "https://api.werdenktwas.de/bmsapi/find_duplicates?long=8.63250891926461&lat=49.8631568181947&categoryid=67&lang=de"
//        network.getRawJSON(temp) { (json) -> Void in
//            print(json)
//        }
    }
    
    func testGetMessageDetail() {
        let url = "https://api.werdenktwas.de/bmsapi/get_message_detail"
        let param = [
            "id" : "10180"
        ]
        
        network.getRawJSON(url, parameters: param) { (json) -> Void in
//            let detail = 
//            print(json["details"])
            print(json["details"][0])
            print(json["details"][1])
        }
        
        

    }
    
    func didUpdateLocation(currentLocation: CLLocation) {
        let url = "https://api.werdenktwas.de/bmsapi/get_nearest_messages"
        let param = [
            "lat"   : String(currentLocation.coordinate.latitude),
            "long"  : String(currentLocation.coordinate.longitude)
        ]
        
        network.getRawJSON(url, parameters: param) { (json) -> Void in
            self.markers.removeAll()
            for (_, value) in json {
                let marker = Marker(json: value)
                self.markers.append(marker)
            }
//            print(self.markers.count)
            
            let navigationController = self.window?.rootViewController as! UINavigationController /// 1E909DF5-C7F9-410D-BA3D-B70239DF3AD7
            
            for viewController in navigationController.childViewControllers {
                if viewController.isKindOfClass(ViewController) {
                    (viewController as! ViewController).markers = self.markers
                    (viewController as! ViewController).dataBinding()
                }
            }
        }
    }
    
    func didUpdateHeading(currentHeading: CLHeading) {
        
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

