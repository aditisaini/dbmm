//
//  DetailSubscriptionViewController.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 12.03.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "DetailViewController.h"

@interface DuplicateDetailViewController : DetailViewController

@end
