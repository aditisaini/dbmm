//
//  TypeSelectionViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 10.01.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import "TypeSelectionViewController.h"
#import "DialogDuplicateReportsViewController.h"
#import "AttributesViewController.h"
#import "ReportOverviewController.h"

#import "DuplicatesJSONAdapter.h"
#import "MMAlertViewHandler.h"
#import "MMTableView.h"

@interface TypeSelectionViewController ()

@end

@implementation TypeSelectionViewController

@synthesize basisPresentingViewController=_basisPresentingViewController;
@synthesize managedObjectContext=_managedObjectContext;
@synthesize fetchedResultsController=_fetchedResultsController;
@synthesize annotation=_annotation;
@synthesize duplicatesAdapter=_duplicatesAdapter;
@synthesize selectedPath=_selectedPath;
@synthesize initialPath=_initialPath;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;

    self.tableView = [[MMTableView alloc] initWithFrame:self.tableView.frame style:UITableViewStylePlain];
    self.tableView.tableHeaderView = self.descriptionView;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"save_button_title", nil)
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(saveButtonClicked:)];
    self.descriptionTitleLabel.text     = NSLocalizedString(@"type_header", nil);
    self.descriptionTextView.text       = NSLocalizedString(@"type_step_description", nil);

    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        self.descriptionTextView.selectable  = NO;
    }
    
    _initialPath = [_fetchedResultsController indexPathForObject:_annotation.type2];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setToolbarHidden:(self.toolbarItems == nil) animated:animated];
    
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
    
    NSError *error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        LogError(@"Type fetch", error);
    }
    
    //Fetch lastly/on default selected ReportType from DB
    if (self.annotation.type2) {
        _selectedPath = [_fetchedResultsController indexPathForObject:_annotation.type2];
    }
    
    [self.tableView reloadData];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [_duplicatesAdapter cancel];
    [[MMAlertViewHandler sharedInstance] cancelActivityIndicatingAlert];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    _fetchedResultsController.delegate = nil;
    _fetchedResultsController = nil;
    
    [_duplicatesAdapter cancel];
    _duplicatesAdapter = nil;
}

- (void)dealloc
{
    _fetchedResultsController.delegate = nil;
    [_duplicatesAdapter cancel];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

#pragma mark -
#pragma mark Table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSArray *sections = _fetchedResultsController.sections;
    NSUInteger count = sections.count;
    if (count == 0) {
        count = 1;
    }
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger count = 0;
    NSArray *sections = _fetchedResultsController.sections;
    if (sections.count) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
        count = [sectionInfo numberOfObjects];
    }
    return count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return NSLocalizedString(@"available_types", nil);
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell
    ReportType *reportType = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = reportType.name;
    cell.imageView.image = reportType.image;
    
    if (_selectedPath && [indexPath isEqual:_selectedPath]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

#pragma mark -
#pragma mark Table view delegate


- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_selectedPath) {
        UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:_selectedPath];
        oldCell.accessoryType = UITableViewCellAccessoryNone;
        oldCell.accessoryView = nil;
    }
    
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    
    _selectedPath = indexPath;
    
    return nil;
}

- (void)duplicatesCheckFinishedWithResults:(NSArray *)results
{
    [[MMAlertViewHandler sharedInstance] cancelActivityIndicatingAlert];
    
    UITableViewCell *newCell = [self.tableView cellForRowAtIndexPath:_selectedPath];
    newCell.accessoryView = nil;
    newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    
    // Any Duplicates? Set next view controller to duplicates view controller
    if (results.count > 0)
    {
        [self presentDuplicatesViewController];
    }
    // No Duplicates? Set next view controller to attributes view controller
    else {
        if (_annotation.type2.attributes.count > 0) {
            [self pushAttributesViewController];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)presentDuplicatesViewController
{
    DialogDuplicateReportsViewController *duplicateReportsViewController = [[DialogDuplicateReportsViewController alloc] initWithStyle:UITableViewStyleGrouped];
    duplicateReportsViewController.userReport = (UserReport*)self.annotation;
    duplicateReportsViewController.managedObjectContext = _managedObjectContext;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:duplicateReportsViewController];
    
    [self presentViewController:navigationController animated:YES completion:^void {
        if (_annotation.type2.attributes.count > 0) {
            [self pushAttributesViewController];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)pushAttributesViewController
{
    AttributesViewController *attributesViewController = [[AttributesViewController alloc] initWithNibName:@"AttributesViewController" bundle:[NSBundle mainBundle]];
    
    attributesViewController.reportType = _annotation.type2;
    attributesViewController.userReport = (UserReport *)_annotation;
    attributesViewController.managedObjectContext = _managedObjectContext;
    attributesViewController.basisPresentingViewController = self.basisPresentingViewController;
    
    [self.navigationController pushViewController:attributesViewController animated:YES];
}


# pragma mark - Action handling

- (IBAction)saveButtonClicked:(id)sender
{
    if (!_selectedPath) {
        [[MMAlertViewHandler sharedInstance] showHintAlertWithMessage:NSLocalizedStringWithDefaultValue(@"missing_type_msg", nil, [NSBundle mainBundle],
                                                                                                        @"Please select a category before saving!",
                                                                                                        @"Please select a category before saving!")];
    }
    else {
        self.annotation.type2 = [_fetchedResultsController objectAtIndexPath:_selectedPath];
        _initialPath = _selectedPath;
        
        [[MMAlertViewHandler sharedInstance] showActivityIndicatingAlertWithMessage:NSLocalizedString(@"checking_duplicates", nil)
                                                                           andTitle:NSLocalizedString(@"please_wait", nil)];
        
        // Get selected type
        ReportType *reportType = [_fetchedResultsController objectAtIndexPath:_selectedPath];
        // If different type has been previously selected, flush non-cached answers
        if (![self.annotation.type2.myid isEqualToString:reportType.myid]) {
            self.annotation.type2 = reportType;
            [reportType flushAttributeAnswers];
        }
        
        // Check for possible report duplicates
        if (self.duplicatesAdapter) {
            [self.duplicatesAdapter cancel];
        } else {
            self.duplicatesAdapter = [[DuplicatesJSONAdapter alloc] initWithReport:(UserReport*)self.annotation];
        }
        [self.duplicatesAdapter queryDuplicatesForReport:(UserReport*)self.annotation delegate:self selector:@selector(duplicatesCheckFinishedWithResults:)];
    }
}

#pragma mark -
#pragma mark Core Data

- (NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController) {
        NSManagedObjectContext *managedObjectContext = _annotation.managedObjectContext;
        NSDictionary *variables = [NSDictionary dictionaryWithObject:_annotation.domain//[(UserReport*)annotation reportDomain]
                                                              forKey:@"DOMAIN"];
        NSFetchRequest *fetchRequest = [_annotation.entity.managedObjectModel
                                        fetchRequestFromTemplateWithName:@"reportTypeForDomain"
                                        substitutionVariables:variables];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                                       ascending:YES];
        fetchRequest.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        // Create and initialize the fetch results controller.
        _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                       managedObjectContext:managedObjectContext
                                                                         sectionNameKeyPath:nil
                                                                                  cacheName:nil];
        _fetchedResultsController.delegate = self;
    }
    
    return _fetchedResultsController;
}

#pragma mark -
#pragma mark NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath]
                    atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
