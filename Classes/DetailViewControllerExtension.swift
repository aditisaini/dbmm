//
//  DetailViewControllerExtension.swift
//  WerDenktWas
//
//  Created by Albert Tra on 27/01/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import Foundation


extension DetailViewController {
    
    
    func downloadImage() {
        let network = Network()
        network.getRawData(self.imageURL) { (data) -> Void in
            self.headerView.photoView.image = UIImage(data: data)
            self.headerView.photoView.setNeedsDisplay()
            self.headerView.activityView.stopAnimating()
            
        }
    }
    
    func logger(string: String?) {
        print("\n Image URL")
        if string != nil {
            
            _ = Logger(sender: self, message: string)
        } else {
            print("invalued Imaga URL")
        }
    }
    
    func getReportWithID(id: NSNumber) {
        let network = AFramework.sharedInstance.network
        let url = "http://api.werdenktwas.de/bmsapi/get_message_detail?id=" + id.stringValue
        network.getRawJSON(url) { (json) -> Void in
            //            print(json)
            //            self.tableView.delegate = self
            //            self.tableView.reloadData()
            //            self.detailsReceived()
            
            
            if self.headerView.photoView.image == nil {
                self.imageURL = json["pictureUrl"].stringValue
                self.downloadImage()
            }
            let count = json["details"].count
            //            var titles = [String]()
            //            var contents = [String]()
            self.titles = NSMutableArray()
            self.contents = NSMutableArray()
            for i in 0..<count {
                self.titles.addObject(json["details"][2*i].stringValue)
                self.contents.addObject(json["details"][2*i + 1].stringValue)
            }
            self.tableView.reloadData()
        }
    }
    
    func numberOfRowsInSection() -> Int {
        return self.titles.count/2
    }
    
    func dataBinding(cell: ReportDetailsCell, indexPath: NSIndexPath) {
        cell.setHeaderText(self.titles[indexPath.row] as! String)
        cell.setDetailsText(self.contents[indexPath.row] as! String)
    }
}