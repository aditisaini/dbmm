//
//  POIMapViewController.m
//  WerDenktWas
//
//  Created by Martin on 07.05.15.
//  Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "POIMapViewController.h"
#import "POIJSONAdapter.h"
#import "MapBoundingBox.h"
#import "ReportAnnotation.h"
#import "POIDetailsTableViewController.h"
#import "PointOfInterest.h"

@interface POIMapViewController ()
@property(strong, nonatomic) IBOutlet MKMapView *mapView;
@property(strong, nonatomic) IBOutlet MapKitDelegate *mapKitDelegate;
@property(nonatomic, assign) BOOL requestPending;

@end


@implementation POIMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self requestPOIUpdate];

    self.mapKitDelegate.enablingCallouts = YES;
    self.navigationController.toolbarHidden = YES;
    self.mapView.region = self.region;
}

- (void)setRegion:(MKCoordinateRegion)region {
    _region = region;
}

- (void)requestPOIUpdate {
    if (self.requestPending) {
        return;
    }

    self.requestPending = YES;

    POIJSONAdapter *poijsonAdapter = [[POIJSONAdapter alloc] init];
    MapBoundingBox *mapBoundingBox = [[MapBoundingBox alloc] initWithMapRect:self.mapView.visibleMapRect];
    [poijsonAdapter queryForPOIForBoundingBox:mapBoundingBox delegate:self selector:@selector(didReceivePOIArray:)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didReceivePOIArray:(NSArray *)poiarray {
    self.requestPending = NO;
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView addAnnotations:poiarray];
}

#pragma mark - MapKitDelegate

- (void)showDetails:(ReportAnnotation *)annotation {
    POIDetailsTableViewController *detailViewController = [[POIDetailsTableViewController alloc] init];
    detailViewController.pointOfInterest = (PointOfInterest *)annotation;

    [self.navigationController pushViewController:detailViewController animated:YES];
}

- (void)editReport:(ReportAnnotation *)annotation {
    //unused
}

- (void)createReport {
    //unused
}

- (void)startAtLocation:(CLLocation *)location {
    //unused
}

- (void)movedToLocation:(CLLocation *)location {
    [self requestPOIUpdate];
}

@end
