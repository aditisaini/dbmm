//
//  GeoCoordinateJSONAdapter.m
//  project 'WerDenktWas'
//
//
//  $Id: GeoCoordinateJSONAdapter.m 79 2010-07-16 17:03:57Z eik $
//

#import "GeoCoordinateJSONAdapter.h"

#import "Constants.h"
#import "ServerConnection.h"
#import "Domain.h"
#import "ReportType.h"

#import "Constants.h"


@implementation GeoCoordinateJSONAdapter


@synthesize street_address;

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext_
{
    self = [super init];
    if (self != nil) {
        managedObjectContext = managedObjectContext_;

    }
        
    return self;
}




#pragma mark API

- (void)queryServer:(NSString *)aServer withLocation:(CLLocation *)location delegate:(id)aDelegate selector:(SEL)aSelector;
{
    self->delegate = aDelegate;
    self->selector = aSelector;

    coordinate = location.coordinate;

    server = aServer;

    NSString *localization = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];

    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSString stringWithFormat:@"%lf", coordinate.latitude], @"lat",
                                [NSString stringWithFormat:@"%lf", coordinate.longitude], @"long",
                                //[NSString stringWithFormat:@"%lf", location.horizontalAccuracy], @"accuracy",	
//								[[UIDevice currentDevice]uniqueIdentifier],@"phone",
                                [[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"], @"phone",
								[[NSUserDefaults standardUserDefaults] stringForKey:kAppIDKey], @"appid",
								localization, @"lang",
                                nil];

    
    
    [self queryWithServer:server method:@"get_reverse_geocoordinates" parameters:parameters];
  
    //http://www.werdenktwas.de/bmsapi/get_reverse_geocoordinates?lat=49.874573&long=8.660660
}
    


//    Structure of query
//{
//    "country" : "",
//    "city" : "",
//    "full_address" : "MerckstraÃŸe 9C, 64283 Darmstadt, Germany",
//    "street" : "",
//    "postcode" : ""
//}



- (void)updateReportTypesWithArray:(NSArray *)types
{
    // make a property list from the array
    NSMutableArray *list = [[NSMutableArray alloc] initWithCapacity:types.count/2];

   // bool needDefault = YES;
    for (NSArray *array in types) {
       
        if ([array isKindOfClass:NSArray.class]) {
           
            //NSString *imageName = [NSString stringWithFormat:kMarkerFormat, [array objectAtIndex:0]];
            /*
            NSDictionary *entry = [NSDictionary dictionaryWithObjectsAndKeys:
								   [array objectAtIndex:4], @"myid",
								   [array objectAtIndex:0], @"ID",
								   [array objectAtIndex:1], @"name", 
								   imageName, @"imageName", 
								   [array objectAtIndex:4], @"myid",
								   [array objectAtIndex:2], @"isPrivate",
								   [array objectAtIndex:3], @"isIdentifictionNeeded",
								   nil];
			*/
            
            NSDictionary *entry = [NSDictionary dictionaryWithObjectsAndKeys:
								   [array objectAtIndex:0], @"country",
								   [array objectAtIndex:1], @"city",
								   [array objectAtIndex:2], @"full_address", 
								   [array objectAtIndex:3], @"street",
								   [array objectAtIndex:4], @"postcode",
								   nil];
			
//            NSLog(@" country %@  - city  %@  -  full_address   %@  -  street   %@   - postcode  %@   ", [array objectAtIndex:0],[array objectAtIndex:1],[array objectAtIndex:2],[array objectAtIndex:3],[array objectAtIndex:4]);
            
            //domain.reportTypeForName([array objectAtIndex:1]).ID=[array objectAtIndex:0];
			//[domain reportTypeForName:[array objectAtIndex:1]].ID=[array objectAtIndex:0];
            
			[list addObject:entry];
            
        }
    }

    //[self updateParent:domain withArray:list deletingOld:YES];
}




#pragma mark ServerConnectionDelegate

- (void)serverConnectionDidFinishLoading:(ServerConnection *)serverConnection
{
    NSDictionary *domainList = [serverConnection jsonData];

    if ([domainList isKindOfClass:NSDictionary.class]) {
                
        street_address = @"";
        street_address = [street_address stringByAppendingString: [domainList objectForKey:@"full_address"]];
        
    }
#if LOG_LEVEL > 0
    else {
        NSLog(@"JSONDATA (Geo): %@", domainList);
        NSLog(@"Could not understand domain result");
    }
#endif

    [self->delegate performSelector:self->selector];
    self->delegate = nil;


}


@end
