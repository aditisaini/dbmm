//
// Created by Martin on 11.05.15.
// Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractJSONAdapter.h"

@class PointOfInterest;


@interface POIPictureAdapter : NSObject <ServerConnectionDelegate> {


@private
    id delegate;
    SEL selector;
    ServerConnection *connection;

#if !__OBJC2__
    ServerReport *report;
#endif

}

- (void)queryWithURL:(NSString *)pictureURL delegate:(id)aDelegate selector:(SEL)aSelector;

@end