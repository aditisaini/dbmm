//
//  ReportPhotoViewController.m
//  MaengelmelderCore
//
//  Created by Franzi Engelmann on 29.11.13.
//  Copyright (c) 2013 WerDenktWas. All rights reserved.
//

#import "PhotoViewController.h"
#import "PositionViewController.h"

#import "UserReport.h"
#import "ReportPicture.h"
#import "MMAlertViewHandler.h"
#import "MMColor.h"
#import "MMSupport.h"

@interface PhotoViewController ()

@property (nonatomic, retain) UIImage *latestPicture;
@property (weak, nonatomic) IBOutlet UIButton *galleryLabeledButton;
@property (weak, nonatomic) IBOutlet UIButton *galleryPlusButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraLabeledButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraPlusButton;

@end

@implementation PhotoViewController

@synthesize managedObjectContext=_managedObjectContext;
@synthesize userReport=_userReport;
@synthesize latestPicture=_latestPicture;

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.toolbarHidden = YES;

    self.imageView.backgroundColor  = [MMColor basisAccentColor];
    self.galleryPlusButton.tintColor= [MMColor secondaryAccentColor];
    self.cameraPlusButton.tintColor = [MMColor secondaryAccentColor];
    self.galleryLabeledButton.titleLabel.textColor = [MMColor secondaryAccentColor];
    self.cameraLabeledButton.titleLabel.textColor  = [MMColor secondaryAccentColor];

    // Title MUST be set, else a cd-update-save-failed error will occur (and context seems to get lost then)!
    if (!_userReport.title) {
        _userReport.title = NSLocalizedString(@"new_message", nil);
    }
    if (_userReport.picture) {
        self.imageView.image = _userReport.picture.image;
        self.missingImageLabel.hidden = YES;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    NSString *galleryTitle = NSLocalizedString(@"load_photo_from_gallery", nil);
    NSString *cameraTitle  = NSLocalizedString(@"take_new_photo", nil);
    NSString *missingImage = NSLocalizedString(@"no_photo_title", nil);
    NSString *cancel       = NSLocalizedString(@"cancel_button_title", nil);
    NSString *save         = NSLocalizedString(@"Speichern", nil);

    [self.missingImageLabel setText:missingImage];
    [self.galleryLabeledButton setTitle:galleryTitle forState:UIControlStateNormal];
    [self.cameraLabeledButton  setTitle:cameraTitle  forState:UIControlStateNormal];
    
    self.descriptionTitleLabel.text     = NSLocalizedString(@"photo_header", nil);
    self.descriptionTextView.text       = NSLocalizedString(@"photo_step_description", nil);

    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        self.descriptionTextView.selectable  = NO;
    }
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:save
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(saveButtonClicked:)];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:cancel
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(cancelButtonClicked:)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    _latestPicture = (UIImage *)[info objectForKey:UIImagePickerControllerEditedImage];
    if (!_latestPicture) {
        _latestPicture = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
    }
    
    [self setReportImage:_latestPicture];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)setReportImage:(UIImage *)image
{
    if (image) {
        _imageView.image = image;
        _missingImageLabel.hidden = YES;
    } else {
        _missingImageLabel.hidden = NO;
    }
}

- (IBAction)cancelButtonClicked:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)saveButtonClicked:(id)sender
{
    [self saveLatestPicture];
    [self dismissModalViewControllerAnimated:YES];
}

- (void)saveLatestPicture
{
    if (_latestPicture) {
        ReportPicture *reportPicture = _userReport.picture;
        
        if (!reportPicture) {
            reportPicture = [NSEntityDescription insertNewObjectForEntityForName:@"ReportPicture"
                                                          inManagedObjectContext:_managedObjectContext];
        }
        
		UIImage *smallImage     = [MMSupport scaleAndRotateImage:_latestPicture];
        reportPicture.imageData = [NSData dataWithData:UIImageJPEGRepresentation(smallImage, 0.5)];
        
        if (!_userReport.picture) {
            _userReport.picture = reportPicture;
        }
    }
    
    NSError *error = nil;
    [_managedObjectContext save:&error];
}

- (IBAction)galleryButtonClicked:(id)sender
{
    // Start camera
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType      = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.delegate        = self;
        imagePicker.allowsEditing   = NO;
        
        [self presentViewController:imagePicker animated:YES completion:NULL];
    }
}

- (IBAction)cameraButtonClicked:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && !TARGET_IPHONE_SIMULATOR) {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType      = UIImagePickerControllerSourceTypeCamera;
        imagePicker.delegate        = self;
		imagePicker.allowsEditing   = NO;
        
        [self presentViewController:imagePicker animated:YES completion:NULL];
    } else {
        [self galleryButtonClicked:sender];
    }
}

@end
