//
//  ReportHeaderView.h
//  WerDenktWas
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 12.07.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ReportHeaderView.h 72 2010-07-14 13:21:40Z eik $
//

#import <UIKit/UIKit.h>

#import "BaseHeaderView.h"


@interface ReportHeaderView : BaseHeaderView
{
@private
#if !__OBJC2__
    UIImageView *emptyPhotoView;
#endif
}

@property (nonatomic, strong) IBOutlet UIImageView *emptyPhotoView;


@end
