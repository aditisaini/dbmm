//
//  DetailHeaderView.h
//  WerDenktWas
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 12.07.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: DetailHeaderView.h 72 2010-07-14 13:21:40Z eik $
//

#import <UIKit/UIKit.h>

#import "BaseHeaderView.h"


@interface DetailHeaderView : BaseHeaderView
{
@private
#if !__OBJC2__
    UIActivityIndicatorView *activityView;
    UIView *mapFrame;

    BOOL _waitingForPicture;
#endif
}

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityView;
@property (nonatomic, strong) IBOutlet UIView *mapFrame;
@property (nonatomic, assign, getter=isWaitingForPicture) BOOL waitingForPicture;
@property (nonatomic, strong) NSString* imageURL;

- (void)stopWaitingForPicture;


@end
