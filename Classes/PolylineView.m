//
//  PolylineView.m
//  AppJobber
//
//  Created by Oliver Eikemeier on 27.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "PolylineView.h"

@implementation PolylineView

- (void)drawMapRect:(MKMapRect)mapRect zoomScale:(MKZoomScale)zoomScale inContext:(CGContextRef)context
{
    self.lineWidth = zoomScale*MKRoadWidthAtZoomScale(zoomScale);
    [super drawMapRect:mapRect zoomScale:zoomScale inContext:context];
}

@end
