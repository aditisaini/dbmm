//
//  ReportDetailsCell.m
//  WerDenktWas
//
//  Created by Franziska Engelmann on 22.10.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import "ReportDetailsCell.h"
#import "MMColor.h"

#define INSET         35.0

@interface ReportDetailsCell()

@property (nonatomic, assign) BOOL accessoryTypeSet;

@end

@implementation ReportDetailsCell

@synthesize subview;
@synthesize headerLabel;
@synthesize contentTextView=_contentTextView;

+ (float)inset
{
    return INSET;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{    
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ReportDetailsCell" owner:self options:nil];
        [self addSubview:(ReportDetailsCell *)[nib objectAtIndex:0]];
        self.headerLabel.textColor = [MMColor reportDetailsHeaderColor];
    }
    
    return self;
}

- (void)setAccessoryType:(UITableViewCellAccessoryType)accessoryType
{
    [super setAccessoryType:accessoryType];
    
    if (accessoryType && !self.accessoryTypeSet) {
        
        self.contentTextView.frame = CGRectMake(_contentTextView.frame.origin.x,
                                                _contentTextView.frame.origin.y,
                                                _contentTextView.frame.size.width - 25.0,
                                                _contentTextView.frame.size.height);
    
        [self adjustHeightOfTextView:_contentTextView];
        [self adaptContainerViewHeight];
        self.accessoryTypeSet = YES;
    }
}

- (void)setHeaderText:(NSString *)text
{
    self.headerTextView.editable = YES;
    self.headerTextView.text = text;
    self.headerTextView.editable = NO;
    
    [self adjustHeightOfTextView:_headerTextView];
    self.contentTextView.frame = CGRectMake(self.contentTextView.frame.origin.x,
                                            self.headerTextView.frame.origin.y + self.headerTextView.frame.size.height - 10.0,
                                            self.contentTextView.frame.size.width,
                                            self.contentTextView.frame.size.height);
    [self adaptContainerViewHeight];
}

- (void)setDetailsText:(NSString *)text
{
    self.contentTextView.editable = YES;
    self.contentTextView.text = text;
    self.contentTextView.editable = NO;
    
    [self adjustHeightOfTextView:_contentTextView];
    [self adaptContainerViewHeight];
}

- (void)adjustHeightOfTextView:(UITextView *)textView
{
    CGSize textViewSize = [textView sizeThatFits:CGSizeMake(textView.frame.size.width, FLT_MAX)];
    textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y,
                                textView.frame.size.width, textViewSize.height);
}


- (void)adaptContainerViewHeight
{
    self.subview.frame = CGRectMake(subview.frame.origin.x, INSET/5,
                                    subview.frame.size.width, [self overallHeight]);
}

- (CGFloat)overallHeight
{
    return self.contentTextView.frame.origin.y + self.contentTextView.frame.size.height;
}


@end
