//
//  WerDenktWasAppDelegate.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 10.05.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: WerDenktWasAppDelegate.m 60 2010-07-12 00:13:02Z eik $
//
//#import <HockeySDK/HockeySDK.h>

#import "WerDenktWasAppDelegate.h"
#import "RootViewController.h"
#import "MMColor.h"
#import "Constants.h"

@interface WerDenktWasAppDelegate ()

@end

const NSString * kEntityName     = @"name";
const NSString * kSortDescriptor = @"sort";

@implementation WerDenktWasAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel   = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

@synthesize window;
@synthesize navigationController;
@synthesize rootViewController;


#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after app launch
    [self setUpHockeyApp];
    [self createDefaultPreferences];
    rootViewController.managedObjectContext = [self managedObjectContext];
    
    // Custom colors for navigation- and toolbar
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                              [MMColor navigationBarTitelItemColor], NSForegroundColorAttributeName,
                                                              [UIFont boldSystemFontOfSize:17.0], NSFontAttributeName,
                                                              nil]];

        [[UINavigationBar appearance] setBarTintColor:[MMColor navigationBarColor]];
        [[UINavigationBar appearance] setTintColor:[MMColor navigationBarItemColor]];
        [[UIToolbar appearance] setBarTintColor:[MMColor toolbarColor]];
        [[UIToolbar appearance] setTintColor:[MMColor toolbarItemColor]];
        [[UITextField appearance] setTintColor:[UIColor blackColor]];
        [[UITextView appearance] setTintColor:[UIColor blackColor]];
    } else {
        [[UINavigationBar appearance] setTintColor:[MMColor navigationBarColor]];
        [[UIToolbar appearance] setTintColor:[MMColor toolbarColor]];
    }

    // Eliminates iOS 6 warnings (concerning the root view controller).
    if ([window respondsToSelector:@selector(setRootViewController:)]) {
        window.rootViewController = navigationController;
    } else {
        [window addSubview:navigationController.view];
    }
    
    NSDictionary *keyValueDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"ReportDuplicate", @"userReport", nil]
                                                             forKeys:[NSArray arrayWithObjects:kEntityName, kSortDescriptor, nil]];
    [self deleteEntitiesWithNames:[NSArray arrayWithObject:keyValueDict]];
    
    [window makeKeyAndVisible];

    return YES;
}

- (void)setUpHockeyApp {
//    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"2caedcc0c61c31f2f6e820dc86fffd13"];
//    [[BITHockeyManager sharedHockeyManager].crashManager setCrashManagerStatus:BITCrashManagerStatusAutoSend];
//    [[BITHockeyManager sharedHockeyManager] startManager];
//    [[BITHockeyManager sharedHockeyManager].authenticator authenticateInstallation];
}

// Avoid inconsistencies of database --> delete accidentally stored entities here
- (void)deleteEntitiesWithNames:(NSArray *)entityNames
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSError *error = nil;
    
    for (NSDictionary *keyValuePair in entityNames) {
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:[keyValuePair objectForKey:kEntityName]
                                                             inManagedObjectContext:self.managedObjectContext];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:[keyValuePair objectForKey:kSortDescriptor] ascending:YES];
        [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        [fetchRequest setEntity:entityDescription];
        [fetchRequest setPredicate:nil];
        
        NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                  managedObjectContext:self.managedObjectContext
                                                                                                    sectionNameKeyPath:nil
                                                                                                             cacheName:nil];
        if (![fetchedResultsController performFetch:&error]) {
            LogError(@"Clean up fetch", error);
        }
        
        for (NSManagedObject *object in [fetchedResultsController fetchedObjects]) {
            [fetchedResultsController.managedObjectContext deleteObject:object];
        }
        
    }
    
    [_managedObjectContext save:&error];
}

/*
 applicationWillTerminate: saves changes in the application's managed object context before the application terminates.
 */
- (void)applicationWillTerminate:(UIApplication *)application
{
    [[NSUserDefaults standardUserDefaults] synchronize];

    [self saveContext];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[NSUserDefaults standardUserDefaults] synchronize];

    [self saveContext];
}

#pragma mark -
#pragma mark Memory management


#pragma mark -
#pragma mark Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
        _managedObjectContext.undoManager = nil;
    }
    return _managedObjectContext;
}

/*
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 */
- (NSManagedObjectModel *)managedObjectModel
{

    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return _managedObjectModel;
}


/*
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{

    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }

    NSString *storePath = [[self applicationCachesDirectory] stringByAppendingPathComponent:@"WerDenktWasCache.sqlite"];
    NSURL *storeUrl = [NSURL fileURLWithPath:storePath];

    NSError *error = nil;

    if ([[NSUserDefaults standardUserDefaults] boolForKey:kDeleteCacheKey]) {
        [[NSFileManager defaultManager] removeItemAtPath:storePath error:&error];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kDeleteCacheKey];
    }

    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error]) {
        [[NSFileManager defaultManager] removeItemAtPath:storePath error:&error];
#if LOG_LEVEL > 0
        NSLog(@"Incompatible cache deleted");
#endif
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error]) {
            LogError(@"Persistent store", error);
        }
    }

    return _persistentStoreCoordinator;
}


#pragma mark -
#pragma mark Application's documents directory

/*
 Returns the path to the application's caches directory.
 */
- (NSString *)applicationCachesDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark -
#pragma mark Preferences

- (void)createDefaultPreferences
{
#if defined(RELEASE)
	NSString *settingsBundlePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"Settings.bundle"];
#else
    NSString *settingsBundlePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"SettingsDebug.bundle"];
# endif
    
    NSString *finalPath = [settingsBundlePath stringByAppendingPathComponent:@"Root.plist"];

    NSDictionary *settingsDict = [NSDictionary dictionaryWithContentsOfFile:finalPath];
    NSArray *prefSpecifierArray = [settingsDict objectForKey:@"PreferenceSpecifiers"];

    NSMutableDictionary *appDefaults = [[NSMutableDictionary alloc] initWithCapacity:prefSpecifierArray.count];
    
    for (NSDictionary *prefItem in prefSpecifierArray) {
        NSString *keyStr = [prefItem objectForKey:@"Key"];
        id defaultValue = [prefItem objectForKey:@"DefaultValue"];
        if (keyStr && defaultValue)
            [appDefaults setObject:defaultValue forKey:keyStr];
    }
    [appDefaults setObject:@"" forKey:kEmailAddressKey];
    [appDefaults setObject:[NSNumber numberWithBool:NO] forKey:kWantsEmailKey];
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end

/**
 Enable HTTPS request
 
 :Author: Albert
 */
@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}
@end

