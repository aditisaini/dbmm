//
//  DialogAttributesViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 31.01.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import "DialogAttributesViewController.h"
#import "ReportOverviewController.h"
#import "TypeAttribute.h"
#import "DetailCell.h"

#import "MMAlertViewHandler.h"
#import "MMTableView.h"

@interface DialogAttributesViewController ()

@end

@implementation DialogAttributesViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"next_button_title", nil)
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(nextStepButtonClicked:)];
    self.rightBarButton = self.navigationItem.rightBarButtonItem;
  
    self.tableView.tableHeaderView  = self.headerView;
    self.headerView.hidden          = NO;
    self.headerTitleLabel.text      = NSLocalizedString(@"attribute_step_header", nil);
    self.headerTextView.text        = NSLocalizedString(@"attribute_step_description", nil);

    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        self.headerTextView.selectable  = NO;
    }
}

- (IBAction)nextStepButtonClicked:(id)sender
{
    BOOL answersComplete = YES;
    
    for (TypeAttribute *attribute in self.visibleAttributes)
    {
        if (![attribute isFulfilledInContextOfAttributes:self.visibleAttributes])
        {
            [[MMAlertViewHandler sharedInstance] showMissingAnswerOnAttributeAlert:attribute.error];
            answersComplete = NO;
            break;
        }
    }
    
    // Store answer of subject title and description dummy-attribute and delete attributes afterwards (is stored at index 0)
    if ([self.userReport.type2.hasSubjectTitle boolValue]) {
        self.userReport.subjectTitle = [[self.visibleAttributes objectAtIndex:0] answer];
        self.userReport.details      = [[self.visibleAttributes objectAtIndex:1] answer];
    } else {
        self.userReport.details      = [[self.visibleAttributes objectAtIndex:0] answer];
    }
    
    if (answersComplete) {
        ReportOverviewController *reportOverviewController = [[ReportOverviewController alloc] initWithNibName:@"ReportOverviewController" bundle:nil];
        reportOverviewController.userReport = self.userReport;
        reportOverviewController.managedObjectContext = self.managedObjectContext;
        [self.navigationController pushViewController:reportOverviewController animated:YES];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
