//
//  ReportPositionViewController.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 06.01.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapKitDelegate.h"

@class ReportAnnotation;
@class MovableAnnotation;
@class DomainJSONAdapter;
@class GeoCoordinateJSONAdapter;
@class UserReport;

@interface PositionViewController : UIViewController <MapActionDelegate>
{
@private
#if !__OBJC2__
    ReportAnnotation *annotation;

    MKMapView *mapView;
    
    MapKitDelegate *mapKitDelegate;
    
    NSManagedObjectContext *managedObjectContext;
#endif
    
    
    GeoCoordinateJSONAdapter *geoCoordinateJSONAdapter;
    DomainJSONAdapter *domainJSONAdapter;
    
    void (^_addressUpdateFinishedBlock)();
}

@property (weak, nonatomic) IBOutlet UILabel *descriptionTitleLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UIView *descriptionView;

@property (nonatomic, strong) UserReport *userReport;
@property (nonatomic, strong) IBOutlet UISegmentedControl *mapType;

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;//MK: Extension
@property (nonatomic, strong) NSString *server;

@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) IBOutlet MapKitDelegate *mapKitDelegate;

@property (nonatomic, strong) void (^addressUpdateFinishedBlock)();

- (IBAction)changeMapType:(id)sender;

- (void)saveStreetAddressOnFinish:(void (^)())block;

@end
