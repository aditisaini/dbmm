//
//  DraggableAnnotation.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 25.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: DraggableAnnotationView.m 74 2010-07-14 22:00:16Z eik $
//

#import "DraggableAnnotationView.h"
#import <QuartzCore/QuartzCore.h>

#import "DraggableAnnotationView3.h"

#import "ReportAnnotationProtocol.h"

#define kLiftDistance 54.0

#define kLiftAnimationKey @"LiftAnimationKey"

@implementation DraggableAnnotationView

@synthesize mapView;

- (id)initWithAnnotation:(id <ReportAnnotationProtocol>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    if ((self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier])) {
        self.mapView = nil;
        
        self.multipleTouchEnabled = NO;
        
#ifdef __IPHONE_4_0
        if ([self respondsToSelector:@selector(setDraggable:)])
            self.draggable = YES;
#endif
        
    }
    
    return self;
}

- (void)dealloc
{
    self.mapView = nil;
    
}

#pragma mark -
#pragma mark Animations

- (void)animateAnnotationLift
{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
    theAnimation.removedOnCompletion = NO;
    theAnimation.fillMode = kCAFillModeForwards;
    
    theAnimation.byValue = [NSValue valueWithCGPoint:CGPointMake(0.0, -kLiftDistance)];
    
    theAnimation.duration = 0.2;
    theAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    [self.layer addAnimation:theAnimation forKey:kLiftAnimationKey];
}

- (void)animateAnnotationCancel
{
    [self.layer removeAnimationForKey:kLiftAnimationKey];
}

- (void)animateAnnotationDrop
{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
    theAnimation.removedOnCompletion = NO;
    theAnimation.fillMode = kCAFillModeForwards;
    
    theAnimation.toValue = [NSValue valueWithCGPoint:self.layer.position];
    theAnimation.byValue = [NSValue valueWithCGPoint:CGPointMake(0.0, kLiftDistance)];
    
    theAnimation.duration = 0.05;
    theAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    [self.layer addAnimation:theAnimation forKey:kLiftAnimationKey];
}

- (void)animateAnnotationDropBouncing
{
    CGPoint target = self.center;
    CAKeyframeAnimation *theAnimation = [self bounceAnimationAtPoint:target];
    
    [self.layer addAnimation:theAnimation forKey:kLiftAnimationKey];
}

- (void)animateAnnotationBounceAndUpdate
{
    CGPoint target = self.center;
    target.y -= kLiftDistance;
    
    CAKeyframeAnimation *theAnimation = [self bounceAnimationAtPoint:target];
    
    [self.layer addAnimation:theAnimation forKey:kLiftAnimationKey];
    
    // Update the map coordinate to reflect the new position.
    id <ReportAnnotationProtocol> myAnnotation = (id <ReportAnnotationProtocol>)self.annotation;
    if (self.mapView && [myAnnotation conformsToProtocol:@protocol(ReportAnnotationProtocol)]) {
        target = self.center;
        target.x -= self.centerOffset.x;
        target.y -= self.centerOffset.y + kLiftDistance;
        myAnnotation.coordinate = [self.mapView convertPoint:target
                                        toCoordinateFromView:self.superview];
        
        if ([myAnnotation respondsToSelector:@selector(setSubtitle:)])
            myAnnotation.subtitle = nil;
    }
}

- (CAKeyframeAnimation *)bounceAnimationAtPoint:(CGPoint)point
{
    // create a CGPath that implements a bounce
    CGMutablePathRef thePath = CGPathCreateMutable();
    CGPathMoveToPoint(thePath, NULL, point.x, point.y);
    
    CGFloat offset = -12.0;
    while (fabsf(offset) > 2.0) {
        CGPathAddLineToPoint(thePath, NULL, point.x, point.y + offset);
        offset = -offset / 1.4;
    }
    CGPathAddLineToPoint(thePath, NULL, point.x, point.y);
    
    CAKeyframeAnimation *theAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    
    theAnimation.path = thePath;
    CFRelease(thePath);
    
    theAnimation.duration = 0.8;
    theAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];//kCAMediaTimingFunctionEaseInEaseOut];
    
    int steps = 100;
    NSMutableArray *values = [NSMutableArray arrayWithCapacity:steps];
    double value = 0;
    float e = 2.71;
    for (int t = 0; t < steps; t++) {
        value = 320 * pow(e, -0.055*t) * cos(0.08*t) + 160;
        [values addObject:[NSNumber numberWithFloat:value]];
    }
    theAnimation.values = values;
    theAnimation.fillMode = kCAFillModeForwards;
    
    return theAnimation;
}


#pragma mark -
#pragma mark MKAnnotationView overrides

- (void)setDragState:(MKAnnotationViewDragState)newDragState animated:(BOOL)animated
{
    [self setDragState:newDragState];	
}

#ifdef __IPHONE_4_0

- (void)setDragState:(MKAnnotationViewDragState)aDragState
{
    switch (aDragState) {
        case MKAnnotationViewDragStateStarting:
            [self animateAnnotationLift];
            super.dragState = MKAnnotationViewDragStateDragging;
            break;
        case MKAnnotationViewDragStateCanceling:
            [self animateAnnotationDropBouncing];
            super.dragState = MKAnnotationViewDragStateNone;
            break;
        case MKAnnotationViewDragStateEnding:
            [self animateAnnotationBounceAndUpdate];
            super.dragState = MKAnnotationViewDragStateNone;
            break;
        default:
            break;
    }
}

#endif

@end
