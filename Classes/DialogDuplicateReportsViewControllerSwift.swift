//
//  DialogDuplicateReportsViewControllerSwiftViewController.swift
//  WerDenktWas
//
//  Created by Albert Tra on 17/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import UIKit
import CoreData
import A_Framework

class DialogDuplicateReportsViewControllerSwift: UIViewController {
    
    var reportType: ReportType?
    var userReport: UserReport?
    var managedObjectContext: NSManagedObjectContext?
    var fetchedResultsController: NSFetchedResultsController!
    var thumbnail: UIImage?
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerTextView: UITextView!
    
    let network = AFramework.sharedInstance.network
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print(userReport?.duplicates)
        
//        network.getRawData((userReport?.duplicates as! ReportDuplicate).pictureURL, completion: <#T##(data: NSData) -> Void#>)
//        print((userReport?.duplicates as! ReportDuplicate).pictureURL)
        
        
        let variables = NSDictionary(object: self.userReport!, forKey: "USERREPORT")
        
        
//        let fetchedResultsController = 
        let fetchRequest = userReport?.entity.managedObjectModel.fetchRequestFromTemplateWithName("duplicatesForUserReport", substitutionVariables: variables as! [String : AnyObject])
        let sortDescriptor = NSSortDescriptor(key: "distance", ascending: true)
        fetchRequest?.sortDescriptors = [sortDescriptor]
//        fetchRequest. 
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest!, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        do {
            print("Catching")
            try self.fetchedResultsController.performFetch()
        } catch let error {
            print("error while fetching database")
        }
        
        self.setupUI()
        self.dataBinding()
    
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UI
    private func setupUI() {
        self.navigationController?.title = NSLocalizedString("duplicates_header", comment: "")
        self.navigationItem.leftBarButtonItem = nil
    }
    
    // MARK: - Data binding
    private func dataBinding() {
        self.headerTitleLabel.text = NSLocalizedString("duplicates_step_header", comment: "")
        self.headerTextView.text = NSLocalizedString("duplicates_step_description", comment: "")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UITableView
extension DialogDuplicateReportsViewControllerSwift: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        guard self.fetchedResultsController != nil else {
            return 1
        }
        
        return (self.fetchedResultsController.sections?.count)!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard self.fetchedResultsController != nil else {
            return 0
        }
        
        return self.fetchedResultsController.sections![section].numberOfObjects

    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! ReportTableViewCell
        
        if let image = self.userReport?.image {
            cell.thumbnail.image = image
            cell.thumbnail.layer.cornerRadius = 10
            cell.thumbnail.clipsToBounds = true
        }
        
        let duplicate = fetchedResultsController.objectAtIndexPath(indexPath) as! ReportDuplicate
//        print("Picture URL: ", duplicate.pictureURL)
        if let URL = NSURL(string: duplicate.pictureURL) {
            cell.thumbnail.hnk_setImageFromURL(URL)
        }
        
        cell.titleLabel.text = duplicate.title
        var distance = String(duplicate.distance.doubleValue * 1000)
        cell.distanceLabel.text = distance.stringByPaddingToLength(5, withString: "", startingAtIndex: 0) + "m"
        cell.statusLabel.text = duplicate.state
        
        cell.accessoryType = .DisclosureIndicator
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("duplicates_section_header", comment: "")
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let detailSubscriptionViewController = DuplicateDetailViewController(nibName: "DetailViewController", bundle: NSBundle.mainBundle())
        
        detailSubscriptionViewController.report = fetchedResultsController.objectAtIndexPath(indexPath) as! ServerReport
        
        self.navigationController?.pushViewController(detailSubscriptionViewController, animated: true)
    }
    
}

extension DialogDuplicateReportsViewControllerSwift: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.reloadData()
    }
}
