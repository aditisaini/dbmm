//
//  ReportsJSONAdapter.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 23.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ReportsJSONAdapter.h 72 2010-07-14 13:21:40Z eik $
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#import "AbstractJSONAdapter.h"

@class Domain;

@interface ReportsJSONAdapter : AbstractJSONAdapter
{
@private
#if !__OBJC2__
    Domain *_domain;

    CLLocationCoordinate2D coordinate;

#endif
    BOOL _cumulative;
}

- (id)initWithDomain:(Domain *)domain;
- (void)queryWithLocation:(CLLocation *)location cumulative:(BOOL)cumulative delegate:(id)delegate selector:(SEL)selector;

@end
