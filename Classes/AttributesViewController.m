//
//  DialogAttributesViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 10.01.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import "AttributesViewController.h"
#import "ReportOverviewController.h"
#import "TypeAttribute.h"
#import "DetailCell.h"

#import "MMAlertViewHandler.h"
#import "MMColor.h"
#import "MMTableView.h"

#define kMyID           @"myID"
#define kTextType       @"text"
#define kNumberType     @"number"
#define kSelectType     @"select"
#define kCheckBoxType   @"checkbox"

#define kTextIdentifier     @"TextIdentifier"
#define kNumberIdentifier   @"NumberIdentifier"
#define kSelectIdentifier   @"SelectIdentifier"
#define kCheckBoxIdentifier @"CeckIdentifier"

#define MAX_HEADER_HEIGHT   70.0
#define TITLE_INSET         12.0

@interface AttributesViewController ()
{
    AttributeSelectableValuesTableViewController *selectableValuesViewController;
}

@property (nonatomic, strong) NSArray *answers;
@property (nonatomic, strong) NSArray *titleViews;

@end

@implementation AttributesViewController

@synthesize basisPresentingViewController=_basisPresentingViewController;
@synthesize managedObjectContext=_managedObjectContext;
@synthesize userReport=_userReport;
@synthesize reportType=_reportType;

@synthesize answers=_answers;
@synthesize titleViews=_titleViews;
@synthesize detailCell=_detailCell;
@synthesize visibleAttributes=_visibleAttributes;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;
    
    self.reportType = self.userReport.type2;
    [self.detailCell setDetailText:self.userReport.details];
    
    self.visibleAttributes = [self visibleAttributesFromAttributes:[_reportType.attributes array]];
    
    [self setUpViewElements];
}

- (void)updateTitleViewArrayWithAttributes:(NSArray *)attributes
{
    self.titleViews = [self loadTitleViewArrayWithAttributes:attributes];
}

- (NSArray *)loadTitleViewArrayWithAttributes:(NSArray *)attributes
{
    NSMutableArray *mutableTitleViewArray = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < attributes.count; i++)
    {
        UIView *view = [self headerViewForAttributeAtIndex:i];
        [mutableTitleViewArray addObject:view];
    }
    
    return [NSArray arrayWithArray:mutableTitleViewArray];
}

- (void)setUpViewElements
{
    self.titleViews = [self loadTitleViewArrayWithAttributes:self.visibleAttributes];
    
    self.tableView = [[MMTableView alloc] initWithFrame:self.tableView.frame style:UITableViewStyleGrouped];
    self.tableView.tableHeaderView = self.headerView;
    self.navigationItem.leftBarButtonItem = nil;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"save_button_title", nil)
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(saveButtonClicked:)];
    self.headerTitleLabel.text  = NSLocalizedString(@"attribute_header", nil);
    self.headerTextView.text    = NSLocalizedString(@"attribute_step_description", nil);
    
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        self.headerTextView.selectable  = NO;
    }
    
    self.rightBarButton = self.navigationItem.rightBarButtonItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setToolbarHidden:(self.toolbarItems == nil)
                                       animated:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    [self.userReport.managedObjectContext save:NULL];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.visibleAttributes.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Detail cell index?
    if ((indexPath.section == 0 && ![self.userReport.type2.hasSubjectTitle boolValue])
    ||  (indexPath.section == 1 &&  [self.userReport.type2.hasSubjectTitle boolValue]))
    {
            return self.detailCell.bounds.size.height;
    }
    
    return 55.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [self tableView:tableView viewForHeaderInSection:section];
    return headerView.frame.size.height;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[_visibleAttributes objectAtIndex:section] help];
}

- (NSString *)subtitleForHeaderInSection:(NSInteger)section
{
    TypeAttribute *attribute = [_visibleAttributes objectAtIndex:section];
    
    NSString *subtitle = @"";
    NSString *public   = NSLocalizedString(@"public", "Public attribute");
    NSString *required = NSLocalizedString(@"required", "Required attribute");
    
    BOOL isPublic   = [attribute publicBool];
    BOOL isRequired = [attribute requiredBool];

    // Since we only regard visible attributes, the pure existance of an attribute with requiredIfCode set
    // proves that this attribute is required.
    if (!isRequired && (attribute.requiredIfCode.length > 0)) {
        isRequired = YES;
    }
    
    if ((isRequired && isPublic)) {
        subtitle = [NSString stringWithFormat:@"(%@, %@)", public, required];
    } else if (isPublic) {
        subtitle = [NSString stringWithFormat:@"(%@)", public];
    } else if (isRequired) {
        subtitle = [NSString stringWithFormat:@"(%@)", required];
    }
    
    return subtitle;
}

- (UIView *)headerViewForAttributeAtIndex:(NSUInteger)index
{
    UITextView *titleView    = [self titleTextViewForAttributeAtIndex:index];
    UITextView *subTitleView = [self subtitleTextViewUnderTitleView:titleView forAttributeAtIndex:index];
    
    CGRect subTitleFrame = CGRectMake(subTitleView.frame.origin.x,
                                      titleView.frame.origin.y + titleView.frame.size.height - TITLE_INSET,
                                      subTitleView.frame.size.width, subTitleView.frame.size.height);
    subTitleView.frame = subTitleFrame;
    
    // Calculate overall height (mind that the subtitle view has an inset into the title view)
    float overallHeight = subTitleFrame.origin.y + subTitleView.frame.size.height;
    overallHeight += (subTitleView.text.length == 0) ? TITLE_INSET : 0;
    
    // Create super container view
    CGRect superFrame = CGRectMake(0, 0, titleView.frame.size.width, overallHeight);
    UIView *superContainer = [[UIView alloc] initWithFrame:superFrame];
    
    // Add title and subtitle view to container
    [superContainer addSubview:titleView];
    [superContainer addSubview:subTitleView];
    
    return superContainer;
}

- (UITextView *)titleTextViewForAttributeAtIndex:(NSUInteger)index
{
    // Create textview with section title and subtitle
    CGRect titleViewFrame = CGRectMake(10, 0, 300, MAX_HEADER_HEIGHT);
    UIView *titleContainerView = [[UIView alloc] initWithFrame:titleViewFrame];
    UITextView *titleView = [[UITextView alloc] initWithFrame:titleViewFrame];
    [titleContainerView addSubview:titleView];
    
    [self configureTextView:titleView forHeaderInSection:index];
    
    [titleView sizeToFit];
    
    return titleView;
}

- (UITextView *)subtitleTextViewUnderTitleView:(UITextView *)titleView forAttributeAtIndex:(NSUInteger)index
{
    CGRect subTitleViewFrame = CGRectMake(titleView.frame.origin.x,
                                          titleView.frame.origin.y + titleView.frame.size.height, // - 2*TITLE_INSET,
                                          self.tableView.frame.size.width - 20.0,
                                          MAX_HEADER_HEIGHT);
    UIView *subTitleContainerView = [[UIView alloc] initWithFrame:subTitleViewFrame];
    UITextView *subTitleView = [[UITextView alloc] initWithFrame:subTitleViewFrame];
    [subTitleContainerView addSubview:subTitleView];
    
    [self configureSubtitleTextView:subTitleView forHeaderInSection:index];
    
    if (subTitleView.text.length > 0) {
        [subTitleView sizeToFit];
    }
    
    return subTitleView;
}

- (void)configureTextView:(UITextView *)textView forHeaderInSection:(NSInteger)section
{

    NSString *htmlTitle = [self tableView:self.tableView titleForHeaderInSection:section];

    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        textView.text = htmlTitle;
    } else {
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[htmlTitle dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        textView.attributedText = attributedString;
        textView.selectable = YES;
    }

    textView.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
    textView.textColor       = [MMColor attributeCellHeaderTitleColor];
    textView.backgroundColor = [UIColor clearColor];
    textView.scrollEnabled   = NO;
    textView.editable        = NO;
    textView.userInteractionEnabled = YES;
    textView.dataDetectorTypes = UIDataDetectorTypeLink;
    
    [self adjustHeightOfTextView:textView];
}

- (void)configureSubtitleTextView:(UITextView *)textView forHeaderInSection:(NSInteger)section
{
	textView.text = [self subtitleForHeaderInSection:section];
    textView.font = [UIFont boldSystemFontOfSize:12];;
    textView.textColor       = [MMColor attributeCellHeaderSubtitleColor];
    textView.backgroundColor = [UIColor clearColor];
    textView.scrollEnabled   = NO;
    textView.editable        = NO;
    textView.userInteractionEnabled = NO;
    
    [self adjustHeightOfTextView:textView];
}

- (void)adjustHeightOfTextView:(UITextView *)textView
{
    // Adapt containerView height to number of lines in textView
    CGFloat newHeight = 0;
    
    // Provide visible frame only if there is text to be displayed
    if (textView.text.length > 0) {
        newHeight = textView.contentSize.height;
    }
    
    textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y,
                                textView.frame.size.width, newHeight);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [_titleViews objectAtIndex:section];
}

- (CheckBoxAttributeCell *)checkableAttributeCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TypeAttribute *attribute    = [self.visibleAttributes objectAtIndex:indexPath.section];
    CheckBoxAttributeCell *cell = [[CheckBoxAttributeCell alloc] initWithAttribute:attribute
                                                                   reuseIdentifier:kCheckBoxIdentifier];
    
    return cell;
}

- (UITableViewCell *)selectableAttributeCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TypeAttribute *attribute = [self.visibleAttributes objectAtIndex:indexPath.section];
    UITableViewCell *cell    = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                      reuseIdentifier:kSelectIdentifier];
    cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    
    [self setSelectableAttributeCell:cell toAttribute:attribute];
    
    return cell;
}

- (void)setSelectableAttributeCell:(UITableViewCell *)cell toAttribute:(TypeAttribute *)attribute
{
    // Check whether user has already entered text/ whether attribute has cached value
    if ([attribute.answer length] > 0) {
        cell.textLabel.textColor = [UIColor blackColor];
        // Value id is stored in answer
        cell.textLabel.text = [attribute textForValueID:attribute.answer];
    } else {
        cell.textLabel.textColor = [UIColor colorWithWhite:0.7 alpha:1.0];
        cell.textLabel.text = attribute.name;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    // Attribute cell
    TypeAttribute *attribute = [self.visibleAttributes objectAtIndex:indexPath.section];
    
    if ([attribute.type isEqualToString:kSelectType]) {
        CellIdentifier = kSelectIdentifier;
    } else if ([attribute.type isEqualToString:kCheckBoxType]) {
        CellIdentifier = kCheckBoxIdentifier;
    } else if ([attribute.type isEqualToString:kTextType]) {
        CellIdentifier = kTextIdentifier;
    } else {
        CellIdentifier = kNumberIdentifier;
    }
    
    
    UITableViewCell *cell;
    // Detail cell section?
    if ((indexPath.section == 0 && ![self.userReport.type2.hasSubjectTitle boolValue])
    ||  (indexPath.section == 1 &&  [self.userReport.type2.hasSubjectTitle boolValue]))
    {
        self.detailCell.delegate = self;
        self.detailCell.textView.font = [UIFont systemFontOfSize:16];
        cell = self.detailCell;
    }
    // Title subject or attribute section?
    else {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            if ([attribute.type isEqualToString:kSelectType]) {
                cell = [self selectableAttributeCellForRowAtIndexPath:indexPath];
            } else if ([attribute.type isEqualToString:kCheckBoxType]) {
                cell = [[CheckBoxAttributeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            } else if ([attribute.type isEqualToString:kTextType]) {
                cell = [[TextAttributeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                [(TextAttributeCell *)cell setDelegate:self];
            } else if ([attribute.type isEqualToString:kNumberType]) {
                cell = [[TextAttributeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                [(TextAttributeCell *)cell setDelegate:self];
            }
        }
        
        if ([attribute.type isEqualToString:kSelectType]) {
            [self setSelectableAttributeCell:cell toAttribute:attribute];
        } else if ([attribute.type isEqualToString:kCheckBoxType]) {
            [(CheckBoxAttributeCell*)cell setToAttribute:attribute];
        } else if ([attribute.type isEqualToString:kTextType]) {
            [(TextAttributeCell*)cell setToAttribute:attribute provideNumericKeyboard:NO];
        } else if ([attribute.type isEqualToString:kNumberType]) {
            [(TextAttributeCell*)cell setToAttribute:attribute provideNumericKeyboard:YES];
        }
    }
    
    // Cell configuration
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    // Set unique cell identifier (= ID of attribute)
    cell.tag = [attribute.myID integerValue];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TypeAttribute *currentAttribute = (TypeAttribute*)[_visibleAttributes objectAtIndex:indexPath.section];
    
    // If cell owning attribute is of type select, display selectable values
    if ([currentAttribute.type isEqualToString:kSelectType]) {
        
        AttributeSelectableValuesTableViewController *selectableAttributesVC = [[[NSBundle mainBundle] loadNibNamed:@"AttributeValuesViewController"
                                                                                                              owner:self
                                                                                                            options:nil]
                                                                                objectAtIndex:0];
        
        selectableAttributesVC.delegate     = self;
        selectableAttributesVC.attribute    = currentAttribute;
        selectableAttributesVC.types        = currentAttribute.selectableValues;
        selectableAttributesVC.title        = currentAttribute.name;
        [self.navigationController pushViewController:selectableAttributesVC
                                             animated:YES];
        
    }
    else if ([currentAttribute.type isEqualToString:kCheckBoxType])
    {
        CheckBoxAttributeCell *cell =  (CheckBoxAttributeCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        (cell.isSelected ? [cell setIsSelected:NO] : [cell setIsSelected:YES]);
        currentAttribute.answer = [NSString stringWithFormat:@"%d", cell.isSelected];
    }
}

#pragma mark -
#pragma mark keyboard notifications

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"done_button_title", nil)
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                            action:@selector(doneButtonClicked:)];
}

// Called when the UIKeyboardDidHideNotification is sent
- (void)keyboardWillHide:(NSNotification *)aNotification
{
    self.navigationItem.rightBarButtonItem = self.rightBarButton;
}


#pragma mark - Input fetching

// Important: The visibleAttributesArray also contains dummy-attributes for the subject title and the description text
- (NSArray *)visibleAttributesFromAttributes:(NSArray *)attributes
{
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.userReport.type2.visibleAttributes];
    
    // Add dummy attribute for description cell
    TypeAttribute *descriptionAttribute = [self dummyTypeAttributeWithHelp:NSLocalizedString(@"description_prompt", nil)
                                                                      name:NSLocalizedString(@"description_cell_prompt", nil)
                                                                     error:NSLocalizedString(@"description_prompt", nil)
                                                                   dummyID:[NSNumber numberWithInt:-2]];
    // If a description already exists, store description as attribute answer
    if (self.userReport.details) {
        descriptionAttribute.answer = self.userReport.details;
    }
    [tempArray insertObject:descriptionAttribute atIndex:0];
    
    // Add a dummy attribute for the subject title cell if required
    if ([self.userReport.type2.hasSubjectTitle boolValue]) {
        TypeAttribute *subjectTitleAttribute = [self dummyTypeAttributeWithHelp:NSLocalizedString(@"subject_prompt", nil)
                                                                           name:NSLocalizedString(@"subject_cell_prompt", nil)
                                                                          error:NSLocalizedString(@"subject_prompt", nil)
                                                                        dummyID:[NSNumber numberWithInt:-1]];
        // If a subject title already exists, store description as attribute answer
        if (self.userReport.subjectTitle) {
            subjectTitleAttribute.answer = self.userReport.subjectTitle;
        }
        [tempArray insertObject:subjectTitleAttribute atIndex:0];
    }

    return [tempArray copy];
}

- (void)updateVisibleAttributesFromAttributes:(NSArray *)attributes
{
    self.visibleAttributes = [self visibleAttributesFromAttributes:attributes];
    [self updateTitleViewArrayWithAttributes:self.visibleAttributes];
}

- (TypeAttribute *)dummyTypeAttributeWithHelp:(NSString *)help name:(NSString *)name error:(NSString *)error dummyID:(NSNumber *)dummyID
{
    TypeAttribute *attribute = [NSEntityDescription insertNewObjectForEntityForName:@"TypeAttribute"
                                                             inManagedObjectContext:_managedObjectContext];
    
    attribute.isPublic = @YES;
    attribute.required = @YES;
    attribute.cached   = @YES;
    attribute.visible  = @YES;
    
    attribute.type     = @"text";
    attribute.help     = help;
    attribute.name     = name;
    attribute.error    = error;
    attribute.myID     = dummyID;
    
    return attribute;
}


#pragma mark - Action handling

- (IBAction)doneButtonClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)saveButtonClicked:(id)sender
{
    BOOL answersComplete = YES;
    
    for (TypeAttribute *attribute in _visibleAttributes)
    {
        if (![attribute isFulfilledInContextOfAttributes:_visibleAttributes])
        {
            [[MMAlertViewHandler sharedInstance] showMissingAnswerOnAttributeAlert:attribute.error];
            answersComplete = NO;
            break;
        }
    }
    
    // Store answer of subject title and description dummy-attribute and delete attributes afterwards (is stored at index 0)
    if ([self.userReport.type2.hasSubjectTitle boolValue]) {
        self.userReport.subjectTitle = [[self.visibleAttributes objectAtIndex:0] answer];
        self.userReport.details      = [[self.visibleAttributes objectAtIndex:1] answer];
    } else {
        self.userReport.details      = [[self.visibleAttributes objectAtIndex:0] answer];
    }
    
    if (answersComplete) {
        if (_basisPresentingViewController) {
            [self.navigationController popToViewController:_basisPresentingViewController animated:YES];
        } else {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

# pragma mark - Detail Cell Delegate
- (void)detailCellTextIsSetOnValue:(NSString *)value
{
    TypeAttribute *attribute = [_visibleAttributes objectAtIndex:0];
    attribute.answer = value;
    
    self.userReport.details = attribute.answer;
    [self updateVisibleAttributesFromAttributes:[self.userReport.type2.attributes array]];
}

# pragma mark - Text Attribute Delegate
- (void)textAttribute:(TypeAttribute *)attribute isSetOnValue:(NSString *)value
{
    if ([attribute.myID integerValue] == -1) {
        self.userReport.subjectTitle = attribute.answer;
    }
    
    [self updateVisibleAttributesFromAttributes:[self.userReport.type2.attributes array]];
}

# pragma mark - Selection Attribute Delegate
- (void)selectableAttribute:(TypeAttribute *)attribute isSetOnValue:(NSString *)value
{
    UITableViewCell *cell = (TextAttributeCell *)[self.tableView viewWithTag:[attribute.myID integerValue]];
    cell.textLabel.text = value;
    cell.textLabel.textColor = [UIColor blackColor];
    
    [self updateVisibleAttributesFromAttributes:[self.userReport.type2.attributes array]];
    [self.tableView reloadData];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.visibleAttributes = nil;
    self.answers = nil;
}

@end
