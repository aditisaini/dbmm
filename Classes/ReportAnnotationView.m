//
//  ReportAnnotationView.m
//  WerDenktWas
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 10.07.10.
//  Copyright (c) Oliver Eikemeier 2010. All rights reserved.
//
//  $Id: ReportAnnotationView.m 74 2010-07-14 22:00:16Z eik $
//

#import "ReportAnnotationView.h"
#import "DraggableAnnotationView.h"
#import "DraggableAnnotationView3.h"

#import "Constants.h"

@implementation ReportAnnotationView

- (id)initWithAnnotation:(id <ReportAnnotationProtocol>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    if ((self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier])) {
        self.centerOffset = CGPointMake(kCenterOffsetX, kCenterOffsetY);
        if (annotation) {
            self.image = annotation.image;
            if ([annotation respondsToSelector:@selector(addObserver:forKeyPath:options:context:)]) {
                // super already calls self.annotation = annotation;
            }
        }
    }

    return self;
}

- (void)dealloc
{
    self.annotation = nil;

}

#pragma mark -
#pragma mark API

+ (ReportAnnotationView *)reportAnnotationViewForMapView:(MKMapView *)mapView withAnnotation:(id <ReportAnnotationProtocol>)annotation movable:(BOOL)movable
{
    NSString *reuseIdentifier;
    Class annotationClass;

    if (movable) {
#ifdef __IPHONE_4_0
        if ([ReportAnnotationView instancesRespondToSelector:@selector(setDraggable:)]) {
            annotationClass = DraggableAnnotationView.class;
            reuseIdentifier = @"DraggableAnnotationView";
        }
        else
#endif
        {
            annotationClass = DraggableAnnotationView3.class;
            reuseIdentifier = @"DraggableAnnotationView3";
        }
    }
    else {
        annotationClass = ReportAnnotationView.class;
        reuseIdentifier = @"ReportAnnotationView";
    }

    ReportAnnotationView *annotationView =
    (ReportAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIdentifier];
    if (annotationView) {
        annotationView.annotation = annotation;
    }
    else {
        // if an existing annotation view was not available, create one
        annotationView = [(ReportAnnotationView*)[annotationClass alloc] initWithAnnotation:annotation
                                                                             reuseIdentifier:reuseIdentifier];
        [annotationView setMapView:mapView];
    }

    return annotationView;
}

- (void)setAnnotation:(id <ReportAnnotationProtocol>)newAnnotation
{
    if (self.annotation != newAnnotation) {
        if (self.annotation) {
            [(id)self.annotation removeObserver:self forKeyPath:@"image"];
        }

        if (newAnnotation) {
            [(id)newAnnotation addObserver:self forKeyPath:@"image" options:0 context:NULL];
            self.image = newAnnotation.image;
        }
    }

    [super setAnnotation:newAnnotation];
}

- (void)enableCallout
{
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    self.rightCalloutAccessoryView = rightButton;
    self.canShowCallout = YES;
}

- (void)setMapView:(MKMapView *)mapView
{
}

#pragma mark -
#pragma mark NSKeyValueObserver

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == self.annotation) {
        id <ReportAnnotationProtocol> reportAnnotation = object;
        if ([keyPath isEqualToString:@"image"]) {
            self.image = reportAnnotation.image;
        }
    }
}

@end
