//
//  DraggableAnnotation3.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 08.07.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: DraggableAnnotationView3.m 69 2010-07-13 21:46:14Z eik $
//

#import "DraggableAnnotationView3.h"

#import "DraggableAnnotationView.h"


@implementation DraggableAnnotationView3

- (id)initWithAnnotation:(id <ReportAnnotationProtocol>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    if ((self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier])) {
        isUp = NO;
        isMoving = NO;

        self.multipleTouchEnabled = NO;
    }
    return self;
}


#pragma mark -
#pragma mark UIResponder overrides

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // The view is configured for single touches only.
    UITouch *aTouch = [touches anyObject];
    startLocation = [aTouch locationInView:[self superview]];
    originalCenter = self.center;

    isMoving = NO;

    if (!isUp) {
        [self animateAnnotationLift];
        isUp = YES;
    }

    self.canShowCallout = NO;
    [super touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *aTouch = [touches anyObject];
    CGPoint newLocation = [aTouch locationInView:[self superview]];
    CGPoint newCenter;

    // If the user's finger moved more than 5 pixels, begin the drag.
    if (abs(newLocation.x - startLocation.x) > 5.0 || abs(newLocation.y - startLocation.y) > 5.0)
        isMoving = YES;

    // If dragging has begun, adjust the position of the view.
    if (isUp && isMoving) {
        newCenter.x = originalCenter.x + (newLocation.x - startLocation.x);
        newCenter.y = originalCenter.y + (newLocation.y - startLocation.y);
        self.center = newCenter;
    }
    else {
       // Let the parent class handle it.
        [super touchesMoved:touches withEvent:event];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (isMoving) {
        if (isUp) {
            [self animateAnnotationBounceAndUpdate];
            isUp = NO;
        }

        // Clean up the state information.
        startLocation = CGPointZero;
        originalCenter = CGPointZero;
        isMoving = NO;
    }
    else {
        if (isUp) {
            [self animateAnnotationDrop];
            isUp = NO;
        }
    }

    [super touchesEnded:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (isUp) {
        [self animateAnnotationCancel];
        isUp = NO;
    }

    if (isMoving) {
        // Move the view back to its starting point.
        self.center = originalCenter;

        // Clean up the state information.
        startLocation = CGPointZero;
        originalCenter = CGPointZero;
        isMoving = NO;
    }

    [super touchesCancelled:touches withEvent:event];
}

@end
