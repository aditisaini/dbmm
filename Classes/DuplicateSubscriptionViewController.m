//
//  DuplicateSubscriptionViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 30.04.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import "DuplicateSubscriptionViewController.h"
#import "MMAlertViewHandler.h"
#import "EmailAdapter.h"
#import "ReportDuplicate.h"
#import "UserReport.h"

@interface DuplicateSubscriptionViewController ()

@end

@implementation DuplicateSubscriptionViewController

- (void)emailDone
{
    NSInteger result = self.emailAdapter.result;
    [[MMAlertViewHandler sharedInstance] cancelActivityIndicatingAlert];
    
    if (result != 1) {
        
        [[MMAlertViewHandler sharedInstance] showEmailSubscriptionFailureAlert];
        self.navigationItem.rightBarButtonItem.enabled = true;
    }
    else {
        [[[[[UIApplication sharedApplication] delegate] window] rootViewController] dismissViewControllerAnimated:YES completion:^(){
            [[MMAlertViewHandler sharedInstance] showAlertWithMessage:NSLocalizedString(@"subscription_success_msg", nil)
                                                             andTitle:NSLocalizedString(@"subscription_success_title", nil)];
        }];
        
        [[(ReportDuplicate *)self.report userReport] flushDuplicates];
    }
}

@end
