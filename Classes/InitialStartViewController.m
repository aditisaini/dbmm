//
//  InitialStartViewController.m
//  WerDenktWas
//
//  Created by Franziska Engelmann on 05.11.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import "InitialStartViewController.h"
#import "AGBViewController.h"
#import "MMColor.h"

@interface InitialStartViewController ()

@end

@implementation InitialStartViewController
@synthesize startButton;
@synthesize initialView=_initialView;
@synthesize textView=_textView;
@synthesize delegate=_delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;

    // Stop navigationbar from overlapping (under iOS7)
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
	// Do any additional setup after loading the view.
    self.initialView = [[[NSBundle mainBundle] loadNibNamed:@"InitialStartView"
                                                      owner:self options:nil]
                        objectAtIndex:0];
    NSString *introductionText = NSLocalizedStringWithDefaultValue(@"intro_message", nil,
                                                                   [NSBundle mainBundle],
                                                                   @"Schlaglöcher, Graffitis, gestapelte Müllbeutel am Straßenrand... \n \nDagegen kann man jetzt etwas tun - Mit dieser App kannst du die Mängel melden, wir leiten sie an deine Kommune weiter!", @"About Maengelmelder");
    [self setTextViewToText:introductionText];
    self.textView.textColor          = [MMColor initialTextViewColor];
    self.startButton.titleLabel.text = NSLocalizedStringWithDefaultValue(@"intro_button_label",
                                                                         nil,
                                                                         [NSBundle mainBundle],
                                                                         @"Jetzt loslegen!",
                                                                         @"Button label");
    self.startButton.showsTouchWhenHighlighted = NO;
    self.startButton.titleLabel.textColor = [MMColor uiButtonTitleColor];
}

- (void)setTextViewToText:(NSString *)text
{
    if (_textView.text != text) {
        _textView.text = text;
    }
}

- (void)viewDidUnload
{
    [self setStartButton:nil];
    _initialView = nil;
    [self setTextView:nil];
    [super viewDidUnload];
}

- (IBAction)clickStart:(id)sender
{
    [self presentModalAGBViewController];
}

- (void)presentModalAGBViewController
{
    AGBViewController *agbViewController = [[AGBViewController alloc] init];
    agbViewController.delegate = self.delegate;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:agbViewController];
    
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
