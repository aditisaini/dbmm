//
//  DuplicateReportsViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 06.03.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MMSupport.h"
#import "PictureAdapter.h"
#import "ReportsJSONAdapter.h"
#import "DetailJSONAdapter.h"

#import "DuplicateReportsViewController.h"
#import "DuplicateDetailViewController.h"
#import "LongTextWithSubtitleCell.h"
#import "ReportDuplicate.h"
#import "ReportPicture.h"
#import "UserReport.h"

#import "MMColor.h"
#import "MMTableView.h"

@interface DuplicateReportsViewController ()
{
    NSArray *pictureAdapters;
}
@end

@implementation DuplicateReportsViewController

@synthesize managedObjectContext;

@synthesize duplicates;
@synthesize userReport=_userReport;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;

    self.tableView = [[MMTableView alloc] initWithFrame:self.tableView.frame style:self.tableView.style];
    
    NSString *cancel = NSLocalizedStringWithDefaultValue(@"cancel_button_title", nil,
                                                         [NSBundle mainBundle],
                                                         @"Cancel", @"");
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:cancel
                                                               style:UIBarButtonItemStyleBordered
                                                              target:self
                                                              action:@selector(cancelButtonClicked:)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    // Custom colors for navigation- and toolbar under iOS 7
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        self.navigationController.navigationBar.barTintColor = [MMColor orangeColor];
    } else {
        self.navigationController.navigationBar.tintColor    = [MMColor orangeColor];
    }
    
    fetchedResultsController.delegate = self;

    NSError *error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        // TODO: handle the error appropriately
        LogError(@"Details fetch", error);
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationController.toolbarHidden = NO;

    fetchedResultsController.delegate = self;
    NSError *error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        LogError(@"Details fetch", error);
    }
    
    NSMutableArray *tempAdapters = [[NSMutableArray alloc] init];
    
    for (int i=0; i < (int)[self tableView:self.tableView numberOfRowsInSection:0]; i++)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        ReportDuplicate *report = [fetchedResultsController objectAtIndexPath:indexPath];
        PictureAdapter *adapter = [[PictureAdapter alloc] init];
        
        if (!report.picture && [report.pictureURL isKindOfClass:[NSString class]] && report.pictureURL.length > 0) {
            [adapter queryReport:report delegate:self selector:@selector(reportPictureReceived)];
        }
        [tempAdapters addObject:adapter];
    }
    
    pictureAdapters = [[NSArray alloc] initWithArray:tempAdapters];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    for (__strong PictureAdapter *adapter in pictureAdapters) {
        [adapter cancel];
        adapter = nil;
    }
    
    fetchedResultsController.delegate = nil;
}


- (void)reportPictureReceived
{
    NSError *error;
    [self.managedObjectContext save:&error];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    fetchedResultsController.delegate = nil;
    fetchedResultsController = nil;

    [_pictureAdapter cancel];
    _pictureAdapter = nil;
    
    self.userReport = nil;
}

- (void)dealloc
{
    fetchedResultsController.delegate = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark -
#pragma mark Core Data

- (NSFetchedResultsController *)fetchedResultsController
{
    if (!fetchedResultsController) {
        // Create and configure a fetch request with the ReportDetail entity.
        NSDictionary *variables = [NSDictionary dictionaryWithObject:_userReport forKey:@"USERREPORT"];
        NSFetchRequest *fetchRequest = [_userReport.entity.managedObjectModel fetchRequestFromTemplateWithName:@"duplicatesForUserReport" substitutionVariables:variables];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
        fetchRequest.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        // Create and initialize the fetch results controller.
        fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                       managedObjectContext:self.managedObjectContext
                                                                         sectionNameKeyPath:nil
                                                                                  cacheName:nil];
        fetchedResultsController.delegate = self;
    }
    
    return fetchedResultsController;
}

#pragma mark -
#pragma mark NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:NO];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    ReportDuplicate *duplicate = [fetchedResultsController objectAtIndexPath:indexPath];
            
    NSString *state = duplicate.state;
    int distance    = [duplicate.distance floatValue]*1000;
    
    cell.textLabel.text       = duplicate.title;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%im\n%@", distance, state];
    
    if (duplicate.picture.image) {
        cell.imageView.image = [MMSupport makeThumbnailOfImage:duplicate.picture.image andSize:CGSizeMake(80, 80)];
    } else {
        cell.imageView.image = nil;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger count = 0;
    NSArray *sections = fetchedResultsController.sections;
    if (sections.count) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
        count = [sectionInfo numberOfObjects];
    }
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120.0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title = NSLocalizedStringWithDefaultValue(@"duplicates_message", nil,
                                                        [NSBundle mainBundle],
                                                        @"Similar reports have been found in your vicinity:",
                                                        @"");
    
    return title;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.numberOfLines        = 3;
    cell.detailTextLabel.numberOfLines  = 2;
    
    cell.imageView.layer.cornerRadius   = 10.0;
    cell.imageView.layer.borderWidth    = 0.0;
    cell.imageView.clipsToBounds        = YES;
    cell.imageView.contentMode          = UIViewContentModeScaleToFill;

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // get details mit der jeweiligen message id --> Server report generieren
    DuplicateDetailViewController *detailSubscriptionViewController = [[DuplicateDetailViewController alloc] initWithNibName:@"DetailViewController" bundle:[NSBundle mainBundle]];
    
    detailSubscriptionViewController.report = [fetchedResultsController objectAtIndexPath:indexPath];
    
    [self.navigationController pushViewController:detailSubscriptionViewController animated:YES];
}

- (IBAction)cancelButtonClicked:(id)sender
{  
    for (ReportDuplicate *duplicate in [fetchedResultsController fetchedObjects]) {
        [fetchedResultsController.managedObjectContext deleteObject:duplicate];
    }
    NSError *error;
    [fetchedResultsController.managedObjectContext save:&error];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
