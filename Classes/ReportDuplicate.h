//
//  ReportDuplicate.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 19.03.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ServerReport.h"

@class UserReport;

@interface ReportDuplicate : ServerReport

@property (nonatomic, strong) NSNumber * distance;
@property (nonatomic, strong) UserReport *userReport;

- (void)loadReportPictureForDelegate:(id)aDelegate andSelector:(SEL)aSelector;

@end
