//
//  PolylineView.h
//  AppJobber
//
//  Created by Oliver Eikemeier on 27.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface PolylineView : MKPolylineView

@end
