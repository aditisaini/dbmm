//
//  EmailAdapter.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 28.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: EmailAdapter.m 62 2010-07-12 18:06:31Z eik $
//

#import "EmailAdapter.h"
#import "ReportAnnotation.h"
#import "Domain.h"

#import "Constants.h"

#define kResultProperty         @"result"

@implementation EmailAdapter

@synthesize result;

- (id)initWithServer:(NSString *)server messageID:(NSInteger)messageID andEmail:(NSString *)email delegate:(id)aDelegate selector:(SEL)aSelector
{
    self = [super init];
    if (self != nil) {
        connection = [[ServerConnection alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [connection cancel];
    connection = nil;

}

- (void)cancel
{
    [connection cancel];

    delegate = nil;
}

#pragma mark -
#pragma mark API

- (void)subscribeToReport:(ReportAnnotation *)report withEmail:(NSString *)email delegate:(id)aDelegate selector:(SEL)aSelector
{
    delegate = aDelegate;
    selector = aSelector;
	
	NSString *localization = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];

    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSString stringWithFormat:@"%d", [report.messageID intValue]], @"id",
                                email, @"email",
//								[[UIDevice currentDevice]uniqueIdentifier], @"phone",
                                [[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"], @"phone",
								[[NSUserDefaults standardUserDefaults] stringForKey:kAppIDKey], @"appid",
								localization,@"lang",
                                nil];

    connection = [[ServerConnection alloc] init];

    [connection queryWithServer:report.domain.server
                         method:@"subscribe_message"
                     parameters:parameters
                       delegate:self];
}


#pragma mark -
#pragma mark ServerConnectionDelegate

- (void)serverConnectionDidFinishLoading:(ServerConnection *)serverConnection
{
    NSDictionary *resultList = [serverConnection jsonData];

    if ([resultList isKindOfClass:NSDictionary.class]) {
        result = [[resultList objectForKey:kResultProperty] integerValue];
#if LOG_LEVEL > 1
        NSLog(@"Subscribe result: %d", result);
#endif
    }
#if LOG_LEVEL > 0
    else {
        NSLog(@"Could not understand subscribe result");
    }
#endif

    [delegate performSelector:selector];
    delegate = nil;
}

@end
