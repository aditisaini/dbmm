//
//  ReportDetailsCell.h
//  WerDenktWas
//
//  Created by Franziska Engelmann on 22.10.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportDetailsCell : UITableViewCell
{    
    UITextView *_contentTextView;
}

@property (strong, nonatomic) IBOutlet UIView *subview;
@property (strong, nonatomic) IBOutlet UITextView *headerTextView;
@property (strong, nonatomic) IBOutlet UILabel *headerLabel;
@property (strong, nonatomic) IBOutlet UITextView *contentTextView;

+ (float)inset;

-(CGFloat)overallHeight;
- (void)setDetailsText:(NSString *)text;
- (void)setHeaderText:(NSString *)text;

@end
