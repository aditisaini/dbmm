//
//  ReportOverviewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 31.01.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import "ReportOverviewController.h"

#import "ReportDetailsCell.h"
#import "PhotoViewController.h"
#import "PositionViewController.h"
#import "TypeSelectionViewController.h"
#import "AttributesViewController.h"
#import "FinalViewController.h"

#import "ReportType.h"

#import "Constants.h"
#import "MMAlertViewHandler.h"
#import "MMColor.h"
#import "MMCommentCompletion.h"
#import "MMTableView.h"
#import "GeoCoordinateJSONAdapter.h"

@interface ReportOverviewController ()
{
    NSInteger rowIndexForLocation;

    BOOL geocoderFailed;
    GeoCoordinateJSONAdapter *geoCoordinateJSONAdapter;
}

@property (nonatomic, strong) NSArray *visibleAttributes;

@end

@implementation ReportOverviewController

@synthesize managedObjectContext=_managedObjectContext;
@synthesize visibleAttributes=_visibleAttributes;
@synthesize headerView=_headerView;
@synthesize userReport=_userReport;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView = [[MMTableView alloc] initWithFrame:self.tableView.frame style:self.tableView.style];

    [_headerView setRoundCorners];
    _headerView.report =_userReport;
    
    self.tableView.tableHeaderView = _headerView;
    
    // If presented modally, add cancel button
    if (self.navigationController.viewControllers.count == 1) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"cancel_button_title", nil)
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(cancelButtonClicked:)];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!_userReport.title) {
        _userReport.title = self.navigationItem.title;
    }
    if (!_userReport.address) {
        [self startGeocoder];
    }
    
    self.visibleAttributes = [self visibleAttributesFromAttributes:[self.userReport.type2.attributes array]];
    rowIndexForLocation = ([_userReport.type2.attributes count] > 0 ? 3 : 2);
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.title = NSLocalizedString(@"overview", nil);
    
    [self setUpToolbar];
    
    [self.tableView reloadData];
}


- (void)setUpToolbar
{
    // Segemented control for map view type
    UIBarButtonItem *deleteButton   = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(trashButtonClicked:)];
    UIBarButtonItem *leftSpace      = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *rightSpace     = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *centerButton   = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"send_button_title", nil) style:UIBarButtonItemStylePlain target:self action:@selector(sendButtonClicked:)];
    
    self.toolbarItems = [NSArray arrayWithObjects:deleteButton, leftSpace, centerButton, rightSpace, nil];
    self.navigationController.toolbarHidden = NO;
    [self.navigationController setToolbarItems:self.toolbarItems];

}


#pragma mark - Input fetching

// Important: The visibleAttributesArray also contains the dummy-attributes for the subject title and the description text
- (NSArray *)visibleAttributesFromAttributes:(NSArray *)attributes
{
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.userReport.type2.visibleAttributes];
    
    // Add dummy attribute for description cell
    TypeAttribute *descriptionAttribute = [self dummyTypeAttributeWithHelp:NSLocalizedString(@"description_prompt", nil)
                                                                      name:NSLocalizedString(@"description", nil)
                                                                     error:NSLocalizedString(@"description_prompt", nil)
                                                                   dummyID:[NSNumber numberWithInt:-2]];
    // If a description already exists, store description as attribute answer
    if (self.userReport.details) {
        descriptionAttribute.answer = self.userReport.details;
    }
    [tempArray insertObject:descriptionAttribute atIndex:0];
    
    // Add a dummy attribute for the subject title cell if required
    if ([self.userReport.type2.hasSubjectTitle boolValue]) {
        TypeAttribute *subjectTitleAttribute = [self dummyTypeAttributeWithHelp:NSLocalizedString(@"subject_prompt", nil)
                                                                           name:NSLocalizedString(@"subject_cell_prompt", nil)
                                                                          error:NSLocalizedString(@"subject_prompt", nil)
                                                                        dummyID:[NSNumber numberWithInt:-1]];
        // If a subject title already exists, store description as attribute answer
        if (self.userReport.subjectTitle) {
            subjectTitleAttribute.answer = self.userReport.subjectTitle;
        }
        [tempArray insertObject:subjectTitleAttribute atIndex:0];
    }
    
    return [tempArray copy];
}


#pragma mark - Property and observer handling

- (UserReport *)userReport
{
    return _userReport;
}

- (void)setUserReport:(UserReport *)userReport
{
    if (_userReport != userReport) {
        [_userReport removeObserver:self forKeyPath:@"address"];
        [_userReport removeObserver:self forKeyPath:@"type2"];
        [_userReport removeObserver:self forKeyPath:@"details"];
        
        _userReport = userReport;
        
        [_userReport addObserver:self forKeyPath:@"address" options:0 context:NULL];
        [_userReport addObserver:self forKeyPath:@"type2" options:0 context:NULL];
        [_userReport addObserver:self forKeyPath:@"details" options:0 context:NULL];
        
        _headerView.report = _userReport;
    }
}

#pragma mark -
#pragma mark MKReverseGeocoderDelegate

- (void)startGeocoder
{
    CLLocation *location = [[CLLocation alloc] initWithLatitude:_userReport.coordinate.latitude longitude:_userReport.coordinate.longitude];
    
    if (geoCoordinateJSONAdapter) {
        [geoCoordinateJSONAdapter cancel];
    } else {
        geoCoordinateJSONAdapter = [[GeoCoordinateJSONAdapter alloc] initWithManagedObjectContext:_managedObjectContext];
    }
    
    [geoCoordinateJSONAdapter queryServer:[self server] withLocation:location delegate:self selector:@selector(updateStreetAddress)];
}

- (void)updateStreetAddress
{
    if (geoCoordinateJSONAdapter.street_address){
        [self.userReport setSubtitle:[NSString stringWithFormat:@"%@", geoCoordinateJSONAdapter.street_address]];
        geocoderFailed = NO;
    } else {
        geocoderFailed = YES;
    }
    [self.tableView reloadData];
}

- (NSString *)server
{
    return [NSString stringWithFormat:kServerFormat, [[NSUserDefaults standardUserDefaults] stringForKey:kServerHostKey]];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        // Add 1 for the "editable" field at row 0
        return self.visibleAttributes.count + 1;
    }
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return NSLocalizedString(@"your_report", nil);
    }
    return NSLocalizedString(@"details", nil);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        // Section 0 and row 0: Report type
        if (indexPath.row == 0) {
            return [self heightForRowWithTitle:NSLocalizedString(@"report_label_type", nil)
                                       andText:_userReport.type2.name];
        }
        // Section 0 and row 1: Location
        else {
            if (_userReport.address) {
                return [self heightForRowWithTitle:NSLocalizedString(@"report_label_adress", nil)
                                           andText:_userReport.address];
                
            } else {
                return [self heightForRowWithTitle:NSLocalizedString(@"determining_report_address", nil)
                                           andText:_userReport.address];
            }
        }
    }
    else if (indexPath.section == 1) {
        // Row 0: Edit option
        // Else : (Dummy) Atttributes
        if (indexPath.row > 0) {
            TypeAttribute *attribute = [self.visibleAttributes objectAtIndex:indexPath.row-1];
            return [self heightForRowWithTitle:attribute.name andText:attribute.answer];
        }
    }
    
    return UITableViewAutomaticDimension;
}

- (CGFloat)heightForRowWithTitle:(NSString *)title andText:(NSString *)text
{
    NSString *content = (text.length > 0) ? text : NSLocalizedString(@"no_details_provided", nil);
    NSInteger width   = self.tableView.frame.size.width - 50;
    
    CGSize sizeForContent = [content sizeWithFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]
                                constrainedToSize:CGSizeMake(width, 999)
                                    lineBreakMode:UILineBreakModeWordWrap];
    CGSize sizeForTitle    = [title sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:14]
                               constrainedToSize:CGSizeMake(width, 999)
                                   lineBreakMode:UILineBreakModeWordWrap];
    
    return sizeForContent.height + sizeForTitle.height + [ReportDetailsCell inset];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"Cell";

    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                cellIdentifier = @"TypeCell";
                break;
            case 1:
                cellIdentifier = @"LocationCell";
                break;
        }
    } else if (indexPath.section == 1) {
        switch (indexPath.row) {
            case 0:
                cellIdentifier = @"ChangeDetailsCell";
                break;
            default:
                cellIdentifier = @"AttributeCell";
                break;
        }
    }
    
    ReportDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[ReportDetailsCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellIdentifier];
    }
  
    if (indexPath.section == 0) {
        cell.tag = indexPath.row;
        cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.contentTextView.editable = NO;

        switch (indexPath.row) {
            case 0:
                return [self typeCellFromCell:cell];
            case 1:
                return [self locationCellFromCell:cell];
            case 2:
                return [self descriptionCellFromCell:cell];
        }
    }
    else if (indexPath.section == 1) {
        // Row 0: Edit details/attributes option
        // Else : (Dummy) Atttributes
        if (indexPath.row > 0) {
            TypeAttribute *attribute = [self.visibleAttributes objectAtIndex:indexPath.row-1];
            return [self attributeCellForAttribute:attribute fromCell:cell];
        }
    }
    
    UITableViewCell *normalCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NormalCell"];
    normalCell.accessoryType    = UITableViewCellAccessoryDisclosureIndicator;
    normalCell.textLabel.text   = NSLocalizedString(@"change_details", nil);
    normalCell.textLabel.font   = [UIFont boldSystemFontOfSize:14.0];
    normalCell.textLabel.textColor = [MMColor iOSSevenSystemBlue];
    
    return normalCell;
}

- (IBAction)typeCellContentTextViewSelected:(UITapGestureRecognizer *)sender
{
    [self selectType:self];
}

- (IBAction)locationCellContentTextViewSelected:(UITapGestureRecognizer *)sender
{
    [self changeLocation:self];
}

- (ReportDetailsCell *)typeCellFromCell:(ReportDetailsCell *)cell
{
    ReportDetailsCell *tempCell = cell;
    
    [tempCell setHeaderText:NSLocalizedStringWithDefaultValue(@"report_label_type", nil,
                                                             [NSBundle mainBundle], @"Art",
                                                             @"Type label in report view")];
    
    if (_userReport.type2) {
        [tempCell setDetailsText:_userReport.type2.name];
        tempCell.contentTextView.textColor = [MMColor blackColor];
    } else {
        [tempCell setDetailsText:NSLocalizedString(@"select_report_type", nil)];
        tempCell.contentTextView.textColor = [MMColor redColor];
    }
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(typeCellContentTextViewSelected:)];
    singleTap.numberOfTapsRequired = 1;
    [cell.contentTextView addGestureRecognizer:singleTap];
   
    tempCell.headerTextView.font = [UIFont boldSystemFontOfSize:14];
    tempCell.headerTextView.textColor = [MMColor reportDetailsHeaderColor];
    
    return tempCell;
}

- (ReportDetailsCell *)locationCellFromCell:(ReportDetailsCell *)cell
{
    ReportDetailsCell *tempCell = cell;
    
    [tempCell setHeaderText:NSLocalizedStringWithDefaultValue(@"report_label_adress", nil,
                                                             [NSBundle mainBundle], @"Ort",
                                                              @"Place label in report view")];
    
    if (_userReport.address) {
        [tempCell setDetailsText:_userReport.address];
        tempCell.contentTextView.textColor = [UIColor blackColor];
    }
    else if (geocoderFailed) {
        [tempCell setDetailsText:NSLocalizedStringWithDefaultValue(@"report_adress_failed", nil,
                                                                   [NSBundle mainBundle],
                                                                   @"unbekannt", @"Failed to determine place text in report view")];
        tempCell.contentTextView.textColor = [UIColor darkGrayColor];
    }
    else {
        [tempCell setDetailsText:NSLocalizedStringWithDefaultValue(@"determining_report_address", nil,
                                                                   [NSBundle mainBundle],
                                                                   @"wird ermittelt…", @"Waiting to determine place text in report view")];
        tempCell.contentTextView.textColor = [UIColor darkGrayColor];
    }
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(locationCellContentTextViewSelected:)];
    singleTap.numberOfTapsRequired = 1;
    [cell.contentTextView addGestureRecognizer:singleTap];

    tempCell.headerTextView.font = [UIFont boldSystemFontOfSize:14];
    tempCell.headerTextView.textColor = [MMColor reportDetailsHeaderColor];
    
    return tempCell;
}

- (ReportDetailsCell *)descriptionCellFromCell:(ReportDetailsCell *)cell
{
    ReportDetailsCell *tempCell = cell;
    
    [tempCell setHeaderText:NSLocalizedString(@"description", nil)];
    tempCell.headerTextView.textColor = [MMColor reportDetailsHeaderColor];
    
    if (_userReport.details.length > 0) {
        [tempCell setDetailsText:_userReport.details];
        tempCell.contentTextView.textColor = [MMColor blackColor];
    } else {
        [tempCell setDetailsText:NSLocalizedString(@"enter_description", nil)];
        tempCell.contentTextView.textColor = [MMColor redColor];
    }

    tempCell.accessoryType  = UITableViewCellAccessoryNone;
    tempCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return tempCell;
}

- (TypeAttribute *)dummyTypeAttributeWithHelp:(NSString *)help name:(NSString *)name error:(NSString *)error dummyID:(NSNumber *)dummyID
{
    TypeAttribute *attribute = [NSEntityDescription insertNewObjectForEntityForName:@"TypeAttribute"
                                                             inManagedObjectContext:_managedObjectContext];
    
    attribute.isPublic = [NSNumber numberWithBool:YES];
    attribute.required = [NSNumber numberWithBool:YES];
    attribute.cached   = [NSNumber numberWithBool:YES];
    
    attribute.type     = @"text";
    attribute.help     = help;
    attribute.name     = name;
    attribute.error    = error;
    attribute.myID     = dummyID;
    
    return attribute;
}

- (ReportDetailsCell *)attributeCellForAttribute:(TypeAttribute *)attribute fromCell:(ReportDetailsCell *)cell
{
    ReportDetailsCell *tempCell = cell;
    
    [tempCell setHeaderText:attribute.name];
    tempCell.headerTextView.textColor = [MMColor reportDetailsHeaderColor];

    if (attribute.answer && attribute.answer.length > 0) {
        NSString *detailText = attribute.answer;
        if ([attribute.type isEqualToString:@"select"]) {
            detailText = [attribute textForValueID:attribute.answer];
        } else if ([attribute.type isEqualToString:@"checkbox"]) {
            detailText = [attribute.answer isEqualToString:@"1"] ? NSLocalizedString(@"yes", nil) : NSLocalizedString(@"no", nil);
        }
        [tempCell setDetailsText:detailText];
        tempCell.contentTextView.textColor = [MMColor blackColor];
    } else {
        [tempCell setDetailsText:[NSString stringWithFormat:@"%@ (%@)", NSLocalizedString(@"no_details_provided", nil), NSLocalizedString(@"optional", nil)]];
        tempCell.contentTextView.textColor = [MMColor grayColor];
        
        if ([attribute.required boolValue]
            || (attribute.requiredIfCode.length > 0)) {
            [tempCell setDetailsText:NSLocalizedString(@"no_details_provided", nil)];
            tempCell.contentTextView.textColor = [MMColor redColor];
        }
    }
    
    tempCell.accessoryType  = UITableViewCellAccessoryNone;
    tempCell.selectionStyle = UITableViewCellSelectionStyleNone;

    return tempCell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                [self selectType:self];
                break;
            case 1:
                [self changeLocation:self];
                break;
            default:
                // Change description
                break;
        }
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        // Load attribute view
        [self editDetailInformation:self];
    }
}


#pragma mark - Action handling

- (IBAction)changeLocation:(id)sender
{
    PositionViewController *positionViewController = [[PositionViewController alloc] init];
    positionViewController.userReport = _userReport;
    positionViewController.managedObjectContext = _managedObjectContext;
    
    [self.navigationController pushViewController:positionViewController animated:YES];
}

- (IBAction)selectType:(id)sender
{
    TypeSelectionViewController *typeSelectionViewController = [[TypeSelectionViewController alloc] initWithNibName:@"TypeSelectionViewController" bundle:nil];

    typeSelectionViewController.annotation = _userReport;
    typeSelectionViewController.managedObjectContext = _managedObjectContext;
    typeSelectionViewController.basisPresentingViewController = self;
    
    [self.navigationController pushViewController:typeSelectionViewController animated:YES];
}

- (IBAction)editDetailInformation:(id)sender
{
    AttributesViewController *attributesViewController = [[AttributesViewController alloc] initWithNibName:@"AttributesViewController" bundle:nil];
    
    attributesViewController.reportType =_userReport.type2;
    attributesViewController.userReport =_userReport;
    attributesViewController.managedObjectContext = _managedObjectContext;
    attributesViewController.basisPresentingViewController = self;
    
    [self.navigationController pushViewController:attributesViewController animated:YES];
}

- (IBAction)imageButtonClicked:(id)sender
{
    PhotoViewController *photoViewController = [[PhotoViewController alloc] initWithNibName:@"PhotoViewController" bundle:nil];
    
    photoViewController.managedObjectContext = _managedObjectContext;
    photoViewController.userReport = _userReport;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:photoViewController];
    navigationController.navigationItem.rightBarButtonItem = nil;
    
    [self presentViewController:navigationController animated:YES completion:^{}];
}

- (IBAction)mapButtonClicked:(id)sender
{
    PositionViewController *positionViewController = [[PositionViewController alloc] initWithNibName:@"PositionViewController" bundle:nil];
    positionViewController.managedObjectContext = _managedObjectContext;
    positionViewController.userReport = _userReport;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:positionViewController];
    
    [self presentViewController:navigationController animated:YES completion:^{}];
}

- (IBAction)trashButtonClicked:(id)sender
{
    __weak ReportOverviewController *weakSelf = self;
    [[MMAlertViewHandler sharedInstance] showDeleteConfirmationAlertWithButtonClickBlock:^(NSInteger index) {
        if (index == 1) {
            [weakSelf deleteCurrentReport:YES];
            [weakSelf dismissViewControllerAnimated:YES completion:^(){}];
        }
    }];
}

- (IBAction)sendButtonClicked:(id)sender
{
    NSString *descriptionMissingError  = NSLocalizedString(@"beschreibungleer", "Please enter a description.");
    NSString *descriptionTooShortError = NSLocalizedString(@"description_short_msg", "Description is too short! Please write a more precise comment.");
    NSInteger minimumTextLength        = 4;
    
    if (! _userReport.picture)
    {
        // If no photo is set, increase minimum commentlength
        minimumTextLength        = 35;
        descriptionTooShortError = NSLocalizedString(@"description_short_no_photo_msg", nil);
	}
    
    if (! _userReport.type2)
    {
        [[MMAlertViewHandler sharedInstance] showHintAlertWithMessage:NSLocalizedString(@"missing_type_msg", nil)];
	}
    else if ([_userReport.type2.hasSubjectTitle boolValue] && !_userReport.subjectTitle)
    {
        [[MMAlertViewHandler sharedInstance] showHintAlertWithMessage:NSLocalizedString(@"subject_prompt", nil)];
	}
    else if (![self.userReport.type2 allRequiredAttributesSet])
    {
        [[MMAlertViewHandler sharedInstance] showHintAlertWithMessage:NSLocalizedString(@"details_missing_msg", nil)];
    }
    else if ([MMCommentCompletion checkForMinimumLength:minimumTextLength
                                                 ofText:_userReport.details
                                     withMissingMessage:descriptionMissingError
                                     andTooShortMessage:descriptionTooShortError])
    {
        __weak ReportOverviewController *weakSelf = self;
        [[MMAlertViewHandler sharedInstance] showSendConfirmationAlertWithButtonClickBlock:^(NSInteger index) {
            if (index == 1) {
                FinalViewController *finalViewController = [[FinalViewController alloc] init];
                finalViewController.userReport = _userReport;
                [weakSelf.navigationController pushViewController:finalViewController animated:YES];
            }
        }];
    }
}

- (IBAction)cancelButtonClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^(){}];
}

- (void)deleteCurrentReport:(BOOL)delete
{
    if (delete) {
        // Delet answers of non-cached attributes and userreport itself
        [_userReport.type2 flushAttributeAnswers];
        [_userReport.managedObjectContext deleteObject:_userReport];

        NSError *error;
        [self.managedObjectContext save:&error];
        self.userReport = nil;
    }
}


#pragma mark -
#pragma mark NSKeyValueObserver

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == _userReport) {
        if ([keyPath isEqualToString:@"address"]) {
            // Update address row (section 0, row 1)
            NSArray *indexPaths = [NSArray arrayWithObject:[self locationRowPath]];
            [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        }
        else {
            if ([keyPath isEqualToString:@"type2"]) {
                if ([_userReport.type2.attributes count] > 0) {
                    self.visibleAttributes = [self visibleAttributesFromAttributes:[self.userReport.type2.attributes array]];
                }
            }
            [self.tableView reloadData];
        }
    }
}

- (NSIndexPath *)typeRowPath
{
    return [NSIndexPath indexPathForRow:0 inSection:0];
}

- (NSIndexPath *)locationRowPath
{
    return [NSIndexPath indexPathForRow:1 inSection:0];
}


#pragma mark - Memory management

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    // Cancel pending processes
    [geoCoordinateJSONAdapter cancel];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [geoCoordinateJSONAdapter cancel];
    geoCoordinateJSONAdapter = nil;
    
    self.headerView = nil;
}

- (void)dealloc
{
    [_userReport removeObserver:self forKeyPath:@"address"];
    [_userReport removeObserver:self forKeyPath:@"type2"];
    [_userReport removeObserver:self forKeyPath:@"details"];
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    [geoCoordinateJSONAdapter cancel];
    geoCoordinateJSONAdapter = nil;
    
    self.headerView = nil;
}

@end
