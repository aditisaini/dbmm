//
//  POIDetailsTableViewController.m
//  WerDenktWas
//
//  Created by Martin on 11.05.15.
//  Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import "POIDetailsTableViewController.h"
#import "PointOfInterest.h"
#import "POIDetailJSONAdapter.h"
#import "ReportDetailsCell.h"
#import "POIDetails.h"
#import "DetailHeaderView.h"
#import "POIPictureAdapter.h"
#import "POIHeaderView.h"

@interface POIDetailsTableViewController ()

@property(strong, nonatomic) POIHeaderView *headerView;

@end

@implementation POIDetailsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self requestDetails];
    [self configureHeaderView];
}

- (void)requestDetails {
    POIDetailJSONAdapter *jsonAdapter = [[POIDetailJSONAdapter alloc] init];
    [jsonAdapter queryWithPOI:self.pointOfInterest
                     delegate:self selector:@selector(didReceiveDetails:)];
}

- (void)didReceiveDetails:(PointOfInterest *)details {
    self.pointOfInterest = details;
    [self.tableView reloadData];

    [self.headerView loadImageWithURL:self.pointOfInterest.pictureURL];
    [self.headerView setGeometryOnMap:self.pointOfInterest.geometry];
}

- (void)configureHeaderView {
    self.headerView = [POIHeaderView loadNib];

    self.tableView.tableHeaderView = self.headerView;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.pointOfInterest.messageDetails.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"detailCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[ReportDetailsCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellIdentifier];
    }

    [self configureCell:cell atIndexPath:indexPath];

    return cell;
}

- (void)configureCell:(ReportDetailsCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    if (self.pointOfInterest.messageDetails.count > indexPath.row) {
        POIDetails *poiDetails = self.pointOfInterest.messageDetails[indexPath.row];
        [cell setHeaderText:poiDetails.name];
        [cell setDetailsText:poiDetails.content];
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.pointOfInterest.messageDetails.count > indexPath.row) {
        POIDetails *poiDetails = self.pointOfInterest.messageDetails[indexPath.row];
        NSString *name = poiDetails.name;
        NSString *content = poiDetails.content;

        CGFloat width = self.tableView.frame.size.width - 35;

        CGSize sizeForContent = [content sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:14]
                                    constrainedToSize:CGSizeMake(width, 999)
                                        lineBreakMode:NSLineBreakByWordWrapping];
        CGSize sizeForName = [name sizeWithFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]
                              constrainedToSize:CGSizeMake(width, 999)
                                  lineBreakMode:NSLineBreakByWordWrapping];

        return sizeForContent.height + sizeForName.height + [ReportDetailsCell inset];
    }
    else {
        return 44;
    }


}

@end
