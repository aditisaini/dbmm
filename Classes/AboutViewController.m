//
//  AboutViewController.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 10.05.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: AboutViewController.m 66 2010-07-13 13:39:07Z eik $
//

#import "AboutViewController.h"

@implementation AboutViewController

@synthesize versionLabel;
@synthesize webView;
@synthesize backButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;
    
    // Stop navigationbar from overlapping (under iOS7)
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if (backButton) {
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.rightBarButtonItem = backButton;
    }
    
    self.navigationItem.title  = NSLocalizedString(@"about_view_title", nil);
    NSString *marketingVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *buildNumber      = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    versionLabel.text = [NSString stringWithFormat:NSLocalizedStringWithDefaultValue(@"version", nil, [NSBundle mainBundle], @"Unlocalized Version %@ (%@)", @"Version in info dialog"), marketingVersion, buildNumber];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"imprint" ofType:@"html"]isDirectory:NO]]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if (backButton) {
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.rightBarButtonItem = backButton;
        self.navigationItem.rightBarButtonItem.title = NSLocalizedString(@"back_button_title", nil);
    }
    
    [self.navigationController setToolbarHidden:(self.toolbarItems == nil) animated:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];

    self.versionLabel = nil;
    self.backButton = nil;
}

- (IBAction)backButtonClicked:(id)sender
{
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:YES];

    [self.navigationController popViewControllerAnimated:NO];

	[UIView commitAnimations];
}

#pragma UIWebView delegate

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }

    return YES;
}

@end
