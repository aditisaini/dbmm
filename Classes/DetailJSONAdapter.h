//
//  DetailJSONAdapter.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 22.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: DetailJSONAdapter.h 72 2010-07-14 13:21:40Z eik $
//

#import <Foundation/Foundation.h>

#import "AbstractJSONAdapter.h"

@class ServerReport;

@interface DetailJSONAdapter : AbstractJSONAdapter
{
@private
#if !__OBJC2__
    ServerReport *report;
#endif
}

@property (nonatomic, strong) ServerReport *report;

- (void)queryReport:(ServerReport *)report delegate:(id)delegate selector:(SEL)selector;

@end
