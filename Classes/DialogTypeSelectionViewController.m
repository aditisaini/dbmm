//
//  DialogTypeSelectionViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 31.01.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import "DialogTypeSelectionViewController.h"
#import "DialogDuplicateReportsViewController.h"
#import "DialogAttributesViewController.h"

#import "DuplicatesJSONAdapter.h"
#import "MMAlertViewHandler.h"

#import "Ma_ngelmelder-Swift.h"

@interface DialogTypeSelectionViewController ()

@end

@implementation DialogTypeSelectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"next_button_title", nil)
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(nextStepButtonClicked:)];
    
    self.descriptionView.hidden         = NO;
    self.descriptionTitleLabel.text     = NSLocalizedString(@"type_step_header", nil);
    self.descriptionTextView.text       = NSLocalizedString(@"type_step_description", nil);
    
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        self.descriptionTextView.selectable  = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[MMAlertViewHandler sharedInstance] cancelActivityIndicatingAlert];
}

- (IBAction)nextStepButtonClicked:(id)sender
{
    if (!self.selectedPath) {
        [[MMAlertViewHandler sharedInstance] showHintAlertWithMessage:NSLocalizedStringWithDefaultValue(@"missing_type_msg", nil, [NSBundle mainBundle],
                                                                                                        @"Please select a category before saving!",
                                                                                                        @"Please select a category before saving!")];
    }
    else {
        self.annotation.type2 = [self.fetchedResultsController objectAtIndexPath:self.selectedPath];
        self.initialPath = self.selectedPath;
        
        [[MMAlertViewHandler sharedInstance] showActivityIndicatingAlertWithMessage:NSLocalizedString(@"checking_duplicates", nil)
                                                                           andTitle:NSLocalizedString(@"please_wait", nil)];
        // Get selected type
        ReportType *reportType = [self.fetchedResultsController objectAtIndexPath:self.selectedPath];
        // If different type has been previously selected, flush non-cached answers
        if (![self.annotation.type2.myid isEqualToString:reportType.myid]) {
            self.annotation.type2 = reportType;
            [reportType flushAttributeAnswers];
        }
        
        // Check for possible report duplicates
        if (self.duplicatesAdapter) {
            [self.duplicatesAdapter cancel];
        } else {
            self.duplicatesAdapter = [[DuplicatesJSONAdapter alloc] initWithReport:(UserReport*)self.annotation];
        }
        [self.duplicatesAdapter queryDuplicatesForReport:(UserReport*)self.annotation delegate:self selector:@selector(duplicatesCheckFinishedWithResults:)];
    }
}

- (void)duplicatesCheckFinishedWithResults:(NSArray *)results
{
    [[MMAlertViewHandler sharedInstance] cancelActivityIndicatingAlert];
    
    UITableViewCell *newCell = [self.tableView cellForRowAtIndexPath:self.selectedPath];
    newCell.accessoryView = nil;
    newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    
    // Any Duplicates? Set next view controller to duplicates view controller
    if (results.count > 0)
    {
        //        [self presentDuplicatesViewController];
        [self presentDuplicatesViewControllerSwift];
    }
    // No Duplicates? Set next view controller to attributes view controller
    else {
        [self pushAttributesViewController];
    }
}

- (void)presentDuplicatesViewController
{
    DialogDuplicateReportsViewController *duplicateReportsViewController = [[DialogDuplicateReportsViewController alloc] initWithStyle:UITableViewStyleGrouped];
    duplicateReportsViewController.userReport = (UserReport*)self.annotation;
    duplicateReportsViewController.managedObjectContext = self.managedObjectContext;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:duplicateReportsViewController];
    
    [self presentViewController:navigationController animated:YES completion:^void {
        [self pushAttributesViewController];
    }];
}

- (void)pushAttributesViewController
{
    DialogAttributesViewController *attributesViewController = [[DialogAttributesViewController alloc] initWithNibName:@"AttributesViewController" bundle:[NSBundle mainBundle]];
    
    attributesViewController.reportType = self.annotation.type2;
    attributesViewController.userReport = (UserReport *)self.annotation;
    attributesViewController.managedObjectContext = self.managedObjectContext;
    
    [self.navigationController pushViewController:attributesViewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
