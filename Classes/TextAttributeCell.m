//
//  TextAttributeCell.m
//  WerDenktWas
//
//  Created by Franziska Engelmann on 25.09.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import "TextAttributeCell.h"

@implementation TextAttributeCell

@synthesize textField;
@synthesize attribute;
@synthesize delegate=_delegate;

- (id)initWithAttribute:(TypeAttribute *)theAttribute provideNumericKeyboard:(BOOL)numeric reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    if (self) {
        NSArray *viewArray = [[NSBundle mainBundle] loadNibNamed:@"TextFieldView"
                                                           owner:self
                                                         options:nil];
        [self addSubview:[viewArray objectAtIndex:0]];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.textField.delegate = self;
        if (numeric) {
            self.textField.keyboardType = UIKeyboardTypeNumberPad;
        }
        
        self.attribute = theAttribute;
        
        self.textField.placeholder = attribute.name;
        if ([attribute.answer length] > 0) {
            self.textField.text = attribute.answer;
        } else {
            self.textField.text = NULL;
        }
    }

    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        NSArray *viewArray = [[NSBundle mainBundle] loadNibNamed:@"TextFieldView"
                                                           owner:self
                                                         options:nil];
        [self addSubview:[viewArray objectAtIndex:0]];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.textField.delegate = self;
    }
    
    return self;
}

- (void)setToAttribute:(TypeAttribute *)theAttribute provideNumericKeyboard:(BOOL)numeric
{
    if (numeric) {
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
    }
    
    self.attribute = theAttribute;
    
    self.textField.placeholder = attribute.name;
    if ([attribute.answer length] > 0) {
        self.textField.text = attribute.answer;
    } else {
        self.textField.text = NULL;
    }
}


- (BOOL)hasText
{
    return (self.textField.text.length > 0);
}

#pragma mark -
#pragma mark API

- (void)setText:(NSString *)text
{
    textField.text = text;
}

- (void)setTextColor:(UIColor *)textColor
{
    textField.textColor = textColor;
}

- (NSString *)text
{
    if ([self hasText])
        return textField.text;
    else
        return nil;
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)theTextField
{
    textField.textColor = [UIColor blackColor];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)theTextField
{
    self.attribute.answer = theTextField.text;
    [self.delegate textAttribute:attribute isSetOnValue:theTextField.text];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField
{
    self.attribute.answer = theTextField.text;
    [theTextField resignFirstResponder];
    
    return YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)dealloc
{    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)doneButtonClicked:(id)sender
{
    //Write your code whatever you want to do on done button tap
    //Removing keyboard or something else
}

@end
