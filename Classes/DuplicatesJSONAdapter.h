//
//  DuplicatesJSONAdapter.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 05.03.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "AbstractJSONAdapter.h"
@class UserReport;

@interface DuplicatesJSONAdapter : AbstractJSONAdapter
{
@private
#if !__OBJC2__
UserReport *_report;
#endif
}

@property (nonatomic, strong) UserReport *report;

- (id)initWithReport:(UserReport *)aReport;
- (void)queryDuplicatesForReport:(UserReport *)aReport delegate:(id)aDelegate selector:(SEL)aSelector;

@end
