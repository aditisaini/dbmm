//
//  AbstractJSONAdapter.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 08.07.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: AbstractJSONAdapter.h 71 2010-07-14 01:33:53Z eik $
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ServerConnection.h"

@class Domain;

@interface AbstractJSONAdapter : NSObject <ServerConnectionDelegate>
{
@protected
    id __weak delegate;
    SEL selector;

    NSString *_parentKey;
    NSString *_entityName;
    NSString *_entityKey;
    NSString *_propertyKey;

    NSDictionary *_mapping;

@private
    ServerConnection *_connection;

    NSManagedObject *_parent;
    NSManagedObjectContext *_managedObjectContext;
}

- (void)updateParent:(NSManagedObject *)parent withArray:(NSArray *)list deletingOld:(BOOL)purge;
- (void)initializeObject:(id)object withProperties:(NSDictionary *)properties;
- (void)updateObject:(id)object withProperties:(NSDictionary *)properties;

- (void)queryWithServer:(NSString *)server method:(NSString *)method parameters:(NSDictionary *)parameters;
- (void)cancel;

@end
