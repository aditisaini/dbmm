//
//  CheckBoxAttributeCell.h
//  WerDenktWas
//
//  Created by Franziska Engelmann on 15.10.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TypeAttribute.h"

@interface CheckBoxAttributeCell : UITableViewCell
{
    BOOL _isSelected;
    
    TypeAttribute *attribute;
}

@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, strong) TypeAttribute *attribute;

- (id)initWithAttribute:(TypeAttribute *)theAttribute reuseIdentifier:(NSString *)reuseIdentifier;
- (void)setToAttribute:(TypeAttribute *)theAttribute;

@end
