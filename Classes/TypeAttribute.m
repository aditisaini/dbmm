//
//  TypeAttribute.m
//  WerDenktWas
//
//  Created by Franziska Engelmann on 04.10.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import "TypeAttribute.h"
#import "ReportType.h"

#define kErrorProperty      @"error"
#define kIDProperty         @"id"
#define kHelpProperty       @"help"
#define kTypeProperty       @"type"
#define kNameProperty       @"name"
#define kValuesProperty     @"values"
#define kCachedProperty     @"cached"
#define kPublicProperty     @"public"
#define kRequiredProperty   @"required"

#define kID                 @"id"
enum AttributeType {
    TEXT,
    NUMBER,
    SELECT,
    CHECKBOX
};

#define kText               @"text"

@implementation TypeAttribute

@dynamic cached;
@dynamic error;
@dynamic help;
@dynamic isPublic;
@dynamic required;
@dynamic visible;
@dynamic myID;
@dynamic name;
@dynamic code;
@dynamic type;
@dynamic selectableValues;
@dynamic selectedValueID;
@dynamic requiredIfCode;
@dynamic requiredIfValue;
@dynamic visibleIfCode;
@dynamic visibleIfValue;
@dynamic reportType;
@dynamic answer;

-(NSInteger)valuesArrayIndexForValueID:(NSString *)answer
{
    if (answer.length > 0 && [self.selectableValues count] > 0) {
        for (NSUInteger i = 0; i < [self.selectableValues count]; i++) {
            if ([[(NSDictionary *)[self.selectableValues objectAtIndex:i] objectForKey:kID] isEqualToString:answer]) {
                return i;
            }
        }
    }
    return -1;
}

// Searches the NSDictionary stored in values that contains the given id
// Returns the NSString-object that is stored for the "text"-key in this dictionary
- (NSString *)textForValueID:(NSString*)valueID
{
    for (NSDictionary *dict in self.selectableValues) {
        if ([[dict objectForKey:kID] isEqualToString:valueID]) {
            return [dict objectForKey:kText];
        }
    }
    return @"";
}

- (void)flushAnswer
{
    // Flush answer of an attribute if it should not be cached
    if (![self cachedBool]) {
        self.answer = NULL;
    }
}

// Checks if the requirements for the visibility of a conditioned attribute are fulfilled. If attribute does not depend
// on external requirements YES is returned on default
- (BOOL)isRequiredInContextOfAttributes:(NSArray *)attributes
{
    for (TypeAttribute *attribute in attributes) {
        // Get attribute with code = required_if_code of conditioned attribute
        if ((self.requiredIfCode.length > 0) && [attribute.code isEqualToString:self.requiredIfCode]) {
            // If it exists, compare this attribute's selected value ID to required_if_value of conditioned attribute
            if ([attribute.selectedValueID isEqualToString:self.requiredIfValue]) {
                // If equals, required requirements are fulfilled
                return YES;
            }
        }
    }
    return [self requiredBool];
}

// Checks if the requirements for the visibility of a conditioned attribute are fulfilled. If attribute does not depend
// on external requirements YES is returned on default
- (BOOL)isVisibleInContextOfAttributes:(NSArray *)attributes
{
    if (self.visibleIfCode.length == 0 || !self.visibleIfCode)
    {
        // If a (conditioned) attribute is required/its required-requirements are filfilled it is automatically visible
        return YES; //[self isRequiredInContextOfAttributes:attributes];
    }
    
    for (TypeAttribute *attribute in attributes) {
        // Get attribute with code = value_if_code of conditioned attribute
        if ((self.visibleIfCode.length > 0) && [attribute.code isEqualToString:self.visibleIfCode]) {
            // If it exists, compare this attribute's selected value ID to visible_if_value of conditioned attribute
            if ([attribute.selectedValueID isEqualToString:self.visibleIfValue]) {
                // If equals, visibility requirements are fulfilled
                return YES;
            }
        }
    }
    return NO;
}

- (BOOL)isFulfilledInContextOfAttributes:(NSArray *)attributes {
    if([self.type isEqualToString:@"checkbox"]){
        NSLog(self.answer);
        return !([self requiredBool] || [self isRequiredInContextOfAttributes:attributes])
            || [self.answer isEqualToString:@"1"];
    }
    return !([self requiredBool] || [self isRequiredInContextOfAttributes:attributes]) || self.answer > 0;
}

- (void)setVisibleInContextOfAttributes:(NSArray *)attributes
{
    self.visible = @([self isVisibleInContextOfAttributes:attributes]);
}

- (void)setRequiredInContextOfAttributes:(NSArray *)attributes
{
    self.required = @([self isRequiredInContextOfAttributes:attributes]);
}

#pragma mark - Scalar getters and setters for NSNumber objects

- (BOOL)cachedBool
{
    return ([self.cached integerValue] == 0 ? NO : YES);
}

- (void)setCachedToBool:(BOOL)cached
{
    self.cached = [NSNumber numberWithBool:cached];
}

- (BOOL)publicBool
{
    return ([self.isPublic integerValue] == 0 ? NO : YES);
}

- (void)setPublicToBool:(BOOL)cached
{
    self.isPublic = [NSNumber numberWithBool:cached];
}

- (BOOL)requiredBool
{
    return ([self.required integerValue] == 0 ? NO : YES);
}

- (BOOL)visibleBool
{
    return ([self.required integerValue] == 0 ? NO : YES);
}

- (void)setRequiredToBool:(BOOL)cached
{
    self.required = [NSNumber numberWithBool:cached];
}

@end
