//
//  DraggableAnnotation3.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 08.07.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: DraggableAnnotationView3.h 72 2010-07-14 13:21:40Z eik $
//

#import "DraggableAnnotationView.h"

@interface DraggableAnnotationView3 : DraggableAnnotationView
{
@private
    BOOL isMoving;
    BOOL isUp;
    CGPoint startLocation;
    CGPoint originalCenter;
}

- (id)initWithAnnotation:(id <ReportAnnotationProtocol>)annotation reuseIdentifier:(NSString *)reuseIdentifier;

@end
