//
//  ReportDuplicateReportsViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 10.01.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import "DialogDuplicateReportsViewController.h"
#import "MMColor.h"

@interface DialogDuplicateReportsViewController ()

@end

@implementation DialogDuplicateReportsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setToolbarHidden:NO animated:NO];

    self.tableView.tableHeaderView = self.headerView;
    
    self.navigationItem.title = NSLocalizedString(@"duplicates_header", nil);
    self.navigationItem.leftBarButtonItem = nil;

    // Setup toolbar
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"duplicates_create_new", nil)
                                                               style:UIBarButtonItemStylePlain
                                                              target:self
                                                              action:@selector(nextStepButtonClicked:)];
    self.navigationController.toolbarHidden = NO;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"marker-new-mini.png"]];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:imageView];
    button.width = 270;//self.navigationController.toolbar.frame.size.width - imageView.frame.size.width;
    self.toolbarItems = [NSArray arrayWithObjects:barButtonItem, button, nil];
    
    self.headerView.hidden = NO;
    self.headerTitleLabel.text     = NSLocalizedString(@"duplicates_step_header", nil);
    self.headerTextView.text       = NSLocalizedString(@"duplicates_step_description", nil);

    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        self.headerTextView.selectable  = NO;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return NSLocalizedString(@"duplicates_section_header", nil);
    }
    return nil;
}


# pragma mark - Action handling

- (IBAction)nextStepButtonClicked:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
