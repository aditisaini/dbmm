//
//  UpdatePicture.h
//  WerDenktWas
//
//  Created by Franziska Engelmann on 19.11.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ReportPicture.h"

@class UserUpdate;

@interface UpdatePicture : Picture

@property (nonatomic, retain) UserUpdate *userUpdate;

@end
