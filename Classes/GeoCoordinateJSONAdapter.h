//
//  GeoCoordinateJSONAdapter.h
//  project 'WerDenktWas'
//
//
//  $Id: DomainJSONAdapter.h 72 2010-07-14 13:21:40Z eik $
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <CoreData/CoreData.h>

#import "AbstractJSONAdapter.h"

//@class Domain;

@interface GeoCoordinateJSONAdapter : AbstractJSONAdapter
{
@private
#if !__OBJC2__
//    Domain *domain;
    NSString *street_address;
#endif

    NSManagedObjectContext *managedObjectContext;

    CLLocationCoordinate2D coordinate;
    NSString *server;

}

@property (nonatomic, copy) NSString *street_address;

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void)queryServer:(NSString *)server withLocation:(CLLocation *)location delegate:(id)aDelegate selector:(SEL)aSelector;
//- (void)updateStreetaddress:(NSString *)streetaddress; 



@end
