//
//  POIHeaderView.m
//  WerDenktWas
//
//  Created by Martin on 11.05.15.
//  Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "POIHeaderView.h"
#import "POIPictureAdapter.h"
#import "WKTObject.h"
#import "PolylineView.h"
#import "Constants.h"

@implementation POIHeaderView

+ (POIHeaderView *)loadNib {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"POIHeaderView" owner:nil options:nil];
    POIHeaderView *view = [nib objectAtIndex:0];
    [view setRoundCorners];

    return view;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)loadImageWithURL:(NSString *)pictureURL {
    if ([pictureURL isKindOfClass:NSString.class] && pictureURL.length > 0) {
        POIPictureAdapter *pictureAdapter = [[POIPictureAdapter alloc] init];
        [pictureAdapter queryWithURL:pictureURL
                            delegate:self
                            selector:@selector(didReceivePhoto:)];
        [self.activityIndicator startAnimating];
        self.imageView.hidden = YES;
    }
    else {
        [self extendMapViewToWholeWidth];
    }

}

- (void)extendMapViewToWholeWidth {
    [UIView beginAnimations:@"PhotoVisibility" context:NULL];
    [UIView setAnimationDuration:0.5];
    self.mapView.frame = CGRectUnion(self.imageView.frame, self.mapView.frame);
    self.imageView.hidden = YES;
    [UIView commitAnimations];

    [self.activityIndicator stopAnimating];
}

- (void)didReceivePhoto:(UIImage *)picture {
    self.imageView.image = picture;
    self.imageView.hidden = NO;
    [self.activityIndicator stopAnimating];
}

- (void)setRoundCorners {
    NSArray *array = @[self.imageView, self.mapView];

    for (UIView *aView in array) {
        CALayer *layer = aView.layer;

        layer.masksToBounds = YES;
        layer.cornerRadius = 10.0;
        layer.borderColor = [UIColor grayColor].CGColor;
        layer.borderWidth = 1.0;
    }
}

- (void)setGeometryOnMap:(id <WKTObject>)geometry {
    [self.mapView removeOverlays:[self.mapView overlays]];
    [self.mapView addOverlays:geometry.overlays];

    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView addAnnotations:geometry.waypoints];

    [self adjustMap];
}

- (void)adjustMap {
    NSArray *annotations = [self.mapView annotations];
    NSArray *overlays = [self.mapView overlays];

    MKMapRect bound = MKMapRectNull;

    for (id <MKAnnotation> waypoint in annotations) {
        MKMapPoint point = MKMapPointForCoordinate(waypoint.coordinate);
        MKMapRect pointRect = MKMapRectMake(point.x, point.y, 0., 0.);
        if (MKMapRectIsNull(bound)) {
            bound = pointRect;
        } else {
            bound = MKMapRectUnion(bound, pointRect);
        }
    }

    for (id <MKOverlay> overlay in overlays) {
        if (MKMapRectIsNull(bound)) {
            bound = [overlay boundingMapRect];
        } else {
            bound = MKMapRectUnion(bound, [overlay boundingMapRect]);
        }
    }

    if (MKMapRectIsNull(bound)) {
        //frankfurt?
        bound = MKMapRectMake(140694269., 90913481., 3000., 1200.);
    }
    else if (MKMapRectIsEmpty(bound)) {
        bound = MKMapRectInset(bound, -1500., -600.);
    }

    self.mapView.visibleMapRect = bound;
}

- (void)setWaypoints:(NSArray *)waypoints {
    NSMutableIndexSet *newAnnotations = [NSMutableIndexSet indexSetWithIndexesInRange:NSMakeRange(0, waypoints.count)];

    NSArray *annotations = [self.mapView annotations];
    NSIndexSet *oldAnnotations = [annotations indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:MKUserLocation.class]) {
            return NO;
        }

        NSUInteger index = [waypoints indexOfObjectIdenticalTo:obj];
        if (index != NSNotFound) {
            [newAnnotations removeIndex:index];
            return NO;
        }

        return YES;
    }];

    [self.mapView removeAnnotations:[annotations objectsAtIndexes:oldAnnotations]];
    [self.mapView addAnnotations:[waypoints objectsAtIndexes:newAnnotations]];
}

#pragma mark - MKMapViewDelegate

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
    MKOverlayPathView *overlayView = nil;

    if ([overlay isKindOfClass:MKPolyline.class]) {
        overlayView = [[PolylineView alloc] initWithOverlay:overlay];
        overlayView.strokeColor = [UIColor colorWithRed:1. green:0. blue:0. alpha:.6];
    }
    else if ([overlay isKindOfClass:MKPolygon.class]) {
        overlayView = [[MKPolygonView alloc] initWithOverlay:overlay];
        overlayView.strokeColor = [UIColor colorWithRed:1. green:0. blue:0. alpha:.6];
        overlayView.lineWidth = 1.f;
    }

    return overlayView;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    static NSString *const annotationIdentifier = @"Maengelmelder POI";

    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
    annotationView.canShowCallout = NO;
    annotationView.image = [UIImage imageNamed:[NSString stringWithFormat:@"marker-white-%@.png", @(14)]];
    annotationView.centerOffset = CGPointMake(kCenterOffsetX, kCenterOffsetY);

    return annotationView;
}

@end
