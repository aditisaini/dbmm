//
//  PictureViewController.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 23.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: PictureViewController.h 72 2010-07-14 13:21:40Z eik $
//

#import <UIKit/UIKit.h>

@interface PictureViewController : UIViewController <UIScrollViewDelegate>
{
@private
#if !__OBJC2__
    UIImage *image;
#endif

    UIScrollView *imageScrollView;

    UIBarStyle savedNavigationBarStyle;
    UIStatusBarStyle savedStatusBarStyle;
}

@property (nonatomic, strong) UIImage *image;

@end
