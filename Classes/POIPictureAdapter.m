//
// Created by Martin on 11.05.15.
// Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import "POIPictureAdapter.h"
#import "PointOfInterest.h"


@implementation POIPictureAdapter

- (id)init {
    self = [super init];
    if (self != nil) {
        connection = [[ServerConnection alloc] init];
    }
    return self;
}

- (void)queryWithURL:(NSString *)pictureURL delegate:(id)aDelegate selector:(SEL)aSelector {
    delegate = aDelegate;
    selector = aSelector;

    [connection startWithURL:pictureURL delegate:self];
}

- (void)dealloc {
    [connection cancel];
    connection = nil;
}

- (void)cancel {
    [connection cancel];

    delegate = nil;
}

#pragma mark ServerConnectionDelegate

- (void)serverConnectionDidFinishLoading:(ServerConnection *)serverConnection {
    NSRange range = [serverConnection.mimeType rangeOfString:@"image/"
                                                     options:NSAnchoredSearch | NSCaseInsensitiveSearch];
    UIImage *picture = nil;
    if (range.location != NSNotFound) {

        if (serverConnection.receivedData) {
            picture = [[UIImage alloc] initWithData:serverConnection.receivedData];
        }
        else {
#if LOG_LEVEL > 0
            NSLog(@"Could not interpret picture data");
#endif
        }

    }

    if ([delegate respondsToSelector:selector]) {
        [delegate performSelector:selector withObject:picture];
    }
    delegate = nil;
}

@end