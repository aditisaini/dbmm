//
//  UserUpdate.h
//  WerDenktWas
//
//  Created by Franziska Engelmann on 19.11.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class UpdatePicture;
@class ServerReport;

@interface UserUpdate : NSManagedObject

@property (nonatomic, retain) NSNumber * messageID;
@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSString * solved;
@property (nonatomic, retain) NSString * pictureURL;
@property (nonatomic, retain) ServerReport *serverReport;
@property (nonatomic, retain) UpdatePicture *updatePicture;
@property (nonatomic, retain) UIImage *updatedImage;

@end
