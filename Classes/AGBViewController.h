//
//  AGBViewController.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 12.08.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AGBViewProtocol <NSObject>

- (void)agbHasBeenAccepted;
- (void)agbHasBeenDeclined;

@end

@interface AGBViewController : UIViewController {
    id <AGBViewProtocol> _delegate;
}

@property (retain, nonatomic) id delegate;
@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (retain, nonatomic) IBOutlet UIButton *agreeButton;
@property (retain, nonatomic) IBOutlet UIButton *disagreeButton;
@property (retain, nonatomic) IBOutlet UIToolbar *toolbar;

- (IBAction)agreeButtonClicked:(id)sender;
- (IBAction)disagreeButtonClicked:(id)sender;

@end
