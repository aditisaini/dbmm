//
//  DuplicateReportsViewController.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 06.03.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>


@class PictureAdapter;
@class UserReport;

@interface DuplicateReportsViewController : UITableViewController <NSFetchedResultsControllerDelegate>
{
    NSArray *_duplicates;
    NSArray *pictures;
    UserReport *_userReport;
    PictureAdapter *_pictureAdapter;
    
    NSFetchedResultsController *fetchedResultsController;
}

- (void)reportPictureReceived;

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) NSArray *duplicates;
@property (nonatomic, strong) UserReport *userReport;
@property (nonatomic, strong) PictureAdapter *pictureAdapter;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *messageID;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResults;

@end
