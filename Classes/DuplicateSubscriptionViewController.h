//
//  DuplicateSubscriptionViewController.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 30.04.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import "SubscriptionViewController.h"

@interface DuplicateSubscriptionViewController : SubscriptionViewController

@end
