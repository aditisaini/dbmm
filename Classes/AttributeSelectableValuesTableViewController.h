//
//  AttributeSelectableValuesTableViewController.h
//  WerDenktWas
//
//  Created by Franziska Engelmann on 01.10.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TypeAttribute.h"

@protocol SelectionAttributeDelegate <NSObject>

//- (void)selectionCell:(NSInteger)tag isSetOnValue:(NSString *)value;
- (void)selectableAttribute:(TypeAttribute*)attribute isSetOnValue:(NSString *)value;

@end

@interface AttributeSelectableValuesTableViewController : UITableViewController
{
    TypeAttribute *attribute;
    NSArray *types;
    NSIndexPath *selectedPath;
    id<SelectionAttributeDelegate> _delegate;
}

@property (nonatomic, strong) NSArray *types;
@property (nonatomic, strong) NSIndexPath *selectedPath;
@property (nonatomic, strong) id delegate;
@property (nonatomic, strong) TypeAttribute *attribute;

- (IBAction)clickCancel:(id)sender;
- (IBAction)clickSave:(id)sender;

@end

