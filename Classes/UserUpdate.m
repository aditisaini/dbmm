//
//  UserUpdate.m
//  WerDenktWas
//
//  Created by Franziska Engelmann on 19.11.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import "UserUpdate.h"
#import "ServerReport.h"
#import "UpdatePicture.h"

@implementation UserUpdate

@dynamic messageID;
@dynamic comment;
@dynamic solved;
@dynamic pictureURL;
@dynamic serverReport;
@dynamic updatePicture;
@dynamic updatedImage;


@end
