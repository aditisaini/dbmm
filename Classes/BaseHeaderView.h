//
//  LocationPictureView.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 22.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: BaseHeaderView.h 74 2010-07-14 22:00:16Z eik $
//

#import <UIKit/UIKit.h>

#import <MapKit/MapKit.h>

#import "ReportAnnotationProtocol.h"
#import "ReportAnnotation.h"

#import "MapKitDelegate.h"

@interface BaseHeaderView : UIView <MKMapViewDelegate>
{
@protected
    UIImageView *photoView;
    MKMapView *mapView;

    ReportAnnotation *_report;
@private
    MapKitDelegate *mapKitDelegate;
}

@property (nonatomic, strong) IBOutlet UIImageView *photoView;
@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) IBOutlet MapKitDelegate *mapKitDelegate;

@property (nonatomic, strong) ReportAnnotation *report;

- (void)setRoundCorners;
- (void)updatePhotoViewInitial:(BOOL)initial;

@end
