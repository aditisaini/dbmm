//
//  LongTextCell.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 10.01.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LongTextCell : UITableViewCell
{
    UITextView *_visibleTextView;
    UITextView *_contentTextViewLong;
    UITextView *_contentTextViewShort;
}

@property (strong, nonatomic) IBOutlet UIView *subview;
@property (strong, nonatomic) IBOutlet UITextView *contentTextViewLong;
@property (strong, nonatomic) IBOutlet UITextView *contentTextViewShort;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

+ (float)inset;

- (void)setLongText:(NSString *)text;
- (void)setLongtextCellImage:(UIImage *)image;

@end
