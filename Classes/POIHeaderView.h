//
//  POIHeaderView.h
//  WerDenktWas
//
//  Created by Martin on 11.05.15.
//  Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//


@class MKMapView;
@protocol WKTObject;

@interface POIHeaderView : UIView <MKMapViewDelegate>


@property(strong, nonatomic) IBOutlet MKMapView *mapView;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property(strong, nonatomic) IBOutlet UIImageView *imageView;

+ (POIHeaderView *)loadNib;

- (void)loadImageWithURL:(NSString *)pictureURL;

- (void)setGeometryOnMap:(id <WKTObject>)geometry;

@end
