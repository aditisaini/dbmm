//
// Created by Martin on 11.05.15.
// Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractJSONAdapter.h"

@class PointOfInterest;


@interface POIDetailJSONAdapter : AbstractJSONAdapter

- (void)queryWithPOI:(PointOfInterest *)pointOfInterest delegate:(id)aDelegate selector:(SEL)aSelector;

@end