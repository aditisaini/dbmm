//
//  DetailViewController.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 17.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: DetailViewController.m 74 2010-07-14 22:00:16Z eik $
//

#import "DetailViewController.h"

#import "Constants.h"

#import "DetailModificationViewController.h"
#import "DetailHeaderView.h"
#import "PictureViewController.h"
#import "SubscriptionViewController.h"

#import "ServerReport.h"
#import "ReportDetail.h"
#import "ReportPicture.h"

#import "DetailJSONAdapter.h"
#import "PictureAdapter.h"

#import "MMAlertViewHandler.h"
#import "MMTableView.h"

@interface DetailViewController ()

- (NSFetchedResultsController *)fetchedResultsController;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)detailsReceived;
- (void)pictureReceived;

@end

@implementation DetailViewController

@synthesize subscribeButton;
@synthesize headerView;
@synthesize loadingCell;
@synthesize pictureViewController;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [headerView setRoundCorners];
    if (_report) {
        headerView.report = _report;
    }
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;
    
    self.navigationItem.rightBarButtonItem = self.subscribeButton;
    self.navigationItem.rightBarButtonItem.title = NSLocalizedString(@"subscribe_button_title", nil);
    self.tableView = [[MMTableView alloc] initWithFrame:self.tableView.frame style:UITableViewStyleGrouped];
    self.tableView.tableHeaderView = headerView;
    self.tableView.allowsSelection = NO;
    
    [self setUpToolbar];
    
    if (fetchedResultsController.sections.count == 0) {
        [[MMAlertViewHandler sharedInstance] showActivityIndicatingAlertWithMessage:NSLocalizedString(@"checking_duplicates", nil)
                                                                           andTitle:NSLocalizedString(@"please_wait", nil)];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self.navigationController setToolbarHidden:(self.toolbarItems == nil) animated:animated];

    fetchedResultsController.delegate = nil;
    fetchedResultsController = nil;

    NSError *error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        // TODO: handle the error appropriately
        LogError(@"Details fetch", error);
    }

    [self.tableView reloadData];

    NSTimeInterval age = -[_report.detailsLastSeen timeIntervalSinceNow];
    if (!_report.detailsLastSeen || age >= kDetailFreshness) {
        if (!detailJSONAdapter)
            detailJSONAdapter = [[DetailJSONAdapter alloc] init];
        [detailJSONAdapter queryReport:_report delegate:self selector:@selector(detailsReceived)];
    }
    else {
#if LOG_LEVEL > 1
        NSLog(@"Using cached details %f seconds old.", age);
#endif
    }    
}

- (void)setUpToolbar
{
    // Segemented control for map view type
    UIBarButtonItem *leftSpace      = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *rightSpace     = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *centerButton   = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"comment_button", nil)
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(commentButtonClicked:)];
    
    self.toolbarItems = [NSArray arrayWithObjects:leftSpace, centerButton, rightSpace, nil];
    self.navigationController.toolbarHidden = NO;
    [self.navigationController setToolbarItems:self.toolbarItems];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

    [detailJSONAdapter cancel];
    detailJSONAdapter = nil;

    [pictureAdapter cancel];
    pictureAdapter = nil;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Cancel pending processes
    [detailJSONAdapter cancel];
    [pictureAdapter cancel];
    [[MMAlertViewHandler sharedInstance] cancelActivityIndicatingAlert];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // Cancel pending processes
    [detailJSONAdapter cancel];
    [pictureAdapter cancel];
    [[MMAlertViewHandler sharedInstance] cancelActivityIndicatingAlert];

    fetchedResultsController.delegate = nil;
    fetchedResultsController = nil;


    self.subscribeButton  = nil;
    self.headerView  = nil;
    self.loadingCell = nil;
    self.pictureViewController = nil;
}


- (void)dealloc
{
    [detailJSONAdapter cancel];
    [pictureAdapter cancel];
    [[MMAlertViewHandler sharedInstance] cancelActivityIndicatingAlert];

    fetchedResultsController.delegate = nil;

    self.report = nil;
}

#pragma mark -
#pragma mark Network delegates

- (void)detailsReceived
{    
    NSString *pictureURL = _report.pictureURL;

    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
    
    if (!_report.picture && [pictureURL isKindOfClass:NSString.class] && pictureURL.length > 0) {
        if (!pictureAdapter)
            pictureAdapter = [[PictureAdapter alloc] init];
        [pictureAdapter queryReport:_report delegate:self selector:@selector(pictureReceived)];
    }
    else {
        [headerView stopWaitingForPicture];
    }
}

- (void)pictureReceived
{
    [headerView stopWaitingForPicture];
}

#pragma mark -
#pragma mark API

- (ServerReport *)report
{
    return _report;
}

- (void)setReport:(ServerReport *)report
{
    _report = report;
    if (headerView) {
        if (_report && _report.detailsLastSeen)
            headerView.waitingForPicture = NO;
        else
            headerView.waitingForPicture = YES;
        headerView.report = _report;
    }
}

- (IBAction)subscribeButtonClicked:(id)sender
{
    SubscriptionViewController *subscriptionViewController = [[SubscriptionViewController alloc] initWithNibName:@"SubscriptionViewController"
                                                                                                          bundle:[NSBundle mainBundle]];
    subscriptionViewController.report = self.report;
 
    [self.navigationController pushViewController:subscriptionViewController animated:YES];
}

- (IBAction)pictureButtonClicked:(id)sender
{
    if (headerView.photoView.image) {
        pictureViewController.title = _report.title;

        [pictureViewController setImage:headerView.photoView.image];

        [self.navigationController pushViewController:pictureViewController animated:YES];
    }
}

- (IBAction)commentButtonClicked:(id)sender
{
    DetailModificationViewController *detailModificationViewController =  [[DetailModificationViewController alloc] initWithNibName:@"DetailModificationViewController" bundle:[NSBundle mainBundle]];
    detailModificationViewController.report = _report;
    detailModificationViewController.delegate = self;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:detailModificationViewController];
    [self.navigationController presentViewController:navigationController animated:YES completion:NULL];
}

#pragma mark -
#pragma mark Core Data

- (NSFetchedResultsController *)fetchedResultsController
{
    if (!fetchedResultsController) {
        NSManagedObjectContext *managedObjectContext = _report.managedObjectContext;

        // Create and configure a fetch request with the ReportDetail entity.
        NSDictionary *variables = [NSDictionary dictionaryWithObject:_report forKey:@"REPORT"];
        NSFetchRequest *fetchRequest = [_report.entity.managedObjectModel fetchRequestFromTemplateWithName:@"reportDetailForReport" substitutionVariables:variables];

        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order" ascending:YES];
        fetchRequest.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];

        // Create and initialize the fetch results controller.
        fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        fetchedResultsController.delegate = self;
    }

    return fetchedResultsController;
}

#pragma mark -
#pragma mark NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
    
    if (controller.sections == 0) {
        [[MMAlertViewHandler sharedInstance] showErrorAlertWithMessage:NSLocalizedString(@"data_loading_error", nil)];
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{

    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;

    switch(type) {

        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath]
                    atIndexPath:indexPath];
            break;

        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSArray *sections = fetchedResultsController.sections;
    NSUInteger count = sections.count;
    if (count == 0) {
        count = 1;
    } else {
        [[MMAlertViewHandler sharedInstance] cancelActivityIndicatingAlert];
    }
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger count = 0;
    NSArray *sections = fetchedResultsController.sections;
    if (sections.count) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
        count = [sectionInfo numberOfObjects];
    }
    return count;
}

- (NSInteger)numberOfLinesForText:(NSString *)text
{
    UIFont *cellFont = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
    CGFloat tableViewWidth = self.tableView.frame.size.width - 113.0;
    CGSize size = [text sizeWithFont:cellFont constrainedToSize:CGSizeMake(tableViewWidth, 200.0) lineBreakMode:UILineBreakModeWordWrap];

    NSInteger numberOfLines = ceilf(size.height / 19.0);
    if (numberOfLines == 0)
        numberOfLines = 1;

    return numberOfLines;
}

- (void)configureCell:(ReportDetailsCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    ReportDetail *reportDetail = [fetchedResultsController objectAtIndexPath:indexPath];
    
    [cell setHeaderText:reportDetail.name];
    [cell setDetailsText:reportDetail.content];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;

    NSString *cellIdentifier = @"detailCell";

    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[ReportDetailsCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellIdentifier];
    }

    [self configureCell:cell atIndexPath:indexPath];

    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title;

    switch (section) {
        case 0:
            title = NSLocalizedStringWithDefaultValue(@"header_details", nil,
                                                      [NSBundle mainBundle], @"Details",
                                                      @"Header in detail view");
            break;
        default:
            title = nil;
            break;
    }

    return title;
}

- (void)dismissCommentNavigationStack
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReportDetail *reportDetail = [fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *name = reportDetail.name;
    NSString *content = reportDetail.content;
    NSInteger width = self.tableView.frame.size.width - 50;
    
    CGSize sizeForContent = [content sizeWithFont:[UIFont boldSystemFontOfSize:14]
                                constrainedToSize:CGSizeMake(width, 999)
                                    lineBreakMode:UILineBreakModeWordWrap];
    CGSize sizeForName = [name sizeWithFont:[UIFont boldSystemFontOfSize:14]
                             constrainedToSize:CGSizeMake(width, 999)
                                 lineBreakMode:UILineBreakModeWordWrap];
        
    return sizeForContent.height + sizeForName.height + [ReportDetailsCell inset];
}

@end

