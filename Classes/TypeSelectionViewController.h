//
//  TypeSelectionViewController.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 10.01.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

@class DuplicatesJSONAdapter;
@class ReportAnnotation;

@interface TypeSelectionViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) UIViewController *basisPresentingViewController;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) ReportAnnotation *annotation;

@property (strong, nonatomic) IBOutlet UIView *descriptionView;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionTitleLabel;

@property (nonatomic, strong) NSIndexPath *selectedPath;
@property (nonatomic, strong) NSIndexPath *initialPath;

@property (nonatomic, strong) DuplicatesJSONAdapter *duplicatesAdapter;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

- (void)presentDuplicatesViewController;
- (void)pushAttributesViewController;

@end
