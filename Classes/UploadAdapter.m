//
//  UploadAdapter.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 23.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: UploadAdapter.m 76 2010-07-15 00:24:01Z eik $
//

#import "UploadAdapter.h"

#import <UIKit/UIKit.h>
#include <Security/Security.h>

#import "Constants.h"
#import "ServerConnection.h"

#import "UserReport.h"
#import "ServerReport.h"
#import "UserUpdate.h"
#import "ReportPicture.h"
#import "UpdatePicture.h"
#import "ReportType.h"
#import "Domain.h"

#define kIDProperty             @"id"
#define kResultProperty         @"result"

@interface UploadAdapter ()

- (void)appendFieldWithName:(NSString *)name boundary:(NSString *)boundary toBody:(NSMutableData *)body;
- (NSURLRequest *)createMessageRequestWithServer:(NSString *)server;

@end


@implementation UploadAdapter

@synthesize result;
@synthesize messageID;
@synthesize messagetext;
@synthesize userReport;
@synthesize userUpdate;

- (id)init
{
    self = [super init];
    if (self != nil) {
        connection = [[ServerConnection alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [connection cancel];
    connection = nil;

    
}

- (void)cancel
{
    [connection cancel];

    delegate = nil;
}

#pragma mark -
#pragma mark API

- (void)uploadReport:(UserReport *)report delegate:(id)aDelegate selector:(SEL)aSelector
{
    delegate = aDelegate;
    selector = aSelector;

    self.userReport = report;

    [connection startWithRequest:[self createMessageRequestWithServer:report.domain.server]
                        delegate:self];
}

- (void)uploadUpdate:(UserUpdate *)update delegate:(id)aDelegate selector:(SEL)aSelector
{

    delegate = aDelegate;
    selector = aSelector;
    
    self.userUpdate = update;
    
    [connection startWithRequest:[self updateMessageRequestWithServer:update.serverReport.domain.server]
                        delegate:self];
}

#pragma mark -
#pragma mark internal

- (void)appendFieldWithName:(NSString *)name boundary:(NSString *)boundary toBody:(NSMutableData *)body
{
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n", name] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Type: text/plain; charset=UTF-8\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
}

- (NSURLRequest *)createMessageRequestWithServer:(NSString *)server
{
    // setting up the URL to post to
    NSURL *theURL = [NSURL URLWithString:[server stringByAppendingString:@"create_message"]];

    // setting up the request object now
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:theURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];

    [theRequest setHTTPMethod:@"POST"];

    // the boundary
    unsigned long long bytes;

    SecRandomCopyBytes(kSecRandomDefault, sizeof(bytes), (u_int8_t *)&bytes);

    NSString *boundary = [NSString stringWithFormat:@"----wdw_iPhoneClient_%qX", bytes];

    // construct the header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theRequest setValue:contentType forHTTPHeaderField: @"Content-Type"];

    // construct the body
    NSMutableData *body = [[NSMutableData alloc] initWithCapacity:102400];

    NSNumber *num;
    NSString *str;
    NSData *img;

    num = userReport.latitude;
    if (num) {
        [self appendFieldWithName:kUploadLatitude boundary:boundary toBody:body];
        [body appendData:[[NSString stringWithFormat:@"%f", [num doubleValue]] dataUsingEncoding:NSUTF8StringEncoding]];
    }

    num = userReport.longitude;
    if (num) {
        [self appendFieldWithName:kUploadLongitude boundary:boundary toBody:body];
        [body appendData:[[NSString stringWithFormat:@"%f", [num doubleValue]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
	
//	num = userReport.accuracyNumber;
//    if (num) {
//        [self appendFieldwithName:kUploadAccuracy boundary:boundary toBody:body];
//        [body appendData:[[NSString stringWithFormat:@"%f", [num doubleValue]] dataUsingEncoding:NSUTF8StringEncoding]];
//    }
	
    str = userReport.details;
    if (str) {
        [self appendFieldWithName:kUploadDescription boundary:boundary toBody:body];
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    str = userReport.subjectTitle;
    if (str) {
        [self appendFieldWithName:kUploadSubjectTitle boundary:boundary toBody:body];
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
    }

    str = userReport.type2.name;
    if (str) {
        [self appendFieldWithName:kUploadType boundary:boundary toBody:body];
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
    }

    str = userReport.type2.myid;
    if (str) {
        [self appendFieldWithName:@"typeid" boundary:boundary toBody:body];
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
    }	

    str = userReport.address;
    if (str) {
        [self appendFieldWithName:kUploadAddress boundary:boundary toBody:body];
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
    }
	
    // Insert attributes here
    str = [userReport.type2 attributesToJSONObject];
    if (str) {
        [self appendFieldWithName:@"answers" boundary:boundary toBody:body];
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    str = [[NSUserDefaults standardUserDefaults] stringForKey:kAppIDKey];
    if (str) {
        [self appendFieldWithName:@"appid" boundary:boundary toBody:body];
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
    }
	
//	str = [[UIDevice currentDevice]uniqueIdentifier];
//    if (str) {
//        [self appendFieldWithName:@"phone" boundary:boundary toBody:body];
//        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
//    }[prefs objectForKey:@"UUID"]

    str = [[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"];
    if (str) {
        [self appendFieldWithName:@"phone" boundary:boundary toBody:body];
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
	str = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    if (str) {
        [self appendFieldWithName:@"lang" boundary:boundary toBody:body];
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
    };
	
	str = @"iPhone";
    if (str) {
        [self appendFieldWithName:@"via" boundary:boundary toBody:body];
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
    };
	
    num = userReport.domain.domainID;
    if (num) {
        [self appendFieldWithName:kUploadDomain boundary:boundary toBody:body];
        [body appendData:[[NSString stringWithFormat:@"%d", [num integerValue]] dataUsingEncoding:NSUTF8StringEncoding]];
    }

    img = userReport.picture.imageData;
    if (img) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"picture.jpg\"\r\n", kUploadPhoto] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Type: image/jpeg\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Transfer-Encoding: binary\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];

        [body appendData:img];
    }

    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [theRequest setHTTPBody:body];
    
#if LOG_LEVEL > 1
    NSLog(@"The body: %@", [[NSString alloc] initWithBytes:[body bytes]
                                                    length:[body length]
                                                  encoding:NSASCIIStringEncoding]);
    NSLog(@"Sending request %d octets", body.length);
#endif

    return theRequest;
}

- (NSURLRequest *)updateMessageRequestWithServer:(NSString *)server
{
    // setting up the URL to post to
    NSURL *theURL = [NSURL URLWithString:[server stringByAppendingString:@"update_message"]];
    
    // setting up the request object now
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:theURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [theRequest setHTTPMethod:@"POST"];
    
    // the boundary
    unsigned long long bytes;
    
    SecRandomCopyBytes(kSecRandomDefault, sizeof(bytes), (u_int8_t *)&bytes);
    
    NSString *boundary = [NSString stringWithFormat:@"----wdw_iPhoneClient_%qX", bytes];
    
    // construct the header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [theRequest setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // construct the body
    NSMutableData *body = [[NSMutableData alloc] initWithCapacity:102400];
    
    NSNumber *num;
    NSString *str;
    NSData *img;
    
    // appid (= 1)
    str = [[NSUserDefaults standardUserDefaults] stringForKey:kAppIDKey];
    if (str) {
        [self appendFieldWithName:@"appid" boundary:boundary toBody:body];
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // id (messageID)
    num = userUpdate.messageID;
    if (num) {
        [self appendFieldWithName:@"id" boundary:boundary toBody:body];
        [body appendData:[[NSString stringWithFormat:@"%d", [num integerValue]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // text (comment)
    str = userUpdate.comment;
    if (str) {
        [self appendFieldWithName:@"text" boundary:boundary toBody:body];
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // solved
    str = userUpdate.solved;
    if (str) {
        [self appendFieldWithName:@"solved" boundary:boundary toBody:body];
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // phone (user-/appID)
    str = [[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"];
    if (str) {
        [self appendFieldWithName:@"phone" boundary:boundary toBody:body];
        [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // picture
    img = userUpdate.updatePicture.imageData;
    if (img) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"picture.jpg\"\r\n", kUploadPhoto] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Type: image/jpeg\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Transfer-Encoding: binary\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:img];
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [theRequest setHTTPBody:body];
    
#if LOG_LEVEL > 1
    NSLog(@"The body: %@", [[NSString alloc] initWithBytes:[body bytes]
                                                    length:[body length]
                                                  encoding:NSASCIIStringEncoding]);
    NSLog(@"Sending request %d octets", body.length);
#endif
    
    return theRequest;
}


#pragma mark -
#pragma mark ServerConnectionDelegate

- (void)serverConnectionDidFinishLoading:(ServerConnection *)serverConnection
{
    NSDictionary *resultList = [serverConnection jsonData];
    
    if ([resultList isKindOfClass:NSDictionary.class]) {
        messageID = [[resultList objectForKey:kIDProperty] integerValue];
        result = [[resultList objectForKey:kResultProperty] integerValue];
		messagetext = [resultList objectForKey:@"message"] ;
#if LOG_LEVEL > 1
        NSLog(@"Upload result: %d, id %d", result, messageID);
#endif
    }
#if LOG_LEVEL > 0
    else {
        NSLog(@"Could not understand upload result");
    }
#endif

    [delegate performSelector:selector];
    delegate = nil;
}

@end
