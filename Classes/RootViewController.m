//
//  RootViewController.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 10.05.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: RootViewController.m 80 2010-07-18 08:59:46Z eik $
//

#import "RootViewController.h"

#import "Domain.h"
#import "ServerReport.h"
#import "UserReport.h"
#import "ReportOverviewController.h"
#import "DialogPhotoViewController.h"
#import "DetailViewController.h"
#import "AGBViewController.h"
#import "AboutViewController.h"
#import "HelpViewController.h"
#import "VotingViewController.h"

#import "DomainJSONAdapter.h"
#import "DomainbundleJSONAdapter.h"
#import "ReportsJSONAdapter.h"

#import "DraggableAnnotationView.h"
#import "Constants.h"
#import "MMAlertViewHandler.h"
#import "MMColor.h"
#import "POIMapViewController.h"

#import "Ma_ngelmelder-Swift.h"

@interface RootViewController ()

@property(nonatomic, strong) CLLocationManager *locationManager;

- (void)recentDomain;

- (void)zoomMapViewToDomain:(BOOL)toDomain animated:(BOOL)animated;

- (void)fetchDomain:(id)object;

- (void)fetchReports:(id)object;

- (void)setMyDomain:(Domain *)domain;

- (void)domainReceived;

- (void)reportsReceived;

- (NSString *)host;

- (NSString *)server;

@end


@implementation RootViewController

@synthesize managedObjectContext = _managedObjectContext;
@synthesize mapView = _mapView;
@synthesize domain;
@synthesize infoButton;
@synthesize reportButton;
@synthesize detailViewController;
@synthesize mapKitDelegate;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;

    CGRect frame = infoButton.frame;
    frame.size.width = 36.0;
    frame.size.height = 36.0;
    infoButton.frame = frame;
    reportButton.title = NSLocalizedStringWithDefaultValue(@"new_message", nil,
            [NSBundle mainBundle], @"Default", @"");

    mapKitDelegate.enablingCallouts = YES;
#ifdef __IPHONE_4_0
    if ([ReportAnnotationView instancesRespondToSelector:@selector(setDraggable:)]) {
        mapKitDelegate.creatingMovableAnnotations = NO;
    }
#endif

    [self recentDomain];

    if (domain && ([domain.latitude doubleValue] <= -180.0 || [domain.longitude doubleValue] <= -180.0)) {
        [self setMyDomain:nil];
    }

    reportButton.enabled = NO;

    [self setupLocationManager];
    [self handleLaunchCount];

    if (launchCount > 1)
        [self isLocationManagerAuthorized];
}

- (void)setupLocationManager {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;

    if ([self.locationManager respondsToSelector:
            @selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }

    self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;

    [self.locationManager startUpdatingLocation];
}

- (void)handleLaunchCount {
    NSUserDefaults *prefs;
    prefs = [NSUserDefaults standardUserDefaults];

    if (![prefs objectForKey:@"UUID"]) {
        [prefs setObject:[self uuidString] forKey:@"UUID"];
    }

    launchCount = [prefs integerForKey:@"launchCount"];

    // Initial launch
    if (!launchCount) {
        launchCount = 1;
        [prefs setInteger:launchCount forKey:@"launchCount"];
        [self fetchDomain:nil];
        prefs = [NSUserDefaults standardUserDefaults];
        [prefs synchronize];
        [self fetchDomain:nil];
    }
        // AGB have not been confirmed yet
    else if (launchCount == -1) {
        [self showInitialStartView];
    }
    else {
        launchCount += 1;
        [prefs setInteger:launchCount forKey:@"launchCount"];
        [prefs synchronize];
        [self fetchDomain:nil];
        // Present voting view
        [self showVotingViewController];
    }
}

- (NSString *)uuidString {
    CFUUIDRef newUniqueId = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidString = (NSString *) CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, newUniqueId));
    CFRelease(newUniqueId);

    return uuidString;
}

- (void)showVotingViewController {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    // Kommt nicht mit 13-24h zurecht, wenn hh statt HH verwendet wird!
    [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];

    // Check for expiration Date
    if ([[NSDate date] compare:[dateFormatter dateFromString:@"21.10.2013 00:00"]] == NSOrderedAscending) {
        VotingViewController *votingVC = [[VotingViewController alloc] initWithNibName:@"VotingView" bundle:nil];
        [self presentViewController:votingVC animated:YES completion:NULL];
    }
}

- (void)startViewHasBeenDismissed {
    [self showVotingViewController];
}

- (void)showInitialStartView {
    launchCount = -1;
    [[NSUserDefaults standardUserDefaults] setInteger:launchCount forKey:@"launchCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    InitialStartViewController *initialVC = [[InitialStartViewController alloc] init];
    initialVC.delegate = self;
    [self.navigationController presentViewController:initialVC animated:YES completion:NULL];
}

- (void)agbHasBeenAccepted {
    launchCount += 3;
    [[NSUserDefaults standardUserDefaults] setInteger:launchCount forKey:@"launchCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [self dismissViewControllerAnimated:YES completion:nil];

    [self isLocationManagerAuthorized];
}

- (void)agbHasBeenDeclined {
    launchCount = -1;
    [[NSUserDefaults standardUserDefaults] setInteger:launchCount forKey:@"launchCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [[MMAlertViewHandler sharedInstance] showAGBHasBeenDeclinedAlert];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.navigationController setToolbarHidden:(self.toolbarItems == nil) animated:animated];

    [_managedObjectContext processPendingChanges];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    if (launchCount == 1) {
        [self showInitialStartView];

        launchCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"launchCount"] + 1;
        [[NSUserDefaults standardUserDefaults] setInteger:launchCount forKey:@"launchCount"];
    }
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

    [domainJSONAdapter cancel];
    domainJSONAdapter = nil;

    [reportsJSONAdapter cancel];
    reportsJSONAdapter = nil;
}

- (void)viewDidUnload {
    [super viewDidUnload];

    [domainJSONAdapter cancel];
    domainJSONAdapter = nil;

    [reportsJSONAdapter cancel];
    reportsJSONAdapter = nil;

    self.mapView = nil;
    self.mapKitDelegate = nil;

    [self setMyDomain:nil];

    self.detailViewController = nil;
}


- (void)dealloc {
    [domainJSONAdapter cancel];
    [reportsJSONAdapter cancel];

    [self setMyDomain:nil];
}

#pragma mark -
#pragma mark Core Data

- (void)recentDomain {
    NSString *server = [self server];

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastSeen" ascending:NO];
    fetchRequest.entity = [NSEntityDescription entityForName:@"Domain" inManagedObjectContext:_managedObjectContext];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"server == %@", server];
    fetchRequest.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    fetchRequest.fetchLimit = 1;

    NSError *error = nil;
    NSArray *domains = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];

    if (domains.count > 0) {
        [self setMyDomain:[domains objectAtIndex:0]];
    } else {
        [self setMyDomain:nil];
    }
}

#pragma mark -
#pragma mark NSKeyValueObserver

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    int changeKind = [[change valueForKey:NSKeyValueChangeKindKey] intValue];

    if (object == domain) {
        if ([keyPath isEqualToString:@"reports"]) {
            switch (changeKind) {
                case NSKeyValueChangeInsertion: {
                    NSSet *newReports = [change valueForKey:NSKeyValueChangeNewKey];
                    [_mapView addAnnotations:[newReports allObjects]];
                }
                    break;
                case NSKeyValueChangeRemoval: {
                    NSSet *oldReports = [change valueForKey:NSKeyValueChangeOldKey];
                    [_mapView removeAnnotations:[oldReports allObjects]];
                    if ([oldReports containsObject:detailViewController.report])
                        detailViewController.report = nil;
                }
                    break;
            }
        }
    }
}

#pragma mark -
#pragma mark MapActionDelegate

- (void)showDetails:(ReportAnnotation *)annotation {
    detailViewController.report = (ServerReport *) annotation;

    [self.navigationController pushViewController:detailViewController animated:YES];
}

- (void)editReport:(ReportAnnotation *)annotation {
    [self presentReportOverviewController];
}

- (void)createReport {
    [self reportButtonUp:self];
}

- (void)startAtLocation:(CLLocation *)location {
    [self performSelector:@selector(fetchDomain:) withObject:nil afterDelay:0.1];
}

#if 1

- (void)movedToLocation:(CLLocation *)location {
    if (domain) {
        if (reportsJSONAdapter) {
            [reportsJSONAdapter cancel];
        } else {
            reportsJSONAdapter = [[ReportsJSONAdapter alloc] initWithDomain:domain];
        }
        [reportsJSONAdapter queryWithLocation:location cumulative:YES delegate:nil selector:NULL];
    }

#warning NOT FINISHED - pretty hacky (also see domainbundle-adapter)
    DomainbundleJSONAdapter *domainBundleAdapter = [[DomainbundleJSONAdapter alloc] init];
    [domainBundleAdapter queryServer:[self server]
                        withLocation:location
                            delegate:self
                            selector:@selector(didReceiveDomainBundle:)];
}

- (void)didReceiveDomainBundle:(NSDictionary *)bundle {
    self.navigationItem.title = bundle[@"name"];
}

#endif

#pragma mark -
#pragma mark API

- (NSString *)host {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kServerHostKey];
}

- (NSString *)hostName {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kServerHostNameKey];
}

- (NSString *)server {
    return [NSString stringWithFormat:kServerFormat, [self host]];
}

- (IBAction)refreshButtonUp:(id)sender {
    [self fetchDomain:nil];
}

- (void)fetchDomain:(id)object {

    CLLocation *location = _mapView.userLocation.location;

    if (!location || location.horizontalAccuracy < 0.0) {
        // Custom coordinates for starting point could be entered here (if app is not allowed to access user location)
        location = [[CLLocation alloc]
                initWithLatitude:_mapView.centerCoordinate.latitude longitude:_mapView.centerCoordinate.longitude];
    }

    NSString *server = [self server];

    BOOL domainFresh = NO;

    // Check if if more than kDomainFreshnessTime seconds have passed or the user distance to the previous domain has exceeded kDomainFreshnessDistance
    if (domain && [domain.server isEqualToString:server]) {
        CLLocation *domainLocation = [[CLLocation alloc]
                initWithLatitude:[domain.latitude doubleValue] longitude:[domain.longitude doubleValue]];
        CLLocationDistance distance = [location distanceFromLocation:domainLocation];
        NSTimeInterval age = -[domain.lastSeen timeIntervalSinceNow];

        // If not, domain does not have to be refreshed
        if (distance < kDomainFreshnessDistance && age < kDomainFreshnessTime) {
            domainFresh = YES;
        }
    }

    if (!domainFresh) {
        if (domainJSONAdapter) {
            [domainJSONAdapter cancel];
        }
        else {
            domainJSONAdapter = [[DomainJSONAdapter alloc] initWithManagedObjectContext:_managedObjectContext];
            reportsJSONAdapter = nil;
        }

        [domainJSONAdapter queryServer:server withLocation:location delegate:self selector:@selector(domainReceived)];
    }
    else {
        [self fetchReports:nil];
    }
}

- (void)setMyDomain:(Domain *)aDomain {
    if (domain) {
        [_mapView removeAnnotations:[domain.reports allObjects]];
        [domain removeObserver:self forKeyPath:@"reports"];
    }

    if (reportsJSONAdapter) {
        [reportsJSONAdapter cancel];
        reportsJSONAdapter = nil;
    }

    domain = aDomain;
    if (domain) {
        [domain addObserver:self
                 forKeyPath:@"reports"
                    options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                    context:NULL];
        [_mapView addAnnotations:[domain.reports allObjects]];

        self.navigationItem.title = domain.title;
    }
}

- (void)domainReceived {
    if (domainJSONAdapter.domain && domainJSONAdapter.domain != domain) {
#if LOG_LEVEL > 1
        NSLog(@"New domain: %@", domainJSONAdapter.domain.title);
#endif
        [self setMyDomain:domainJSONAdapter.domain];

        reportButton.enabled = YES;
    }

    [self fetchReports:nil];
}

- (void)fetchReports:(id)object {
    if (domain) {
        CLLocation *location = _mapView.userLocation.location;

        if (!location || location.horizontalAccuracy < 0.0) {
            location = [[CLLocation alloc]
                    initWithLatitude:_mapView.centerCoordinate.latitude longitude:_mapView.centerCoordinate.longitude];
        }

        BOOL reportsFresh = NO;

        if (domain.reportsLastSeen) {
            CLLocation *reportsLocation = [[CLLocation alloc]
                    initWithLatitude:[domain.reportsLatitude doubleValue]
                           longitude:[domain.reportsLongitude doubleValue]];
            CLLocationDistance distance = [location distanceFromLocation:reportsLocation];

            NSTimeInterval age = -[domain.reportsLastSeen timeIntervalSinceNow];

            if (distance < kReportsFreshnessDistance && age < kReportsFreshnessTime) {
                reportsFresh = YES;
#if LOG_LEVEL > 1
                NSLog(@"Using cached reports %f seconds old, %f meters away.", age, distance);
#endif
            }
        }

        if (!reportsFresh) {
            if (reportsJSONAdapter)
                [reportsJSONAdapter cancel];
            else
                reportsJSONAdapter = [[ReportsJSONAdapter alloc] initWithDomain:domain];

            [reportsJSONAdapter queryWithLocation:location
                                       cumulative:NO
                                         delegate:self
                                         selector:@selector(reportsReceived)];
        }
        else {
//            [self zoomMapViewToDomain:NO animated:YES];
        }
    }
}

- (void)reportsReceived {
//    [self zoomMapViewToDomain:NO animated:YES];
}

- (IBAction)reportButtonUp:(id)sender {
    if (![self isLocationManagerAuthorized])
        return;

    // Check if a user report already exists
    if ([domain.userReports count] > 0) {
        // YES: Show alert and offer choice (delete/modify)
        [[MMAlertViewHandler sharedInstance]
                showAReportAlreadyExistsAlertWithButtonClickBlock:^(NSInteger buttonIndex) {
                    // Delete existing report and load new report dialog
                    if (buttonIndex == 1) {
                        [[MMAlertViewHandler sharedInstance]
                                showDeleteConfirmationAlertWithButtonClickBlock:^(NSInteger buttonIndex) {
                                    if (buttonIndex == 1) {
                                        [self deleteUserReport];
                                        [self presentReportPhotoViewController];
                                    }
                                }];
                    }
                        // Else show overview
                    else {
                        [self presentReportOverviewController];
                    }
                }];
    } else {
        // NO: Load new report dialog
        [self presentReportPhotoViewController];
    }
}

- (void)deleteUserReport {
    if ([domain.userReports count] > 0) {
        NSError *error;
        [self.managedObjectContext deleteObject:[domain.userReports anyObject]];
        [self.managedObjectContext save:&error];

        [self.view setNeedsDisplay];
    } else {
        NSLog(@"Error deleting user report: No report exists!");
    }
}

- (void)presentReportOverviewController {
    if ([domain.userReports count] > 0) {
        ReportOverviewController *reportOverviewController = [[ReportOverviewController alloc]
                initWithNibName:@"ReportOverviewController" bundle:nil];
        reportOverviewController.userReport = [domain.userReports anyObject];
        reportOverviewController.managedObjectContext = _managedObjectContext;

        UINavigationController *navigationController = [[UINavigationController alloc]
                initWithRootViewController:reportOverviewController];
        [self.navigationController presentViewController:navigationController animated:YES completion:nil];
    }
    else {
        NSLog(@"Error loading user report: No report exists!");
    }
}

- (void)presentReportPhotoViewController {
    UserReport *userReport = nil;
    DialogPhotoViewController *reportPhotoViewController = [[DialogPhotoViewController alloc]
            initWithNibName:@"PhotoViewController" bundle:nil];

    if ([domain.userReports count] > 0) {
        userReport = [domain.userReports anyObject];
    }
    else {
        CLLocation *userLocation = _mapView.userLocation.location;
        CLLocationCoordinate2D coordinate;

        if (userLocation && userLocation.horizontalAccuracy >= 0.0) {
            coordinate = userLocation.coordinate;
        } else {
            coordinate = _mapView.centerCoordinate;
        }

        userReport = [NSEntityDescription insertNewObjectForEntityForName:@"UserReport"
                                                   inManagedObjectContext:_managedObjectContext];
        userReport.coordinate = coordinate;
        userReport.domain = domain;
        userReport.reportDomain = domain;


        reportPhotoViewController.managedObjectContext = _managedObjectContext;
        reportPhotoViewController.userReport = userReport;

        UINavigationController *navigationController = [[UINavigationController alloc]
                initWithRootViewController:reportPhotoViewController];

        [self.navigationController presentViewController:navigationController animated:YES completion:nil];
    }
}

- (IBAction)constructionButtonUp:(id)sender {
    POIMapViewController *poiMapViewController = [[POIMapViewController alloc]
            initWithNibName:@"POIMapViewController" bundle:nil];
    poiMapViewController.region = self.mapView.region;

    [self.navigationController pushViewController:poiMapViewController animated:YES];

}

- (IBAction)infoButtonUp:(id)sender {
    NSString *cancel = NSLocalizedStringWithDefaultValue(@"cancel_button_title", nil,
            [NSBundle mainBundle], @"Cancel", @"");
    NSString *help = NSLocalizedStringWithDefaultValue(@"help_button_title", nil,
            [NSBundle mainBundle], @"Show help", @"");
    NSString *about = NSLocalizedStringWithDefaultValue(@"about_button_title", nil,
            [NSBundle mainBundle], @"About us", @"");
    NSString *agb = NSLocalizedStringWithDefaultValue(@"agb", nil,
            [NSBundle mainBundle], @"General Business Conditions", @"");


    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:cancel
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:about, help, agb, nil];

    [actionSheet showFromToolbar:self.navigationController.toolbar];
}


#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // Workaround (avoids that barbuttonitems remain gray => Apple bug)
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        [[[[[[UIApplication sharedApplication]
                delegate]
                window]
                rootViewController]
                view]
                setTintAdjustmentMode:UIViewTintAdjustmentModeNormal];
    }

    UIViewController *viewController = [[AboutViewController alloc] init];

    if (buttonIndex == 1) {
        viewController = [[HelpViewController alloc] initWithNibName:@"HelpViewController" bundle:nil];
    } else if (buttonIndex == 2) {
        viewController = [[AGBViewController alloc] init];
    }

    if (buttonIndex <= 2) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.75];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                               forView:self.navigationController.view
                                 cache:YES];

        [self.navigationController pushViewController:viewController animated:NO];

        [UIView commitAnimations];
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    [self processNewLocation:newLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *lastLocation = locations.lastObject;
    [self processNewLocation:lastLocation];
}

- (void)processNewLocation:(CLLocation *)location {
    [self.locationManager stopUpdatingLocation];
    reportButton.enabled = YES;

    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location.coordinate, 3000, 3000);

    [self.mapView regionThatFits:region];
    [self.mapView setRegion:region animated:YES];
}

- (BOOL)isLocationManagerAuthorized {
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways) {
        [[MMAlertViewHandler sharedInstance] showErrorAlertWithMessage:NSLocalizedString(@"new_message_error", nil)];
        return NO;
    }
    return YES;
}

@end
