//
//  POIDetailsTableViewController.h
//  WerDenktWas
//
//  Created by Martin on 11.05.15.
//  Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PointOfInterest;

@interface POIDetailsTableViewController : UITableViewController

@property (nonatomic, strong) PointOfInterest *pointOfInterest;

@end
