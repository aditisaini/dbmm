//
//  HelpTableViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 09.01.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "HelpViewController.h"
#import "SBJson.h"
#import "LongTextCell.h"
#import "ReportDetailsCell.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

@synthesize backButton;

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSString *help = NSLocalizedStringWithDefaultValue(@"help_view_title", nil,
                                                       [NSBundle mainBundle], @"Help", @"");
    NSString *back = NSLocalizedStringWithDefaultValue(@"back_button_title", nil,
                                                       [NSBundle mainBundle], @"Back", @"");
    
    backButton = [[UIBarButtonItem alloc] initWithTitle:back style:UIBarButtonItemStyleBordered target:self action:@selector(barButtonClicked:)];
    
    self.navigationItem.title = help;
    self.navigationItem.rightBarButtonItem = backButton;
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.toolbarHidden = YES;
    self.navigationController.navigationBar.translucent = NO;
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"help" ofType:@"html"]isDirectory:NO]]];
}

- (IBAction)barButtonClicked:(id)sender
{
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:YES];
    
    [self.navigationController popViewControllerAnimated:NO];
    
	[UIView commitAnimations];
}

- (void)viewDidUnload
{
    [self setBackButton:nil];
    [super viewDidUnload];
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

@end
