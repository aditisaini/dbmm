//
//  VotingViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 01.10.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "VotingViewController.h"

@interface VotingViewController ()

@end

@implementation VotingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)voteButtonClicked:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"http://www.land-der-ideen.de/ausgezeichnete-orte/preistraeger/m-ngelmelder-b-rger-f-r-ihre-stadt"];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)appButtonClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return NO;
}

@end
