//
//  DomainJSONAdapter.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 23.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: DomainJSONAdapter.m 79 2010-07-16 17:03:57Z eik $
//

#import "DomainJSONAdapter.h"

#import "Constants.h"
#import "ServerConnection.h"
#import "Domain.h"
#import "ReportType.h"
#import "TypeAttribute.h"
#import "Constants.h"

#define kDomainProperty         @"domainid"
#define kNameProperty           @"name"
#define kTypesProperty          @"types"
#define kDefaultProperty        @"isDefault"

#define kImageNameProperty      @"imageName"
#define kAttributesProperty     @"attributes"
#define khasSubjectTitleProperty @"hasSubjectTitle"

@implementation DomainJSONAdapter

@synthesize domain;

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext_
{
    self = [super init];
    
    if (self != nil) {
        managedObjectContext = managedObjectContext_;

        self->_parentKey    = @"domain";
        self->_entityName   = @"ReportType";
        self->_entityKey    = @"myid";
        self->_propertyKey  = @"myid";

        self->_mapping = [[NSDictionary alloc] initWithObjectsAndKeys:
                          kImageNameProperty, kImageNameProperty,
                          kNameProperty,kNameProperty,
                          kAttributesProperty, kAttributesProperty,
                          khasSubjectTitleProperty, khasSubjectTitleProperty,
						  nil];
    }
    
    return self;
}


#pragma mark API

- (void)queryServer:(NSString *)aServer withLocation:(CLLocation *)location delegate:(id)aDelegate selector:(SEL)aSelector;
{
    self->delegate = aDelegate;
    self->selector = aSelector;

    coordinate = location.coordinate;

    server = aServer;

    NSString *localization = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];

    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSString stringWithFormat:@"%lf", coordinate.latitude],        @"lat",
                                [NSString stringWithFormat:@"%lf", coordinate.longitude],       @"long",
                                [NSString stringWithFormat:@"%lf", location.horizontalAccuracy],@"accuracy",
                                [[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"],   @"phone",
								[[NSUserDefaults standardUserDefaults] stringForKey:kAppIDKey], @"appid",
								localization,                                                   @"lang",
                                nil];

    [self queryWithServer:server method:@"get_domain" parameters:parameters];
}

#pragma mark Core Data

- (TypeAttribute *)typeAttributeWithID:(NSNumber *)attributeID
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:@"TypeAttribute"
                                      inManagedObjectContext:managedObjectContext];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(myID == %@)", attributeID];
    fetchRequest.fetchLimit = 1;
    
    NSError *error = nil;
    NSArray *attributes = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
    if (attributes.count > 0)
        return [attributes objectAtIndex:0];
    else
        return nil;
}

- (NSArray *)attributesForReportType:(ReportType *)reportType
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:@"TypeAttribute"
                                      inManagedObjectContext:managedObjectContext];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"SELF IN %@", reportType.attributes];
    fetchRequest.fetchLimit = 1;
    
    NSError *error = nil;
    NSArray *values = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (values.count > 0)
        return values;
    else
        return nil;
}

- (void)domainWithID:(NSNumber *)domainID
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:@"Domain"
                                      inManagedObjectContext:managedObjectContext];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(domainID == %@) AND (server == %@)", domainID, server];
    fetchRequest.fetchLimit = 1;
    //[fetchRequest setIncludesPropertyValues:NO];

    NSError *error = nil;
    NSArray *domains = [managedObjectContext executeFetchRequest:fetchRequest error:&error];

    if (domains.count > 0)
        self.domain = [domains objectAtIndex:0];
    else
        self.domain = nil;
}

- (TypeAttribute *)typeAttributeFromDictionary:(NSDictionary *)dict
{
    TypeAttribute *attribute = [self typeAttributeWithID:@([dict[@"id"] integerValue])];
    
    if (attribute == nil) { //|| attribute.valuesArray == nil) {
        
        attribute = [NSEntityDescription insertNewObjectForEntityForName:@"TypeAttribute"
                                                  inManagedObjectContext:managedObjectContext];
        
        attribute.myID      = @([dict[@"id"] integerValue]);
        
        attribute.cached    = @([dict[@"cached"] integerValue]);
        attribute.isPublic  = @([dict[@"public"] integerValue]);
        attribute.required  = @([dict[@"required"] integerValue]);
        
        attribute.name      = dict[@"name"];
        id code             = dict[@"code"];
        attribute.code      = ([code isKindOfClass:[NSString class]]) ? code : [attribute.myID stringValue];
        attribute.help      = dict[@"help"];
        attribute.error     = dict[@"error"];
        attribute.type      = dict[@"type"];
        
        attribute.requiredIfCode   = [dict[@"required_if_code"] isKindOfClass:[NSString class]] ? dict[@"required_if_code"] :nil;
        attribute.requiredIfValue  = [dict[@"required_if_value"] isKindOfClass:[NSString class]] ? dict[@"required_if_value"] :nil;
        attribute.visibleIfCode   = [dict[@"visible_if_code"] isKindOfClass:[NSString class]] ? dict[@"visible_if_code"] :nil;
        attribute.visibleIfValue  = [dict[@"visible_if_value"] isKindOfClass:[NSString class]] ? dict[@"visible_if_value"] :nil;
        attribute.selectableValues = dict[@"values"];
    
        // Set default visibility
        attribute.visible = @(((!attribute.requiredIfCode) && attribute.requiredIfCode.length==0)
                            &&((!attribute.visibleIfCode ) && attribute.visibleIfCode.length ==0));
    }
    
    return attribute;
}

- (void)updateDomainWithID:(NSNumber *)domainID andTitle:(NSString *)title andWarning:(BOOL)warning
{
    [self domainWithID:domainID];

    NSNumber *unsupported = [NSNumber numberWithBool:warning];

    if (domain != nil) {
        if (title && !(domain.title && [title isEqualToString:domain.title]))
            domain.title = title;
        if (![unsupported isEqualToNumber:domain.unsupported])
            domain.unsupported = unsupported;
    }
    else {
        self.domain = [NSEntityDescription insertNewObjectForEntityForName:@"Domain"
                                                    inManagedObjectContext:managedObjectContext];
        domain.domainID = domainID;
        domain.server = server;
        domain.title = title;
        domain.unsupported = [NSNumber numberWithBool:warning];
        domain.defaultTypeName = @"";
    }

    domain.coordinate = coordinate;
    domain.lastSeen = [NSDate date];
}

- (void)updateReportTypesWithArray:(NSArray *)types
{
    // make a property list from the array
    NSMutableArray *list = [[NSMutableArray alloc] initWithCapacity:types.count/2];

    bool needDefault = YES;
    for (NSArray *array in types) {
        if ([array isKindOfClass:NSArray.class]) {
            NSString *imageName = [NSString stringWithFormat:kMarkerFormat, [array objectAtIndex:0]];
            
            NSDictionary *entry = [NSDictionary dictionaryWithObjectsAndKeys:
								   [array objectAtIndex:4], @"myid",
								   [array objectAtIndex:0], @"ID",
								   [array objectAtIndex:1], @"name", 
								   imageName,               @"imageName", 
								   [array objectAtIndex:2], @"isPrivate",
								   [array objectAtIndex:3], @"isIdentifictionNeeded",
                                   [self attributeSetForReportTypeArray:array], @"attributes",
                                   [self hasTitleForReportTypeArray:array],     @"hasSubjectTitle",
								   nil];
			//domain.reportTypeForName([array objectAtIndex:1]).ID=[array objectAtIndex:0];
			//[domain reportTypeForName:[array objectAtIndex:1]].ID=[array objectAtIndex:0];
			[list addObject:entry];
            if (needDefault) {
                domain.defaultTypeName = [array objectAtIndex:1];
                needDefault = NO;
            }
        }
    }

    [self updateParent:domain withArray:list deletingOld:YES];
}

- (NSOrderedSet *)attributeSetForReportTypeArray:(NSArray *)array
{
    NSMutableArray *attributesArray = [[NSMutableArray alloc] init];
    
    if ([array count] > 5) {
        NSArray *unformattedAttributesArray = [array objectAtIndex:5];
        
        for (NSDictionary *dict in unformattedAttributesArray) {
            [attributesArray addObject:[self typeAttributeFromDictionary:dict]];
        }
    }
    
    NSOrderedSet *set = [NSOrderedSet orderedSetWithArray:[NSArray arrayWithArray:attributesArray]];
    
    return set;
}

- (NSNumber *)hasTitleForReportTypeArray:(NSArray *)array
{
    if (array.count > 6) {
        return [array objectAtIndex:6];
    }
    // Default value = 0 (=> NO title)
    else return [NSNumber numberWithInt:0];
}

#pragma mark ServerConnectionDelegate

- (void)serverConnectionDidFinishLoading:(ServerConnection *)serverConnection
{
    NSDictionary *domainList = [serverConnection jsonData];

    if ([domainList isKindOfClass:NSDictionary.class]) {
        NSNumber *domainID = [domainList objectForKey:kDomainProperty];
        NSString *title = [domainList objectForKey:kNameProperty];
        BOOL warning = [[domainList objectForKey:kDefaultProperty] boolValue];
        [self updateDomainWithID:domainID andTitle:title andWarning:warning];

        NSArray *types = [domainList objectForKey:kTypesProperty];
        
        if ([types isKindOfClass:NSArray.class]) {
            [self updateReportTypesWithArray:types];
        }
    }
#if LOG_LEVEL > 0
    else {
        NSLog(@"Could not understand domain result");
    }
#endif

    [self->delegate performSelector:self->selector];
    self->delegate = nil;
}

@end
