//
//  AbstractJSONAdapter.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 08.07.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id$
//

#import "AbstractJSONAdapter.h"

#import "ServerConnection.h"

@implementation AbstractJSONAdapter


- (id)init
{
    self = [super init];
    if (self != nil) {
        _connection = [[ServerConnection alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [_connection cancel];
    _connection = nil;

}

#pragma mark API

- (void)cancel
{
    [_connection cancel];

    delegate = nil;
}

- (void)queryWithServer:(NSString *)server method:(NSString *)method parameters:(NSDictionary *)parameters
{
    [_connection queryWithServer:server method:method parameters:parameters delegate:self];
}

#pragma mark ServerConnectionDelegate

- (void)serverConnectionDidFinishLoading:(ServerConnection *)serverConnection
{
    [self doesNotRecognizeSelector:_cmd];
}

#pragma mark Core Data

- (void)initializeObject:(id)object withProperties:(NSDictionary *)properties
{
    for (id key in _mapping) {
        id value = [properties objectForKey:[_mapping objectForKey:key]];
        [object setValue:value forKey:key];
    }
}

- (void)updateObject:(id)object withProperties:(NSDictionary *)properties
{
    for (id key in _mapping) {
        id value = [properties objectForKey:[_mapping objectForKey:key]];
        if (![[object valueForKey:key] isEqual:value]) {
            [object setValue:value forKey:key];
        }
    }
}

- (void)deleteStaleObjects:(NSArray *)values
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:_entityName inManagedObjectContext:_managedObjectContext];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@ AND NOT %K IN %@", _parentKey, _parent, _entityKey, values];

    NSError *error = nil;
    NSArray *oldObjects = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];

    // delete stale entries
    for (NSManagedObject *object in oldObjects) {
        [_managedObjectContext deleteObject:object];
    }
}

- (NSArray *)existingObjectsForKeys:(NSArray *)values
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:_entityName
                                      inManagedObjectContext:_managedObjectContext];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@ AND %K IN %@", _parentKey,
                              _parent, _entityKey, values];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:_entityKey
                                                                   ascending:YES];
    fetchRequest.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];

    NSError *error = nil;
    NSArray *existingObjects = [_managedObjectContext executeFetchRequest:fetchRequest
                                                                    error:&error];

    return existingObjects;
}

- (void)insertObjects:(NSArray *)newProperties existingObjects:(NSArray *)existingObjects
{
    NSEnumerator *existingObject = [existingObjects objectEnumerator];
    id object = [existingObject nextObject];

    for (NSDictionary *properties in newProperties) {
        id value = [properties objectForKey:_propertyKey];
        
        if ([value isEqual:[object valueForKey:_entityKey]]) {
            // update object
            [self updateObject:object withProperties:properties];
            NSError *error = nil;
            if (![object validateForUpdate:&error]) {
                LogError(@"Update", error);
                [_managedObjectContext deleteObject:object];
            }
            
            object = [existingObject nextObject];
        }
        else {
            id newObject = [NSEntityDescription insertNewObjectForEntityForName:_entityName
                                                         inManagedObjectContext:_managedObjectContext];
            // initialize new object
            [newObject setValue:value forKey:_entityKey];
            [self initializeObject:newObject withProperties:properties];
           
            [newObject setValue:_parent forKey:_parentKey];
            NSError *error = nil;
            if (![newObject validateForInsert:&error]) {
                LogError(@"Insert", error);
                [_managedObjectContext deleteObject:newObject];
            }
        }
    }
}

- (void)updateParent:(NSManagedObject *)parent withArray:(NSArray *)list deletingOld:(BOOL)purge
{
    _parent = parent;
    _managedObjectContext = parent.managedObjectContext;

    @autoreleasepool {

    // sort names and fetch existing sorted names
        NSSortDescriptor *listSortDescriptor = [[NSSortDescriptor alloc] initWithKey:_propertyKey ascending:YES];
        // store contents of list in ascending order according to their value stored under key _propertyKey
        NSArray *sortedList = [list sortedArrayUsingDescriptors:[NSArray arrayWithObject:listSortDescriptor]];

        // Additionally store messageIDs of objects in ascending order
        NSArray *messageIDs = [sortedList valueForKeyPath:_propertyKey];

        if (purge)
            [self deleteStaleObjects:messageIDs];

        // Get all already existing ReportTypes from storage
        NSArray *existingReportsArray = [self existingObjectsForKeys:messageIDs];

        [self insertObjects:sortedList existingObjects:existingReportsArray];

#if LOG_LEVEL > 1
        NSLog(@"%@ : %d received (%d new)", _entityName, sortedList.count, sortedList.count - existingReportsArray.count);
#endif

    }

    NSError *error = nil;
    if (![_managedObjectContext save:&error]) {
        [_managedObjectContext rollback];
        LogError(@"Update save", error);
    }

    _parent = nil;
    _managedObjectContext = nil;
}

@end
