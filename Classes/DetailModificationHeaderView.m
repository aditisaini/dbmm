//
//  DetailModificationHeaderView.m
//  WerDenktWas
//
//  Created by Franziska Engelmann on 09.11.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "DetailModificationHeaderView.h"

#import "UserUpdate.h"
#import "UpdatePicture.h"


@implementation DetailModificationHeaderView

@synthesize update=_update;
@synthesize currentPhotoContainer;
@synthesize updatePhotoContainer;
@synthesize currentReportMapView;
@synthesize updatePhotoView;
@synthesize emptyPhotoView;
@synthesize currentPhotoView;
@synthesize detailDisclosureButton;
@synthesize currentDetailsLabel;

- (void)setRoundCorners
{
    NSArray *array = [[NSArray alloc] initWithObjects:currentPhotoContainer,
                      updatePhotoContainer, currentReportMapView, nil];
    
    for (UIView *aView in array) {
        CALayer *layer = aView.layer;
        
        layer.masksToBounds = YES;
        layer.cornerRadius  = 10.0;
        layer.borderColor   = [UIColor grayColor].CGColor;
        layer.borderWidth   = 1.0;
    }
    
}

- (void)updatePhotoViewInitial:(BOOL)initial
{
    self->photoView.image = self->_report.photo;
    self->updatePhotoView.image = self->_update.updatePicture.image;
    
    BOOL shouldHideCurrentPhotoView = self->_report.picture == nil;
    BOOL shouldHideEmptyPhotoView = self.updatePhotoView.image != nil;
    
    [self activateActivityIndicator:NO];
    [self setCurrentPhotoViewInvisible:shouldHideCurrentPhotoView];
    self.emptyPhotoView.hidden = shouldHideEmptyPhotoView;
}

- (void)setUpdatePhotoViewToImage:(UIImage *)newImage
{    
    if (newImage) {
        updatePhotoView.image = newImage;
        self.emptyPhotoView.hidden = YES;
    }
}

- (void)setCurrentPhotoViewInvisible:(BOOL)shouldHide
{
    if (shouldHide) {
        self->updatePhotoContainer.frame = CGRectUnion(self->currentPhotoContainer.frame, self->updatePhotoContainer.frame);
        self->currentPhotoContainer.hidden = YES;
    }
    else {
        self->currentPhotoContainer.frame = self.currentPhotoContainer.frame;
        self->currentPhotoContainer.hidden = NO;
    }
}

- (void)activateActivityIndicator:(BOOL)isActive
{
    if (isActive) {
        [self.activityView startAnimating];
    }
    else {
        [self.activityView stopAnimating];
    }
}

- (void)setEmptyPhotoViewInvisible:(BOOL)shouldHide
{
    if (!self.updatePhotoView.image) {
        self.emptyPhotoView.hidden = NO;
    }
    else {
        self.emptyPhotoView.hidden = YES;
    }
}

- (void)setUpdate:(UserUpdate *)update
{
    if (_update != update) {
        if (_update) {
            [_update removeObserver:self forKeyPath:@"updatePicture"];
        }
        if ((_update = update)) {
            [self updatePhotoViewInitial:YES];
            [_update addObserver:self forKeyPath:@"updatePicture" options:0 context:NULL];
        }
    }
}

#pragma mark -
#pragma mark NSKeyValueObserver

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == _update) {
        if ([keyPath isEqualToString:@"updatePicture"]) {
            [self updatePhotoViewInitial:NO];
        }
    }
}

- (void)dealloc
{
    [_update removeObserver:self forKeyPath:@"updatePicture"];
}

@end
