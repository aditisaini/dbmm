//
// Created by Martin on 07.05.15.
// Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractJSONAdapter.h"

@class MapBoundingBox;


@interface POIJSONAdapter : AbstractJSONAdapter

- (void)queryForPOIForBoundingBox:(MapBoundingBox *)boundingBox delegate:(id)aDelegate selector:(SEL)aSelector;

@end