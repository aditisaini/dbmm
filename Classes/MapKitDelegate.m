//
//  MapKitDelegate.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 14.07.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: MapKitDelegate.m 74 2010-07-14 22:00:16Z eik $
//

#import "MapKitDelegate.h"

#import "ReportAnnotation.h"
#import "UserReport.h"
#import "ServerReport.h"
#import "ReportAnnotationView.h"
#import "DraggableAnnotationView.h"
#import "PointOfInterest.h"

@implementation MapKitDelegate

@synthesize delegate;
@synthesize creatingMovableAnnotations;
@synthesize enablingCallouts;

#pragma mark -
#pragma mark Memory management

- (id)init
{
    if ((self = [super init])) {
        creatingMovableAnnotations = NO;
    }

    return self;
}


#pragma mark -
#pragma mark MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    for (MKAnnotationView *annotationView in views) {
        if ([annotationView.annotation isKindOfClass:MKUserLocation.class]) {
            CLLocation *location = mapView.userLocation.location;
            if ([(id)delegate respondsToSelector:@selector(startAtLocation:)])
                [delegate startAtLocation:location];
        }
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    // user location
    if ([annotation isKindOfClass:MKUserLocation.class]) {
        return nil;
    }

    if ([annotation conformsToProtocol:@protocol(ReportAnnotationProtocol)]) {
        id <ReportAnnotationProtocol> reportAnnotation = (id <ReportAnnotationProtocol>) annotation;
        ReportAnnotationView *annotationView = [ReportAnnotationView reportAnnotationViewForMapView:mapView
                                                                                     withAnnotation:reportAnnotation
                                                                                            movable:creatingMovableAnnotations && [reportAnnotation isMovable]];
        if (enablingCallouts)
            [annotationView enableCallout];

        return annotationView;
    }

    return nil;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)theView calloutAccessoryControlTapped:(UIControl *)control
{
    if ([theView.annotation isKindOfClass:ServerReport.class]) {
        if ([(id)delegate respondsToSelector:@selector(showDetails:)])
            [delegate showDetails:(id <ReportAnnotationProtocol>)theView.annotation];
    }
    if ([theView.annotation isKindOfClass:PointOfInterest.class]) {
        if ([(id)delegate respondsToSelector:@selector(showDetails:)])
            [delegate showDetails:(id <ReportAnnotationProtocol>)theView.annotation];
    }
    else if ([theView.annotation isKindOfClass:UserReport.class]) {
        if ([(id)delegate respondsToSelector:@selector(editReport:)])
            [delegate editReport:(id <ReportAnnotationProtocol>)theView.annotation];
    }
    else if ([theView.annotation isKindOfClass:MKUserLocation.class]) {
        if ([(id)delegate respondsToSelector:@selector(createReport)])
            [delegate createReport];
    }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if ([(id)delegate respondsToSelector:@selector(movedToLocation:)]) {
        CLLocation *location = [[CLLocation alloc] initWithLatitude:mapView.centerCoordinate.latitude longitude:mapView.centerCoordinate.longitude];
		[(id)delegate performSelector:@selector(movedToLocation:)
								 withObject:location
								 afterDelay:3.0f
		 ];	
      //  [delegate movedToLocation:location];
    }
}

@end
