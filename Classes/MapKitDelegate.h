//
//  MapKitDelegate.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 14.07.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: MapKitDelegate.h 74 2010-07-14 22:00:16Z eik $
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class ReportAnnotation;

@protocol MapActionDelegate

- (void)showDetails:(ReportAnnotation *)annotation;
- (void)editReport:(ReportAnnotation *)annotation;
- (void)createReport;
- (void)startAtLocation:(CLLocation *)location;

@optional

- (void)movedToLocation:(CLLocation *)location;

@end

@interface MapKitDelegate : NSObject <MKMapViewDelegate>
{
#if !__OBJC2__
    __weak id <MapActionDelegate> delegate;
#endif
    BOOL creatingMovableAnnotations;
    BOOL enablingCallouts;
}

@property (nonatomic, strong) IBOutlet id <MapActionDelegate> delegate;
@property (nonatomic, assign, getter=isCreatingMovableAnnotations) BOOL creatingMovableAnnotations;
@property (nonatomic, assign, getter=isEnablingCallouts) BOOL enablingCallouts;

@end
