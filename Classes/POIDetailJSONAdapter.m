//
// Created by Martin on 11.05.15.
// Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import "POIDetailJSONAdapter.h"
#import "Constants.h"
#import "PointOfInterest.h"
#import "POIDetails.h"
#import "WKTParser.h"


@interface POIDetailJSONAdapter ()

@property(nonatomic, strong) PointOfInterest *pointOfInterest;

@end


@implementation POIDetailJSONAdapter

- (id)init {
    self = [super init];

    if (self != nil) {
    }

    return self;
}

- (void)queryWithPOI:(PointOfInterest *)pointOfInterest delegate:(id)aDelegate selector:(SEL)aSelector {
    self->delegate = aDelegate;
    self->selector = aSelector;
    self.pointOfInterest = pointOfInterest;

    NSString *server = [[NSUserDefaults standardUserDefaults] stringForKey:kPOIHostKey];

    NSDictionary *parameters = @{
            @"id" : [pointOfInterest.id stringValue]
    };

    NSString *method = [NSString stringWithFormat:@"poi/%@/", pointOfInterest.id];

    [self queryWithServer:server method:method parameters:parameters];
}

#pragma mark ServerConnectionDelegate

- (void)serverConnectionDidFinishLoading:(ServerConnection *)serverConnection {
    id jsonData = [serverConnection jsonData];

    if ([jsonData isKindOfClass:NSDictionary.class]) {
        [self parseServerResult:jsonData];
    }

#if LOG_LEVEL > 0
    else {
        NSLog(@"Could not understand domain result");
    }
#endif

    [self->delegate performSelector:self->selector withObject:self.pointOfInterest];
    self->delegate = nil;
}

- (void)parseServerResult:(NSDictionary *)jsonData {
    [self parsePictureData:jsonData];
    [self parseMessageData:jsonData];
    [self parseGeometryData:jsonData];
}

- (void)parseMessageData:(NSDictionary *)jsonData {
    NSMutableArray *poiDetailsArray = [[NSMutableArray alloc] init];

    id details = jsonData[@"details"];
    if ([details isKindOfClass:NSArray.class]) {
        NSArray *detailsJSONArray = (NSArray *) details;

        for (int i = 1; i < detailsJSONArray.count; i += 2) {
            POIDetails *poiDetails = [[POIDetails alloc] init];
            poiDetails.name = detailsJSONArray[i - 1];
            poiDetails.content = detailsJSONArray[i];

            [poiDetailsArray addObject:poiDetails];
        }
    }

    self.pointOfInterest.messageDetails = poiDetailsArray;
}

- (void)parseGeometryData:(NSDictionary *)jsonData {
    __autoreleasing NSError *error;
    id <WKTObject> shape = [WKTParser wktObjectWithString:jsonData[@"geometry"] error:&error];
    self.pointOfInterest.geometry = shape;
    if (error) {
        NSLog(@"error");
    }
}

- (void)parsePictureData:(NSDictionary *)jsonData {
    NSString *pictureURL = jsonData[@"pictureUrl"];
    if ((id) pictureURL == [NSNull null]) {
        pictureURL = nil;
    }

    self.pointOfInterest.pictureURL = pictureURL;
}

@end