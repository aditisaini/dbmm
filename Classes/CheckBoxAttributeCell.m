//
//  CheckBoxAttributeCell.m
//  WerDenktWas
//
//  Created by Franziska Engelmann on 15.10.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import "CheckBoxAttributeCell.h"

@implementation CheckBoxAttributeCell

@synthesize isSelected=_isSelected;
@synthesize attribute;

- (id)initWithAttribute:(TypeAttribute *)theAttribute reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        self.attribute = theAttribute;
        self.textLabel.textColor = [UIColor blackColor];
        self.textLabel.font      = [UIFont systemFontOfSize:16.0];
        
        self.textLabel.text = theAttribute.name;
        [self adaptAccessoryView];
        
        if ([theAttribute.answer length] > 0) {
            self.isSelected = [attribute.answer boolValue];
        } else {
            self.isSelected = NO;
        }
    }
    
    return self;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        if (!self.isSelected) {
            _isSelected = NO;
        }
        self.textLabel.textColor = [UIColor colorWithRed:0.265 green:0.294 blue:0.367 alpha:1.0];
        [self adaptAccessoryView];
//        [self arrangeLabel];
    }
    return self;
}


- (void)setToAttribute:(TypeAttribute *)theAttribute
{
    self.attribute = theAttribute;
    
    self.textLabel.text = theAttribute.name;
    [self adaptAccessoryView];
    
    if ([theAttribute.answer length] > 0) {
        self.isSelected = [attribute.answer boolValue];
    } else {
        self.isSelected = NO;
    }
}


- (void)adaptAccessoryView
{
    // Outsource string! --> better image!
    UIImage *checkbox_empty = [UIImage imageNamed:@"checkbox_empty.png"];
    UIImage *checkbox_checked = [UIImage imageNamed:@"checkbox_selected.png"];
    
    if (self.selected) {
        self.accessoryView = [[UIImageView alloc] initWithImage:checkbox_checked];
    } else {
        self.accessoryView = [[UIImageView alloc] initWithImage:checkbox_empty];
    }
    
    self.accessoryView.frame = CGRectMake(0.0, 0.0, 20.0, 20.0);
}

- (void)setIsSelected:(BOOL)selected
{
    self.accessoryType = (selected ?
                          UITableViewCellAccessoryCheckmark :
                          UITableViewCellAccessoryNone);

    _isSelected = selected;
    [self adaptAccessoryView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)dealloc
{
    self.accessoryView = nil;
    
}

@end
