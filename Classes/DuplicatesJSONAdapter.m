//
//  DuplicatesJSONAdapter.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 05.03.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "DuplicatesJSONAdapter.h"
#import "SBJson.h"
#import "Constants.h"
#import "UserReport.h"
#import "ReportDuplicate.h"
#import "ReportType.h"
#import "Domain.h"

static NSString * kPictureURL   = @"pictureUrl";
static NSString * kDistance     = @"distance";
static NSString * kDescription  = @"description";
static NSString * kColor        = @"color";
static NSString * kMarkerID     = @"markerid";
static NSString * kMessageID    = @"messageid";
static NSString * kState        = @"state";
static NSString * kType         = @"type";
static NSString * kLongitude    = @"long";
static NSString * kLatitude     = @"lat";
static NSString * kDomain       = @"domain";
static NSString * kReport       = @"userReport";

@implementation DuplicatesJSONAdapter

@synthesize report=_report;

- (id)initWithReport:(UserReport *)aReport
{
    if (!aReport) {
        return nil;
    }
    
    self = [super init];
    self.report = aReport;
    
    if (self != nil) {
        self->_parentKey    = @"userReport"; //The relationship that should be kept up to date
        self->_entityName   = @"ReportDuplicate";
        self->_entityKey    = @"messageID"; //--> the messageid-property is written in that way
        self->_propertyKey  = @"messageid";
        
        self->_mapping = [[NSDictionary alloc] initWithObjectsAndKeys:
                          kPictureURL,  @"pictureURL",
                          kDistance,    kDistance,
                          kDescription, @"title",
                          kColor,       kColor,
                          kMarkerID,    kMarkerID,
                          kMessageID,   @"messageID",
                          kState,       kState,
                          kType,        kType,
                          kLongitude,   @"longitude",
                          kLatitude,    @"latitude",
                          kDomain,      kDomain,
                          nil];
        
    }
    return self;
}

- (void)queryDuplicatesForReport:(UserReport *)aReport delegate:(id)aDelegate selector:(SEL)aSelector
{
    self.report = aReport;
    self->delegate = aDelegate;
    self->selector = aSelector;
	
	NSString *localization = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSString stringWithFormat:@"%lf", _report.coordinate.latitude],    @"lat",
                                [NSString stringWithFormat:@"%lf", _report.coordinate.longitude],   @"long",
                                [[NSUserDefaults standardUserDefaults] stringForKey:kAppIDKey],     @"appid",
                                localization,                                                       @"lang",
                                [NSString stringWithFormat:@"%d", [_report.reportDomain.domainID intValue]], @"domainid",
                                aReport.type2.myid,                                                 @"categoryid",
                                nil];
    
    [self queryWithServer:_report.reportDomain.server method:@"find_duplicates" parameters:parameters];
}

- (void)addReportDuplicatesWithArray:(NSArray *)duplicates
{
    // make a property list from the array
    NSMutableArray *duplicatesList = [[NSMutableArray alloc] initWithCapacity:duplicates.count/2];
    
    for (NSDictionary *dict in duplicates) {

        Domain *domain      = self.report.domain;
        UserReport *report  = self.report;
        NSString *title     = [dict objectForKey:kDescription];
        NSString *color     = [dict objectForKey:kColor];
        NSNumber *latitude  = [dict objectForKey:kLatitude];
        NSNumber *longitude = [dict objectForKey:kLongitude];
        NSString *markerid  = [dict objectForKey:kMarkerID];
        NSNumber *messageID = [dict objectForKey:kMessageID];
        NSString *state     = [dict objectForKey:kState];
        NSString *type      = [dict objectForKey:kType];
        NSNumber *distance  = [dict objectForKey:kDistance];
        
        NSString *pictureURL = [dict objectForKey:kPictureURL];
        
        if ((id)pictureURL == [NSNull null])
        {
            pictureURL = nil;
//            pictureURL = @"";
//            // if url is null, supplement mapping with url-key{
//            NSMutableDictionary *oldMapping = [NSMutableDictionary dictionaryWithDictionary:self->_mapping];
//            [oldMapping removeObjectForKey:@"pictureURL"];
//            self->_mapping = [NSDictionary dictionaryWithDictionary:oldMapping];
        }
        
        NSMutableDictionary *entry = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      report,       kReport,
                                      domain,       kDomain,
                                      title,        kDescription,
                                      color,        kColor,
                                      latitude,     kLatitude,
                                      longitude,    kLongitude,
                                      markerid,     kMarkerID,
                                      messageID,    kMessageID,
                                      state,        kState,
                                      type,         kType,
                                      distance,     kDistance,
                                      pictureURL,   kPictureURL,
                                      nil];
        
//        if ((id)pictureURL == [NSNull null]) {//[[dict objectForKey:kPictureURL] isKindOfClass:[NSString class]]) {
//            [entry removeObjectForKey:kPictureURL];
//        }
        
        [duplicatesList addObject:[NSDictionary dictionaryWithDictionary:entry]];
    }
    
    
    [self updateParent:_report withArray:duplicatesList deletingOld:YES];
    
}

- (void)addReportDuplicateWithDictionary:(NSDictionary *)dict
{
    // make a property list from the array
    NSMutableArray *duplicatesList = [[NSMutableArray alloc] initWithCapacity:1];
    
    Domain *domain      = self.report.domain;
    UserReport *report  = self.report;
    NSString *title     = [dict objectForKey:kDescription];
    NSString *color     = [dict objectForKey:kColor];
    NSNumber *latitude  = [dict objectForKey:kLatitude];
    NSNumber *longitude = [dict objectForKey:kLongitude];
    NSString *markerid  = [dict objectForKey:kMarkerID];
    NSNumber *messageID = [dict objectForKey:kMessageID];
    NSString *state     = [dict objectForKey:kState];
    NSString *type      = [dict objectForKey:kType];
    NSNumber *distance  = [dict objectForKey:kDistance];
    
    NSString *pictureURL = [dict objectForKey:kPictureURL];
    
    if ((id)pictureURL == [NSNull null])
    {
        pictureURL = nil;
        pictureURL = @"";
        // if url is null, supplement mapping with url-key{
        NSMutableDictionary *oldMapping = [NSMutableDictionary dictionaryWithDictionary:self->_mapping];
        [oldMapping removeObjectForKey:@"pictureURL"];
        self->_mapping = [NSDictionary dictionaryWithDictionary:oldMapping];
    }

    
    NSMutableDictionary *entry = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  report,       kReport,
                                  domain,       kDomain,
                                  title,        kDescription,
                                  color,        kColor,
                                  latitude,     kLatitude,
                                  longitude,    kLongitude,
                                  markerid,     kMarkerID,
                                  messageID,    kMessageID,
                                  state,        kState,
                                  type,         kType,
                                  distance,     kDistance,
                                  pictureURL,   kPictureURL,
                                  nil];
    
    if ((id)pictureURL == [NSNull null]) {//[[dict objectForKey:kPictureURL] isKindOfClass:[NSString class]]) {
        [entry removeObjectForKey:kPictureURL];
    }
    
    [duplicatesList addObject:[NSDictionary dictionaryWithDictionary:entry]];
    
    
    [self updateParent:_report withArray:duplicatesList deletingOld:NO];
    
}


#pragma mark ServerConnectionDelegate

- (void)serverConnectionDidFinishLoading:(ServerConnection *)serverConnection
{
    NSArray *possibleDuplicates = [serverConnection jsonData];
    
    if ([possibleDuplicates isKindOfClass:NSArray.class]) {
    }
#if LOG_LEVEL > 0
    else {
        NSLog(@"Could not understand details result");
    }
#endif
    [self addReportDuplicatesWithArray:possibleDuplicates];

//    for (NSDictionary *duplicate in possibleDuplicates) {
//        [self addReportDuplicateWithDictionary:duplicate];
//    }
    
    [self->delegate performSelector:self->selector withObject:possibleDuplicates];
    self->delegate = nil;
}


@end
