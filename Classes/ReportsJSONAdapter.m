//
//  ReportsJSONAdapter.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 23.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ReportsJSONAdapter.m 74 2010-07-14 22:00:16Z eik $
//

#import "ReportsJSONAdapter.h"
#import "Constants.h"
#import "ServerConnection.h"

#import "Domain.h"
#import "ServerReport.h"

#define kMessageIDProperty      @"messageid"
#define kLatitudeProperty       @"lat"
#define kLongitudeProperty      @"long"
#define kTypeProperty           @"type"
#define kDescriptionProperty    @"description"
#define kStateProperty          @"state"
#define kMarkeridProperty       @"markerid"

@interface ReportsJSONAdapter ()

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) Domain *domain;

@end


@implementation ReportsJSONAdapter

@synthesize coordinate;
@synthesize domain = _domain;

- (id)initWithDomain:(Domain *)domain
{
    if (!domain) {
        return nil;
    }

    self = [super init];
    if (self != nil) {
        self->_parentKey = @"domain";
        self->_entityName = @"ServerReport";
        self->_entityKey = @"messageID";
        self->_propertyKey = kMessageIDProperty;

        self->_mapping = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 kDescriptionProperty, @"title",
                                 kStateProperty, @"state",
                                 kLatitudeProperty, @"latitude",
                                 kLongitudeProperty, @"longitude",
								 kMarkeridProperty,@"markerid",
								 @"color",@"color",
                                 nil];

        self.domain = domain;
    }
    return self;
}


#pragma mark API

- (void)queryWithLocation:(CLLocation *)location cumulative:(BOOL)cumulative delegate:(id)aDelegate selector:(SEL)aSelector;
{
    coordinate = location.coordinate;

    self->delegate = aDelegate;
    self->selector = aSelector;

    _cumulative = cumulative;

	NSString *localization = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
	
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSString stringWithFormat:@"%lf", coordinate.latitude], @"lat",
                                [NSString stringWithFormat:@"%lf", coordinate.longitude], @"long",
                                [NSString stringWithFormat:@"%d", [_domain.domainID intValue]], @"domainid",
//								[[UIDevice currentDevice]uniqueIdentifier],@"phone",
                                [[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"], @"phone",
								[[NSUserDefaults standardUserDefaults] stringForKey:kAppIDKey], @"appid",
								localization,@"lang",
                                nil];

    [self queryWithServer:_domain.server method:@"get_nearest_messages" parameters:parameters];
}

#pragma mark Core Data

- (void)initializeObject:(id)object withProperties:(NSDictionary *)properties
{
    [super initializeObject:object withProperties:properties];

    id value = [_domain reportTypeForName:[properties valueForKey:kTypeProperty]];
    [object setValue:value forKey:@"type2"];
}

- (void)updateObject:(id)object withProperties:(NSDictionary *)properties
{
    [super updateObject:object withProperties:properties];

    id value = [_domain reportTypeForName:[properties valueForKey:kTypeProperty]];
    if (![[object valueForKey:@"type2"] isEqual:value])
        [object setValue:value forKey:@"type2"];
}

#pragma mark ServerConnectionDelegate

- (void)serverConnectionDidFinishLoading:(ServerConnection *)serverConnection
{
    NSArray *list = [serverConnection jsonData];

    if ([list isKindOfClass:NSArray.class]) {
        if (!_cumulative) {
            _domain.reportsCoordinate = coordinate;
            _domain.reportsLastSeen = [NSDate date];
        }

        [self updateParent:_domain withArray:list deletingOld:!_cumulative];
    }
#if LOG_LEVEL > 0
    else {
        NSLog(@"Could not understand reports result");
    }
#endif

    [self->delegate performSelector:self->selector];
    self->delegate = nil;
}

@end
