//
//  AGBViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 12.08.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "AGBViewController.h"
#import "MMColor.h"

@interface AGBViewController ()

@end

@implementation AGBViewController

@synthesize delegate=_delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;
    
    self.navigationItem.title = NSLocalizedStringWithDefaultValue(@"agb", nil, [NSBundle mainBundle], @"AGBs", nil);
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"terms" ofType:@"html"]isDirectory:NO]]];
    
    [[self.toolbar.items objectAtIndex:3] setTitle:NSLocalizedStringWithDefaultValue(@"agree", nil, [NSBundle mainBundle], @"Agree", nil)];
    [[self.toolbar.items objectAtIndex:1] setTitle:NSLocalizedStringWithDefaultValue(@"disagree", nil, [NSBundle mainBundle], @"Disagree", nil)];
    [self setToolbarItems:self.toolbar.items];
    
    if (self.presentingViewController) {
        [self.navigationController setToolbarHidden:NO animated:YES];
    } else {
        [self.navigationController setToolbarHidden:YES animated:NO];

        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"back_button_title", nil)
                                                                                  style:UIBarButtonItemStylePlain
                                                                                 target:self
                                                                                 action:@selector(backButtonClicked:)];
    }
}

- (IBAction)backButtonClicked:(id)sender
{
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:YES];
    
    [self.navigationController popViewControllerAnimated:NO];
    
	[UIView commitAnimations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
    [self setWebView:nil];
    [self setAgreeButton:nil];
    [self setDisagreeButton:nil];
    [self setToolbar:nil];
    [super viewDidUnload];
}

- (IBAction)agreeButtonClicked:(id)sender
{
    [self.delegate agbHasBeenAccepted];
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)disagreeButtonClicked:(id)sender
{
    [self.delegate agbHasBeenDeclined];
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return NO;
}

@end
