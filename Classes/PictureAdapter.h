//
//  PictureAdapter.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 22.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: PictureAdapter.h 78 2010-07-15 16:33:13Z eik $
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "ServerConnection.h"

@class ServerReport;

@interface PictureAdapter : NSObject <ServerConnectionDelegate>
{
@private
    id delegate;
    SEL selector;
    ServerConnection *connection;

#if !__OBJC2__
    ServerReport *report;
#endif
}

@property (nonatomic, strong) ServerReport *report;

- (void)queryReport:(ServerReport *)report delegate:(id)delegate selector:(SEL)selector;
- (void)cancel;

@end
