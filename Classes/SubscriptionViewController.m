//
//  SubscriptionViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 12.03.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "SubscriptionViewController.h"

#import "EmailAdapter.h"
#import "ServerReport.h"
#import "ReportDuplicate.h"
#import "UserReport.h"

#import "Constants.h"
#import "MMAlertViewHandler.h"

@interface SubscriptionViewController ()

@end

@implementation SubscriptionViewController
@synthesize shiftableView;
@synthesize titleLabel;
@synthesize textView;
@synthesize rightBarButton;
@synthesize leftBarButton;
@synthesize textField;
@synthesize containerScrollView;

@synthesize report;
@synthesize emailAdapter;

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;

    // Stop navigationbar from overlapping (under iOS7)
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.textField.delegate = self;
    self.textField.placeholder = NSLocalizedStringWithDefaultValue(@"final_email_default_text", nil, [NSBundle mainBundle], @"Enter email adress", @"");
    self.textField.text = [[NSUserDefaults standardUserDefaults] stringForKey:kEmailAddressKey];


    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    NSString *title = NSLocalizedStringWithDefaultValue(@"subscription_title", nil,
                                                        [NSBundle mainBundle],
                                                        @"Do you want to stay informed of this report's status?", @"");
    
    NSString *text = NSLocalizedStringWithDefaultValue(@"subscription_info_text", nil,
                                                       [NSBundle mainBundle],
                                                       @"You just have to insert your email adress in order to keep up with this report.\n\nAn email will be sent to you if there are any news concerning this report.", @"");
    
    NSString *send = NSLocalizedStringWithDefaultValue(@"send_button_title", nil,
                                                       [NSBundle mainBundle],
                                                       @"Send", @"");
        
    
    self.titleLabel.text = title;
    self.textView.text  = text;
    
    self.rightBarButton = [[UIBarButtonItem alloc] initWithTitle:send
                                                           style:UIBarButtonItemStyleBordered
                                                          target:self
                                                          action:@selector(sendButtonClicked:)];
        
    self.navigationItem.rightBarButtonItem = self.rightBarButton;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setToolbarHidden:(self.toolbarItems == nil) animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[MMAlertViewHandler sharedInstance] cancelActivityIndicatingAlert];
}

- (void)viewDidUnload
{
    [self setRightBarButton:nil];
    [self setLeftBarButton:nil];
    [self setTitleLabel:nil];
    [self setTextView:nil];
    [self setTextField:nil];
    [self setContainerScrollView:nil];
    [self setShiftableView:nil];
    [super viewDidUnload];
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)theTextField
{
    textField.textColor = [UIColor blackColor];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                 target:self
                                                                                 action:@selector(doneButtonClicked:)];
    
    self.navigationItem.rightBarButtonItem = doneButton;
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)theTextField
{
    self.emailAdress = theTextField.text;
    self.navigationItem.rightBarButtonItem = self.rightBarButton;
    [[NSUserDefaults standardUserDefaults] setObject:theTextField.text forKey:kEmailAddressKey];

    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.textField resignFirstResponder];
    
    return YES;
}

#pragma mark keyboard notifications

- (void)moveViewWithKeyboard:(NSNotification *)aNotification show:(BOOL)show
{
    if (keyboardShown == show)
        return;
    
    // Get the size of the keyboard.
    CGRect keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect toView:nil];
    CGFloat keyboardHeight = keyboardRect.size.height;
    
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    // Shift the view
    CGRect viewFrame = shiftableView.frame;
    if (show)
        viewFrame.origin.y -= keyboardHeight;
    else
        viewFrame.origin.y += keyboardHeight;
    
    [UIView beginAnimations:@"MoveForKeyboard" context:NULL];
    [UIView setAnimationDuration:animationDuration];
    shiftableView.frame = viewFrame;
    
    [UIView commitAnimations];
    
    keyboardShown = show;
}


#pragma mark keyboard notifications
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    [self moveViewWithKeyboard:aNotification show:YES];
}


// Called when the UIKeyboardDidHideNotification is sent
- (void)keyboardWillHide:(NSNotification *)aNotification
{
    [self moveViewWithKeyboard:aNotification show:NO];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}


- (IBAction)doneButtonClicked:(id)sender
{
    [self.textField resignFirstResponder];
}

- (IBAction)sendButtonClicked:(id)sender
{
    if (self.emailAdress.length < 6
               || ([self.emailAdress rangeOfString:@"."].location == NSNotFound)
               || ([self.emailAdress rangeOfString:@"@"].location == NSNotFound)
               || ([self.emailAdress rangeOfString:@"@"].location >= self.emailAdress.length - 4)
               || ([self.emailAdress rangeOfString:@"@"].location < 1)
               || (self.emailAdress.length <= 0)) {
        [[MMAlertViewHandler sharedInstance] showMissingEmailAddressAlert];
    } else
    {
        [[MMAlertViewHandler sharedInstance] showActivityIndicatingAlertWithMessage:NSLocalizedString(@"uploading_email", nil)
                                                                           andTitle:NSLocalizedString(@"please_wait", nil)];
        emailAdapter = [[EmailAdapter alloc] init];
        [emailAdapter subscribeToReport:self.report withEmail:self.emailAdress delegate:self selector:@selector(emailDone)];
    }
}

- (void)emailDone
{
    NSInteger result = emailAdapter.result;
    [[MMAlertViewHandler sharedInstance] cancelActivityIndicatingAlert];
    
    if (result != 1) {
        
        [[MMAlertViewHandler sharedInstance] showEmailSubscriptionFailureAlert];
        self.navigationItem.rightBarButtonItem.enabled = true;
    }
    else {
        
        [[MMAlertViewHandler sharedInstance] showAlertWithMessage:NSLocalizedString(@"subscription_success_msg", nil)
                                                            title:NSLocalizedString(@"subscription_success_title", nil)
                                              andButtonClickBlock:^(NSInteger index)
         {
             [self.navigationController popViewControllerAnimated:YES];
             
         }];
    }
}

- (IBAction)cancelButtonClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
