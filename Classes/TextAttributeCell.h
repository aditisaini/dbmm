//
//  TextAttributeCell.h
//  WerDenktWas
//
//  Created by Franziska Engelmann on 25.09.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TypeAttribute.h"

@protocol TextAttributeDelegate <NSObject>

- (void)textAttribute:(TypeAttribute*)attribute isSetOnValue:(NSString *)value;

@end

@interface TextAttributeCell : UITableViewCell <UITextViewDelegate, UITextFieldDelegate>
{
    IBOutlet UITextField *textField;    
    BOOL hasText;
    
    TypeAttribute *attribute;
    id<TextAttributeDelegate> _delegate;
}

@property (nonatomic, strong) id delegate;
@property (nonatomic, strong) TypeAttribute *attribute;
@property (strong, nonatomic) IBOutlet UITextField *textField;

- (id)initWithAttribute:(TypeAttribute *)theAttribute provideNumericKeyboard:(BOOL)numeric reuseIdentifier:(NSString *)reuseIdentifier;
- (void)setToAttribute:(TypeAttribute *)theAttribute provideNumericKeyboard:(BOOL)numeric;


@end
