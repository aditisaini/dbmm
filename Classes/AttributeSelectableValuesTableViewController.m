//
//  AttributeSelectableValuesTableViewController.m
//  WerDenktWas
//
//  Created by Franziska Engelmann on 01.10.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import "AttributeSelectableValuesTableViewController.h"
#import "MMTableView.h"

@interface AttributeSelectableValuesTableViewController ()

@end

@implementation AttributeSelectableValuesTableViewController

@synthesize attribute;
@synthesize types;
@synthesize selectedPath;

static const NSString * kText = @"text";
static const NSString * kID   = @"id";

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;
    
    self.tableView = [[MMTableView alloc] initWithFrame:self.tableView.frame style:self.tableView.style];

    if (self.attribute.answer.length > 0) {
        NSInteger rowIndex = [self.attribute valuesArrayIndexForValueID:self.attribute.answer];
        self.selectedPath = [NSIndexPath indexPathForRow:rowIndex inSection:0];
    }
    
    if (!self.selectedPath || self.selectedPath.row == -1) {
        self.selectedPath = [NSIndexPath indexPathForRow:0 inSection:0];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.types      = nil;
    self.selectedPath = nil;
    self.delegate   = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.types count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    if ([indexPath isEqual:selectedPath]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.text = [(NSDictionary *)[self.attribute.selectableValues objectAtIndex:indexPath.row] objectForKey:kText];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedPath) {
        UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:selectedPath];
        oldCell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    self.selectedPath = indexPath;
}

#pragma mark - Action handling

- (IBAction)clickCancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickSave:(id)sender
{
    if (self.attribute.selectableValues.count > 0) {
        // Store the unique ID of selected value NOT its text
        attribute.answer                = [(NSDictionary *)[self.attribute.selectableValues objectAtIndex:self.selectedPath.row] objectForKey:kID];
        self.attribute.selectedValueID  = [(NSDictionary *)[self.attribute.selectableValues objectAtIndex:self.selectedPath.row] objectForKey:kID];

        [self.delegate selectableAttribute:self.attribute
                              isSetOnValue:[(NSDictionary *)[self.attribute.selectableValues objectAtIndex:self.selectedPath.row] objectForKey:kText]];
    }
        
    [self.navigationController popViewControllerAnimated:YES];
}

@end
