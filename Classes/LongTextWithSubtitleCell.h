//
//  LongTextWithSubtitleCell.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 15.03.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "LongTextCell.h"

@interface LongTextWithSubtitleCell : LongTextCell

@end
