//
//  DialogAttributesViewController.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 10.01.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import "DetailCell.h"

#import "ReportType.h"
#import "UserReport.h"
#import "TextAttributeCell.h"
#import "AttributeSelectableValuesTableViewController.h"
#import "CheckBoxAttributeCell.h"

@interface AttributesViewController : UITableViewController <TextAttributeDelegate, SelectionAttributeDelegate, NSFetchedResultsControllerDelegate, DetailCellDelegate>

@property (strong, nonatomic) UIViewController *basisPresentingViewController;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSArray *visibleAttributes;

@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UITextView *headerTextView;
@property (nonatomic, strong) UIBarButtonItem *rightBarButton;
@property (strong, nonatomic) IBOutlet DetailCell *detailCell;

@property (nonatomic, strong) ReportType *reportType;
@property (nonatomic, strong) UserReport *userReport;

@end
