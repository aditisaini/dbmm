//
//  ServerConnection.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 22.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ServerConnection.m 76 2010-07-15 00:24:01Z eik $
//

#import "ServerConnection.h"
#import <UIKit/UIKit.h>
#import "SBJsonParser.h"
#import "Constants.h"
#import "MMAlertViewHandler.h"
#import "Reachability.h"
#import "Ma_ngelmelder-Swift.h"

@implementation ServerConnection

@synthesize receivedData;
@synthesize mimeType;

- (void)dealloc
{
    [self cancel];
}

- (void)cancel
{
    if (activeConnection) {
        [activeConnection cancel];
        activeConnection = nil;

        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }

    delegate = nil;

    self.receivedData = nil;
    self.mimeType = nil;
}

#pragma mark -
#pragma mark API

- (void)queryWithServer:(NSString *)server method:(NSString *)method parameters:(NSDictionary *)parameters delegate:(id <ServerConnectionDelegate>)aDelegate
{
    NSMutableString *theURL = [NSMutableString stringWithCapacity:110];

    
    [theURL appendString:server];
    [theURL appendString:method];
    
#undef NSLOG
    NSLog(@"%@", theURL);

    BOOL first = YES;
    for (NSString *key in [parameters allKeys]) {
        if (first) {
            [theURL appendString:@"?"];
            first = NO;
        }
        else {
            [theURL appendString:@"&"];
        }

        [theURL appendString:key];
        [theURL appendString:@"="];
        [theURL appendString:[(NSString *)[parameters objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }

    [self startWithURL:theURL delegate:aDelegate];
}

- (void)startWithURL:(NSString *)theURL delegate:(id <ServerConnectionDelegate>)aDelegate
{
    // By now, only availability of internet connection is checked (no server availability)
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable) {
        [[MMAlertViewHandler sharedInstance] showNetworkAlert];
    }
    
#if LOG_LEVEL > 0
    NSLog(@"Query (%d): %@", theURL.length, theURL);
#endif

    NSURL *connectionURL = [NSURL URLWithString:theURL];

    NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:connectionURL
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:60.0];
    [theRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];

    [self startWithRequest:theRequest delegate:aDelegate];
}

- (void)startWithRequest:(NSURLRequest *)theRequest delegate:(id)aDelegate
{
    if ([aDelegate conformsToProtocol:@protocol(ServerConnectionDelegate)])
        delegate = aDelegate;
    else
        [NSException raise:NSInternalInconsistencyException format:@"Delegate doesn't conform to protocol ServerConnectionDelegate"];

    if (theRequest) {
        delegate = aDelegate;

        self.receivedData = [NSMutableData data];
        self.mimeType = nil;

        [activeConnection cancel];
        activeConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    }
    else {
        [NSException raise:NSInvalidArgumentException format:@"Request may not be nil"];
    }

}

- (id)jsonData
{
    id retVal = nil;

    if ([mimeType caseInsensitiveCompare:@"application/json"] == NSOrderedSame && receivedData != NULL) {
        
        NSString *jsonString = [NSJSONSerialization JSONObjectWithData:receivedData
                                                               options:kNilOptions
                                                                 error:nil];
        
//        SBJsonParser *jsonParser = [[SBJsonParser alloc] init];
//        NSString *jsonString = [[NSString alloc] initWithData:receivedData
//                                                     encoding:NSUTF8StringEncoding];

#if LOG_LEVEL > 0
        NSLog(@"Response: %@", jsonString);
#endif
        retVal = [NSJSONSerialization JSONObjectWithData:receivedData
                                                 options:kNilOptions
                                                   error:nil];
        //[jsonParser objectWithString:jsonString];
    }
#if LOG_LEVEL > 0
    else {
        NSLog(@"Incompatible mime type (expected application/json): %@", mimeType);
    }
#endif

    return retVal;
}

#pragma mark -
#pragma mark NSURLConnection delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
    self.mimeType = [response MIMEType];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    activeConnection = nil;

    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    self.mimeType = nil;
    self.receivedData = nil;

    if ([delegate respondsToSelector:@selector(serverConnectionDidFinishLoading:)])
        [delegate serverConnectionDidFinishLoading:self];

    delegate = nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    activeConnection = nil;

    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    if ([delegate respondsToSelector:@selector(serverConnectionDidFinishLoading:)])
        [delegate serverConnectionDidFinishLoading:self];

    delegate = nil;
    self.mimeType = nil;
    self.receivedData = nil;
}


@end
