//
//  DomainJSONAdapter.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 23.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: DomainJSONAdapter.h 72 2010-07-14 13:21:40Z eik $
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <CoreData/CoreData.h>
#import "SBJson.h"
#import "AbstractJSONAdapter.h"

@class Domain;

@interface DomainJSONAdapter : AbstractJSONAdapter
{
@private
#if !__OBJC2__
    Domain *domain;
    NSSet *attributes;
#endif

    NSManagedObjectContext *managedObjectContext;

    CLLocationCoordinate2D coordinate;
    NSString *server;
}

@property (nonatomic, strong) Domain *domain;
@property (nonatomic, strong) NSSet *attributes;

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void)queryServer:(NSString *)server withLocation:(CLLocation *)location delegate:(id)aDelegate selector:(SEL)aSelector;

@end
