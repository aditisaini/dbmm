//
//  Picture.m
//  WerDenktWas
//
//  Created by Franziska Engelmann on 19.11.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import "Picture.h"


@implementation Picture

@dynamic image;
@dynamic imageData;


- (UIImage *)image {
    return [UIImage imageWithData:self.imageData];
}

+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key
{
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
    
    if ([key isEqualToString:@"image"])
    {
        keyPaths = [keyPaths setByAddingObject:@"imageData"];
    }
    
    return keyPaths;
}


@end
