//
//  DetailCell.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 19.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: DetailCell.h 77 2010-07-15 09:13:19Z eik $
//

#import <UIKit/UIKit.h>

@protocol DetailCellDelegate <NSObject>

- (void)detailCellTextIsSetOnValue:(NSString *)value;

@end

@interface DetailCell : UITableViewCell <UITextViewDelegate>
{
@private
#if !__OBJC2__
    UITextView *textView;

    NSString *emptyText;
    BOOL hasText;
#endif
    id<DetailCellDelegate> _delegate;
}

- (void)setDetailText:(NSString *)text;

@property (nonatomic, strong) id delegate;
@property (nonatomic, strong) IBOutlet UITextView *textView;
@property (nonatomic, strong) NSString *emptyText;

@property (nonatomic) BOOL hasText;
@property (nonatomic, strong) NSString *text;

@end
