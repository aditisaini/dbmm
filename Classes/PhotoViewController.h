//
//  ReportPhotoViewController.h
//  MaengelmelderCore
//
//  Created by Franzi Engelmann on 29.11.13.
//  Copyright (c) 2013 WerDenktWas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserReport;

@interface PhotoViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) UserReport *userReport;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *missingImageLabel;

@property (weak, nonatomic) IBOutlet UILabel *descriptionTitleLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UIView *descriptionView;

- (void)saveLatestPicture;
- (IBAction)galleryButtonClicked:(id)sender;
- (IBAction)cameraButtonClicked:(id)sender;

@end
