//
//  DetailHeaderViewExtension.swift
//  WerDenktWas
//
//  Created by Albert Tra on 27/01/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import Foundation

extension DetailHeaderView {
    func test(string: NSString) {
        _ = Logger(sender: self, message: string)
    }
}