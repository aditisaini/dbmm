//
//  AboutViewController.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 10.05.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: AboutViewController.h 72 2010-07-14 13:21:40Z eik $
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>


@interface AboutViewController : UIViewController <MFMailComposeViewControllerDelegate>
{
@private
#if !__OBJC2__
    UIScrollView *scrollView;
    UIView *contentView;
    UILabel *versionLabel;

    UIBarButtonItem *backButton;
#endif
}

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UILabel *versionLabel;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *backButton;

- (IBAction)backButtonClicked:(id)sender;

@end
