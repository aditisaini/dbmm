//
// Created by Martin on 07.05.15.
// Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "POIJSONAdapter.h"
#import "Constants.h"
#import "MapBoundingBox.h"
#import "PointOfInterest.h"


@implementation POIJSONAdapter

- (id)init {
    self = [super init];

    if (self != nil) {
    }

    return self;
}

- (void)queryForPOIForBoundingBox:(MapBoundingBox *)boundingBox delegate:(id)aDelegate selector:(SEL)aSelector {

    self->delegate = aDelegate;
    self->selector = aSelector;

    NSString *server = [[NSUserDefaults standardUserDefaults] stringForKey:kPOIHostKey];


    NSDictionary *parameters = @{
            @"right" : [[boundingBox top] stringValue],
            @"left" : [[boundingBox bottom] stringValue],
            @"bottom" : [[boundingBox left] stringValue],
            @"top" : [[boundingBox right] stringValue],
//API-Logikfehler, deswegen andere Werte bei uns
//            @"top" : [[boundingBox top] stringValue],
//            @"bottom" : [[boundingBox bottom] stringValue],
//            @"left" : [[boundingBox left] stringValue],
//            @"right" : [[boundingBox right] stringValue],
            @"format" : @"app",
            @"rows" : [@(100) stringValue]
    };

    [self queryWithServer:server method:@"poi/list.json" parameters:parameters];
}

#pragma mark ServerConnectionDelegate

- (void)serverConnectionDidFinishLoading:(ServerConnection *)serverConnection {
    NSMutableArray *pois = [[NSMutableArray alloc] init];
    id poisJSON = [[serverConnection jsonData] valueForKey:@"marker"];

    if ([poisJSON isKindOfClass:NSArray.class]) {
        pois = [self parseServerResult:poisJSON];
    }

#if LOG_LEVEL > 0
    else {
        NSLog(@"Could not understand domain result");
    }
#endif

    [self->delegate performSelector:self->selector withObject:pois];
    self->delegate = nil;
}

- (NSMutableArray *)parseServerResult:(NSArray*)jsonArray {
    NSMutableArray *pois = [[NSMutableArray alloc] init];

    for(NSDictionary *dict in jsonArray) {
      PointOfInterest *poi = [[PointOfInterest alloc] init];
        poi.color = dict[@"color"];
        poi.markerID = @([dict[@"markerid"] integerValue]);
        poi.title = dict[@"title"];
        poi.id = @([dict[@"id"] integerValue]);

        CLLocationCoordinate2D coord;
        coord.longitude = [dict[@"lon"] doubleValue];
        coord.latitude = [dict[@"lat"] doubleValue];
        poi.coordinate = coord;

        [pois addObject:poi];
    }

    return pois;
}

@end