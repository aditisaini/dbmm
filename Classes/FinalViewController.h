//
//  FinalViewController.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 22.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: FinalViewController.h 72 2010-07-14 13:21:40Z eik $
//

#import <UIKit/UIKit.h>

@class UploadAdapter;
@class UserReport;
@class KVObserver;
@class EmailAdapter;
@class UserUpdate;
@class DetailModificationViewController;

@protocol DismissCommentNavigationStackDelegate <NSObject>
- (void)dismissCommentNavigationStack;
@end

@protocol DismissFinalViewControllerDelegate <NSObject>
- (void)dismissFinalViewController;
@end

@interface FinalViewController : UIViewController <UITextFieldDelegate>
{
@private
#if !__OBJC2__
    UIView *shiftableView;

    UIActivityIndicatorView *activityView;
    UIBarButtonItem *cancelButton;
    UIBarButtonItem *doneButton;
    UIView *successView;
    UISwitch *wantsEmail;

    UILabel *statusLabel;
    UITextView *statusText;
    UILabel *emailLabel;
    UITextField *emailText;

    UserReport *userReport;
    UserUpdate *userUpdate;
#endif

    UploadAdapter *uploadAdapter;
    EmailAdapter *emailAdapter;

    NSInteger messageID;
    
    BOOL keyboardShown;
    
    id <DismissCommentNavigationStackDelegate> _delegate;

}

@property (nonatomic, strong) id delegate;

@property (nonatomic, strong) IBOutlet UIView *shiftableView;

@property (nonatomic, strong) IBOutlet UILabel *statusLabel;
@property (nonatomic, strong) IBOutlet UITextView *statusText;
@property (nonatomic, strong) IBOutlet UILabel *emailLabel;
@property (nonatomic, strong) IBOutlet UITextField *emailText;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityView;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *cancelButton;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *doneButton;
@property (nonatomic, strong) IBOutlet UIView *successView;
@property (nonatomic, strong) IBOutlet UISwitch *wantsEmail;
@property (nonatomic, strong) UserReport *userReport;
@property (nonatomic, strong) UserUpdate *userUpdate;

- (IBAction)cancelButtonUp:(id)sender;

- (IBAction)doneButtonClicked:(id)sender;

@end
