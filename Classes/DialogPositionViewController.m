//
//  DialogPositionViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 31.01.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import "DialogPositionViewController.h"
#import "DialogTypeSelectionViewController.h"

#import "MovableAnnotation.h"
#import "ReportAnnotation.h"
#import "DraggableAnnotationView.h"

#import "UserReport.h"

#import "DomainJSONAdapter.h"
#import "GeoCoordinateJSONAdapter.h"
#import "Constants.h"


@interface DialogPositionViewController ()
{
    GeoCoordinateJSONAdapter *geoCoordinateJSONAdapter;
    DomainJSONAdapter *domainJSONAdapter;
}

@end

@implementation DialogPositionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"next_button_title", nil)
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(nextStepButtonClicked:)];
    self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem;
    
    // Setup description texts
    self.descriptionView.hidden = NO;
    self.descriptionTitleLabel.text     = NSLocalizedString(@"position_step_header", nil);
    self.descriptionTextView.text       = NSLocalizedString(@"position_step_description", nil);

    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        self.descriptionTextView.selectable  = NO;
    }
}

- (IBAction)nextStepButtonClicked:(id)sender
{
    [self saveStreetAddressOnFinish:^(){
        [self pushTypeSelectionViewController];
    }];
}

- (IBAction)cancelButtonClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^(){}];
}

- (void)pushTypeSelectionViewController
{
    DialogTypeSelectionViewController *typeSelectionViewController = [[DialogTypeSelectionViewController alloc] initWithNibName:@"TypeSelectionViewController" bundle:[NSBundle mainBundle]];
    typeSelectionViewController.annotation = self.userReport;
    typeSelectionViewController.managedObjectContext = self.managedObjectContext;
    [self.navigationController pushViewController:typeSelectionViewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
