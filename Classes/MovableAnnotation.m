//
//  MovableAnnotation.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 10.05.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: MovableAnnotation.m 74 2010-07-14 22:00:16Z eik $
//

#import "MovableAnnotation.h"

#import "Constants.h"

#import "ReportType.h"

@implementation MovableAnnotation

@synthesize coordinate;
@synthesize title;
@synthesize subtitle;

@synthesize image;

- (id)initWithAnnotation:(id <ReportAnnotationProtocol>)annotation
{
    if ((self = [super init])) {
        coordinate = annotation.coordinate;
        self.image = annotation.image;

        self.title = annotation.title;
        self.subtitle = annotation.subtitle;
    }
    return self;
}


- (BOOL)isMovable
{
    return YES;
}

@end
