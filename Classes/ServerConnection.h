//
//  ServerConnection.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 22.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ServerConnection.h 72 2010-07-14 13:21:40Z eik $
//

#import <Foundation/Foundation.h>

@protocol ServerConnectionDelegate;

@interface ServerConnection : NSObject
{
@private
#if !__OBJC2__
    NSMutableData *receivedData;
    NSString *mimeType;
#endif

    NSURLConnection *activeConnection;
    id delegate;
}

@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic, strong) NSString *mimeType;

- (void)queryWithServer:(NSString *)server method:(NSString *)method parameters:(NSDictionary *)parameters delegate:(id <ServerConnectionDelegate>)aDelegate;
- (void)startWithURL:(NSString *)theURL delegate:(id <ServerConnectionDelegate>)delegate;
- (void)startWithRequest:(NSURLRequest *)theRequest delegate:(id <ServerConnectionDelegate>)delegate;
- (void)cancel;

- (id)jsonData;

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;

@end

@protocol ServerConnectionDelegate

- (void)serverConnectionDidFinishLoading:(ServerConnection *)serverConnection;

@end

