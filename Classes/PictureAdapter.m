//
//  PictureAdapter.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 22.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: PictureAdapter.m 78 2010-07-15 16:33:13Z eik $
//

#import "PictureAdapter.h"

#import "Constants.h"
#import "ServerConnection.h"

#import "ServerReport.h"
#import "ReportPicture.h"

#import "Ma_ngelmelder-Swift.h"

@implementation PictureAdapter

@synthesize report;

- (id)init
{
    self = [super init];
    if (self != nil) {
        connection = [[ServerConnection alloc] init];
    }
    return self;
}

- (void)queryReport:(ServerReport *)aReport delegate:(id)aDelegate selector:(SEL)aSelector
{
    delegate = aDelegate;
    selector = aSelector;

    self.report = aReport;
    
    [connection startWithURL:report.pictureURL delegate:self];
}

- (void)dealloc
{
    [connection cancel];
    connection = nil;


}

- (void)cancel
{
    [connection cancel];

    delegate = nil;
}

#pragma mark ServerConnectionDelegate

- (void)serverConnectionDidFinishLoading:(ServerConnection *)serverConnection
{
    NSRange range = [serverConnection.mimeType rangeOfString:@"image/" options:NSAnchoredSearch|NSCaseInsensitiveSearch];
    if (range.location != NSNotFound) {
        NSManagedObjectContext *managedObjectContext = report.managedObjectContext;
        UIImage *picture = nil;

        if (serverConnection.receivedData)
            picture = [[UIImage alloc ] initWithData:serverConnection.receivedData];

        if (picture) {
#if LOG_LEVEL > 1
            NSLog(@"Received picture %d octets", serverConnection.receivedData.length);
#endif
            if (managedObjectContext) {
                ReportPicture *reportPicture = [NSEntityDescription insertNewObjectForEntityForName:@"ReportPicture" inManagedObjectContext:managedObjectContext];
                
                reportPicture.imageData = serverConnection.receivedData;
                reportPicture.report = self.report;
                
                NSError *error = nil;
                if (![managedObjectContext save:&error])
                    LogError(@"Picture save", error);
            } else {
            }
        }
        else {
#if LOG_LEVEL > 0
            NSLog(@"Could not interpret picture data");
#endif
            self.report.picture = nil;
        }

    }
    else {
#if LOG_LEVEL > 0
        NSLog(@"Incompatible mime type (expected image/*): %@", serverConnection.mimeType);
#endif
        self.report.picture = nil;
    }

    if ([delegate respondsToSelector:selector]) {
        [delegate performSelector:selector];
    }
    delegate = nil;
}

@end
