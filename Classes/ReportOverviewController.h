//
//  ReportOverviewController.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 31.01.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportHeaderView.h"

#import "UserReport.h"

@interface ReportOverviewController : UITableViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, weak) IBOutlet ReportHeaderView *headerView;
@property (nonatomic, strong) UserReport *userReport;

- (IBAction)trashButtonClicked:(id)sender;
- (IBAction)sendButtonClicked:(id)sender;


@end
