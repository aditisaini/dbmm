//
//  DetailModificationViewController.h
//  WerDenktWas
//
//  Created by Franziska Engelmann on 09.11.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailModificationHeaderView;
@class ServerReport;
@class UserUpdate;
@class DetailViewController;
@class PictureViewController;
@class UpdatePicture;
@class DetailCell;
@class CheckBoxAttributeCell;
//@class FinalViewController;
#import "FinalViewController.h"

@interface DetailModificationViewController : UITableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UIAlertViewDelegate>
{
    ServerReport *_report;
    UserUpdate *_update;
    DetailCell *_detailCell;
    
    UIImage *_commentImage;
    
    id <DismissCommentNavigationStackDelegate> _delegate;
}

@property (nonatomic, strong) id delegate;

@property (nonatomic, strong) ServerReport *report;
@property (nonatomic, strong) UserUpdate *update;
@property (strong, nonatomic) IBOutlet DetailCell *detailCell;

@property (strong, nonatomic) UIImage *commentImage;

@property (strong, nonatomic) IBOutlet DetailModificationHeaderView *headerView;
@property (strong, nonatomic) IBOutlet UITextView *promptTextView;
@property (strong, nonatomic) IBOutlet UILabel *currentPictureLabel;
@property (strong, nonatomic) IBOutlet UILabel *updatedPictureLabel;

@property (strong, nonatomic) IBOutlet UIButton *imageButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sendButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@property (strong, nonatomic) NSString *comment;
@property (assign, nonatomic) BOOL solved;

- (IBAction)clickNewImageButton:(id)sender;
- (IBAction)clickImageButton:(id)sender;
- (IBAction)clickCancelButton:(id)sender;
- (IBAction)clickSendButton:(id)sender;
- (IBAction)clickDoneButton:(id)sender;


@end

