//
//  DomainbundleJSONAdapter.m
//  WerDenktWas
//
//  Created by kom tu-darmstadt on 11.11.11.
//  Copyright (c) 2011 Robert Lokaiczyk. All rights reserved.
//

#import "DomainbundleJSONAdapter.h"
#import "Constants.h"
#import "ServerConnection.h"
#import "Domain.h"
#import "ReportType.h"

#import "Constants.h"

#define kDomainProperty         @"domainid"
#define kNameProperty           @"name"
#define kTypesProperty          @"types"
#define kDefaultProperty        @"isDefault"

#define kImageNameProperty      @"imageName"



@implementation DomainbundleJSONAdapter

@synthesize domain;

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext_
{
    self = [super init];
    if (self != nil) {
        managedObjectContext = managedObjectContext_;
        
        self->_parentKey = @"domain";
        self->_entityName = @"ReportType";
        self->_entityKey = @"myid";
        self->_propertyKey = @"myid";
        
        self->_mapping = [[NSDictionary alloc] initWithObjectsAndKeys:
                          kImageNameProperty, kImageNameProperty,
                          kNameProperty,kNameProperty,
						  nil];
    }
    return self;
}


#pragma mark API

//MK: we ask the server for the current domain depending on our position
// here we prepare to tranform arguments to the parameterlist 
- (void)queryServer:(NSString *)aServer withLocation:(CLLocation *)location delegate:(id)aDelegate selector:(SEL)aSelector;
{
    
    self->delegate = aDelegate;
    self->selector = aSelector;
    
    coordinate = location.coordinate;
    
//    NSLog(@" Query JSON address for GPS location lat : %f ", coordinate.latitude);
//    NSLog(@" Query JSON address for GPS location lon : %f ", coordinate.longitude);

    
    server = aServer;
    
    NSString *localization = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
	//NSLocale *locale = [NSLocale currentLocale];
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSString stringWithFormat:@"%lf", coordinate.latitude], @"lat",
                                [NSString stringWithFormat:@"%lf", coordinate.longitude], @"long",
                                [NSString stringWithFormat:@"%lf", location.horizontalAccuracy], @"accuracy",	
//								[[UIDevice currentDevice]uniqueIdentifier],@"phone",
                                [[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"], @"phone",
								[[NSUserDefaults standardUserDefaults] stringForKey:kAppIDKey], @"appid",
								localization, @"lang",
                                nil];
    
    [self queryWithServer:server method:@"get_domainbundle" parameters:parameters];
    
    
}

#pragma mark Core Data

- (void)domainWithID:(NSNumber *)domainID
{
    
     
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];

    fetchRequest.entity = [NSEntityDescription entityForName:@"Domain" inManagedObjectContext:managedObjectContext];
  
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(domainID == %@) AND (server == %@)", domainID, server];

    fetchRequest.fetchLimit = 1;
    //[fetchRequest setIncludesPropertyValues:NO];
    

    NSError *error = nil;
    
  
    NSArray *domains = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    

    if (domains.count > 0)
    {

        self.domain = [domains objectAtIndex:0];
    }
    else{
      
        self.domain = nil;
    }
}


- (void)updateDomainWithID:(NSNumber *)domainID andTitle:(NSString *)title andWarning:(BOOL)warning

{
  
    [self domainWithID:domainID];
   
    
    NSNumber *unsupported = [NSNumber numberWithBool:warning];
    
    if (domain != nil) {
        if (title && !(domain.title && [title isEqualToString:domain.title]))
            domain.title = title;
        
        if (![unsupported isEqualToNumber:domain.unsupported])
            domain.unsupported = unsupported;
    }
    else {
        self.domain = [NSEntityDescription insertNewObjectForEntityForName:@"Domain" inManagedObjectContext:managedObjectContext];
        domain.domainID = domainID;
        domain.server = server;
        domain.title = title;
        domain.unsupported = [NSNumber numberWithBool:warning];
        domain.defaultTypeName = @"";
    }
    
    domain.coordinate = coordinate;
    domain.lastSeen = [NSDate date];
}


// type decoding (string and id) of the jason result is done here 
//
// types: [
//          ["5","Falschparker"],["2","Graffiti"],["3","Müllablagerung"],["1","Schlagloch"], ["4","Sonstiges"]
// ]
//

- (void)updateReportTypesWithArray:(NSArray *)types
{
    // make a property list from the array
    NSMutableArray *list = [[NSMutableArray alloc] initWithCapacity:types.count/2];
    
    bool needDefault = YES;
    
    NSLog(@"#################################");
    NSLog(@"Decode Datatypestring");

    for (NSArray *array in types) {
        if ([array isKindOfClass:NSArray.class]) {
            
            NSString *imageName = [NSString stringWithFormat:kMarkerFormat, [array objectAtIndex:0]];
            
            NSDictionary *entry = [NSDictionary dictionaryWithObjectsAndKeys:
								   [array objectAtIndex:4], @"myid",
								   [array objectAtIndex:0], @"ID",
								   [array objectAtIndex:1], @"name", 
								   imageName, @"imageName", 
								   [array objectAtIndex:2], @"isPrivate",
								   [array objectAtIndex:3], @"isIdentifictionNeeded",
								   nil];

            
            
            
			//domain.reportTypeForName([array objectAtIndex:1]).ID=[array objectAtIndex:0];
			//[domain reportTypeForName:[array objectAtIndex:1]].ID=[array objectAtIndex:0];
            
			[list addObject:entry];
            if (needDefault) {
                domain.defaultTypeName = [array objectAtIndex:1];
                needDefault = NO;
            }
        }
    }
    NSLog(@"#################################");
    
    [self updateParent:domain withArray:list deletingOld:YES];
}

#pragma mark ServerConnectionDelegate

- (void)serverConnectionDidFinishLoading:(ServerConnection *)serverConnection
{
    //MK:
    // it seems as the base class calls serverConnectionDidFinishLoading and pass over the serverConnection object.
    // The *domainList is the key-value list for the expected resulting arguments.
    // The defined properties like "kDomainProperty" are used to transform
    // the general KV list into the related attibutes
    
    // NSLog(@"called DomainbundleJSONAdapter.serverConnectionDidFinishLoading");
    
    NSDictionary *domainList = [serverConnection jsonData];
    
    if ([domainList isKindOfClass:NSDictionary.class]) {
        // decode simple values here
        NSNumber *domainID = [domainList objectForKey:kDomainProperty];
        NSString *title = [domainList objectForKey:kNameProperty];
        
        
        //[domainList objectForKey:kAddressProperty];
        NSLog(@"#################################");
        NSLog(@"DomainID string %@", domainID);
        NSLog(@"Title string %@", title);
        NSLog(@"#################################");
        
        
        // here any kind of warning is included???
//        BOOL warning = [[domainList objectForKey:kDefaultProperty] boolValue];
//        [self updateDomainWithID:domainID andTitle:title andWarning:warning];
        
        // all types are decoded here
//        NSArray *types = [domainList objectForKey:kTypesProperty];
//        if ([types isKindOfClass:NSArray.class]) {
//            [self updateReportTypesWithArray:types];
//        }
        
    }
#if LOG_LEVEL > 0
    else {
        NSLog(@"Could not understand domain result");
    }
#endif
    
//     NSLog(@"call back the delegate");
    //MK: I think the selector is a method which is called after this method has been finished
//    [self->delegate performSelector:self->selector];
    [self->delegate performSelector:self->selector withObject:domainList];
    self->delegate = nil;
}

@end
