//
//  DetailJSONAdapter.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 22.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: DetailJSONAdapter.m 75 2010-07-14 22:30:48Z eik $
//

#import "DetailJSONAdapter.h"

#import "Constants.h"
#import "ServerConnection.h"
#import "ServerReport.h"
#import "ReportDetail.h"
#import "Domain.h"

#define kPictureURLProperty     @"pictureUrl"
#define kDetailsProperty        @"details"
#define kExtrasProperty         @"extras"

@implementation DetailJSONAdapter

@synthesize report;

- (id)init
{
    self = [super init];
    if (self != nil) {
        self->_parentKey    = @"report";
        self->_entityName   = @"ReportDetail";
        self->_entityKey    = @"order";
        self->_propertyKey  = @"order";

        self->_mapping = [[NSDictionary alloc] initWithObjectsAndKeys:
                          @"name", @"name",
                          @"content", @"content",
                          nil];
    }
    return self;
}


- (void)queryReport:(ServerReport *)aReport delegate:(id)aDelegate selector:(SEL)aSelector
{
    self.report = aReport;

    self->delegate = aDelegate;
    self->selector = aSelector;
	
	NSString *localization = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];

    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSString stringWithFormat:@"%d", [report.messageID intValue]], @"id",
//                                [NSMainBundle user];
//                                [[UIDevice currentDevice]uniqueIdentifier], @"phone",
                                [[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"], @"phone",
                                [[NSUserDefaults standardUserDefaults] stringForKey:kAppIDKey], @"appid",
                                localization, @"lang",
                                nil];

    [self queryWithServer:report.domain.server method:@"get_message_detail" parameters:parameters];
}

#pragma mark Core Data
- (void)addReportDetailsWithArray:(NSArray *)details
{
    // make a property list from the array
    NSMutableArray *list = [[NSMutableArray alloc] initWithCapacity:details.count/2];
    
    for (NSUInteger i = 0; i < details.count/2; i++) {
        NSString *name = [details objectAtIndex:2*i];
        NSString *content = [details objectAtIndex:2*i+1];
        NSNumber *order = [NSNumber numberWithUnsignedInteger:i];

        NSDictionary *entry = [NSDictionary dictionaryWithObjectsAndKeys:
                               name,    @"name",
                               content, @"content",
                               order,   @"order",
                               nil];
        [list addObject:entry];
    }


    report.detailsLastSeen = [NSDate date];
    [self updateParent:report withArray:list deletingOld:YES];
}



- (NSArray *)reportDetailsArrayFromExtrasArray:(NSArray *)extras andDetailsArray:(NSArray *)details
{
    NSMutableArray *mergedList = [[NSMutableArray alloc] initWithArray:details];

    // index*2, as the details' contents are alternatingly stored along with their names
    if (details.count >= 4 && extras.count >= 2) {
        NSRange range = NSMakeRange(4, extras.count);
        NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:range];
        [mergedList insertObjects:extras atIndexes:indexes];
    }
    
    NSArray *finalList = [NSArray arrayWithArray:mergedList];
        
    return finalList;
}

#pragma mark ServerConnectionDelegate

- (void)serverConnectionDidFinishLoading:(ServerConnection *)serverConnection
{
    NSDictionary *details = [serverConnection jsonData];

    if ([details isKindOfClass:NSDictionary.class]) {
        NSString *pictureURL = [details objectForKey:kPictureURLProperty];
        if ((id)pictureURL == [NSNull null])
        {
            pictureURL = nil;
        }
//        if (pictureURL &&
//            !(report.pictureURL && [pictureURL isEqualToString:report.pictureURL]))
//        {
//            report.pictureURL = pictureURL;
//            // Throws update save error!
//            report.picture = nil;
//        }
        if (pictureURL &&
            !(report.pictureURL && [pictureURL isEqualToString:report.pictureURL])) {
            report.pictureURL = pictureURL;
        }


        NSArray *mergedDetails = [self reportDetailsArrayFromExtrasArray:[details objectForKey:kExtrasProperty] andDetailsArray:[details objectForKey:kDetailsProperty]];
        [self addReportDetailsWithArray:mergedDetails];
    }
#if LOG_LEVEL > 0
    else {
        NSLog(@"Could not understand details result");
    }
#endif

    [self->delegate performSelector:self->selector];
    self->delegate = nil;
}

@end
