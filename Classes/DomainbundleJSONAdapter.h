//
//  DomainbundleJSONAdapter.h
//  WerDenktWas
//
//  Created by kom tu-darmstadt on 11.11.11.
//  Copyright (c) 2011 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <CoreData/CoreData.h>

#import "AbstractJSONAdapter.h"

@class Domain;

@interface DomainbundleJSONAdapter : AbstractJSONAdapter
{
@private
#if !__OBJC2__
    Domain *domain;
#endif
    
    NSManagedObjectContext *managedObjectContext;
    
    CLLocationCoordinate2D coordinate;
    NSString *server;
}

// Property for the domaininformation, no atomic access, 
// "setDomain" and "domain" (getter) are not explicitly implemented in the implementaion part

@property (nonatomic, strong) Domain *domain;

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void)queryServer:(NSString *)server withLocation:(CLLocation *)location delegate:(id)aDelegate selector:(SEL)aSelector;

@end


