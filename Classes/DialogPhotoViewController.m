//
//  DialogPhotoViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 31.01.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import "DialogPhotoViewController.h"
#import "DialogPositionViewController.h"

#import "UserReport.h"
#import "ReportPicture.h"
#import "MMAlertViewHandler.h"
#import "MMSupport.h"

@interface DialogPhotoViewController ()

@end

@implementation DialogPhotoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSString *next   = NSLocalizedString(@"next_button_title", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:next
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(nextStepButtonClicked:)];
    
    // Setup description texts
    self.descriptionView.hidden = NO;
    self.descriptionTitleLabel.text     = NSLocalizedString(@"photo_step_header", nil);
    self.descriptionTextView.text       = NSLocalizedString(@"photo_step_description", nil);

    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        self.descriptionTextView.selectable  = NO;
    }
}

- (IBAction)cancelButtonClicked:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)nextStepButtonClicked:(id)sender
{
    [self saveLatestPicture];
    
    if (self.userReport.picture) {
        DialogPositionViewController *positionViewController = [[DialogPositionViewController alloc] initWithNibName:@"PositionViewController" bundle:nil];
        positionViewController.managedObjectContext = self.managedObjectContext;
        positionViewController.userReport = self.userReport;
        
        [self.navigationController pushViewController:positionViewController animated:YES];
    } else {
        __weak PhotoViewController *weakSelf = self;
        [[MMAlertViewHandler sharedInstance] showNoPhotoSetAlertWithButtonClickBlock:^(NSInteger index) {
            if (index == 1) {
                DialogPositionViewController *positionViewController = [[DialogPositionViewController alloc] initWithNibName:@"PositionViewController" bundle:nil];
                
                positionViewController.managedObjectContext = weakSelf.managedObjectContext;
                positionViewController.userReport = weakSelf.userReport;
                
                [weakSelf.navigationController pushViewController:positionViewController animated:YES];
            }
        }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
