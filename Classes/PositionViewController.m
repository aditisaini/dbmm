//
//  ReportPositionViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 06.01.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import "PositionViewController.h"
#import "TypeSelectionViewController.h"

#import "ReportAnnotation.h"
#import "DraggableAnnotationView.h"

#import "UserReport.h"

#import "DomainJSONAdapter.h"
#import "GeoCoordinateJSONAdapter.h"
#import "Constants.h"
#import "MMAlertViewHandler.h"

@interface PositionViewController ()
{
    BOOL domainUpdated;
    BOOL addressUpdated;
}

@end

@implementation PositionViewController

@synthesize addressUpdateFinishedBlock=_addressUpdateFinishedBlock;
@synthesize managedObjectContext=_managedObjectContext;
@synthesize userReport=_userReport;
@synthesize mapKitDelegate;

@synthesize mapView;
@synthesize mapType=_mapType;

@synthesize server;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;

    mapKitDelegate.creatingMovableAnnotations = NO;
    mapKitDelegate.delegate = self;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"save_button_title", nil)
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(saveButtonClicked:)];
    
    // If presented modally, add cancel button
    if (self.navigationController.viewControllers.count == 1) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"cancel_button_title", nil)
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(cancelButtonClicked:)];
    }
    
    self.descriptionTitleLabel.text     = NSLocalizedString(@"position_header", nil);
    self.descriptionTextView.text       = NSLocalizedString(@"position_step_description", nil);

    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        self.descriptionTextView.selectable  = NO;
    }
    
    // Segemented control for map view type
    UIBarButtonItem *leftSpace      = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *rightSpace     = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *centerButton   = [[UIBarButtonItem alloc] initWithCustomView:_mapType];
    
    self.toolbarItems = [NSArray arrayWithObjects:leftSpace, centerButton, rightSpace, nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.toolbarHidden = NO;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    _mapType.selectedSegmentIndex    = 0;
    mapView.mapType                 = MKMapTypeStandard;


    CLLocationCoordinate2D centerCoordinate = _userReport.coordinate;
    MKCoordinateRegion region = [mapView regionThatFits:MKCoordinateRegionMakeWithDistance(centerCoordinate, 500.0, 500.0)];
    
    [mapView setRegion:region animated:YES];
    [mapView setCenterCoordinate:_userReport.coordinate animated:YES];
}


#pragma mark -
#pragma mark API

- (IBAction)changeMapType:(id)sender
{
    switch (_mapType.selectedSegmentIndex) {
        case 0:
            mapView.mapType = MKMapTypeStandard;
            break;
        case 1:
            mapView.mapType = MKMapTypeSatellite;
            break;
        case 2:
            mapView.mapType = MKMapTypeHybrid;
            break;
    }
}

- (void)updateStreetAddress
{
    if(geoCoordinateJSONAdapter.street_address){
        
#if LOG_LEVEL > 1
        NSLog(@" Set new street_address %@", geoCoordinateJSONAdapter.street_address);
#endif
        
        [self.userReport setSubtitle:[NSString stringWithFormat:@"%@", geoCoordinateJSONAdapter.street_address]];
        
        addressUpdated = YES;
        if (domainUpdated) {
            [[MMAlertViewHandler sharedInstance] cancelActivityIndicatingAlert];
            self.addressUpdateFinishedBlock();
        }
    }
}

- (void)domainReceived
{
    if (domainJSONAdapter.domain) {
#if LOG_LEVEL > 1
        NSLog(@" Set new domain %@", domainJSONAdapter.domain);
#endif
    
        self.userReport.domain = domainJSONAdapter.domain;
       
        domainUpdated = YES;
        if (addressUpdated) {
            [[MMAlertViewHandler sharedInstance] cancelActivityIndicatingAlert];
            self.addressUpdateFinishedBlock();
        }
    }
}

- (void)saveStreetAddressOnFinish:(void (^)())block
{
    _addressUpdateFinishedBlock = block;
    
    // Check if the user has changed the position manually
    if ((_userReport.coordinate.latitude != mapView.centerCoordinate.latitude) ||
            (_userReport.coordinate.longitude != mapView.centerCoordinate.longitude)) {
        
        [[MMAlertViewHandler sharedInstance] showActivityIndicatingAlertWithMessage:NSLocalizedString(@"updating_position", nil) andTitle:nil];

        domainUpdated = NO;
        addressUpdated = NO;
        
        // Coordinate has changed manually, so we have to update the coordinates of the annotation
        _userReport.coordinate = mapView.centerCoordinate;
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:_userReport.coordinate.latitude longitude:_userReport.coordinate.longitude];
        NSString *theServer  = [self server];
        
        // Get the corresponding address via requesting the server
        if (geoCoordinateJSONAdapter) {
            [geoCoordinateJSONAdapter cancel];
        }
        else {
            geoCoordinateJSONAdapter = [[GeoCoordinateJSONAdapter alloc] initWithManagedObjectContext:_managedObjectContext];
        }
        [geoCoordinateJSONAdapter queryServer:theServer withLocation:location delegate:self selector:@selector(updateStreetAddress)];
     
        // Get the domain related to the new coordinate via requesting the server
        
        if (domainJSONAdapter) {
            [domainJSONAdapter cancel];
        }
        else {
            domainJSONAdapter = [[DomainJSONAdapter alloc] initWithManagedObjectContext:_managedObjectContext];
        }
        [domainJSONAdapter queryServer:theServer withLocation:location delegate:self selector:@selector(domainReceived)];

        
    } else {
#if LOG_LEVEL > 1
        NSLog(@"GPS address has NOT been changed manually");
#endif
        self.addressUpdateFinishedBlock();
    }
}

- (IBAction)saveButtonClicked:(id)sender
{
    [self saveStreetAddressOnFinish:^(){
        // Presented or pushed?
        if (self.navigationController.viewControllers.count == 1) {
            [self dismissViewControllerAnimated:YES completion:^(){}];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (IBAction)cancelButtonClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^(){}];
}

- (NSString *)server
{
    return [NSString stringWithFormat:kServerFormat, [[NSUserDefaults standardUserDefaults] stringForKey:kServerHostKey]];
}

#pragma mark -
#pragma mark MapActionDelegate

- (void)showDetails:(ReportAnnotation *)annotation
{
    // Nothing to do here
}

- (void)editReport:(ReportAnnotation *)annotation
{
    // Nothing to do here
}

- (void)createReport
{
    // Nothing to do here
}

- (void)startAtLocation:(CLLocation *)location
{
    // Nothing to do here
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    // Cancel pending processes
    [geoCoordinateJSONAdapter cancel];
    [domainJSONAdapter cancel];
    
    [[MMAlertViewHandler sharedInstance] cancelActivityIndicatingAlert];
}

- (void)viewDidUnload
{
    [super viewDidUnload];

    // Cancel all ongoing server requests
    [geoCoordinateJSONAdapter cancel];
    [domainJSONAdapter cancel];
    geoCoordinateJSONAdapter = nil;
    domainJSONAdapter = nil;
    
    self.mapView = nil;
    self.mapKitDelegate = nil;
    self.mapType = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
