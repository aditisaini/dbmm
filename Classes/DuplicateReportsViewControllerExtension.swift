//
//  DuplicateReportsViewControllerExtension.swift
//  WerDenktWas
//
//  Created by Albert Tra on 27/01/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import Foundation

extension DuplicateReportsViewController {
    func logger(string: NSString) {
        _ = Logger(sender: self, message: string)
    }
    
    func downloadImage(cell: UITableViewCell) {
        let network = AFramework.sharedInstance.network
        network.getRawData(self.imageURL) { (data) -> Void in
            print("Image data downloaded")
            cell.imageView?.image = MMSupport.makeThumbnailOfImage(
                    UIImage(data: data),
                    andSize: CGSizeMake(80, 80))
            cell.imageView?.setNeedsDisplay()
            
            cell.layoutIfNeeded()
            cell.reloadInputViews()
        }
    }
    
    func downloadImageAtURL(url: String) {
        let network = AFramework.sharedInstance.network
        network.getRawData(url) { (data) -> Void in
            print("Image data downloaded")
//            cell.imageView?.image = MMSupport.makeThumbnailOfImage(
//                UIImage(data: data),
//                andSize: CGSizeMake(80, 80))
//            cell.imageView?.setNeedsDisplay()
//            
//            cell.layoutIfNeeded()
//            cell.reloadInputViews()
            self.image = UIImage(data: data)
            self.tableView.reloadData()
//            cell.imageView?.setNeedsDisplay()
            
        }
    }
    
    func setImage(cell: UITableViewCell, indexPath: NSIndexPath) {
        let network = AFramework.sharedInstance.network

        network.getRawData(self.imageURL) { (data) -> Void in
            print("Query URL: ", self.imageURL)
            print("Image data downloaded")
            cell.imageView?.image = MMSupport.makeThumbnailOfImage(
                    UIImage(data: data),
                    andSize: CGSizeMake(80, 80))
            cell.imageView?.setNeedsDisplay()
        }
    }

    
    func configureCellSwift(cell: UITableViewCell, indexPath: NSIndexPath) {
        let dupicate = self.fetchedResults.objectAtIndexPath(indexPath) as! ReportDuplicate
        let state = dupicate.state
        let distance = dupicate.distance.floatValue * 1000
        cell.textLabel?.text = dupicate.title
        cell.detailTextLabel!.text = String(distance) + state
        self.downloadImage(cell)
    }
    
//    func getID(id: String, completion: () -> Void) {
//        let url = "http://api.werdenktwas.de/bmsapi/get_message_detail?id=" + "10180"
//        Network.sharedInstance.getRawJSON(url) { (json) -> Void in
//            print(json)
//        }
//    }
    
//    func getID(completion: () -> Void) {
////        let url = "http://api.werdenktwas.de/bmsapi/get_message_detail?id=" + "10180"
////        Network.sharedInstance.getRawJSON(url) { (json) -> Void in
////            print(json)
////        }
////        print("ID from network: ", Network.sharedInstance.rawJSON![0]["messageid"].stringValue)
//    }
    
//    func storeID(id: String) {
//        Network.sharedInstance.id = id
//        print(Network.sharedInstance.id)
//    }
    
    
}