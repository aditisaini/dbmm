//
//  DetailViewController.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 17.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: DetailViewController.h 72 2010-07-14 13:21:40Z eik $
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "ReportAnnotation.h"
#import "ReportDetailsCell.h"



@class DetailHeaderView;
@class DetailJSONAdapter;
@class PictureAdapter;

@class PictureViewController;
//@class DetailModificationViewController;
@class ServerReport;

#import "FinalViewController.h"

@interface DetailViewController : UITableViewController <NSFetchedResultsControllerDelegate, DismissCommentNavigationStackDelegate>
{
@protected
#if !__OBJC2__
    DetailHeaderView *headerView;
    UITableViewCell *loadingCell;
    PictureViewController *pictureViewController;
#endif
    
    DetailJSONAdapter *detailJSONAdapter;
    PictureAdapter *pictureAdapter;

    ServerReport *_report;
    
    NSFetchedResultsController *fetchedResultsController;
}

@property (nonatomic, strong) IBOutlet DetailHeaderView *headerView;
@property (nonatomic, strong) IBOutlet UITableViewCell *loadingCell;
@property (nonatomic, strong) IBOutlet PictureViewController *pictureViewController;
@property (nonatomic, strong) ServerReport *report;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *messageID;
@property (nonatomic, strong) NSMutableArray *titles;
@property (nonatomic, strong) NSMutableArray *contents;


- (void)detailsReceived;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *subscribeButton;

- (IBAction)subscribeButtonClicked:(id)sender;
- (IBAction)pictureButtonClicked:(id)sender;

@end
