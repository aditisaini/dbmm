//
//  DialogDuplicateReportsViewController.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 10.01.14.
//  Copyright (c) 2014 Robert Lokaiczyk. All rights reserved.
//

#import "DuplicateReportsViewController.h"

@interface DialogDuplicateReportsViewController : DuplicateReportsViewController

@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UITextView *headerTextView;

@end
