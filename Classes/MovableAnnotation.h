//
//  MovableAnnotation.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 10.05.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: MovableAnnotation.h 74 2010-07-14 22:00:16Z eik $
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "ReportAnnotationProtocol.h"

@class ReportType;

@interface MovableAnnotation : NSObject <ReportAnnotationProtocol>
{
@private
#if !__OBJC2__
    CLLocationCoordinate2D coordinate;

    NSString *title;
    NSString *subtitle;

    UIImage *image;
#endif
}

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

//@property (nonatomic, retain) ReportType *type;

@property (nonatomic, strong) UIImage *image;

- (id)initWithAnnotation:(id <ReportAnnotationProtocol>)annotation;

@end
