//
//  MMSupport.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 26.09.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMSupport : NSObject

+ (UIImage *)resizeImage:(UIImage*)inImage toSizeOfRect:(CGRect)thumbRect;
+ (UIImage *)makeThumbnailOfImage:(UIImage *)image andSize:(CGSize)size;
+ (UIImage *)scaleAndRotateImage:(UIImage *)image;

@end
