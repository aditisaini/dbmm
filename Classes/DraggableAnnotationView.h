//
//  DraggableAnnotation.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 25.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: DraggableAnnotationView.h 74 2010-07-14 22:00:16Z eik $
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "ReportAnnotationView.h"

@interface DraggableAnnotationView : ReportAnnotationView
{
@private
#if !__OBJC2__
    MKMapView *mapView;
#endif
}

@property (assign) __unsafe_unretained MKMapView *mapView;

- (void)animateAnnotationLift;
- (void)animateAnnotationCancel;
- (void)animateAnnotationDrop;
- (void)animateAnnotationBounceAndUpdate;

- (id)initWithAnnotation:(id <ReportAnnotationProtocol>)annotation reuseIdentifier:(NSString *)reuseIdentifier;

@end
