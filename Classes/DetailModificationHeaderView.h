//
//  DetailModificationHeaderView.h
//  WerDenktWas
//
//  Created by Franziska Engelmann on 09.11.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailHeaderView.h"

@class UserUpdate;
@class UpdatePicture;

@interface DetailModificationHeaderView : DetailHeaderView
{
    UserUpdate *_update;
}

@property (strong, nonatomic) UserUpdate *update;

@property (strong, nonatomic) IBOutlet UIView *currentPhotoContainer;
@property (strong, nonatomic) IBOutlet UIView *updatePhotoContainer;
@property (strong, nonatomic) IBOutlet UIView *currentReportMapView;
@property (strong, nonatomic) IBOutlet UIImageView *updatePhotoView;
@property (strong, nonatomic) IBOutlet UIImageView *emptyPhotoView;
@property (strong, nonatomic) IBOutlet UIImageView *currentPhotoView;
@property (strong, nonatomic) IBOutlet UIButton *detailDisclosureButton;

@property (strong, nonatomic) IBOutlet UILabel *currentDetailsLabel;

- (void)setRoundCorners;
- (void)setUpdatePhotoViewToImage:(UIImage *)newImage;

@end
