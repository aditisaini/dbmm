//
//  PictureViewController.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 23.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: PictureViewController.m 60 2010-07-12 00:13:02Z eik $
//

#import "PictureViewController.h"

#define ZOOM_VIEW_TAG 100
#define ZOOM_STEP 1.5

@implementation PictureViewController

@synthesize image;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;

    imageScrollView = (UIScrollView *)self.view;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self.navigationController setToolbarHidden:(self.toolbarItems == nil) animated:animated];

    UIApplication *application = [UIApplication sharedApplication];
    UINavigationBar *navigationBar = self.navigationController.navigationBar;

    savedNavigationBarStyle = navigationBar.barStyle;
    savedStatusBarStyle = application.statusBarStyle;

    navigationBar.barStyle = UIBarStyleBlack;
    application.statusBarStyle = UIStatusBarStyleBlackOpaque;

    // first remove previous image view, if any
    [[imageScrollView viewWithTag:ZOOM_VIEW_TAG] removeFromSuperview];

    UIImageView *zoomView = [[UIImageView alloc] initWithImage:image];
    zoomView.tag = ZOOM_VIEW_TAG;
    [imageScrollView addSubview:zoomView];
    imageScrollView.contentSize = zoomView.frame.size;

    // choose minimum scale so image width fits screen
    float minScale = imageScrollView.frame.size.width / zoomView.frame.size.width;
    imageScrollView.minimumZoomScale = minScale;
    imageScrollView.maximumZoomScale = 2.0;
    imageScrollView.zoomScale = minScale;
    imageScrollView.contentOffset = CGPointZero;

}

- (void)viewWillDisappear:(BOOL)animated
{
    UIApplication *application = [UIApplication sharedApplication];
    UINavigationBar *navigationBar = self.navigationController.navigationBar;

    navigationBar.barStyle = savedNavigationBarStyle;
    application.statusBarStyle = savedStatusBarStyle;

    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    [[imageScrollView viewWithTag:ZOOM_VIEW_TAG] removeFromSuperview];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

/*
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}
*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}




#pragma mark UIScrollViewDelegate methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    UIView *view = nil;
    if (scrollView == imageScrollView) {
        view = [imageScrollView viewWithTag:ZOOM_VIEW_TAG];
    }
    return view;
}

@end
