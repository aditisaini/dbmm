//
//  Picture.h
//  WerDenktWas
//
//  Created by Franziska Engelmann on 19.11.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Picture : NSManagedObject

@property (nonatomic, retain) UIImage * image;
@property (nonatomic, retain) NSData * imageData;

@end
