//
//  VotingViewController.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 01.10.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VotingViewController : UIViewController

- (IBAction)voteButtonClicked:(id)sender;
- (IBAction)appButtonClicked:(id)sender;

@end
