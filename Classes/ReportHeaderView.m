//
//  ReportHeaderView.m
//  WerDenktWas
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 12.07.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ReportHeaderView.m 74 2010-07-14 22:00:16Z eik $
//

#import "ReportHeaderView.h"

#import "ReportAnnotation.h"
#import "ReportPicture.h"


@implementation ReportHeaderView

@synthesize emptyPhotoView;


#pragma mark -
#pragma mark Utility functions

- (void)updatePhotoViewInitial:(BOOL)initial
{
    BOOL hasPhoto = self->_report.picture != nil;
    BOOL showPlaceholder = !hasPhoto;

    self->photoView.image = self->_report.photo;

    if (showPlaceholder) {
        emptyPhotoView.hidden = NO;
    }
    else {
        emptyPhotoView.hidden = YES;
    }
}


@end
