//
//  ReportDuplicate.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 19.03.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "ReportDuplicate.h"
#import "UserReport.h"
#import "PictureAdapter.h"
#import "DetailJSONAdapter.h"

@implementation ReportDuplicate
{
    PictureAdapter *pictureAdapter;
}

@dynamic userReport;
@dynamic distance;


- (void)loadReportDetails
{
    DetailJSONAdapter *detailJSONAdapter = [[DetailJSONAdapter alloc] init];
    [detailJSONAdapter queryReport:self delegate:self selector:@selector(receivedDetails)];
}

- (void)loadReportPictureForDelegate:(id)aDelegate andSelector:(SEL)aSelector
{
    selector = aSelector;
    delegate = aDelegate;
    if (self.pictureURL) {
        pictureAdapter = [[PictureAdapter alloc] init];
        [pictureAdapter queryReport:self delegate:self selector:@selector(receivedPicture)];
    }
}

- (void)receivedDetails
{
    if ([delegate respondsToSelector:selector]) {
        [delegate performSelector:selector];
    }
}

- (void)receivedPicture
{
    NSError *error;
    [self.managedObjectContext save:&error];
    
    if ([delegate respondsToSelector:selector]) {
        [delegate performSelector:selector];
    }
    
    [pictureAdapter cancel];
}

@end
