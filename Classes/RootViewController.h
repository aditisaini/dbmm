//
//  RootViewController.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 10.05.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: RootViewController.h 74 2010-07-14 22:00:16Z eik $
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreData/CoreData.h>
#import "AGBViewController.h"

#import "MapKitDelegate.h"
#import "InitialStartViewController.h"

@class DetailViewController;
@class AboutViewController;
@class HelpTableViewController;

@class DomainJSONAdapter;
@class ReportsJSONAdapter;

@class Domain;

@interface RootViewController : UIViewController <MapActionDelegate, InitialStartViewDelegate, UIActionSheetDelegate, AGBViewProtocol, CLLocationManagerDelegate> {
@private
#if !__OBJC2__
    MKMapView *_mapView;

    UIButton *infoButton;
    UIBarButtonItem *reportButton;

    DetailViewController *detailViewController;
    AboutViewController *aboutViewController;
    HelpTableViewController *helpViewController;
    
    MapKitDelegate *mapKitDelegate;
#endif

    DomainJSONAdapter *domainJSONAdapter;
    ReportsJSONAdapter *reportsJSONAdapter;

    Domain *domain;

    int launchCount;
}

@property(nonatomic, strong) Domain *domain;

@property(nonatomic, strong) IBOutlet MKMapView *mapView;

@property(nonatomic, strong) IBOutlet UIButton *infoButton;
@property(nonatomic, strong) IBOutlet UIBarButtonItem *reportButton;

@property(nonatomic, strong) IBOutlet DetailViewController *detailViewController;

@property(nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property(nonatomic, strong) IBOutlet MapKitDelegate *mapKitDelegate;

- (IBAction)infoButtonUp:(id)sender;

- (IBAction)refreshButtonUp:(id)sender;

- (IBAction)constructionButtonUp:(id)sender;

- (IBAction)reportButtonUp:(id)sender;

@end
