//
//  TypeAttribute.h
//  WerDenktWas
//
//  Created by Franziska Engelmann on 04.10.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ReportType;

@interface TypeAttribute : NSManagedObject

@property (nonatomic, assign) NSNumber * cached;
@property (nonatomic, assign) NSNumber * isPublic;
@property (nonatomic, assign) NSNumber * required;
@property (nonatomic, assign) NSNumber * visible;

@property (nonatomic, retain) NSString * error;
@property (nonatomic, retain) NSString * help;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * answer;
@property (nonatomic, retain) NSString * selectedValueID;
@property (nonatomic, retain) NSString * requiredIfCode;
@property (nonatomic, retain) NSString * requiredIfValue;
@property (nonatomic, retain) NSString * visibleIfCode;
@property (nonatomic, retain) NSString * visibleIfValue;

@property (nonatomic, retain) NSNumber * myID;
@property (nonatomic, retain) NSArray  * selectableValues;
@property (nonatomic, retain) NSSet    * reportType;

- (BOOL)isRequiredInContextOfAttributes:(NSArray *)attributes;
- (BOOL)isVisibleInContextOfAttributes:(NSArray *)attributes;

- (BOOL)isFulfilledInContextOfAttributes:(NSArray *)attributes;
//- (void)setVisibleInContextOfAttributes:(NSArray *)attributes;
//- (void)setRequiredInContextOfAttributes:(NSArray *)attributes;

- (NSInteger)valuesArrayIndexForValueID:(NSString *)answer;
- (NSString *)textForValueID:(NSString*)valueID;
- (void)flushAnswer;

- (BOOL)cachedBool;
- (BOOL)publicBool;
- (BOOL)requiredBool;

@end
