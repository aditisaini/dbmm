//
//  InitialStartViewController.h
//  WerDenktWas
//
//  Created by Franziska Engelmann on 05.11.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InitialStartViewDelegate <NSObject>
- (void)startViewHasBeenDismissed;
@end

@interface InitialStartViewController : UIViewController
{
    id _delegate;
    UITextView *_textView;
    UIView *_initialView;
}

@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) id <InitialStartViewDelegate> delegate;
@property (strong, nonatomic) UIView *initialView;
@property (strong, nonatomic) IBOutlet UIButton *startButton;

- (IBAction)clickStart:(id)sender;


@end
