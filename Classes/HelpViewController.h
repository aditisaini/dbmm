//
//  HelpTableViewController.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 09.01.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController
{
    NSArray *_helpTextsArray;
    NSArray *cellArray;
}

@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *backButton;

- (IBAction)barButtonClicked:(id)sender;

@end
