//
//  UploadAdapter.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 23.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: UploadAdapter.h 72 2010-07-14 13:21:40Z eik $
//

#import <Foundation/Foundation.h>

#import "ServerConnection.h"

@class UserReport;
@class ServerReport;
@class UserUpdate;

#define kUploadLatitude         @"lat"
#define kUploadLongitude        @"long"
// #define kUploadAccuracy         @"accuracy"
#define kUploadDescription      @"description"
#define kUploadSubjectTitle     @"title"
#define kUploadType             @"type"
#define kUploadAddress          @"adr"
#define kUploadDomain           @"domainid"
#define kUploadPhoto            @"picture"

@interface UploadAdapter : NSObject <ServerConnectionDelegate>
{
@private
    id delegate;
    SEL selector;
    ServerConnection *connection;

#if !__OBJC2__
    UserReport *userReport;
    UserUpdate *userUpdate;
    
    NSInteger result;
    NSInteger messageID;
#endif
}

@property (nonatomic, assign) NSInteger result;
@property (nonatomic, assign) NSInteger messageID;
@property (nonatomic, assign) NSString *messagetext;

@property (nonatomic, strong) UserReport *userReport;
@property (nonatomic, strong) UserUpdate *userUpdate;

- (void)uploadReport:(UserReport *)report delegate:(id)delegate selector:(SEL)selector;
- (void)uploadUpdate:(UserUpdate *)update delegate:(id)aDelegate selector:(SEL)aSelector;
- (void)cancel;

@end
