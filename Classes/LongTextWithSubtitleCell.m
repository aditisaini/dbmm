//
//  LongTextWithSubtitleCell.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 15.03.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "LongTextWithSubtitleCell.h"

@implementation LongTextWithSubtitleCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)adjustHeightOfTextView:(UITextView *)textView
{
    // Do nothing
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
