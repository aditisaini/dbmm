//
//  EmailAdapter.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 28.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: EmailAdapter.h 72 2010-07-14 13:21:40Z eik $
//

#import <Foundation/Foundation.h>
#import "ServerConnection.h"

@class ReportAnnotation;

@interface EmailAdapter : NSObject <ServerConnectionDelegate>
{
@private
    id delegate;
    SEL selector;
    ServerConnection *connection;

#if !__OBJC2__
    NSInteger result;
#endif
}

@property (nonatomic, readonly) NSInteger result;

- (void)subscribeToReport:(ReportAnnotation *)report withEmail:(NSString *)email delegate:(id)aDelegate selector:(SEL)aSelector;
- (void)cancel;

@end
