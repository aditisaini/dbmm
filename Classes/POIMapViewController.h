//
//  POIMapViewController.h
//  WerDenktWas
//
//  Created by Martin on 07.05.15.
//  Copyright (c) 2015 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapKitDelegate.h"

@interface POIMapViewController : UIViewController <MapActionDelegate>

@property (nonatomic, assign) MKCoordinateRegion region;

@end
