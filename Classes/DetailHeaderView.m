//
//  DetailHeaderView.m
//  WerDenktWas
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 12.07.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: DetailHeaderView.m 74 2010-07-14 22:00:16Z eik $
//

#import "DetailHeaderView.h"

#import "ReportAnnotation.h"
#import "ReportPicture.h"


@implementation DetailHeaderView

@synthesize activityView;
@synthesize mapFrame;

@synthesize waitingForPicture = _waitingForPicture;

- (id)initWithCoder:(NSCoder *)decoder
{
    if ((self = [super initWithCoder:decoder])) {
        // Initialization code
        _waitingForPicture = YES;
    }
    return self;
}


#pragma mark -
#pragma mark Utility functions

- (void)stopWaitingForPicture
{
    _waitingForPicture = NO;

    [self updatePhotoViewInitial:NO];
}

- (void)updatePhotoViewInitial:(BOOL)initial
{
    BOOL hasPhoto = self->_report.picture != nil;
    BOOL shouldHide = !hasPhoto && !_waitingForPicture;
    BOOL changeHidden = shouldHide != self->photoView.hidden;
    BOOL showPlaceholder = !hasPhoto && _waitingForPicture;

    self->photoView.image = self->_report.photo;

    if (changeHidden) {
        if (!initial) {
            [UIView beginAnimations:@"PhotoVisibility" context:NULL];
            [UIView setAnimationDuration:0.5];
        }
        if (shouldHide) {
            self->mapView.frame = CGRectUnion(self->photoView.frame, self->mapView.frame);
            self->photoView.hidden = YES;
        }
        else {
            self->mapView.frame = mapFrame.frame;
            self->photoView.hidden = NO;
        }
        if (!initial) {
            [UIView commitAnimations];
        }
    }

    if (showPlaceholder) {
        [activityView startAnimating];
    }
    else {
        [activityView stopAnimating];
    }
}


@end
