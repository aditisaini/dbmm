//
//  ReportAnnotationView.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 10.05.10.
//  Copyright (c) Oliver Eikemeier 2010. All rights reserved.
//
//  $Id: ReportAnnotationView.h 74 2010-07-14 22:00:16Z eik $
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "ReportAnnotationProtocol.h"


@interface ReportAnnotationView : MKAnnotationView
{
}

- (id)initWithAnnotation:(id <ReportAnnotationProtocol>)annotation reuseIdentifier:(NSString *)reuseIdentifier;
- (void)enableCallout;
- (void)setMapView:(MKMapView *)mapView;

+ (ReportAnnotationView *)reportAnnotationViewForMapView:(MKMapView *)mapView withAnnotation:(id <ReportAnnotationProtocol>)annotation movable:(BOOL)movable;

@end
