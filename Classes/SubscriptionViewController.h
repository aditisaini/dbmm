//
//  SubscriptionViewController.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 12.03.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EmailAdapter;
@class ServerReport;

@interface SubscriptionViewController : UIViewController <UITextFieldDelegate, UIScrollViewDelegate>
{
    BOOL keyboardShown;
}

@property (strong, nonatomic) EmailAdapter *emailAdapter;
@property (strong, nonatomic) ServerReport *report;

@property (strong, nonatomic) IBOutlet UIView *shiftableView;
@property (strong, nonatomic) NSString *emailAdress;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightBarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *leftBarButton;
@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UIScrollView *containerScrollView;

- (IBAction)sendButtonClicked:(id)sender;
- (IBAction)cancelButtonClicked:(id)sender;

@end
