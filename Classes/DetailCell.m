//
//  DetailCell.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 19.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: DetailCell.m 60 2010-07-12 00:13:02Z eik $
//

#import "DetailCell.h"


@implementation DetailCell
@synthesize delegate=_delegate;
@synthesize textView;

@synthesize hasText;
@synthesize emptyText;



#pragma mark -
#pragma mark API

- (void)setDetailText:(NSString *)text
{
    if (text.length > 0) {
        if (!hasText) {
            self.emptyText = textView.text;
            textView.textColor = [UIColor blackColor];
            hasText = YES;
        }
        textView.text = text;
    }
    else {
        textView.text = NSLocalizedStringWithDefaultValue(@"description_cell_prompt", nil,
                                                          [NSBundle mainBundle], @"Type description...",
                                                          @"Description field");
        textView.textColor = [UIColor colorWithWhite:0.7 alpha:1.0];;
        hasText = NO;
    }
}

- (NSString *)text
{
    if (hasText)
        return textView.text;
    else
        return nil;
}

#pragma mark -
#pragma mark UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)theTextView
{
    if (!hasText) {
        self.emptyText = theTextView.text;
        textView.text  = nil;
        textView.textColor = [UIColor blackColor];
        hasText = YES;
    }

    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)theTextView
{
    NSString *enteredText = theTextView.text;
    
    if (enteredText.length == 0) {
        [self setDetailText:nil];
    }
    
    if ([self.delegate respondsToSelector:@selector(detailCellTextIsSetOnValue:)]) {
            [self.delegate detailCellTextIsSetOnValue:enteredText];
    }
}

@end
