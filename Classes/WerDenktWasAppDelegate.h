//
//  WerDenktWasAppDelegate.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 10.05.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: WerDenktWasAppDelegate.h 72 2010-07-14 13:21:40Z eik $
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@class RootViewController;

@interface WerDenktWasAppDelegate : NSObject <UIApplicationDelegate> {
@private
#if !__OBJC2__
    UIWindow *window;
    UINavigationController *navigationController;
    RootViewController *rootViewController;
#endif

//    NSManagedObjectModel *_managedObjectModel;
//    NSManagedObjectContext *_managedObjectContext;
//    NSPersistentStoreCoordinator *_persistentStoreCoordinator;
}

@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) IBOutlet UINavigationController *navigationController;
@property (nonatomic, strong) IBOutlet RootViewController *rootViewController;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSString *)applicationCachesDirectory;

@end

