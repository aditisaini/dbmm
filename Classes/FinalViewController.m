//
//  FinalViewController.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 22.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: FinalViewController.m 79 2010-07-16 17:03:57Z eik $
//

#import "FinalViewController.h"

#import "UploadAdapter.h"
#import "EmailAdapter.h"
#import "UserReport.h"
#import "ReportType.h"
#import "Domain.h"
#import "UserUpdate.h"
#import "UpdatePicture.h"
#import "Constants.h"
#import "DetailModificationViewController.h"

#import "MMAlertViewHandler.h"
#import "TypeAttribute.h"

@interface FinalViewController ()

- (void)keyboardWillShow:(NSNotification *)aNotification;
- (void)keyboardWillHide:(NSNotification *)aNotification;
- (void)uploadDone;
- (void)emailDone;
@property (nonatomic, assign) BOOL hasEmailAttribute;

@end


@implementation FinalViewController

@synthesize delegate=_delegate;
@synthesize shiftableView;

@synthesize wantsEmail;
@synthesize doneButton;
@synthesize cancelButton;

@synthesize statusLabel;
@synthesize statusText;
@synthesize emailLabel;
@synthesize emailText;

@synthesize activityView;

@synthesize userReport;
@synthesize userUpdate;
@synthesize successView;


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setToolbarHidden:(self.toolbarItems == nil) animated:animated];
    
    self.navigationItem.hidesBackButton = NO;
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    successView.hidden = YES;
    
    statusLabel.text = NSLocalizedStringWithDefaultValue(@"uploading", nil, [NSBundle mainBundle],
                                                         @"Meldung wird gesendet", @"Uploading message");
    statusLabel.textColor = [UIColor blackColor];
    statusText.text = NSLocalizedStringWithDefaultValue(@"uploadingdetail", nil, [NSBundle mainBundle],
                                                        @"Ihre Nachricht wird in unsere Datenbank eingetragen. Bitte haben Sie etwas Geduld.", @"Uploading detail");
 
    // TODO: Localoze default emailLabel
    emailText.placeholder = NSLocalizedString(@"final_email_default_text", nil);
    emailLabel.text       = NSLocalizedString(@"final_email_label", nil);
    emailText.text  = [[NSUserDefaults standardUserDefaults] stringForKey:kEmailAddressKey];
    wantsEmail.on   = [[NSUserDefaults standardUserDefaults] boolForKey:kWantsEmailKey];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [activityView startAnimating];
    
    if (!uploadAdapter) {
        uploadAdapter = [[UploadAdapter alloc] init];
    }
    
    uploadAdapter.result = 2; // ????
    
    if (userReport) {
        self.hasEmailAttribute = NO;
        NSArray *array = [self.userReport.type2.attributes array];
        for(TypeAttribute *typeAttribute in array){
            if([typeAttribute.code isEqualToString:@"email"]) self.hasEmailAttribute = YES;
        }

        [uploadAdapter uploadReport:userReport delegate:self selector:@selector(uploadDone)];
    } else if (userUpdate){
        [uploadAdapter uploadUpdate:userUpdate delegate:self selector:@selector(updateDone)];
    }
    
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Memory management
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;
    
    // Stop navigationbar from overlapping (under iOS7)
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    [uploadAdapter cancel];
    uploadAdapter = nil;
    
    [emailAdapter cancel];
    emailAdapter = nil;
    
    self.shiftableView = nil;
    self.statusLabel = nil;
    self.statusText = nil;
    self.emailText = nil;
    self.activityView = nil;
    self.cancelButton = nil;
    self.doneButton = nil;
    self.successView = nil;
    self.wantsEmail = nil;
    self.userReport = nil;
    self.userUpdate = nil;
}

- (void)dealloc
{
    [uploadAdapter cancel];
    [emailAdapter cancel];
}

#pragma mark - Keyboard notifications

- (void)moveViewWithKeyboard:(NSNotification *)aNotification show:(BOOL)show
{
    if (keyboardShown == show)
        return;
    
    // Get the size of the keyboard.
    CGRect keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect toView:nil];
    CGFloat keyboardHeight = keyboardRect.size.height;
    
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    // Shift the view
    CGRect viewFrame = shiftableView.frame;
    if (show)
        viewFrame.origin.y -= keyboardHeight;
    else
        viewFrame.origin.y += keyboardHeight;
    
    [UIView beginAnimations:@"MoveForKeyboard" context:NULL];
    [UIView setAnimationDuration:animationDuration];
    shiftableView.frame = viewFrame;
    
    [UIView commitAnimations];
    
    keyboardShown = show;
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    [self moveViewWithKeyboard:aNotification show:YES];
}


// Called when the UIKeyboardDidHideNotification is sent
- (void)keyboardWillHide:(NSNotification *)aNotification
{
    [self moveViewWithKeyboard:aNotification show:NO];
}

#pragma mark - API

- (IBAction)cancelButtonUp:(id)sender
{
    [uploadAdapter cancel];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)uploadDone
{
    messageID = uploadAdapter.messageID;
    NSInteger result = uploadAdapter.result;
    
    userReport.messageID = [NSNumber numberWithInteger:messageID];
    
    [activityView stopAnimating];
    doneButton.enabled = true;
    
    BOOL successViewHidden;
    
    switch (result) {
            
        case 1:
			if ([uploadAdapter.messagetext length]==0) {
				statusLabel.text = NSLocalizedStringWithDefaultValue(@"sendsuccess", nil, [NSBundle mainBundle],
                                                                     @"Meldung erfolgreich erstellt", @"Upload success message");
			} else {
				statusLabel.text = [NSString stringWithFormat:@"%@ %@ %@",
                                    NSLocalizedStringWithDefaultValue(@"sendsuccess", nil, [NSBundle mainBundle],
                                                                      @"Meldung erfolgreich erstellt", @"Upload success message"),
                                    NSLocalizedStringWithDefaultValue(@"to", nil, [NSBundle mainBundle], @"an", @"to"),
                                    uploadAdapter.messagetext];
			}
			statusText.text = NSLocalizedStringWithDefaultValue(@"sendsuccessdetail", nil, [NSBundle mainBundle],
                                                                @"Ihre Meldung wird nun von einem Sachbearbeiter überprüft. Entspricht Sie den Nutzungsbedingungen, wird Sie freigeschaltet und weitergeleitet.", @"Upload success detail");

            self.navigationItem.hidesBackButton = YES;
            if(self.hasEmailAttribute == NULL) self.hasEmailAttribute = NO;
            successViewHidden = self.hasEmailAttribute;
            if(!self.hasEmailAttribute){
                [statusText.text stringByAppendingString:NSLocalizedStringWithDefaultValue(@"sendsuccesssubscription", nil, [NSBundle mainBundle], @"Geben Sie Ihre E-Mail Adresse ein, um über die Meldung informiert zu bleiben.", @"Upload success detail")];
            }
            self.navigationItem.rightBarButtonItem = doneButton;
            break;
            
        case 2:
            statusLabel.text = NSLocalizedStringWithDefaultValue(@"senderror", nil, [NSBundle mainBundle],
                                                                 @"Fehler beim Erstellen", @"Upload error message");
            statusLabel.textColor = [UIColor redColor];
            statusText.text = NSLocalizedStringWithDefaultValue(@"senderrordetail", nil, [NSBundle mainBundle],
                                                                @"Es ist ein Fehler beim Erstellen Ihrer Meldung aufgetreten, bitte versuchen Sie es später noch ein mal.", @"Upload error detail");
            successViewHidden = YES;
            break;
            
        case 3:
            statusLabel.text = NSLocalizedStringWithDefaultValue(@"sendunsupported", nil, [NSBundle mainBundle],
                                                                 @"System nicht unterstützt", @"Upload unsupported message");
            statusText.text = NSLocalizedStringWithDefaultValue(@"sendunsupporteddetail", nil, [NSBundle mainBundle],
                                                                @"Die Kommune unterstützt das System nicht, Ihre Meldung wurde trotzdem gespeichert.", @"Upload unsupported detail");
            self.navigationItem.hidesBackButton = YES;
            self.navigationItem.rightBarButtonItem = doneButton;
            successViewHidden = NO;
            break;
            
        default:
            successViewHidden = YES;
            break;
    }
    
    self.navigationItem.leftBarButtonItem = nil;
    
    [UIView beginAnimations:@"UploadResult" context:NULL];
    [UIView setAnimationDuration:0.5];
    successView.hidden = successViewHidden;
    [UIView commitAnimations];
}

- (void)updateDone
{
    messageID = uploadAdapter.messageID;
    NSInteger result = uploadAdapter.result;
    
    userUpdate.messageID = [NSNumber numberWithInteger:messageID];
    
    [activityView stopAnimating];
    doneButton.enabled = true;
    
    switch (result) {
            // 1 The update was created without any problems.
        case 1:
            statusLabel.text = NSLocalizedStringWithDefaultValue(@"updatesuccess", nil, [NSBundle mainBundle],
                                                                 @"Kommentar erfolgreich erstellt", @"Update success message");
            
			statusText.text = NSLocalizedStringWithDefaultValue(@"updatesuccessdetail", nil, [NSBundle mainBundle],
                                                                @"Ihr Kommentar wird nun von einem Sachbearbeiter überprüft. Entspricht er den Nutzungsbedingungen, wird er freigeschaltet.", @"Update success detail");
            self.navigationItem.hidesBackButton = YES;
            self.navigationItem.rightBarButtonItem = doneButton;
            break;
            
            // 2 There is no message corresponding to the id provided. No update is possible.
        case 2:
            statusLabel.text = NSLocalizedStringWithDefaultValue(@"commenterror", nil,
                                                                 [NSBundle mainBundle],
                                                                 @"Fehler beim Erstellen", @"Upload error message");
            statusLabel.textColor = [UIColor redColor];
            statusText.text = NSLocalizedStringWithDefaultValue(@"commenterrordetail", nil,
                                                                [NSBundle mainBundle],
                                                                @"Die von Ihnen kommentierte Meldung kann leider nicht mehr im System gefunden werden.", @"Comment error detail");
            break;
            
            // 3 The user cannot perform an update, as he is not logged in and no anonymous messages are allowed.
        case 3:
            statusLabel.text = NSLocalizedStringWithDefaultValue(@"commentnotallowed", nil, [NSBundle mainBundle],
                                                                 @"Fehler bei der Kommentator Authentifizierung", @"Commentator aunautorized message");
            statusText.text = NSLocalizedStringWithDefaultValue(@"commentnotalloweddetail", nil, [NSBundle mainBundle],
                                                                @"Nur der Ersteller der Meldung ist authorisiert, diese zu kommentieren.", @"Commentator aunautorized detail");
            self.navigationItem.rightBarButtonItem = doneButton;
            break;
            
            // 4 The photo provided could not be uploaded. The update was created without the photo.
        case 4:
            statusLabel.text = NSLocalizedStringWithDefaultValue(@"commentphotoerror", nil, [NSBundle mainBundle],
                                                                 @"Fehler beim Hochladen des Fotos", @"Comment photo error message");
            statusText.text = NSLocalizedStringWithDefaultValue(@"commentphotoerrordetail", nil,
                                                                [NSBundle mainBundle],
                                                                @"Es kam zu einem Fehler beim Hochladen Ihres Fotos. Ihr Kommentar wurde daher leider ohne Foto weitergeleitet.", @"Comment photo error detail");
            self.navigationItem.rightBarButtonItem = doneButton;
            break;
            
        default:
            break;
    }
    
    self.navigationItem.leftBarButtonItem = nil;
    self.wantsEmail = NO;
    
    [UIView beginAnimations:@"UploadResult" context:NULL];
    [UIView setAnimationDuration:0.5];
    successView.hidden = YES;
    [UIView commitAnimations];
}


- (IBAction)doneButtonClicked:(id)sender
{
    if (keyboardShown) {
        [emailText resignFirstResponder];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setBool:wantsEmail.on forKey:kWantsEmailKey];
        
        if (wantsEmail.on) {
			if ([emailText.text length] > 0) {
				self.navigationItem.rightBarButtonItem.enabled = false;
				if (!emailAdapter)
					emailAdapter = [[EmailAdapter alloc] init];
				[emailAdapter subscribeToReport:userReport withEmail:emailText.text delegate:self selector:@selector(emailDone)];
			} else {
				[[MMAlertViewHandler sharedInstance] showMissingEmailAddressAlert];
			}
		}
        else {
            
            if (userUpdate) {
                if (userUpdate.updatePicture) {
                    [userUpdate.managedObjectContext deleteObject:userUpdate.updatePicture];
                }
                [userUpdate.managedObjectContext deleteObject:userUpdate];
                NSError *error = nil;
                [userUpdate.managedObjectContext save:&error];
                
                [self dismissViewControllerAnimated:YES completion:NULL];
                [self.delegate dismissCommentNavigationStack];
            } else
                [userReport.type2 flushAttributeAnswers];
                [userReport.managedObjectContext deleteObject:userReport];
                [[[[[UIApplication sharedApplication] delegate] window] rootViewController] dismissViewControllerAnimated:YES completion:^(){}];
        }
    }
}

- (void)emailDone
{
    NSInteger result = emailAdapter.result;
    
    if (result != 1) {
        [[MMAlertViewHandler sharedInstance] showEmailSubscriptionFailureAlert];
        
        [wantsEmail setOn:NO animated:YES];
        self.navigationItem.rightBarButtonItem.enabled = true;
    }
    else {
        [userReport.managedObjectContext deleteObject:userReport];
        self.userReport = nil;
        [[[[[UIApplication sharedApplication] delegate] window] rootViewController] dismissViewControllerAnimated:YES completion:^(){}];
    }
}

#pragma mark -
#pragma mark Text Field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSRange range = [textField.text rangeOfString:@"@"];
    
    [wantsEmail setOn:(range.location != NSNotFound) animated:YES];
    
    [[NSUserDefaults standardUserDefaults] setObject:textField.text forKey:kEmailAddressKey];
}

@end
