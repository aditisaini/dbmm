//
//  DialogTypeSelectionViewControllerExtension.swift
//  WerDenktWas
//
//  Created by Albert Tra on 17/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import Foundation

extension DialogTypeSelectionViewController {
    func presentDuplicatesViewControllerSwift() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)

        let duplicateReportsViewController = mainStoryboard.instantiateViewControllerWithIdentifier("dialogDuplicateReportViewController") as! DialogDuplicateReportsViewControllerSwift
        
        duplicateReportsViewController.reportType = self.annotation.type2
        duplicateReportsViewController.userReport = (self.annotation as! UserReport)
        duplicateReportsViewController.managedObjectContext = self.managedObjectContext
        
        self.navigationController?.pushViewController(duplicateReportsViewController, animated: true)
    }
}