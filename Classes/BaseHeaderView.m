//
//  LocationPictureView.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 22.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: BaseHeaderView.m 74 2010-07-14 22:00:16Z eik $
//

#import "BaseHeaderView.h"

#import <QuartzCore/QuartzCore.h>

#import "ReportAnnotationView.h"
#import "ReportAnnotation.h"
#import "ReportPicture.h"

#import "Constants.h"

@interface BaseHeaderView ()

@end;


@implementation BaseHeaderView

@synthesize photoView;
@synthesize mapView;
@synthesize mapKitDelegate;


#pragma mark -
#pragma mark Utility functions

- (void)updatePhotoViewInitial:(BOOL)initial
{
    [self doesNotRecognizeSelector:_cmd];
}

#pragma mark -
#pragma mark API

- (ReportAnnotation *)report
{
    return _report;
}

- (void)setReport:(ReportAnnotation *)report
{
    if (!self.mapKitDelegate) {
        self.mapKitDelegate = [[MapKitDelegate alloc] init];
    }
    mapView.delegate = self.mapKitDelegate;
    
    if (_report != report) {
        if (_report) {
            [_report removeObserver:self forKeyPath:@"photo"];
            [_report removeObserver:self forKeyPath:@"coordinate"];

            if (mapView) {
                [mapView removeAnnotation:_report];
            }
        }

        if ((_report = report)) {
            if (mapView) {
                [mapView addAnnotation:_report];                
                MKCoordinateRegion region = [mapView regionThatFits:MKCoordinateRegionMakeWithDistance(_report.coordinate, 500.0, 500.0)];
                [mapView setRegion:region animated:NO];
            }

            [self updatePhotoViewInitial:YES];

            [_report addObserver:self forKeyPath:@"photo" options:0 context:NULL];
            [_report addObserver:self forKeyPath:@"coordinate" options:0 context:NULL];
        }
    }
}

- (void)setRoundCorners
{
    NSArray *array = [[NSArray alloc] initWithObjects:photoView, mapView, nil];

    for (UIView *aView in array) {
        CALayer *layer = aView.layer;

        layer.masksToBounds = YES;
        layer.cornerRadius  = 10.0;
        layer.borderColor   = [UIColor grayColor].CGColor;
        layer.borderWidth   = 1.0;
    }
}

#pragma mark -
#pragma mark NSKeyValueObserver

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == _report) {
        if ([keyPath isEqualToString:@"photo"]) {
            [self updatePhotoViewInitial:NO];
        }
        else if ([keyPath isEqualToString:@"coordinate"]) {
            if (mapView) {
                MKCoordinateRegion region = [mapView regionThatFits:MKCoordinateRegionMakeWithDistance(_report.coordinate, 500.0, 500.0)];
                [mapView setRegion:region animated:NO];
            }
        }
    }
}

- (void)dealloc
{    
    [_report removeObserver:self forKeyPath:@"photo"];
    [_report removeObserver:self forKeyPath:@"coordinate"];
}

@end
