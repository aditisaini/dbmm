//
//  AnnotationProtocol.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 27.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ReportAnnotationProtocol.h 74 2010-07-14 22:00:16Z eik $
//

#import <MapKit/MapKit.h>

@class ReportType;

@protocol ReportAnnotationProtocol <MKAnnotation>

- (BOOL)isMovable;

@property (nonatomic, readonly, retain) UIImage *image;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

@optional

@property (nonatomic, copy) NSString *subtitle;

@end
