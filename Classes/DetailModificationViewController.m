//
//  DetailModificationViewController.m
//  WerDenktWas
//
//  Created by Franziska Engelmann on 09.11.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//
#import "MMCommentCompletion.h"

#import "DetailModificationViewController.h"
//#import "FinalViewController.h"
#import "DetailModificationHeaderView.h"
#import "ServerReport.h"
#import "UserUpdate.h"
#import "DetailViewController.h"
#import "PictureViewController.h"
#import "UpdatePicture.h"
#import "DetailCell.h"
#import "CheckBoxAttributeCell.h"

#import "MMSupport.h"
#import "MMTableView.h"

@interface DetailModificationViewController ()

@end

@implementation DetailModificationViewController

@synthesize delegate=_delegate;

@synthesize report=_report;
@synthesize update=_update;
@synthesize detailCell=_detailCell;
@synthesize commentImage=_commentImage;
@synthesize headerView;
@synthesize promptTextView;
@synthesize currentPictureLabel;
@synthesize updatedPictureLabel;
@synthesize imageButton;
@synthesize cancelButton;
@synthesize sendButton;
@synthesize doneButton;
@synthesize comment;


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.title = NSLocalizedStringWithDefaultValue(@"comment_header", nil,
                                                                  [NSBundle mainBundle], @"Comment", @"Comment");
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    _update = _report.userUpdate;
    
    if (!_update) {
        _update = [NSEntityDescription insertNewObjectForEntityForName:@"UserUpdate"
                                                inManagedObjectContext:_report.managedObjectContext];
        _report.userUpdate = _update;        
    }
    
    [headerView setRoundCorners];
    
    if (_report) {
        headerView.report = _report;
    }
    if (_update) {
        headerView.update = _update;
        _update.messageID = _report.messageID;
    }
    
    [self.tableView reloadData];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    NSError *error = nil;
    [_update.managedObjectContext save:&error];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView = [[MMTableView alloc] initWithFrame:self.tableView.frame style:UITableViewStyleGrouped];
    
    self.detailCell = [[[NSBundle mainBundle] loadNibNamed:@"DetailCell"
                                                     owner:self
                                                   options:nil]
                       objectAtIndex:0];
    [self.detailCell setDetailText:NSLocalizedStringWithDefaultValue(@"detail_cell_prompt", nil,
                                                                     [NSBundle mainBundle], @"Type description...",
                                                                     @"Description field")];

    headerView.currentDetailsLabel.text = self.report.title;
    self.tableView.tableHeaderView = headerView;
    
    self.sendButton.title   = NSLocalizedStringWithDefaultValue(@"send_button_title", nil, [NSBundle  mainBundle], @"Send" , @"Send");
    self.cancelButton.title = NSLocalizedStringWithDefaultValue(@"cancel_button_title", nil, [NSBundle  mainBundle], @"Cancel" , @"Cancel");
    self.doneButton.title   = NSLocalizedStringWithDefaultValue(@"done_button_title", nil, [NSBundle  mainBundle], @"Done" , @"Done");
    
    
    self.promptTextView.text      = NSLocalizedStringWithDefaultValue(@"comment_prompt", nil, [NSBundle  mainBundle], @"Comment or supplement the selected report!" , @"Prompt");
    self.currentPictureLabel.text = NSLocalizedStringWithDefaultValue(@"current_picture_header", nil, [NSBundle  mainBundle], @"Current picture" , @"Current picture");
    self.updatedPictureLabel.text = NSLocalizedStringWithDefaultValue(@"new_picture_header", nil, [NSBundle  mainBundle], @"New picture" , @"New picture");
    
    // Translucent property causes bar to flash black once on appeareance, thus, it should be set to NO (iOS7)!
    self.navigationController.navigationBar.translucent = NO;

    self.navigationItem.rightBarButtonItem = self.sendButton;
    self.navigationItem.leftBarButtonItem  = self.cancelButton;

    [self.promptTextView sizeToFit];
}

- (void)viewDidUnload
{
    [self setHeaderView:nil];
    [self setImageButton:nil];
    [self setCancelButton:nil];
    [self setSendButton:nil];
    [self setDoneButton:nil];
    [self setCommentImage:nil];
    [self setDetailCell:nil];
    [self setPromptTextView:nil];
    [self setCurrentPictureLabel:nil];
    [self setUpdatedPictureLabel:nil];
    [self setComment:nil];
    [self setDelegate:nil];
    [self setReport:nil];
    [self setUpdate:nil];
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    NSString *errorTitle = NSLocalizedStringWithDefaultValue(@"error", nil,
                                                            [NSBundle mainBundle],
                                                            @"Error", @"Memory warning");
    
    NSString *errorMessage = NSLocalizedStringWithDefaultValue(@"memory_alert", nil,
                                                               [NSBundle mainBundle],
                                                               @"Unfortunatly, your system has been overloaded! Please try again.", @"Memory warning");
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:errorTitle
                                                    message:errorMessage
                                                   delegate:self cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    [alert show];
    [self.delegate dismissCommentNavigationStack];
}

- (UserUpdate *)userUpdateForServerReport:(ServerReport*)report
{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = [NSEntityDescription entityForName:@"UserUpdate" inManagedObjectContext:report.managedObjectContext];
//    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"UserUpdate"];

    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"messageID == %@", report];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"serverReport" ascending:YES];
    fetchRequest.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    fetchRequest.fetchLimit = 1;
    
    NSError *error = nil;
    NSArray *updates = [report.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
    if (updates.count > 0)
        return [updates objectAtIndex:0];
    else
        return nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat ret;
    
    switch (indexPath.row) {
        case 0:
            ret = _detailCell.bounds.size.height;
            break;
        default:
            ret = tableView.rowHeight;
            break;
    }
    return ret;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title;
    
    switch (section) {
        case 0:
            title = NSLocalizedStringWithDefaultValue(@"header_comments", nil,
                                                      [NSBundle mainBundle], @"Comments",
                                                      @"Header in detail modification view");
            break;
        default:
            title = nil;
            break;
    }
    
    return title;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    switch (indexPath.row) {
        case 0:
            cell = _detailCell;
            [(DetailCell *)cell setDetailText:_update.comment];
            break;
        default:
            if (cell == nil) {
                cell = [[CheckBoxAttributeCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                     reuseIdentifier:CellIdentifier];
                [(CheckBoxAttributeCell*)cell setIsSelected:[self.update.solved isEqualToString:@"1"]];
            }
            cell.textLabel.text = NSLocalizedStringWithDefaultValue(@"problem_resolved", nil,
                                                                    [NSBundle mainBundle],
                                                                    @"Problem resolved", @"Problem resolved");
            
            break;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.detailTextLabel.textColor = [UIColor blackColor];
    
    return cell;
}


#pragma mark -
#pragma mark keyboard notifications

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    self.navigationItem.rightBarButtonItem = doneButton;
}


// Called when the UIKeyboardDidHideNotification is sent
- (void)keyboardWillHide:(NSNotification *)aNotification
{
    self.comment = self.detailCell.text;
    self.update.comment = self.detailCell.text;
    self.navigationItem.rightBarButtonItem = sendButton;
}


# pragma mark - UIImagePicker Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *theImage = (UIImage *)[info objectForKey:UIImagePickerControllerEditedImage];
    if (!theImage) {
        theImage = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
    }
    
    if (theImage)
    {
        _commentImage = [MMSupport scaleAndRotateImage:theImage];
        
        // Update headerview --> display new image
        [self.headerView setUpdatePhotoViewToImage:_commentImage];
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)saveImage:(UIImage *)image
{
    if (image) {
        // Delete previous update-picture if entry already exists
        if (_update.updatePicture) {
            [_update.managedObjectContext deleteObject:_update.updatePicture];
            NSError *error = nil;
            [_update.managedObjectContext save:&error];
        }
        
        // Save and release uiimage
        UpdatePicture *updatePicture = [NSEntityDescription insertNewObjectForEntityForName:@"UpdatePicture"
                                                                      inManagedObjectContext:_update.managedObjectContext];
        updatePicture.imageData = [NSData dataWithData:UIImageJPEGRepresentation(image, 0.5)];
        updatePicture.image = image;
        _update.updatePicture = updatePicture;

//        [image release];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if ([cell isMemberOfClass:[DetailCell class]]) {
        [[(DetailCell *)cell textView] becomeFirstResponder];
    } else if ([cell isMemberOfClass:[CheckBoxAttributeCell class]]) {
        [(CheckBoxAttributeCell *)cell setIsSelected:!cell.isSelected];
        _update.solved = [NSString stringWithFormat:@"%d", cell.isSelected];
    }
}


- (IBAction)clickNewImageButton:(id)sender
{
    UIImagePickerController *theImagePicker = [[UIImagePickerController alloc] init];
    theImagePicker.delegate = self;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]
        && !TARGET_IPHONE_SIMULATOR)
    {
//        NSString *cancel = NSLocalizedString(@"cancel_button_title", "Cancel");
//        NSString *cameraPicture = NSLocalizedString(@"capture_image_title", "Capture new image");
//        NSString *libraryPicture = NSLocalizedString(@"load_image_title", "Load image from library");
        [self loadCameraForImagePicker:theImagePicker];
    }
    else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])//UIImagePickerControllerSourceTypePhotoLibrary])
    {
        [self loadPhotoLibraryForImagePicker:theImagePicker];
    }
}

- (IBAction)clickImageButton:(id)sender
{
    if (headerView.photoView.image) {
        
        PictureViewController *pictureViewController = [[PictureViewController alloc] init];
        pictureViewController.image = headerView.photoView.image;
        
        [self.navigationController pushViewController:pictureViewController animated:YES];
        
    }
}

- (IBAction)clickCancelButton:(id)sender
{
    [self saveImage:_commentImage];
    
    NSError *error = nil;
    [_report.managedObjectContext save:&error];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)clickSendButton:(id)sender
{
    NSString *commentMissingError = NSLocalizedString(@"comment_missing_msg",
                                                      "Please enter a comment!");
    NSString *commentTooShortError = NSLocalizedString(@"comment_short_msg",
                                                       "Comment is too short! Please write a more precise comment.");
    
    // Check whether checkbox cell has been selected
    CheckBoxAttributeCell *checkBoxCell = (CheckBoxAttributeCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    DetailCell *detailCell = (DetailCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    self.solved = [checkBoxCell isSelected];
    self.comment = [detailCell text];
    
    
    // Check if all necessary attributes are completed
    BOOL commentComplete = [MMCommentCompletion checkForMinimumLength:10
                                                               ofText:self.comment
                                                   withMissingMessage:commentMissingError
                                                   andTooShortMessage:commentTooShortError];
    
    // Update the userUpdate-object's updatePicture (in order to guarantee that the picutre is sent, too)
    [self saveImage:_commentImage];
    
    if (commentComplete) {
        FinalViewController *finalViewController = [[FinalViewController alloc] initWithNibName:@"FinalViewController"
                                                                                         bundle:[NSBundle mainBundle]];
        finalViewController.userUpdate = _update;
        finalViewController.delegate = self.delegate;
        [self.navigationController pushViewController:finalViewController animated:YES];
    }
}

- (IBAction)clickDoneButton:(id)sender
{
    [_detailCell.textView resignFirstResponder];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIImagePickerController *theImagePicker = [[UIImagePickerController alloc] init];
    theImagePicker.delegate = self;
    
    switch (buttonIndex) {
        case 0:
            [self loadCameraForImagePicker:theImagePicker];
            break;
        case 1:
            [self loadPhotoLibraryForImagePicker:theImagePicker];
            break;
        default:
            break;
    }
}

- (void)loadPhotoLibraryForImagePicker:(UIImagePickerController *)newImagePicker
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        newImagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
		newImagePicker.allowsEditing = NO;
        [self presentViewController:newImagePicker animated:YES completion:NULL];
    }
}

- (void)loadCameraForImagePicker:(UIImagePickerController *)newImagePicker
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]
        && !TARGET_IPHONE_SIMULATOR)
    {
        newImagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
		newImagePicker.allowsEditing = NO;
        [self presentViewController:newImagePicker animated:YES completion:NULL];
    }
}

@end
