//
//  LongTextCell.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 10.01.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "LongTextCell.h"

#define INSET         35.0

@implementation LongTextCell

@synthesize subview;
@synthesize contentTextViewLong=_contentTextViewLong;
@synthesize contentTextViewShort=_contentTextViewShort;
@synthesize imageView;

+ (float)inset
{
    return INSET;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]))
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LongTextCell"
                                                     owner:self
                                                   options:nil];
        [self addSubview:(LongTextCell *)[nib objectAtIndex:0]];
        
        if (style == UITableViewCellStyleDefault) {
            self.contentTextViewShort.hidden = YES;
            self.contentTextViewLong.hidden = NO;
            _visibleTextView = self.contentTextViewLong;
        } else {
            self.contentTextViewShort.hidden = NO;
            self.contentTextViewLong.hidden = YES;
            
            _visibleTextView = self.contentTextViewShort;
        }
    }
    
    return self;
}

- (void)setLongtextCellImage:(UIImage *)image
{
    if (!self.imageView.hidden) {
        self.imageView.image = image;
        self.contentTextViewShort.hidden = NO;
        self.contentTextViewLong.hidden = YES;
        
        _visibleTextView = self.contentTextViewShort;
        [self adjustHeightOfTextView:_visibleTextView];
        [self adaptContainerViewHeight];
    }

}

- (void)setLongText:(NSString *)text
{
    self.contentTextViewLong.text = text;
    self.contentTextViewShort.text = text;
    _visibleTextView.text = text;

    [self adjustHeightOfTextView:_visibleTextView];
    [self adaptContainerViewHeight];
}

- (void)adjustHeightOfTextView:(UITextView *)textView
{    
    // Adapt containerView height to number of lines in textView
    CGFloat newHeight = 0;
    
    // Provide visible frame only if there is text to be displayed
    if (textView.text.length > 0) {
        newHeight = textView.contentSize.height;
    }
        
    textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y,
                                textView.frame.size.width, newHeight);
    
    [textView sizeToFit];
}

- (void)setAccessoryType:(UITableViewCellAccessoryType)accessoryType
{
    [super setAccessoryType:accessoryType];
 
    CGRect newRect = CGRectMake(self.contentTextViewLong.frame.origin.x,
                                self.contentTextViewLong.frame.origin.y,
                                self.contentTextViewLong.frame.size.width - self.accessoryView.frame.size.width*3,
                                self.contentTextViewLong.frame.size.height);
    self.contentTextViewLong.frame = newRect;
    [self adjustHeightOfTextView:self.contentTextViewLong];
    
    newRect = CGRectMake(self.contentTextViewShort.frame.origin.x,
                         self.contentTextViewShort.frame.origin.y,
                         self.contentTextViewShort.frame.size.width - 40,
                         self.contentTextViewShort.frame.size.height);
    self.contentTextViewShort.frame = newRect;
    [self adjustHeightOfTextView:self.contentTextViewShort];
}

- (void)adaptContainerViewHeight
{
    self.subview.frame = CGRectMake(subview.frame.origin.x, INSET/5,
                                    subview.frame.size.width, _visibleTextView.frame.size.height + _visibleTextView.frame.origin.y);
}

@end
