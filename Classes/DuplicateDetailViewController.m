//
//  DetailSubscriptionViewController.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 12.03.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "DuplicateDetailViewController.h"
#import "DuplicateSubscriptionViewController.h"
#import "Constants.h"
#import "ServerReport.h"
#import "ReportDuplicate.h"
#import "DetailJSONAdapter.h"

@interface DuplicateDetailViewController ()

@end

@implementation DuplicateDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setToolbarHidden:YES];
}


- (IBAction)subscribeButtonClicked:(id)sender
{
    DuplicateSubscriptionViewController *duplicateSubscriptionViewController = [[DuplicateSubscriptionViewController alloc] initWithNibName:@"SubscriptionViewController" bundle:[NSBundle mainBundle]];
    duplicateSubscriptionViewController.report = (ReportDuplicate*)self.report;

    [self.navigationController pushViewController:duplicateSubscriptionViewController animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
