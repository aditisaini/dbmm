//
//  Version20Tests.swift
//  Version20Tests
//
//  Created by Albert Tra on 02/02/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import Version20

class Version20Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testParsingCategory() {
        let network = AFramework.sharedInstance.network

        let param = [ /// SOMEWHRE AROUND LUISENPLATZ
            "lat": "51.4463131846354",
            "long": "10.3352192657213"
        ]
        
        let url = "https://api.werdenktwas.de/bmsapi/get_domainbundle"
        
        network.getRawJSON(url, parameters: param) { (json) -> Void in
            print(json)
        }
    }
    
}
