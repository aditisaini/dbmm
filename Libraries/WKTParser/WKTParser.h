//
//  WKTParser.h
//  AppJobber
//
//  Created by Oliver Eikemeier on 26.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTObject.h"

@interface WKTParser : NSObject

+ (id <WKTObject>)wktObjectWithString:(NSString *)string error:(__autoreleasing NSError **)error;

@end
