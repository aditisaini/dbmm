//
//  WKTPoint.m
//  AppJobber
//
//  Created by Oliver Eikemeier on 26.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTPoint.h"

@interface WKTPoint ()
{
    CLLocationCoordinate2D _coordinate;
}

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end

@implementation WKTPoint

- (void)encodeWithCoder:(NSCoder *)coder
{
    // [super encodeWithCoder:coder];
    [coder encodeArrayOfObjCType:@encode(CLLocationDegrees) count:2 at:&_coordinate];
}

- (id)initWithCoder:(NSCoder *)coder
{
    // self = [super initWithCoder:coder];
    self = [super init];
    if (self) {
        [coder decodeArrayOfObjCType:@encode(CLLocationDegrees) count:2 at:&_coordinate];
    }
    return self;
}

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    self = [super init];
    if (self) {
        self->_coordinate = coordinate;
    }
    return self;
}

+ (WKTPoint *)wktPointWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    return [[WKTPoint alloc] initWithCoordinate:coordinate];
}

+ (id <WKTObject>)wktObjectFromSerialization:(WKTSerialization *)serialization
{
    CLLocationCoordinate2D coordinate;
    if ([serialization parseOpeningBracket] && [serialization parseCoordinates:&coordinate] && [serialization parseClosingBracket]) {
        return [[WKTPoint alloc] initWithCoordinate:coordinate];
    } else {
        return nil;
    }
}

- (CLLocationCoordinate2D)coordinate
{
    return _coordinate;
}

- (NSArray *)overlays
{
    return [[NSArray alloc] init];
}

- (NSArray *)waypoints
{
    return [NSArray arrayWithObject:self];
}

@end
