//
//  WKTPolygon.m
//  AppJobber
//
//  Created by Oliver Eikemeier on 26.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTPolygon.h"
#import "WKTLineString+WKTPolyBase.h"

@interface WKTPolygon ()
{
    __strong NSArray *polys;
}

@end

@implementation WKTPolygon

- (void)encodeWithCoder:(NSCoder *)coder
{
    // [super encodeWithCoder:coder];
    [coder encodeObject:polys forKey:@"WKTPolygon"];
}

- (id)initWithCoder:(NSCoder *)coder
{
    // self = [super initWithCoder:coder];
    self = [super init];
    if (self) {
        polys = [coder decodeObjectForKey:@"WKTPolygon"];
    }
    return self;
}

+ (id <WKTObject>)wktObjectFromSerialization:(WKTSerialization *)serialization
{
    if ([serialization parseOpeningBracket]) {
        WKTLineString *base = [WKTLineString wktObjectFromSerialization:serialization];
        if (base) {
            NSMutableArray *polys = [NSMutableArray arrayWithCapacity:1];
            [polys addObject:base];
            while([serialization parseComma]) {
                base = [WKTLineString wktObjectFromSerialization:serialization];
                if (!base) {
                    return nil;
                }
            }
            if ([serialization parseClosingBracket]) {
                WKTPolygon *polygon = [[WKTPolygon alloc] init];
                polygon->polys = polys;
                return polygon;
            }
        }
    }
    
    return nil;
}

- (CLLocationCoordinate2D)coordinate
{
    NSAssert(FALSE, @"coordinate called on %@ object", [self class]);
    return ((WKTLineString *)[polys objectAtIndex:0]).coordinate;
}

- (NSArray *)overlays
{
    if ([polys count] == 1) {
        return [NSArray arrayWithObject:[(WKTLineString *)[polys objectAtIndex:0] polygon]];
    }
    else if ([polys count] > 1) {
        NSMutableArray *interiorPolygons = [NSMutableArray arrayWithCapacity:[polys count]-1];
        for (NSUInteger i = 1; i < [polys count]; i++) {
            [interiorPolygons addObject:[(WKTLineString *)[polys objectAtIndex:i] polygon]];
        }
        return [NSArray arrayWithObject:[(WKTLineString *)[polys objectAtIndex:0] polygonWithInteriorPolygons:interiorPolygons]];
    }
    
    return nil;
}

- (NSArray *)waypoints
{
    return [[NSArray alloc] init];
}

@end
