//
//  WKTObjectFactory.m
//  AppJobber
//
//  Created by Oliver Eikemeier on 27.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTObjectFactory.h"
#import "WKTPoint.h"
#import "WKTLineString.h"
#import "WKTPolygon.h"
#import "WKTMultiPoint.h"
#import "WKTMultiLineString.h"
#import "WKTMultiPolygon.h"
#import "WKTGeometryCollection.h"

@implementation WKTObjectFactory

+ (id <WKTObject>)wktObjectFromSerialization:(WKTSerialization *)serialization
{
    __autoreleasing NSString *keyword;
    if ([serialization parseKeyword:&keyword]) {
        if ([@"POINT" isEqualToString:keyword]) {
            return [WKTPoint wktObjectFromSerialization:serialization];
        }
        else if ([@"LINESTRING" isEqualToString:keyword]) {
            return [WKTLineString wktObjectFromSerialization:serialization];
        }
        else if ([@"POLYGON" isEqualToString:keyword]) {
            return [WKTPolygon wktObjectFromSerialization:serialization];
        }
        else if ([@"MULTIPOINT" isEqualToString:keyword]) {
            return [WKTMultiPoint wktObjectFromSerialization:serialization];
        }
        else if ([@"MULTILINESTRING" isEqualToString:keyword]) {
            return [WKTMultiLineString wktObjectFromSerialization:serialization];
        }
        else if ([@"MULTIPOLYGON" isEqualToString:keyword]) {
            return [WKTMultiPolygon wktObjectFromSerialization:serialization];
        }
        else if ([@"GEOMETRYCOLLECTION" isEqualToString:keyword]) {
            return [WKTGeometryCollection wktObjectFromSerialization:serialization];
        }
        else {
            [serialization setErrorCode:kWKTParseErrorExpectedKeyword];
            return nil;
        }
    }
    
    return nil;
}

@end
