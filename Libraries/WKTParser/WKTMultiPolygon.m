//
//  MKTMulti.m
//  AppJobber
//
//  Created by Oliver Eikemeier on 26.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTMultiPolygon.h"
#import "WKTPolygon.h"

@implementation WKTMultiPolygon

+ (id <WKTObject>)wktObjectFromSerialization:(WKTSerialization *)serialization
{
    NSArray *array = [serialization wktObjectsWithBlock:^{
        return [WKTPolygon wktObjectFromSerialization:serialization];
    }];

    if (array) {
        WKTMultiPolygon *shape = [[WKTMultiPolygon alloc] init];
        shape->wktObjects = array;
        return shape;
    }

    return nil;
}

@end
