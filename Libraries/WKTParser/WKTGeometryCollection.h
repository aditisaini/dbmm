//
//  WKTGeometryCollection.h
//  AppJobber
//
//  Created by Oliver Eikemeier on 26.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTCollectionBase.h"
#import "WKTSerialization.h"

@interface WKTGeometryCollection : WKTCollectionBase

+ (id <WKTObject>)wktObjectFromSerialization:(WKTSerialization *)serialization;

@end
