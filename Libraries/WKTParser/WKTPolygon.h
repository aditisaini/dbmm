//
//  WKTPolygon.h
//  AppJobber
//
//  Created by Oliver Eikemeier on 26.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTObject.h"
#import "WKTSerialization.h"

@interface WKTPolygon : NSObject <WKTObject>

+ (id <WKTObject>)wktObjectFromSerialization:(WKTSerialization *)serialization;

@end
