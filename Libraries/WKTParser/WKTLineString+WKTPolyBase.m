//
//  WKTLineString+WKTPolyBase.m
//  AppJobber
//
//  Created by Oliver Eikemeier on 27.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTLineString+WKTPolyBase.h"

@implementation WKTLineString (WKTPolyBase)

- (MKPolygon *)polygon
{
    return [MKPolygon polygonWithCoordinates:_coordinates count:_count];
}

- (MKPolygon *)polygonWithInteriorPolygons:(NSArray *)interiorPolygons
{
    return [MKPolygon polygonWithCoordinates:_coordinates count:_count interiorPolygons:interiorPolygons];
}

@end
