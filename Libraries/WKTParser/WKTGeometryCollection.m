//
//  MKTMulti.m
//  AppJobber
//
//  Created by Oliver Eikemeier on 26.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTGeometryCollection.h"
#import "WKTObjectFactory.h"

@implementation WKTGeometryCollection

+ (id <WKTObject>)wktObjectFromSerialization:(WKTSerialization *)serialization
{
    NSArray *array = [serialization wktObjectsWithBlock:^{
        return [WKTObjectFactory wktObjectFromSerialization:serialization];
    }];

    if (array) {
        WKTGeometryCollection *shape = [[WKTGeometryCollection alloc] init];
        shape->wktObjects = array;
        return shape;
    }

    return nil;
}

@end
