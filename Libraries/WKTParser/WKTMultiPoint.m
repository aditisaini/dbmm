//
//  MKTMulti.m
//  AppJobber
//
//  Created by Oliver Eikemeier on 26.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTMultiPoint.h"
#import "WKTPoint.h"

@implementation WKTMultiPoint

+ (id <WKTObject>)wktObjectFromSerialization:(WKTSerialization *)serialization
{
    NSArray *array = [serialization wktObjectsWithBlock:^{
        return [WKTPoint wktObjectFromSerialization:serialization];
    }];

    if (array) {
        WKTMultiPoint *shape = [[WKTMultiPoint alloc] init];
        shape->wktObjects = array;
        return shape;
    }

    return nil;
}

@end
