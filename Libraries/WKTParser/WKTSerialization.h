//
//  WKTSerialization.h
//  AppJobber
//
//  Created by Oliver Eikemeier on 24.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "WKTObject.h"

enum WKTParseError {
    kWKTParseErrorExpectedWord = 1,
    kWKTParseErrorExpectedKeyword = 2,
    kWKTParseErrorExpectedOpeningBrace = 3,
    kWKTParseErrorExpectedClosingBrace = 4,
    kWKTParseErrorExpectedCoordinates = 5,
    kWKTParseErrorExpectedEnd = 6,
};

@interface WKTSerialization : NSObject

- (id)initWithString:(NSString *)string;

- (BOOL) parseOpeningBracket;
- (BOOL) parseClosingBracket;
- (BOOL) parseComma;
- (BOOL) parseEnd;
- (BOOL) parseCoordinates:(CLLocationCoordinate2D *)coordinates __attribute__ ((nonnull (1)));
- (BOOL) parseRepeatingCoordinates:(CLLocationCoordinate2D **)coordsOut withLength:(NSUInteger *)coordsLenOut __attribute__ ((nonnull (1,2)));
- (BOOL) parseKeyword:(__autoreleasing NSString **)keyword __attribute__ ((nonnull (1)));

- (NSArray *)wktObjectsWithBlock:(id <WKTObject> (^)(void))block;

- (void)setErrorCode:(NSInteger)code;

@property (nonatomic, readonly, strong) NSError *error;

@end
