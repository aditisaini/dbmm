//
//  MKTMulti.m
//  AppJobber
//
//  Created by Oliver Eikemeier on 26.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTMultiLineString.h"
#import "WKTLineString.h"

@implementation WKTMultiLineString

+ (id <WKTObject>)wktObjectFromSerialization:(WKTSerialization *)serialization
{
    NSArray *array = [serialization wktObjectsWithBlock:^{
        return [WKTLineString wktObjectFromSerialization:serialization];
    }];

    if (array) {
        WKTMultiLineString *shape = [[WKTMultiLineString alloc] init];
        shape->wktObjects = array;
        return shape;
    }

    return nil;
}

@end
