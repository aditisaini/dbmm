//
//  WKTLineString+WKTPolyBase.h
//  AppJobber
//
//  Created by Oliver Eikemeier on 27.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTLineString.h"

@interface WKTLineString (WKTPolyBase)

- (MKPolygon *)polygon;
- (MKPolygon *)polygonWithInteriorPolygons:(NSArray *)interiorPolygons;

@end
