//
//  WKTPoint.h
//  AppJobber
//
//  Created by Oliver Eikemeier on 26.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "WKTObject.h"
#import "WKTSerialization.h"

@interface WKTPoint : NSObject <WKTObject>

+ (id <WKTObject>)wktObjectFromSerialization:(WKTSerialization *)serialization;
+ (WKTPoint *)wktPointWithCoordinate:(CLLocationCoordinate2D)coordinate;

- (CLLocationCoordinate2D)coordinate;

@end
