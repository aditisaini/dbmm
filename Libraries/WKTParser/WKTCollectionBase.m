//
//  WKTCollectionBase.m
//  AppJobber
//
//  Created by Oliver Eikemeier on 27.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTCollectionBase.h"

@implementation WKTCollectionBase

- (void)encodeWithCoder:(NSCoder *)coder
{
    // [super encodeWithCoder:coder];
    [coder encodeObject:wktObjects forKey:@"WKTObjects"];
}

- (id)initWithCoder:(NSCoder *)coder
{
    // self = [super initWithCoder:coder];
    self = [super init];
    if (self) {
        wktObjects = [coder decodeObjectForKey:@"WKTObjects"];
    }
    return self;
}

- (NSArray *)overlays
{
    return [wktObjects valueForKeyPath:@"@unionOfArrays.overlays"];
}

- (NSArray *)waypoints
{
    return [wktObjects valueForKeyPath:@"@unionOfArrays.waypoints"];
}

- (CLLocationCoordinate2D)coordinate
{
    NSAssert(FALSE, @"coordinate called on %@ object", [self class]);
    return ((id <WKTObject>)[wktObjects objectAtIndex:0]).coordinate;
}

@end
