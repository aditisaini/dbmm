//
//  WKTParser.m
//  AppJobber
//
//  Created by Oliver Eikemeier on 26.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTParser.h"
#import "WKTSerialization.h"
#import "WKTObjectFactory.h"

@implementation WKTParser

+ (id <WKTObject>)wktObjectWithString:(NSString *)string error:(__autoreleasing NSError **)error
{
    WKTSerialization *serialization = [[WKTSerialization alloc] initWithString:string];
    
    id <WKTObject> shape = [WKTObjectFactory wktObjectFromSerialization:serialization];

    if (shape && ![serialization parseEnd]) {
        shape = nil;
    }

    if (error)
        *error = serialization.error;

    return shape;
}

@end
