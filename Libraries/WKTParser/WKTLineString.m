//
//  WKTLineString.m
//  AppJobber
//
//  Created by Oliver Eikemeier on 26.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTLineString.h"
#import "WKTPoint.h"

@implementation WKTLineString

- (void)encodeWithCoder:(NSCoder *)coder
{
    // [super encodeWithCoder:coder];
    [coder encodeInteger:_count forKey:@"count"];
    [coder encodeArrayOfObjCType:@encode(CLLocationDegrees) count:2*_count at:_coordinates];
}

- (id)initWithCoder:(NSCoder *)coder
{
    // self = [super initWithCoder:coder];
    self = [super init];
    if (self) {
        _count = [coder decodeIntegerForKey:@"count"];
        _coordinates = malloc(_count * sizeof(*_coordinates));
        [coder decodeArrayOfObjCType:@encode(CLLocationDegrees) count:2*_count at:_coordinates];
    }
    return self;
}

+ (id <WKTObject>)wktObjectFromSerialization:(WKTSerialization *)serialization
{
    CLLocationCoordinate2D *coordinates = NULL;
    NSUInteger length = 0;
    
    if ([serialization parseRepeatingCoordinates:&coordinates withLength:&length]) {
        WKTLineString *lineString = [[WKTLineString alloc] init];
        lineString->_coordinates = coordinates;
        lineString->_count = length;
        return lineString;
    }
    
    if (coordinates)
        free(coordinates);
    return nil;
}

- (void)dealloc
{
    free(_coordinates);
}

- (CLLocationCoordinate2D)coordinate
{
    NSAssert(FALSE, @"coordinate called on %@ object", [self class]);
    return _coordinates[0];
}

- (NSArray *)overlays
{
    if (_count > 2 || memcmp(_coordinates, _coordinates + _count-1, sizeof(*_coordinates)) != 0) {
        return [NSArray arrayWithObject:[MKPolyline polylineWithCoordinates:_coordinates count:_count]];
    }
    else {
        return [[NSArray alloc] init];
    }
}

- (NSArray *)waypoints
{
    return [NSArray arrayWithObjects:[WKTPoint wktPointWithCoordinate:_coordinates[0]], [WKTPoint wktPointWithCoordinate:_coordinates[_count-1]], nil];
}

@end
