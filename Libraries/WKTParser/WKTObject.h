//
//  WKTObject.h
//  AppJobber
//
//  Created by Oliver Eikemeier on 26.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import <MapKit/MapKit.h>

@protocol WKTObject <MKAnnotation, NSCoding>

// @property (nonatomic, readonly, assign) CLLocationCoordinate2D coordinate;

- (NSArray *)overlays;
- (NSArray *)waypoints;

@end
