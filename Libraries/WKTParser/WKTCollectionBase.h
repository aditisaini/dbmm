//
//  WKTCollectionBase.h
//  AppJobber
//
//  Created by Oliver Eikemeier on 27.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTObject.h"
#import "WKTSerialization.h"

@interface WKTCollectionBase : NSObject <WKTObject>
{
@protected
    __strong NSArray *wktObjects;
}

@end
