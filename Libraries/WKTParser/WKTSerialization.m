//
//  WKTSerialization.m
//  AppJobber
//
//  Created by Oliver Eikemeier on 24.02.12.
//  Copyright (c) 2012 Oliver Eikemeier. All rights reserved.
//

#import "WKTSerialization.h"


@interface WKTSerialization ()
{
@private
    __strong NSScanner *scanner;
}

@property (nonatomic, readwrite, strong) NSError *error;

@end

@implementation WKTSerialization

@synthesize error = _error;

- (id)initWithString:(NSString *)string
{
    self = [super init];
    if (self) {
        scanner = [NSScanner scannerWithString:string];
        // scanner.caseSensitive = NO;
        // scanner.charactersToBeSkipped = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    }
    return self;
}

- (void)setErrorCode:(NSInteger)code
{
    __autoreleasing NSString *errorString;
    [scanner scanUpToString:@"wrzl" intoString:&errorString];
    NSLog(@"Parse Error at %@", errorString);

    self.error = [NSError errorWithDomain:@"de.werdenktwas.AppJobber.WKTParser" code:code userInfo:nil];
}

- (BOOL) parseKeyword:(__autoreleasing NSString **)keyword __attribute__ ((nonnull (1)))
{
    if ([scanner scanCharactersFromSet:[NSCharacterSet uppercaseLetterCharacterSet] intoString:keyword]) {
        return YES;
    }
    else {
        [self setErrorCode:kWKTParseErrorExpectedWord];
        return NO;
    }
}

- (BOOL) parseCoordinates:(CLLocationCoordinate2D *)coordinates __attribute__ ((nonnull (1)))
{
    if ([scanner scanDouble:&coordinates->longitude] && [scanner scanDouble:&coordinates->latitude]) {
        // CLLocationCoordinate2DIsValid
        return YES;
    }
    else {
        [self setErrorCode:kWKTParseErrorExpectedCoordinates];
        return NO;
    }
}

- (BOOL) parseOpeningBracket
{
    if ([scanner scanString:@"(" intoString:NULL]) {
        return YES;
    }
    else {
        [self setErrorCode:kWKTParseErrorExpectedOpeningBrace];
        return NO;
    }
}

- (BOOL) parseClosingBracket
{
    if ([scanner scanString:@")" intoString:NULL]) {
        return YES;
    }
    else {
        [self setErrorCode:kWKTParseErrorExpectedClosingBrace];
        return NO;
    }
}

- (BOOL) parseComma
{
    if ([scanner scanString:@"," intoString:NULL]) {
        return YES;
    }
    else {
        return NO;
    }
}

- (BOOL) parseEnd
{
    if ([scanner isAtEnd]) {
        return YES;
    }
    else {
        [self setErrorCode:kWKTParseErrorExpectedEnd];
        return NO;
    }
}

- (BOOL) parseRepeatingCoordinates:(CLLocationCoordinate2D **)coordsOut withLength:(NSUInteger *)coordsLenOut __attribute__ ((nonnull (1,2)))
{
    NSUInteger space = 10;
    CLLocationCoordinate2D *coordinates = malloc(sizeof(CLLocationCoordinate2D) * space);
    
    NSUInteger read = 0;
    if ([self parseOpeningBracket] && [self parseCoordinates:coordinates + read++]) {
        while ([self parseComma]) {
            if (read == space) {
                space += space/2;
                coordinates = reallocf(coordinates, sizeof(CLLocationCoordinate2D) * space);
                if (!coordinates)
                    return NO;
            }
            if (![self parseCoordinates:coordinates + read++]) {
                free(coordinates);
                return NO;
            }
        }
        if (![self parseClosingBracket]) {
            free(coordinates);
            return NO;
        }
    }
    else {
        free(coordinates);
        return NO;
    }
    
    *coordsOut = coordinates;
    *coordsLenOut = read;
    return YES;
}

- (NSArray *)wktObjectsWithBlock:(id <WKTObject> (^)(void))block
{
    NSMutableArray *shapes = [NSMutableArray arrayWithCapacity:4];

    if ([self parseOpeningBracket]) {
        do {
            id  <WKTObject> shape = block();
            if (shape) {
                [shapes addObject:shape];
            }
            else {
                return nil;
            }
        } while ([self parseComma]);

        if ([self parseClosingBracket]) {
            return shapes;
        }
    }
    return nil;
}

@end
