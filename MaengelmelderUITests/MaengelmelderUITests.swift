//
//  MaengelmelderUITests.swift
//  MaengelmelderUITests
//
//  Created by Albert Tra on 26/01/16.
//  Copyright © 2016 Robert Lokaiczyk. All rights reserved.
//

import XCTest

class MaengelmelderUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testAddDuplicate() {
        XCUIDevice.sharedDevice().orientation = .Portrait
        XCUIDevice.sharedDevice().orientation = .Portrait
        
        let app = XCUIApplication()
        app.toolbars.buttons["Create new report"].tap()
        app.alerts.collectionViews.buttons["Create new report"].tap()
        app.alerts["Do you really want to delete your report?"].collectionViews.buttons["Delete"].tap()
        app.navigationBars["DialogPhotoView"].buttons["Next"].tap()
        app.alerts["No picture chosen"].collectionViews.buttons["Next"].tap()
        
        let element = app.childrenMatchingType(.Window).elementBoundByIndex(0).childrenMatchingType(.Other).element.childrenMatchingType(.Other).element
        element.tap()
        element.tap()
        element.tap()
        element.tap()
    }
    
}
