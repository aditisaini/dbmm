# A-Framework

[![CI Status](http://img.shields.io/travis/Albert Tra/A-Framework.svg?style=flat)](https://travis-ci.org/Albert Tra/A-Framework)
[![Version](https://img.shields.io/cocoapods/v/A-Framework.svg?style=flat)](http://cocoapods.org/pods/A-Framework)
[![License](https://img.shields.io/cocoapods/l/A-Framework.svg?style=flat)](http://cocoapods.org/pods/A-Framework)
[![Platform](https://img.shields.io/cocoapods/p/A-Framework.svg?style=flat)](http://cocoapods.org/pods/A-Framework)

## Usage
This only a test pod, you should not use it

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

A-Framework is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "A-Framework"
```


## Author

Albert Tra, albert.tra@icloud.com

## License

A-Framework is available under the MIT license. See the LICENSE file for more info.
