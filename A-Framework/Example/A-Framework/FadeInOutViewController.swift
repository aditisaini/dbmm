//
//  FadeInOutViewController.swift
//  A-Framework
//
//  Created by Albert Tra on 03/03/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import UIKit

class FadeInOutViewController: UIViewController {
    
    @IBOutlet weak var box: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        box.layer.cornerRadius = 5
        box.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func toggleButtonPressed(sender: AnyObject) {
        if box.alpha == 0 {
            box.fadeIn()
        } else {
            box.fadeOut()
        }
    }
}
