//
//  DateTimeService.swift
//  Pods
//
//  Created by Albert Tra on 03/03/16.
//
//

import Foundation

public class DateTimeServices {
    var dateFormatter: NSDateFormatter!
    
    init() {
        dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale.currentLocale() /// itx | EEAD7C2A-A368-4247-B7BD-67ACFC76AC70

    }
    
    /**
     Retrun string acording to givven format from NSDate
     
     Ref: Date Format Patterns: http://unicode.org/reports/tr35/tr35-6.html#Date_Format_Patterns
     
     :Author: Albert Tra
     */
    public func stringFromDate(date: NSDate, withFormat: String) -> String {
        dateFormatter.dateFormat = withFormat
        return dateFormatter.stringFromDate(date)
    }
}
