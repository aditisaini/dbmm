//
//  Reporter.swift
//  Journey
//
//  Created by Albert Tra on 08/01/16.
//  Copyright © 2016 Albert Tra. All rights reserved.
//

import Foundation
import CoreLocation

public class Logger {
    init() {
        
    }
    
    public init<T, U>(sender: T, function: String = __FUNCTION__, uuid: String, message: U!) { // TODO: Delete this init
        let classname = NSStringFromClass(T.self as! AnyClass)
        print("\n")
        print("====================================================")
        print("Location: \t", (classname as String) + " --> " + function)
        print("UUID: \t\t", uuid)
        print("Message: \t", message)
        print("====================================================")
    }
    
    public func log<T, U>(sender: T, function: String = __FUNCTION__, uuid: String, message: U!) {
        let classname = NSStringFromClass(T.self as! AnyClass)
        print("\n")
        print("====================================================")
        print("Location: \t", (classname as String) + " --> " + function)
        print("UUID: \t\t", uuid)
        print("Message: \t", message)
        print("====================================================")
    }
}

