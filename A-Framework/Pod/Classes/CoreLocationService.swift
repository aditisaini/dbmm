//
//  CoreLocationServices.swift
//  Journey
//
//  Created by Albert Tra on 05/01/16.
//  Copyright © 2016 Albert Tra. All rights reserved.
//

import Foundation
import CoreLocation

protocol CoreLocationServicesDelegate {
    func didUpdateLocation(currentLocation: CLLocation)
    func didUpdateHeading(currentHeading: CLHeading)
}

public class CoreLocationService: NSObject, CLLocationManagerDelegate {
    
    // MARK: C
    /**
    Containt current location information
    */
    var currentLocation: CLLocation?
    var currentHeading: CLHeading!
    
    // MARK: D
    var delegate: CoreLocationServicesDelegate!
    
    // MARK: L
    var locationManager = CLLocationManager()
    
    override init() {
        super.init()
        
        self.setupCoreLocationDelegate()
        self.requestUserLocation()
//        self.locationManager.startUpdatingLocation()
    }
    
    func startUpdatingLocation() {
        self.locationManager.startUpdatingLocation()
    }
    
    func startUpdateHeading() {
        self.locationManager.startUpdatingHeading()
    }
    
    func distanceString(distance: Double) -> String {
        if distance < 1000 {
//            self.distanceLabel.text = String(Int(distanceInMeter)) + " m"
            return (String(Int(distance)) + " m")
        } else {
            let distanceInKilometer = distance / 1000
//            self.distanceLabel.text = String(Int(distanceInKilometer!)) + " km"
            return (String(Int(distanceInKilometer)) + " km")
            
        }
    }
    
    /**
     Set up all necessary delegate in order to use CoreLocationService
     */
    private func setupCoreLocationDelegate() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters /// Call GPS
        locationManager.requestWhenInUseAuthorization()
        locationManager.headingFilter = 1
    }
    
    public func locationManagerShouldDisplayHeadingCalibration(manager: CLLocationManager) -> Bool {
        if currentHeading == nil {
            return true
        } else {
            return false
        }
    }
    
    public func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        self.currentHeading = newHeading
        delegate.didUpdateHeading(newHeading)
    }
    
    public func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        guard delegate != nil else {
//            print("349360C9-5DD6-4844-BBAA-DDF0E3DA7C01 \t Delegate is nil")
            Logger(sender: self, uuid: "349360C9-5DD6-4844-BBAA-DDF0E3DA7C01", message: "Delegate is NIL")
            return
        }
//        print(locations.last)
        self.currentLocation = locations.last!
        delegate.didUpdateLocation(self.currentLocation!)
    }
    
    public func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        Logger(sender: self, uuid: "681377DC-8E52-484B-925C-755CAE4CD3E0", message: "CANNOT UPDATE LOCATION")
    }
    
    private func requestUserLocation() {
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
                if #available(iOS 9.0, *) {
                    locationManager.requestLocation()
                } else {
                    // Fallback on earlier versions
                }
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
}
