//
//  Network.swift
//  Pods
//
//  Created by Albert Tra on 27/01/16.
//
//

import Foundation
import Alamofire /// How to setup dependencies: 3CC09F7B-ABAC-42AE-B84E-0831D1F0371B
import SwiftyJSON /// How to setup dependencies: 3CC09F7B-ABAC-42AE-B84E-0831D1F0371B
import Haneke

public class Network {
//    static let sharedInstance = Network() /// Single ton BB99C1B5-0355-4BF7-A6AE-C47FCDE8F0DC
    var credential: NSURLCredential!
    let cache = Shared.dataCache
    var rawJSON: SwiftyJSON.JSON?
    var id: String = ""
    
    /**
     Custom configuration
     */
    let networkManager: Alamofire.Manager = {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "api.werdenktwas.de": .DisableEvaluation /// Disable Server Trust Policies for this domain
//            "https://dacityapp.werdenktwas.de": ServerTrustPolicy.DisableEvaluation
//            "https://api.werdenktwas.de": .PinCertificates (
        
        ]
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        return Alamofire.Manager(
            configuration: configuration,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
    }()
    
//    func postRequest(url: String, parameters: [String: String], completion: (rawJSON: JSON) -> Void) {
//        networkManager.request(.POST, url, parameters: parameters)
//            .responseJSON { data in
//                if data.result.error == nil {
//                    completion(rawJSON: JSON(data.result.value!))
//                } else {
//                    print(data.result.error)
//                }
//        }
//    }
    
    init() {
        self.credential = NSURLCredential(user: "", password: "", persistence: .ForSession)
//        let url = "http://api.werdenktwas.de/bmsapi/find_duplicates?lang=de&categoryid=67&lat=49.863044&appid=1&domainid=32&long=8.632512"
//        let url = "https://dacityapp.werdenktwas.de/cityapp"
//        let url = "https://api.werdenktwas.de/bmsapi/get_message_detail?id=" + "12116"
//        self.getRawJSON(url) { (json) -> Void in
//            print(json)
//        }
    }
    
    
    /**
     Return JSON Object
     :Author: Albert Tra
     */
    public func getRawJSON(url: String, completion: (json: SwiftyJSON.JSON) -> Void) {
        networkManager.request(Alamofire.Method.GET, url) /// Using this networkManager for special configurations, which are set above in let networkManager: Alamofire.Manager = { ... }
//        Alamofire.request(Alamofire.Method.GET, url) /// Using this for default configuration
            .authenticate(usingCredential: credential)
            .responseJSON { response in
                print("\nQuery URL: ", url)
//                print("=== Request: ", response.request)  // original URL request
//                print("=== Response: ", response.response) // URL response
//                print(response.data)     // server data
//                print(response.result)   // result of response serialization
                
                if let _ = response.result.value {
                    self.rawJSON = SwiftyJSON.JSON(response.result.value!)
                    completion(json: self.rawJSON!)
                } else {
                    print("Some thing wrong")
                    print(response.debugDescription)
                }
        }
    }
    
    
    public func getRawJSON(url: String, parameters: [String: String], completion: (json: SwiftyJSON.JSON) -> Void) {
        networkManager.request(Alamofire.Method.GET, url, parameters: parameters) /// Using this networkManager for special configurations, which are set above in let networkManager: Alamofire.Manager = { ... }
            //        Alamofire.request(Alamofire.Method.GET, url) /// Using this for default configuration
            .authenticate(usingCredential: credential)
            .responseJSON { response in
                print("\nQuery URL: ", url)
                print("Parameters: ", parameters)
                //                print("=== Request: ", response.request)  // original URL request
                //                print("=== Response: ", response.response) // URL response
                //                print(response.data)     // server data
                //                print(response.result)   // result of response serialization
                
                if let _ = response.result.value {
                    self.rawJSON = SwiftyJSON.JSON(response.result.value!)
                    completion(json: self.rawJSON!)
                } else {
                    print("\n80DF8EF6-90EB-440E-B7DC-7FBE4CB11C3A \t", response.debugDescription)
                }
        }
    }
    
    /**
     Return as NSData
     :Author: Albert Tra
     */
    public func getRawData(url: String!, completion: (data: NSData) -> Void) {
        if url != nil {
            let URL = NSURL(string: url)
            self.cache.fetch(URL: URL!).onSuccess { (data) -> () in
                completion(data: data)
            }
        } else {
            print("invalue URL")
        }

    }
    
}
