//
//  Fadeable.swift
//  Pods
//
//  Created by Albert Tra on 03/03/16.
//
//

import Foundation

public protocol Fadeable { /// ff | 2D66CF90-3C98-4A49-A966-DBD29E6ADA6C
    var alpha: CGFloat { get set }
    
    mutating func fadeIn(duration: NSTimeInterval, delay: NSTimeInterval, completion: (Bool) -> Void)
    mutating func fadeOut(duration: NSTimeInterval, delay: NSTimeInterval, completion: (Bool) -> Void)
    
}

public extension Fadeable { /// ff | 2D66CF90-3C98-4A49-A966-DBD29E6ADA6C
    public mutating func fadeIn(duration: NSTimeInterval = 1, delay: NSTimeInterval = 0, completion: ( (Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animateWithDuration(duration, delay: delay, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
            self.alpha = 1
            }, completion: completion)
    }
    
    public mutating func fadeOut(duration: NSTimeInterval = 1, delay: NSTimeInterval = 0, completion: ( (Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animateWithDuration(duration, delay: delay, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
            self.alpha = 0
            }, completion: completion)
    }
}
