//
//  MapService.swift
//  Pods
//
//  Created by Albert Tra on 02/03/16.
//
//

import Foundation
import MapKit

public class MapServices {
    
    
    /**
     Add a simple single point to map view and center map view at that coordination
     
     Ref:
        + itx 06FB32BB-F5BA-411C-8A7A-012444336983
     
     Albert
     */
    public func addPointTo(mapView: MKMapView, at coordinate: CLLocationCoordinate2D, withTitle title: String) {
        let pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = coordinate
        pointAnnotation.title = title
        mapView.addAnnotation(pointAnnotation)
        
        ///////////////////////////////////////////////////////////////////
        ///
        /// CENTER MAP AT THIS COORDINATE
        ///
        ///////////////////////////////////////////////////////////////////
        mapView.centerCoordinate = coordinate  /// 87F65B82-F8D2-4B49-8CC2-DF3AB3AC0BD8
    }
    
    /**
     Draw route from source location coordinat to destination location coordinate
     
     Ref: 
        + ff | 40152E1A-C11F-48BA-B7B1-3AA3C69C0716
        + itx | 8E9F8294-A604-4B8E-93BC-BDBDE285C8C6
     
     Albert
     */
    public func route(mapView: MKMapView, sourceLocation: CLLocationCoordinate2D, destinationLocation: CLLocationCoordinate2D, transportType: MKDirectionsTransportType = .Automobile) {
        ///////////////////////////////////////////////////////////////////
        ///
        /// NEEDED IN ORDER TO DEFINE MKMapItem()
        ///
        ///////////////////////////////////////////////////////////////////
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
        
        
        // 5.
        let sourceAnnotation = MKPointAnnotation()
        sourceAnnotation.title = "Times Square"
        
        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }
        
        
        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.title = "Empire State Building"
        
        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
        ///////////////////////////////////////////////////////////////////
        ///
        /// DISPLACE POINT ON THE MAP
        ///
        ///////////////////////////////////////////////////////////////////
        mapView.showAnnotations([sourceAnnotation, destinationAnnotation], animated: true )
        /// ---------------------------------------------------------------

        
        ///////////////////////////////////////////////////////////////////
        ///
        /// NEEDED FOR MKDirectionsRequest()
        ///
        ///////////////////////////////////////////////////////////////////
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        /// ---------------------------------------------------------------
        
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .Automobile
        
        let directions = MKDirections(request: directionRequest)
        directions.calculateDirectionsWithCompletionHandler { (directionResponse, error) -> Void in
            if error == nil {
                let route = directionResponse!.routes[0]
                mapView.addOverlay(route.polyline, level: MKOverlayLevel.AboveRoads)
            }
        }
    }
}