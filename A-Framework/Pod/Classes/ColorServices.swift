//
//  Color.swift
//  Pods
//
//  Created by Albert Tra on 20/02/16.
//
//

import Foundation

public class ColorServices {
    
}

extension UIColor {
    /**
     Convert HEX to UIColor RBGA
     
     :Author: Albert Tra
     */
    public static func colorFromHex(code: Int) -> UIColor {
        let red = CGFloat(((code & 0xFF0000) >> 16)) / 255
        let green = CGFloat(((code & 0xFF00) >> 8)) / 255
        let blue = CGFloat((code & 0xFF)) / 255
        
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
}