//
//  Bundle.swift
//  Pods
//
//  Created by Albert Tra on 25/02/16.
//
//

import Foundation


public class Bundle {
    
}

extension NSBundle { /// 6E92DFAC-EB5A-43FA-BCF0-3C1223EF83AE
    public var versionNumber: String? {
        return self.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    public var buildNumber: String?{
        return self.infoDictionary?["CFBundleVersion"] as? String
    }
}
