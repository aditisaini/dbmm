//
//  CloudDataGateway.swift
//  Money
//
//  Created by Albert Tra on 06/03/16.
//  Copyright © 2016 AT Engineering GmbH. All rights reserved.
//

import Foundation
import CloudKit

protocol CloudDataDelegate {
    func mapping()
    func dataModelFromRecord() -> (key: String, value: String)
}

/**
 This class handles the connection between CoreData and CloudKit (iCloud)
 
 :Author: Albert Tra
 */
public class CloudDataServices {
    
    var backgroundOperation: NSOperation!
    let container: CKContainer!
    let logger = AFramework.sharedInstance.logger
    var operationQueue: NSOperationQueue!
    let publicDatabase: CKDatabase!
    
    // MARK: R
    private var recordType: String?
    
    // MARK: S
    var subscriptionID = "subscriptionID"
    var subscribed = false
    
    public init(recordType: String) {
        /*
        */
        print("CloudData.init(_:)")
        container = CKContainer.defaultContainer()
        publicDatabase = container.publicCloudDatabase
        operationQueue = NSOperationQueue.mainQueue()
        
        self.recordType = recordType
    }
    
    private func setupBackgroundOperation() {
        
    }
    
    /**
     Create queue to upload data to CloudKit
     
     REF. ff | BE9BD388-A971-4162-9136-EA4FC9B22246
     
     :Author: Albert Tra
     */
    func uploadQueue() {
        backgroundOperation = NSBlockOperation(block: { () -> Void in
            
        })
        backgroundOperation.queuePriority = .Low
        backgroundOperation.qualityOfService = .Background
        
        
        operationQueue.addOperation(backgroundOperation)
    }
    
    public func saveRecord(record: CKRecord, completion: (done: Bool, error: NSError?) -> Void) {
//        backgroundOperation = NSBlockOperation(block: { () -> Void in
            self.publicDatabase.saveRecord(record) { (record, error) -> Void in
                if error == nil {
                    completion(done: true, error: error)
                } else {
                    completion(done: false, error: error)
                }
            }
//        })
//        backgroundOperation.queuePriority = .Low
//        backgroundOperation.qualityOfService = .Background
        

//        operationQueue.addOperation(backgroundOperation)
        
    }
    
    
    /**
     Qeuery all records from iCloud
     
     :Author: Albert Tra
     */
    public func queryAllRecords(completion: (results: [CKRecord]?, error: NSError?) -> Void) {
        let predicate = NSPredicate(value: true)
        let query = CKQuery(recordType: recordType!, predicate: predicate)
        publicDatabase.performQuery(query, inZoneWithID: nil) { (results, error) -> Void in
            completion(results: results, error: error)
        }
    }
    
    public func deleteRecordWithID(id: String, completion: (done: Bool, error: NSError?) -> Void) {
        let recordID = CKRecordID(recordName: id)
        publicDatabase.deleteRecordWithID(recordID) { (record, error) -> Void in
            // TODO: Handle error
            if error == nil {
                completion(done: true, error: error)
            } else {
                completion(done: false, error: error)
            }
        }
    }
    
    /**
     Create subscription
     
     REF. pdf 399 | 9AD4CF9D-B117-4C99-A442-2E4DEA50FE6F

     
     :Author: Albert Tra
     */
    public func subscribe(completion: (done: Bool, error: NSError?) -> Void) {
        if subscribed { return }
        
        let predicate = NSPredicate(value: true)
        
        let subscription = CKSubscription(recordType: recordType!, predicate: predicate, options: [.FiresOnRecordDeletion, .FiresOnRecordCreation, .FiresOnRecordUpdate])
        
        subscription.notificationInfo = CKNotificationInfo()
        subscription.notificationInfo?.alertBody = ""
        
        publicDatabase.saveSubscription(subscription) { (subscription, error) -> Void in
            if error != nil {
                completion(done: false, error: error)
            } else {
                completion(done: true, error: error)
                self.subscribed = true
            }
        }
    }
    
        
}
