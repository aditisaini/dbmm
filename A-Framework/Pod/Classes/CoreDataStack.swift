//
//  CoreDataStack.swift
//  Pods
//
//  Created by Albert Tra on 31/01/16.
//
//

import Foundation
import CoreData

public class CoreDataStack { /// 758E8C5B-CF61-4D8A-A2EC-79DEBA46EE6A Pg. 65
    //    let modelName = "CoreData"
    /**
    Should be the same as your *.xcdatamodeld
    
    For example, if you create a data model named Customer.xcdatamodeld, the your model name in this case is Customer
    */
    private var modelName: String!
    
    /**
     This is the name of core data which is stored in your device
     */
    private var storeName: String!
    
    /**
     NSMigratePersistentStoresAutomaticallyOption: true     Ref: itx | D01A6F5E-F9FD-4E01-808A-73B1A26D8FE5
     NSInferMappingModelAutomaticallyOption: true           Ref: itx | AA727045-16DD-44DE-B8C9-1AF813B798DB
     NSPersistentStoreUbiquitousContentNameKey: "Journey"   Ref: itx | 3E18EDA3-78DE-4379-A10E-F2720A33AEF9
     */
    private var options: [NSObject: AnyObject]?
    
    /**
     updateContextWithUbiquitousContentUpdates is a flag that tells the stack to start or stop listening for the notification. It has a property observer, which sets an internal property to either the default notification center or nil. Using a property observer on that property actually starts or stops observing the notification
     
     Ref: /// itx | DA5339A0-2599-4723-BABE-A5C596833469
     
     :Author: Albert Tra
     */
    public var updateContextWithUbiquitousContentUpdates: Bool = false {         willSet {
            ubiquitousChangesObserver = newValue ? NSNotificationCenter.defaultCenter() : nil
        }
    }
    
    /**
     
     
     :Author: Albert Tra
     */
    private var ubiquitousChangesObserver: NSNotificationCenter? {
        didSet {
            oldValue?.removeObserver(self, name: NSPersistentStoreDidImportUbiquitousContentChangesNotification, object: coordinator) /// itx | A78D64F1-FFAE-40CB-986E-FD981F548814
            ubiquitousChangesObserver?.addObserver(self, selector: "persistentStoreDidImportUbiquitousContentChanges:", name: NSPersistentStoreDidImportUbiquitousContentChangesNotification, object: coordinator)
        }
    }

    public init(modelName: String, storeName: String, options: [NSObject: AnyObject]? = nil) {
        self.modelName = modelName
        self.storeName = storeName
        self.options = options
    }
    
    lazy var model: NSManagedObjectModel = NSManagedObjectModel(contentsOfURL: NSBundle.mainBundle().URLForResource(self.modelName, withExtension: "momd")!)!
    
    var store: NSPersistentStore?
    
    var storeURL : NSURL {
        var storePaths = NSSearchPathForDirectoriesInDomains(.ApplicationSupportDirectory, .UserDomainMask, true) as [String]
        let storePath = String(storePaths[0]) as NSString
        let fileManager = NSFileManager.defaultManager()
        
        do {
            try fileManager.createDirectoryAtPath(storePath as String, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print("Error creating storePath \(storePath): \(error)")
        }
        let sqliteFilePath = storePath.stringByAppendingPathComponent(storeName + ".sqlite")
        return NSURL(fileURLWithPath: sqliteFilePath)
    }
    
    lazy var coordinator : NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.model)
        do {
            self.store = try coordinator.addPersistentStoreWithType(
                NSSQLiteStoreType,
                configuration: nil,
                URL: self.storeURL,
                options: self.options)
        } catch let error as NSError {
            print("Store Error: \(error)")
            self.store = nil
        } catch {
            fatalError()
        }
        return coordinator
    }()
    
    
    

    /**
     Every time iCloud sync (merge) with others, this method will be called
     
     :Author: Albert Tra
     */
    @objc func persistentStoreDidImportUbiquitousContentChanges(notification: NSNotification) {
        print("\n6A6030A4-287E-4D43-B28C-2B0C1DDD6E1F \t persistentStoreDidImportUbiquitousContentChanges()")
        print("MERGING CHANGES FROM CONTEXT")
        context.performBlock { () -> Void in
            self.context.mergeChangesFromContextDidSaveNotification(notification)
        }
    }
    
    private lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = NSBundle.mainBundle().URLForResource(self.modelName, withExtension: "momd")! // momd: 758E8C5B-CF61-4D8A-A2EC-79DEBA46EE6A Pg. 666
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
    
    public lazy var context: NSManagedObjectContext = {
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        
        managedObjectContext.persistentStoreCoordinator = self.coordinator
        
        return managedObjectContext
    }()
    
    
    public func save(completion completion: (done: Bool?, error: NSError?) -> Void) {
        do {
            try context.save()
        } catch let error as NSError {
            completion(done: false, error: error)
            print(error.localizedDescription)
        }
    }

}

//extension CoreDataStack: NSManagedObjectContext {
//    
//}

