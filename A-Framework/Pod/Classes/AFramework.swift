//
//  AFramework.swift
//  Pods
//
//  Created by Albert Tra on 31/01/16.
//
//

import Foundation

public class AFramework {
    public static let sharedInstance = AFramework()
    
    public var coreLocationServices = CoreLocationService()
    public var mapServices = MapServices()
    public var network = Network()
    public var color = ColorServices()
    public var dateTimeServices = DateTimeServices()
    public var logger = Logger()
}