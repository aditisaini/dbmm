#
# Be sure to run `pod lib lint A-Framework.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "A-Framework"
  s.version          = "0.7.5"
  s.summary          = "This is a private pod"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = "This is a cocoapod framework of Albert Tra. A Framework provices log for Xcode, core location services. map services, network services - You should not try this :D"

  s.homepage         = "https://github.com/alberttra/A-Framework"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Albert Tra" => "albert.tra@icloud.com" }
# s.source           = { :git => "https://github.com/alberttra/A-Framework.git", :tag => s.version.to_s } # Origiral Location
  s.source           = { :git => "https://alberttra@bitbucket.org/alberttra/a-framework.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'A-Framework' => ['Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  
  
  s.dependency 'Alamofire', '~> 3.2.1'
  s.dependency 'SwiftyJSON'#, :git => 'https://github.com/SwiftyJSON/SwiftyJSON.git'
  s.dependency 'HanekeSwift' # https://github.com/Haneke/HanekeSwift
  # s.dependency 'CVCalendar', '~> 1.2.8'
end
