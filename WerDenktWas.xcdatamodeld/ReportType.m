//
//  ReportType.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 27.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ReportType.m 68 2010-07-13 20:09:54Z eik $
//

#import "ReportType.h"

#import "Domain.h"
#import "ReportAnnotation.h"
#import "TypeAttribute.h"
#import "Constants.h"
#import "SBJsonWriter.h"

@implementation ReportType

@dynamic imageName;
@dynamic name;
@dynamic ID;
@dynamic myid;
@dynamic isPrivate;
@dynamic isIdentificationNeeded;
@dynamic reports;
@dynamic hasSubjectTitle;
@dynamic attributes;
@dynamic domain;

@dynamic image;

@synthesize visibleAttributes=_visibleAttributes;

- (UIImage *)image
{
    UIImage *ret = [UIImage imageNamed:[NSString stringWithFormat:@"marker-white-%@.png", self.imageName]];

    if (!ret)
        ret = [UIImage imageNamed:kMarkerDefault];

    return ret;
}

- (void)flushAttributeAnswers
{
    // Flush all non-cached attributes
    for (TypeAttribute *attribute in self.attributes) {
        [attribute flushAnswer];
    }
}

- (NSUInteger)amountOfAllSetAttributes
{
    NSInteger amount = 0;
    for (TypeAttribute *attribute in self.attributes) {
        if (attribute.answer.length > 0) {
            amount++;
        }
    }
    return amount;
}

- (NSInteger)amountOfAllRequiredAttributes
{
    NSInteger amount = 0;
    for (TypeAttribute *attribute in self.attributes) {
        if ([attribute requiredBool] ) {
            amount++;
        }
    }
    return amount;
}

- (NSInteger)amountOfSetRequiredAttributes
{
    NSInteger amount = 0;
    for (TypeAttribute *attribute in self.attributes) {
        if ([attribute requiredBool] && (attribute.answer.length > 0)) {
            amount++;
        }
    }
    return amount;
}

- (BOOL)allRequiredAttributesSet
{
    for (TypeAttribute *attribute in self.attributes)
    {
        if (([attribute.answer length] <= 0)
            && ([attribute requiredBool] || [attribute isRequiredInContextOfAttributes:self.visibleAttributes]))
        {
            return NO;
        }
    }
    return YES;
}

// Returns an array of all currently visible attributes
- (NSArray *)visibleAttributes
{
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    for (TypeAttribute *attribute in self.attributes) {
        if ([attribute isVisibleInContextOfAttributes:[self.attributes array]]) {
            [tempArray addObject:attribute];
        }
    }
    return [tempArray copy];
}

- (NSArray *)attributesToDictionary
{
    NSMutableArray *mutableAttributesArray = [[NSMutableArray alloc] init];
    NSString *theAnswer, *theID;

//    NSArray *objects = [[NSArray alloc] initWithObjects:theID, theAnswer, nil];
//    NSArray *keys    = [[NSArray alloc] initWithObjects:@"id", @"value", nil];
//    NSDictionary *attributeDict = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    for (TypeAttribute *attribute in self.attributes) {
                
        // Add id/value only if the answer is not empty
        if (attribute.answer && attribute.answer.length > 0) {
            theAnswer = attribute.answer;
    
            theID = [NSString stringWithFormat:@"%@", attribute.myID];
            //theID = [NSNumber numberWithInteger:[attribute.myID integerValue]];
            
            NSArray *objects = [[NSArray alloc] initWithObjects:theID, theAnswer, nil];
            NSArray *keys    = [[NSArray alloc] initWithObjects:@"id", @"value", nil];
            NSDictionary *attributeDict = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
        
            [mutableAttributesArray addObject:attributeDict];
        }
    }
    
    NSArray *attributesArray = [NSArray arrayWithArray:mutableAttributesArray];
    
    return attributesArray;
}

- (NSString *)attributesToJSONObject
{
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    NSString *jsonObject = [jsonWriter stringWithObject:[self attributesToDictionary]];
    
    return jsonObject;
}

+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key
{
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

    if ([key isEqualToString:@"image"])
    {
        keyPaths = [keyPaths setByAddingObject:@"imageName"];
    } 
    if ([key isEqualToString:@"myid"])
    {
        keyPaths = [keyPaths setByAddingObject:@"myid"];
    } 
	if ([key isEqualToString:@"name"])
    {
        keyPaths = [keyPaths setByAddingObject:@"name"];
    } 
    return keyPaths;
}

@end
