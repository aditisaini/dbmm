//
//  ReportDetail.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 27.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ReportDetail.m 55 2010-07-09 12:38:50Z eik $
//

#import "ReportDetail.h"
#import "ServerReport.h"

@implementation ReportDetail

@dynamic content;
@dynamic order;
@dynamic name;
@dynamic report;

@end
