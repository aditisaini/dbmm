//
//  ReportPicture.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 27.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ReportPicture.m 68 2010-07-13 20:09:54Z eik $
//

#import "ReportPicture.h"

#import "ReportAnnotation.h"

@implementation ReportPicture

//@dynamic imageData;
@dynamic report;
//
//@dynamic image;
//
//
//- (UIImage *)image {
//    return [UIImage imageWithData:self.imageData];
//}
//
//+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key
//{
//    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
//
//    if ([key isEqualToString:@"image"])
//    {
//        keyPaths = [keyPaths setByAddingObject:@"imageData"];
//    }
//
//    return keyPaths;
//}

@end
