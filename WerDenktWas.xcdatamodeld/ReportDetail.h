//
//  ReportDetail.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 27.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ReportDetail.h 55 2010-07-09 12:38:50Z eik $
//

#import <CoreData/CoreData.h>

@class ServerReport;

@interface ReportDetail : NSManagedObject
{
}

@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSNumber * order;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) ServerReport * report;

@end



