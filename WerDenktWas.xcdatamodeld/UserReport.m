//
//  UserReport.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 27.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: UserReport.m 74 2010-07-14 22:00:16Z eik $
//

#import "UserReport.h"
#import "Constants.h"

#import "Domain.h"
#import "ReportPicture.h"

#import "DuplicatesJSONAdapter.h"
#import "ReportDuplicate.h"

@implementation UserReport

@dynamic details;
@dynamic subjectTitle;
@dynamic address;
@dynamic reportDomain;
@dynamic duplicates;

- (NSString *)subtitle
{
    return self.address;
}

- (void)flushDuplicates
{
    for (ReportDuplicate *duplicate in self.duplicates) {
        [self.managedObjectContext deleteObject:duplicate];
    }
    NSError *error;
    [self.managedObjectContext save:&error];
}

- (void)setSubtitle:(NSString *)subtitle
{
    self.address = subtitle;
}

- (BOOL)isMovable
{
    return YES;
}

- (UIImage *)image
{
    return [UIImage imageNamed:kMarkerUserReport];
}

@end
