//
//  ReportType.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 27.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ReportType.h 74 2010-07-14 22:00:16Z eik $
//

#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
//#import "SBJsonWriter.h"

@class Domain;
@class ReportAnnotation;
@class TypeAttribute;

@interface ReportType : NSManagedObject
{
}

@property (nonatomic, retain) NSString * imageName;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * isPrivate;
@property (nonatomic, retain) NSString * isIdentificationNeeded;
@property (nonatomic, retain) NSString * ID;
@property (nonatomic, retain) NSString * myid;
@property (nonatomic, retain) NSNumber * hasSubjectTitle;
@property (nonatomic, retain) NSSet * reports;
@property (nonatomic, retain) NSOrderedSet * attributes;
@property (nonatomic, retain) Domain * domain;

@property (nonatomic, retain, readonly) UIImage * image;

@property (nonatomic, strong, readonly) NSArray * visibleAttributes;

- (NSString *)attributesToJSONObject;
- (void)flushAttributeAnswers;

@end


@interface ReportType (CoreDataGeneratedAccessors)
- (void)addReportsObject:(ReportAnnotation *)value;
- (void)removeReportsObject:(ReportAnnotation *)value;
- (void)addReports:(NSSet *)value;
- (void)removeReports:(NSSet *)value;

- (NSUInteger)amountOfAllSetAttributes;
- (NSInteger)amountOfAllRequiredAttributes;
- (NSInteger)amountOfSetRequiredAttributes;
- (BOOL)allRequiredAttributesSet;

@end

