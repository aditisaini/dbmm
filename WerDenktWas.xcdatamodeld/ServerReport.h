//
//  ServerReport.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 27.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ServerReport.h 55 2010-07-09 12:38:50Z eik $
//

#import <CoreData/CoreData.h>
#import "ReportAnnotation.h"

@class Domain;
@class ReportDetail;
@class ReportPicture;
@class UserUpdate;

@interface ServerReport : ReportAnnotation
{
    NSFetchedResultsController *fetchedResultsController;
    id delegate;
    SEL selector;
}

@property (nonatomic, retain) UserUpdate * userUpdate;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * pictureURL;
@property (nonatomic, retain) NSSet* details;
@property (nonatomic, retain) NSDate * detailsLastSeen;

@end


@interface ServerReport (CoreDataGeneratedAccessors)

//+ (id)disconnectedEntity;
- (NSFetchedResultsController *)reportDetails;

//- (void)loadReportPictureForDelegate:(id)aDelegate andSelector:(SEL)aSelector;
//- (void)loadReportPicture;

- (void)addDetailsObject:(ReportDetail *)value;
- (void)removeDetailsObject:(ReportDetail *)value;
- (void)addDetails:(NSSet *)value;
- (void)removeDetails:(NSSet *)value;

@end

