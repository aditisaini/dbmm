//
//  ServerReport.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 27.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ServerReport.m 74 2010-07-14 22:00:16Z eik $
//

#import "ServerReport.h"

#import "Domain.h"
#import "ReportDetail.h"
#import "UserUpdate.h"
#import "ServerConnection.h"
#import "DetailJSONAdapter.h"
#import "PictureAdapter.h"

@implementation ServerReport

@dynamic state;
@dynamic pictureURL;
@dynamic details;
@dynamic detailsLastSeen;
@dynamic userUpdate;

//- (void)loadReportDetails
//{
//    DetailJSONAdapter *detailJSONAdapter = [[DetailJSONAdapter alloc] init];
//    [detailJSONAdapter queryReport:self delegate:self selector:@selector(receivedDetails)];
//}
//
//- (void)loadReportPictureForDelegate:(id)aDelegate andSelector:(SEL)aSelector
//{
//    selector = aSelector;
//    delegate = aDelegate;
//    
//    PictureAdapter *pictureAdapter = [[PictureAdapter alloc] init];
//    [pictureAdapter queryReport:self delegate:self selector:@selector(receivedPicture)];
//}
//
//- (void)receivedDetails//:(NSSet *)details
//{
//    [self.managedObjectContext updatedObjects];
//}
//
//- (void)receivedPicture//:(NSSet *)details
//{
//    NSError *error;
//    [self.managedObjectContext save:&error];
//    
//    if ([delegate respondsToSelector:selector]) {
//        [delegate performSelector:selector];
//    }
//}

- (NSString *)subtitle
{
    return self.state;
}

- (void)setSubtitle:(NSString *)subtitle
{
    self.state = subtitle;
}

- (BOOL)isMovable
{
    return NO;
}

@end
