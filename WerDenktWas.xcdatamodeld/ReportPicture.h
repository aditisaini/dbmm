//
//  ReportPicture.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 27.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ReportPicture.h 64 2010-07-12 22:57:26Z eik $
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Picture.h"

@class ReportAnnotation;

@interface ReportPicture : Picture
{
}

//@property (nonatomic, readonly) UIImage * image;
//@property (nonatomic, retain) NSData * imageData;
@property (nonatomic, retain) ReportAnnotation * report;

@end



