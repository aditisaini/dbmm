//
//  ReportAnnotation.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 27.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ReportAnnotation.h 74 2010-07-14 22:00:16Z eik $
//

#import <CoreData/CoreData.h>
#import <MapKit/MapKit.h>

#import "ReportAnnotationProtocol.h"

@class Domain;
@class ReportType;
@class ReportPicture;

@interface ReportAnnotation : NSManagedObject <ReportAnnotationProtocol>
{
}

@property (nonatomic, strong) NSNumber * messageID;
@property (nonatomic, strong) NSNumber * longitude;
@property (nonatomic, copy) NSString * title;
@property (nonatomic, strong) NSNumber * latitude;
@property (nonatomic, strong) NSNumber * accuracyNumber;
//@property (nonatomic, assign) CLLocationAccuracy accuracy;
@property (nonatomic, strong) ReportType * type2;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * color;
@property (nonatomic, strong) NSString * markerid;
@property (nonatomic, strong) ReportPicture * picture;
@property (nonatomic, strong) Domain * domain;

@property (nonatomic, copy) NSString * subtitle;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong, readonly) UIImage *image;
@property (nonatomic, strong, readonly) UIImage *photo;



@end



