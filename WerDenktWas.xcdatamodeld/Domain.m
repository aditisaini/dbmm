//
//  Domain.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 28.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: Domain.m 79 2010-07-16 17:03:57Z eik $
//

#import "Domain.h"

#import "ServerReport.h"
#import "ReportType.h"
#import "UserReport.h"

@implementation Domain

@dynamic domainID;
@dynamic unsupported;
@dynamic server;
@dynamic longitude;
@dynamic lastSeen;
@dynamic title;
@dynamic latitude;
@dynamic reportTypes;
@dynamic defaultTypeName;
@dynamic reports;
@dynamic reportsLastSeen;
@dynamic reportsLatitude;
@dynamic reportsLongitude;
@dynamic userReports;



@dynamic defaultType;

@dynamic coordinate;
@dynamic reportsCoordinate;

- (CLLocationCoordinate2D)coordinate
{
    CLLocationCoordinate2D ret;
    ret.latitude = [self.latitude doubleValue];
    ret.longitude = [self.longitude doubleValue];
    return ret;
}

- (void)setCoordinate:(CLLocationCoordinate2D)coordinate
{
    self.latitude = [NSNumber numberWithDouble:coordinate.latitude];
    self.longitude = [NSNumber numberWithDouble:coordinate.longitude];
}

- (CLLocationCoordinate2D)reportsCoordinate
{
    CLLocationCoordinate2D ret;
    ret.latitude = [self.reportsLatitude doubleValue];
    ret.longitude = [self.reportsLongitude doubleValue];
    return ret;
}

- (void)setReportsCoordinate:(CLLocationCoordinate2D)coordinate
{
    self.reportsLatitude = [NSNumber numberWithDouble:coordinate.latitude];
    self.reportsLongitude = [NSNumber numberWithDouble:coordinate.longitude];
}

+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key
{
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

    if ([key isEqualToString:@"coordinate"])
    {
        NSSet *affectingKeys = [NSSet setWithObjects:@"latitude", @"longitude",nil];
        keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKeys];
    }
    else if ([key isEqualToString:@"reportsCoordinate"]) {
        NSSet *affectingKeys = [NSSet setWithObjects:@"reportsLatitude", @"reportsLongitude",nil];
        keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKeys];
    }

    return keyPaths;
}

- (ServerReport *)serverReportByMessageID:(NSNumber *)messageID
{
    NSManagedObjectModel *managedObjectModel = self.entity.managedObjectModel;
    
    NSDictionary *variables = [NSDictionary dictionaryWithObjectsAndKeys:self, @"DOMAIN", messageID, @"MESSAGEID", nil];
    NSFetchRequest *fetchRequest = [managedObjectModel fetchRequestFromTemplateWithName:@"serverReportByMessageID" substitutionVariables:variables];
    fetchRequest.fetchLimit = 1;
    
    NSError *error = nil;
    NSArray *reports = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    ServerReport *serverReport;
    
    
    if (reports.count > 0)
        serverReport = [reports objectAtIndex:0];
    else
        serverReport = nil;
    
    return serverReport;
}

- (ReportType *)reportTypeForName:(NSString *)name
{
    NSManagedObjectModel *managedObjectModel = self.entity.managedObjectModel;

    NSDictionary *variables = [NSDictionary dictionaryWithObjectsAndKeys:self, @"DOMAIN", name, @"NAME", nil];
    NSFetchRequest *fetchRequest = [managedObjectModel fetchRequestFromTemplateWithName:@"reportTypeByName" substitutionVariables:variables];
    fetchRequest.fetchLimit = 1;

    NSError *error = nil;
    NSArray *reportTypes = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

    ReportType *reportType;

    if (reportTypes.count > 0)
        reportType = [reportTypes objectAtIndex:0];
    else
        reportType = nil;

    return reportType;
}


@end
