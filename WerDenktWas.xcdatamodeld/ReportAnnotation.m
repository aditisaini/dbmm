//
//  ReportAnnotation.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 27.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: ReportAnnotation.m 74 2010-07-14 22:00:16Z eik $
//

#import "ReportAnnotation.h"

#import "ReportType.h"
#import "ReportPicture.h"
#import "Constants.h"

@implementation ReportAnnotation

@dynamic messageID;
@dynamic longitude;
//@dynamic accuracy;
@dynamic accuracyNumber;
@dynamic subtitle;
@dynamic title;
@dynamic latitude;
@dynamic type;
@dynamic type2;
@dynamic markerid;
@dynamic picture;
@dynamic domain;
@dynamic color;


@dynamic coordinate;
@dynamic image;
@dynamic photo;

- (CLLocationCoordinate2D)coordinate
{
    CLLocationCoordinate2D ret;
    ret.latitude = [self.latitude doubleValue];
    ret.longitude = [self.longitude doubleValue];
    return ret;
}

//- (void)setAccuracy:(CLLocationAccuracy)accuracy
//{
//    self.accuracyNumber = [NSNumber numberWithDouble:accuracy];
//}

- (void)setCoordinate:(CLLocationCoordinate2D)coordinate
{
    self.latitude = [NSNumber numberWithDouble:coordinate.latitude];
    self.longitude = [NSNumber numberWithDouble:coordinate.longitude];
}

- (UIImage *)image
{
	NSString *imageName = [NSString stringWithFormat:@"marker-%@-%@.png", self.color, self.markerid];//self.type.imageName];
	
    UIImage *ret = [UIImage imageNamed:imageName];
	
    if (!ret)
        ret = [UIImage imageNamed:kMarkerDefault];
	
    return ret;	
}

- (BOOL)isMovable
{
    [self doesNotRecognizeSelector:_cmd];

    return NO;
}

- (UIImage *)photo
{
    if (self.picture)
        return self.picture.image;
    else
        return nil;
}

+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key
{
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

    if ([key isEqualToString:@"image"])
    {
        keyPaths = [keyPaths setByAddingObject:@"markerid"];//@"type"];
    }
    if ([key isEqualToString:@"photo"])
    {
        NSSet *affectingKeys = [NSSet setWithObjects:@"picture", @"picture.image",nil];
        keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKeys];
    }
//	if ([key isEqualToString:@"accuracy"])
//   {
//        keyPaths = [keyPaths setByAddingObject:@"accuracyNumber"];
//    }
    else if ([key isEqualToString:@"coordinate"]) {
        NSSet *affectingKeys = [NSSet setWithObjects:@"latitude", @"longitude",nil];
        keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKeys];
    }

    return keyPaths;
}

@end
