//
//  Domain.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 28.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: Domain.h 79 2010-07-16 17:03:57Z eik $
//

#import <CoreData/CoreData.h>
#import <MapKit/MapKit.h>

@class ServerReport;
@class ReportType;
@class UserReport;

@interface Domain : NSManagedObject
{
}

@property (nonatomic, retain) NSNumber * domainID;
@property (nonatomic, retain) NSNumber * unsupported;
@property (nonatomic, retain) NSString * server;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSDate * lastSeen;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSSet * reportTypes;
@property (nonatomic, retain) NSString * defaultTypeName;
@property (nonatomic, retain) NSSet * reports;
@property (nonatomic, retain) NSDate * reportsLastSeen;
@property (nonatomic, retain) NSNumber * reportsLatitude;
@property (nonatomic, retain) NSNumber * reportsLongitude;
@property (nonatomic, retain) NSSet * userReports;


@property (nonatomic, readonly) NSArray * defaultType;

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) CLLocationCoordinate2D reportsCoordinate;

- (ReportType *)reportTypeForName:(NSString *)name;
- (ServerReport *)serverReportByMessageID:(NSNumber *)messageID;

@end


@interface Domain (CoreDataGeneratedAccessors)
- (void)addReportTypesObject:(ReportType *)value;
- (void)removeReportTypesObject:(ReportType *)value;
- (void)addReportTypes:(NSSet *)value;
- (void)removeReportTypes:(NSSet *)value;

- (void)addReportsObject:(ServerReport *)value;
- (void)removeReportsObject:(ServerReport *)value;
- (void)addReports:(NSSet *)value;
- (void)removeReports:(NSSet *)value;

- (void)addUserReportsObject:(ServerReport *)value;
- (void)removeUserReportsObject:(ServerReport *)value;
- (void)addUserReports:(NSSet *)value;
- (void)removeUserReports:(NSSet *)value;

@end

