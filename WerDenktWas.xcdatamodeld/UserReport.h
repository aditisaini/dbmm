//
//  UserReport.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 27.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: UserReport.h 55 2010-07-09 12:38:50Z eik $
//

#import <CoreData/CoreData.h>
#import "ReportAnnotation.h"

@class Domain;
@class UserPicture;

@interface UserReport : ReportAnnotation
{
}

- (void)flushDuplicates;

@property (nonatomic, retain) NSString * details;
@property (nonatomic, retain) NSString * subjectTitle;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) Domain * reportDomain;
@property (nonatomic, retain) NSSet * duplicates;


@end



