//
//  main.m
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 26.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: main.m 78 2010-07-15 16:33:13Z eik $
//

#import <UIKit/UIKit.h>
#import "WerDenktWasAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, nil);

        return retVal;
//        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WerDenktWasAppDelegate class]));
    }
}
