//
//  Constants.h
//  project 'WerDenktWas'
//
//  Urheber nach §7 UrhG: Oliver Eikemeier <iphone@eikemeier.com>, erstellt am 14.06.10.
//  Copyright (c) Oliver Eikemeier 2010.
//
//  $Id: Constants.h 80 2010-07-18 08:59:46Z eik $
//

//#define kMarkerFormat           @"Marker-%@.png"
#define kMarkerFormat           @"%@"
#define kMarkerDefault          @"Marker-0.png"
#define kMarkerUserReport       @"marker-new.png"

#define kMarkerWidth            32.0
#define kMarkerHeight           41.0

#define kCenterOffsetX          0.0
#define kCenterOffsetY          -20.0

#define kDeleteCacheKey         @"delete_cache"
#define kServerHostKey          @"server_host"
#define kPOIHostKey             @"poi_host"
#define kServerHostNameKey      @"server_hostname"
#define kServerFormat           @"https://%@/bmsapi/"
#define kDeleteCacheKey         @"delete_cache"
#define kEmailAddressKey        @"email_address"
#define kWantsEmailKey          @"wants_email"
#define kAppIDKey               @"app_id"

#define kDomainFreshnessTime    300.0
#define kDomainFreshnessDistance        500.0
#define kReportsFreshnessTime    5.0
#define kReportsFreshnessDistance        20.0
#define kDetailFreshness        30.0

#define kReportImageQuality     0.1
#define kAppID					@"1"

