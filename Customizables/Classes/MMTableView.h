//
//  MMTableView.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 13.08.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMTableView : UITableView

@end
