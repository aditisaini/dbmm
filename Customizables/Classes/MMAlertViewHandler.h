//
//  MMAlertViewHandler.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 14.10.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMAlertViewHandler : NSObject <UIAlertViewDelegate>
{
@private
    MMAlertViewHandler *handler;
    NSArray *_blocks;
    void (^_buttonClickedBlock)(NSInteger);
    
    UIAlertView *_activityIndicatingAlertView;
    UIAlertView *_networkAlertView;
}

@property (nonatomic, strong) void (^buttonClickedBlock)(NSInteger);
@property (nonatomic, strong) UIAlertView *activityIndicatingAlertView;
@property (nonatomic, strong) UIAlertView *networkAlertView;

+ (id)sharedInstance;

- (void)cancelActivityIndicatingAlert;
- (void)showActivityIndicatingAlertWithMessage:(NSString *)message andTitle:(NSString *)title;

- (void)showAGBHasBeenDeclinedAlert;
- (void)showMissingEmailAddressAlert;
- (void)showNetworkAlert;
- (void)showEmailSubscriptionFailureAlert;
- (void)showHintAlertWithMessage:(NSString *)message;
- (void)showEmptyAlertWithTitle:(NSString *)title;
- (void)showAlertWithMessage:(NSString *)message andTitle:(NSString *)title;
- (void)showErrorAlertWithMessage:(NSString *)message;
- (void)showMissingAnswerOnAttributeAlert:(NSString *)attributeName;
- (void)showNoPhotoSetAlertWithButtonClickBlock:(void (^)(NSInteger))block;
- (void)showNetworkAlertWithButtonClickBlock:(void (^)(NSInteger))block;
- (void)showAReportAlreadyExistsAlertWithButtonClickBlock:(void (^)(NSInteger))block;
- (void)showDeleteConfirmationAlertWithButtonClickBlock:(void (^)(NSInteger))block;
- (void)showSendConfirmationAlertWithButtonClickBlock:(void (^)(NSInteger))block;
- (void)showAlertWithMessage:(NSString *)message title:(NSString *)title andButtonClickBlock:(void (^)(NSInteger))block;

@end
