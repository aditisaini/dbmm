//
//  MMTableView.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 13.08.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "MMTableView.h"
#import "MMColor.h"

@implementation MMTableView

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

@end
