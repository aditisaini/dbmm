//
//  MMCommentCompletion.m
//  WerDenktWas
//
//  Created by Franziska Engelmann on 07.12.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import "MMCommentCompletion.h"
#import "MMAlertViewHandler.h"

//static const NSString *commentMissingError        = NSLocalizedString(@"comment_missing_msg", "Please enter a comment!");
//static NSString *commentTooShortError       = NSLocalizedString(@"comment_short_msg", "Comment is too short! Please write a more precise comment.");
//static NSString *descriptionMissingError    = NSLocalizedString(@"beschreibungleer", "Please enter a description.");
//static NSString *descriptionTooShortError   = NSLocalizedString(@"description_short_msg", "Description is too short! Please write a more precise comment.");


@implementation MMCommentCompletion

+ (BOOL)checkForMinimumLength:(NSUInteger)length ofText:(NSString *)text withMissingMessage:(NSString *)missingMessage andTooShortMessage:(NSString *)tooShortMessage
{
    NSString *strippedText = [self stripText:text];
    
    if (strippedText.length <= 0) {
        [[MMAlertViewHandler sharedInstance] showEmptyAlertWithTitle:missingMessage];
    } else if (strippedText.length <= length) {
        [[MMAlertViewHandler sharedInstance] showEmptyAlertWithTitle:tooShortMessage];
    } else {
        return YES;
    }
    return NO;
}

+ (NSString *)stripText:(NSString *)text
{
    NSString *textWithoutSpaces = [text stringByReplacingOccurrencesOfString:@" "
                                                                  withString:@""];
    
    NSString *strippedText = [textWithoutSpaces stringByReplacingOccurrencesOfString:@"\n"
                                                                          withString:@""];
    return strippedText;
}

@end
