//
//  MMCommentCompletion.h
//  WerDenktWas
//
//  Created by Franziska Engelmann on 07.12.12.
//  Copyright (c) 2012 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMCommentCompletion : NSObject

+ (BOOL)checkForMinimumLength:(NSUInteger)length ofText:(NSString *)text withMissingMessage:(NSString *)missingMessage andTooShortMessage:(NSString *)tooShortMessage;
//+ (BOOL)checkCommentCompletion:(NSString *)comment;
//+ (BOOL)checkDescriptionCompletion:(NSString *)description;

@end
