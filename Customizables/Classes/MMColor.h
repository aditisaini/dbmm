//
//  MMColor.h
//  WerDenktWas
//
//  Created by Franzi Engelmann on 12.08.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMColor : UIColor

// Custom system attribute colors
+ (MMColor *)navigationBarTitelItemColor;
+ (MMColor *)navigationBarColor;
+ (MMColor *)navigationBarItemColor;
+ (MMColor *)toolbarItemColor;
+ (MMColor *)toolbarColor;
+ (MMColor *)uiButtonTitleColor;

// Custom colors
+ (MMColor *)basisAccentColor;
+ (MMColor *)basisAccentContrastColor;
+ (MMColor *)secondaryAccentColor;
+ (MMColor *)backgroundColor;
+ (MMColor *)tableViewBackgroundColor;
+ (MMColor *)reportDetailsHeaderColor;
+ (MMColor *)attributeCellHeaderTitleColor;
+ (MMColor *)attributeCellHeaderSubtitleColor;
+ (MMColor *)initialTextViewColor;

// iOS colors
+ (MMColor *)iOSSevenSystemBlue;

@end
