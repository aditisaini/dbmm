//
//  MMAlertViewHandler.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 14.10.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "MMAlertViewHandler.h"

@implementation MMAlertViewHandler

@synthesize buttonClickedBlock=_buttonClickedBlock;
@synthesize activityIndicatingAlertView=_activityIndicatingAlertView;
@synthesize networkAlertView=_networkAlertView;

- (id)init
{
    if (self = [super init]) {
        handler = self;
        _blocks  = [[NSArray alloc] init];
        _activityIndicatingAlertView = nil;
    }
    return self;
}

+ (id)sharedInstance
{
    // Initialize a sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    // dispatch_once_t => Used to test whether the block has completed or not
    static dispatch_once_t onceToken = 0;
    // dispatch_once => Executes a block object once in the application lifetime
    dispatch_once(&onceToken, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // Same object returned each time
    return _sharedObject;
}

- (void)cancelActivityIndicatingAlert
{
    if (self.activityIndicatingAlertView) {
        [self.activityIndicatingAlertView dismissWithClickedButtonIndex:0 animated:YES];
        self.activityIndicatingAlertView = nil;
        
        // Workaround (avoids that barbuttonitems remain gray => Apple bug)
        NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
        if ([[ver objectAtIndex:0] intValue] >= 7) {
            [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] setTintAdjustmentMode:UIViewTintAdjustmentModeNormal];
        }
    }
}

- (void)showActivityIndicatingAlertWithMessage:(NSString *)message andTitle:(NSString *)title
{
    self.activityIndicatingAlertView =  [[UIAlertView alloc] initWithTitle:title
                                                           message:message
                                                          delegate:nil
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:nil];
    [self.activityIndicatingAlertView show];
}

- (void)showNetworkAlert
{
    // After pressing ok the alert object is removed
    __weak MMAlertViewHandler *weakSelf = self;
    self.buttonClickedBlock = ^(NSInteger buttonIndex){
        if (buttonIndex == 0) {
            weakSelf.networkAlertView = nil;
        }
    };

    NSString *alertTitle    = NSLocalizedStringWithDefaultValue(@"network_title",
                                                                nil,
                                                                [NSBundle mainBundle],
                                                                @"Server nicht erreichbar",
                                                                @"Network alert title");
    NSString *alertMessage  = NSLocalizedStringWithDefaultValue(@"network_message",
                                                                nil,
                                                                [NSBundle mainBundle],
                                                                @"Der WerDenktWas-Server ist nicht erreichbar. Bitte prüfen Sie Ihre Netzwerkverbindung",
                                                                @"Network alert message");
    
    
    if (!_networkAlertView) {
        _networkAlertView = [[UIAlertView alloc] initWithTitle:alertTitle
                                                       message:alertMessage
                                                      delegate:handler
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"Ok", nil];
        [_networkAlertView show];
    }
}

- (void)showNetworkAlertWithButtonClickBlock:(void (^)(NSInteger))block
{
    [self showNetworkAlert];
    
    self.buttonClickedBlock = block;
}

- (void)showAGBHasBeenDeclinedAlert
{
    NSString *alertTitle = NSLocalizedStringWithDefaultValue(@"agb_rejected_msg", nil, [NSBundle mainBundle], @"Leider können Sie diese Applikation nur verwenden, wenn Sie den AGBs zustimmen!", @"Network alert title");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle
                                                    message:nil
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)showMissingEmailAddressAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringWithDefaultValue(@"hinweis",
                                                                                              nil,
                                                                                              [NSBundle mainBundle],
                                                                                              @"hinweis",
                                                                                              @"hinweis")
                                                    message:NSLocalizedStringWithDefaultValue(@"emailleer",
                                                                                              nil,
                                                                                              [NSBundle mainBundle],
                                                                                              @"emailleer",
                                                                                              @"emailleer")
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    
    [alert show];
}

- (void)showEmailSubscriptionFailureAlert
{
    NSString *alertTitle = NSLocalizedStringWithDefaultValue(@"email_title",
                                                             nil,
                                                             [NSBundle mainBundle],
                                                             @"Anmeldung fehlgeschlagen",
                                                             @"Email alert title");
    NSString *alertMessage = NSLocalizedStringWithDefaultValue(@"email_message",
                                                               nil,
                                                               [NSBundle mainBundle],
                                                               @"Die Anforderung der E-Mailbenachrichtigung ist fehlgeschlagen",
                                                               @"Email alert message");
    NSString *alertCancel = NSLocalizedStringWithDefaultValue(@"email_cancel",
                                                              nil,
                                                              [NSBundle mainBundle],
                                                              @"Abbrechen",
                                                              @"Email alert cancel");
    
    UIAlertView *emailAlert = [[UIAlertView alloc] initWithTitle:alertTitle
                                                         message:alertMessage
                                                        delegate:nil
                                               cancelButtonTitle:alertCancel
                                               otherButtonTitles:nil];
    [emailAlert show];
}

- (void)showEmptyAlertWithTitle:(NSString *)title
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:@""
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)showAlertWithMessage:(NSString *)message andTitle:(NSString *)title
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)showErrorAlertWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)showHintAlertWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringWithDefaultValue(@"hinweis", nil,
                                                                                              [NSBundle mainBundle],
                                                                                              @"Hint", @"hinweis")
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    [alert show];
}


- (void)showMissingAnswerOnAttributeAlert:(NSString *)attributeName
{
    NSString *errorMessage = NSLocalizedString(@"missing_required_attribute_msg", "Required attribute is missing!");

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:errorMessage
                                                    message:attributeName
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)showAReportAlreadyExistsAlertWithButtonClickBlock:(void (^)(NSInteger))block
{
    self.buttonClickedBlock = block;
    
    NSString *reportExistsMessage   = NSLocalizedString(@"report_already_exists", nil);
    NSString *edit                  = NSLocalizedString(@"edit", nil);
    NSString *newMessage            = NSLocalizedString(@"new_message", nil);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:reportExistsMessage
                                                   delegate:handler
                                          cancelButtonTitle:edit
                                          otherButtonTitles:newMessage, nil];
    [alert show];
}

- (void)showDeleteConfirmationAlertWithButtonClickBlock:(void (^)(NSInteger))block
{
    self.buttonClickedBlock = block;
    
    NSString *confirmationTitle     = NSLocalizedString(@"confirm_delete", nil);
    NSString *confirmationMessage   = NSLocalizedString(@"confirm_delete_text", nil);
    NSString *delete                = NSLocalizedString(@"delete_button_title", nil);
    NSString *cancel                = NSLocalizedString(@"cancel_button_title", nil);
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:confirmationTitle
                                                    message:confirmationMessage
                                                   delegate:handler
                                          cancelButtonTitle:cancel
                                          otherButtonTitles:delete, nil];
    [alert show];
}

- (void)showSendConfirmationAlertWithButtonClickBlock:(void (^)(NSInteger))block
{
    self.buttonClickedBlock = block;
    
    NSString *confirmationMessage   = NSLocalizedString(@"confirm_send", nil);
    NSString *delete                = NSLocalizedString(@"send_button_title", nil);
    NSString *cancel                = NSLocalizedString(@"cancel_button_title", nil);
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:confirmationMessage
                                                   delegate:handler
                                          cancelButtonTitle:cancel
                                          otherButtonTitles:delete, nil];
    [alert show];
}

- (void)showNoPhotoSetAlertWithButtonClickBlock:(void (^)(NSInteger))block
{
    self.buttonClickedBlock = block;

    NSString *noPhotoTitle   = NSLocalizedString(@"no_photo_title", nil);
    NSString *noPhotoMessage = NSLocalizedString(@"no_photo_message", nil);
    NSString *addPhoto       = NSLocalizedString(@"add_photo", nil);
    NSString *next           = NSLocalizedString(@"next_button_title", nil);

    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:noPhotoTitle
                                                    message:noPhotoMessage
                                                   delegate:self
                                          cancelButtonTitle:addPhoto
                                          otherButtonTitles:next, nil];
    alert.delegate = self;
    [alert show];
}

- (void)showAlertWithMessage:(NSString *)message title:(NSString *)title andButtonClickBlock:(void (^)(NSInteger))block
{
    self.buttonClickedBlock = block;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    alert.delegate = self;
    [alert show];
}

- (void)setButtonClickedBlock:(void(^)(NSInteger))block
{
    // Cache old blocks
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:_blocks];
    [tempArray addObject:block];
    _blocks = [NSArray arrayWithArray:tempArray];
    
    _buttonClickedBlock = block;
}

- (void (^)(NSInteger))buttonClickedBlock
{
    // Save oldest block and then pop it from the stack
    void (^oldBlock)(NSInteger) = _buttonClickedBlock;
    
    // Does _blocks contain more than one object, pop the oldest one
    if ([_blocks count] >= 2) {
        _buttonClickedBlock = [_blocks objectAtIndex:[_blocks count] - 2];
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        for (NSUInteger i = 0; i < [_blocks count] - 2; i++) {
            [tempArray insertObject:[_blocks objectAtIndex:i] atIndex:i];
        }
        _blocks = [NSArray arrayWithArray:tempArray];
    }
    // Else completely re-initialize the block-stack
    else {
        _blocks = [[NSArray alloc] init];
    }
    
    return oldBlock;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.buttonClickedBlock) {
        self.buttonClickedBlock(buttonIndex);
    }
}

@end
