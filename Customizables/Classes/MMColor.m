//
//  MMColor.m
//  WerDenktWas
//
//  Created by Franzi Engelmann on 12.08.13.
//  Copyright (c) 2013 Robert Lokaiczyk. All rights reserved.
//

#import "MMColor.h"

@implementation MMColor

# pragma mark - Custom system attribute colors

+ (MMColor *)navigationBarTitelItemColor
{
    return nil;
}

+ (MMColor *)navigationBarColor
{
    return nil;
}

+ (MMColor *)navigationBarItemColor
{
    return nil;
}

+ (MMColor *)toolbarItemColor
{
    return nil;
}

+ (MMColor *)toolbarColor
{
    return nil;
}

+ (MMColor *)uiButtonTitleColor
{
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        return (MMColor *)[UIColor whiteColor];
    }
    return (MMColor *)[UIColor orangeColor];
}

# pragma mark - Custom colors

+ (MMColor *)basisAccentColor
{
    return [self iOSSevenSystemBlue];
}

+ (MMColor *)basisAccentContrastColor
{
    return (MMColor *)[UIColor whiteColor];
}

+ (MMColor *)secondaryAccentColor
{
    return [self iOSSevenSystemBlue];
}

+ (MMColor *)backgroundColor
{
    return nil;
}

+ (MMColor *)tableViewBackgroundColor
{
    return nil;
}

+ (MMColor *)reportDetailsHeaderColor
{
    return [self iOSSevenSystemBlue];
}

+ (MMColor *)attributeCellHeaderTitleColor
{
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        return [self iOSSevenSystemBlue];
    }
    return (MMColor *)[UIColor darkGrayColor];
}

+ (MMColor *)attributeCellHeaderSubtitleColor
{
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        return (MMColor *)[UIColor grayColor];
    }
    return (MMColor *)[UIColor darkGrayColor];
}

+ (MMColor *)initialTextViewColor
{
    return (MMColor *)[UIColor whiteColor];
}

# pragma mark - iOS colors

+ (MMColor *)iOSSevenSystemBlue
{
    return (MMColor *)[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
}

@end
